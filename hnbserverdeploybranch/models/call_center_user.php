<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of call_center_user
 *
 * @since 27/6/2017
 * @author Prabath
 */

namespace app\models;

use Yii;
//use yii\base\Model;
use yii\db\ActiveRecord;

class call_center_user extends ActiveRecord {

    public static function getUserIdByPhoneNumber($phoneNumber) {
        $user = new call_center_user();
        $userId = $customer->find()
                ->select("id")
                ->where("contact_number = $phoneNumber")
                ->all();

        if ($userId) {
            return $userId[0]['id'];
        } else {
            return NULL;
        }
    }

    // This function takes user role as a parameter and return relevant results for te output
    public static function getUserData($userRole) {
        $user = new call_center_user();
        if ($userRole == 1) {
            return $user->find()
                            ->where("role_id = 2")
                            ->orWhere("role_id = 3")
                            ->orWhere("role_id = 4")
                            ->andWhere("status = 'active'")
                            ->all();
        } else if ($userRole == 3) {
            return $user->find()
                            ->where("role_id = 2")
                            ->orWhere("role_id = 4")
                            ->andWhere("status = 'active'")
                            ->all();
        } else {
            return null;
        }
    }

    public static function getUser($userId) {
        $query = call_center_user::find();
        return $query->where("id = $userId")
                        ->all();
    }

    public static function getAllAgents() {
        $agents = new call_center_user();
        return $agents->find()
                        ->select('*')
                        ->where("role_id = 2")
                        ->orWhere("role_id = 4")
                        ->andWhere("status = 'active'")
                        ->all();
    }

    public static function getAllAgentsExceptMe() {
        $myId = Yii::$app->session->get('user_id');
        $agents = new call_center_user();
        return $agents->find()
                        ->select('*')
                        ->where("role_id = 2")
                        ->orWhere("role_id = 4")
                        ->andWhere("status = 'active'")
                        ->andWhere("id <> $myId")
                        ->all();
    }

    /**
     * <b>Get the VOIP extension of an agent</b>
     * <p>This function returns the VOIP extension of the user identified by the ID passed as the parameter</p>
     * 
     * @param int $agentId
     * @return string agent VOIP extension
     * @author Sandun
     * @since 2017-07-25
     */
    public static function getUserVoipExtension($agentId) {
        $agent = new call_center_user();
        $agentVoip = $agent->find()
                ->select("voip_extension")
                ->where("id = $agentId")
                ->one();

        return $agentVoip['voip_extension'];
    }

    /**
     * <b>Set VOIP extension of an agent</b>
     * <p>This function is used to save the VOIP extension of the user passed as the parameter to the DB table</p>
     * 
     * @param int $agentId
     * @param int $voipExtension
     * @return boolean
     * @since 2017-07-27
     * @author Sandun
     */
    public static function setUserVoipExtension($agentId, $voipExtension) {
        return call_center_user::updateAll(['voip_extension' => $voipExtension], "id = $agentId");
    }

    /**
     * <b>Save Call center user</b>
     * <p>This function save call center user</p>
     * 
     * @param int $data
     * @param int $recordType
     * @return boolean
     * @since 2017-07-28
     * @author Vikum
     */
    public static function saveCallCenterUser($data, $recordType) {
        if ($recordType == 'save') {
            $user = new call_center_user();
        } else {
            $user = call_center_user::findOne($data['userId']);
        }
        if ($data['recordState'] == 1 || $data['recordState'] == 3) {
            $user->password = $data['hashedPassword'];
        }
        if ($data['recordState'] == 2 || $data['recordState'] == 3) {
            $user->user_profile_pic = $data['user_profile_pic'];
        }
        $user->name = $data['name'];
        $user->role_id = $data['role_id'];
        $user->fullname = $data['fullname'];
        $user->voip_extension = $data['extension'];
        $user->status = $data['status'];
        $user->created_date = $data['created_date'];
        if ($recordType == 'save') {
            return $user->insert();
        } else {
            return $user->update();
        }
    }

    /**
     * <b>Get agent data from VOIP extension</b>
     * <p>This function returns the agent information associated with the VOIP extension number passed as the parameter</p>
     * 
     * @param int $voipExtension
     * @return array agent data
     * 
     * @since 2017-08-03
     * @author Sandun
     */
    public static function getUserInfoFromVOIPExtension($voipExtension) {
        $agent = new call_center_user();
        return $agent->find()
                        ->where("voip_extension = $voipExtension")
                        ->one();
    }

    /**
     * <b>Get all assigned VOIP extension numbers</b>
     * <p>This function returns an array of all assigned VOIP numbers taken by the registered agents</p>
     * 
     * @return array taken VOIP extensions
     * @since 2017-08-04
     * @author Sandun
     */
    public static function getAllAssignedVOIPExtensions() {
        $voipExtensions = new call_center_user();
        $takenExtensions = $voipExtensions->find()
                ->select("voip_extension")
                ->where("status = 'active'")
                ->all();

        $takenExtensionsRemade = array();

        for ($x = 0; $x < count($takenExtensions); $x++) {
            $takenExtensionsRemade[$x] = $takenExtensions[$x]['voip_extension'];
        }

        return $takenExtensionsRemade;
    }

    public static function setUserStatusDeleted($userId) {
        return call_center_user::updateAll(['status' => 3], ['id' => $userId]);
    }

    /**
     * <b>Get supervisor email</b>
     * <p>This function returns Supervisor email. At the moment this will return admin user's email.</p>
     * 
     * @return $string
     * @since 2017-08-15
     * @author Vikum
     */
    public static function getSupervisorEmail() {
        $agent = new call_center_user();
        $data = $agent->find()
                ->where("name = 'admin'")
                ->one();
        return $data['user_email'];
    }

    public static function setUserRole($userId, $roleId) {
        return call_center_user::updateAll(['role_id' => $roleId], ['id' => $userId]);
    }

    public function getCall_records() {
        return $this->hasMany(Call_records::className(), ['user_id' => 'id']);
    }
    

}
