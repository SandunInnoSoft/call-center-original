<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\Call_records;
use app\models\agent_notifications;
use app\models\web_presence;

/**
 * This controller is to access dashboard data from hnb databases
 *
 * @author Vikumn
 * @since 2017-08-17
 */
class DashboardController extends Controller {

    /**
     * * Dashboard records
     * This controller is to access from hnb databases
     *
     * @author Vikum
     * @since 2017-08-17
     */
    public function actionDashboard_records() {
        $allCalls = $this->getAllCallsToday();
        $allAnswered = $this->getAllAnsweredCallsToday();
        $extensionSummary = $this->getAllAgentSummary();
        $averageWaitingTime= $this->getAllCallWaitingAverage();
        $maxWaitingTime= $this->getAllCallWaitingMax();
        $serviceLevelCalls= $this->getAllServiceLevelledCalls();
        $states = array(
            'NOT_INUSE' => 0,
            'UNAVAILABLE' => 0,
            'INUSE' => 0,
            'RINGING' => 0,
            'NONE' => 0
        );
        foreach ($extensionSummary as $state) {
            if ($state['state'] == "NOT_INUSE") {
                $states['NOT_INUSE'] = $state['num'];
            } else if ($state['state'] == "UNAVAILABLE") {
                $states['UNAVAILABLE'] = $state['num'];
            } else if ($state['state'] == "INUSE") {
                $states['INUSE'] = $state['num'];
            } else if ($state['state'] == "RINGING") {
                $states['RINGING'] = $state['num'];
            } else {
                $states['NONE'] = $state['num'];
            }
        }
        $data = array(
            'allCalls' => $allCalls,
            'answeredCalls' => $allAnswered,
            'agentSummary' => $states,
            'waiting'=>round($averageWaitingTime, 2),
            'maxWaiting'=>round($maxWaitingTime, 2),
            'serviceLevelCalls'=>$serviceLevelCalls
        );
//        print_r($states);
        return json_encode($data);
    }

    private function getAllCallsToday() {
        $data = cdr::getTodaysCalls();
        return $data[0]['num'];
    }

    private function getAllAnsweredCallsToday() {
        $data = Call_records::getAllCallsToday();
        return $data;
    }

    private function getAllAgentSummary() {
        $data = web_presence::getAllSummary();
        return $data;
    }
    private function getAllCallWaitingAverage() {
        $data = Call_records::getAverageWaitingTime();
        return $data;
    }
    private function getAllCallWaitingMax() {
        $data = Call_records::getMaxWaitingTime();
        return $data;
    }
    private function getAllServiceLevelledCalls() {
        $data = Call_records::getServiceLeveledCalls();
        return $data;
    }
}
