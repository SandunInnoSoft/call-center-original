# HNB call center system DB update version 1.17
# @author Sandun
# @since 2017-07-31

# This adds 4 new columns to the call_records table and removes the identical columns on call_center_user table

-- ------------- BEGIN SCRIPT ------------------------------------------
ALTER TABLE `call_records` 
ADD COLUMN `cli_number` INT NULL COMMENT 'This keeps the phone number of the call made from' AFTER `caller_number`,
ADD COLUMN `policy_number` INT NULL COMMENT 'This keeps the phone number that is actually associated with the customer data in the customer database' AFTER `cli_number`,
ADD COLUMN `comment` VARCHAR(500) NULL COMMENT 'This keeps the typed comment of the call record' AFTER `policy_number`;

ALTER TABLE `call_center_user`
DROP COLUMN `cli_number`,
DROP COLUMN `policy_number`,
DROP COLUMN `comment`,
DROP COLUMN `cdr_id`;
-- ----------------- END SCRIPT -------------------------------------------