/*
HNB GENERAL DB update version 4.0.1
@author Sandun
@since 2018-1-30
@target DB3 callevents


Navicat MariaDB Data Transfer

Source Server         : HNB Assurance CC DB prod
Source Server Version : 100128
Source Host           : 192.168.192.62:3306
Source Database       : callevents

Target Server Type    : MariaDB
Target Server Version : 100128
File Encoding         : 65001

Date: 2018-01-30 11:09:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for abandon_calls
-- ----------------------------
DROP TABLE IF EXISTS `abandon_calls`;
CREATE TABLE `abandon_calls` (
  `agent_id` varchar(45) DEFAULT NULL,
  `caller_number` varchar(45) DEFAULT NULL,
  `timestamp` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table ';

-- ----------------------------
-- Table structure for call_answer_times
-- ----------------------------
DROP TABLE IF EXISTS `call_answer_times`;
CREATE TABLE `call_answer_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caller_number` int(11) DEFAULT NULL,
  `answered_extension` int(6) DEFAULT NULL,
  `answered_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `answerTimesUniqueness` (`answered_time`,`answered_extension`)
) ENGINE=InnoDB AUTO_INCREMENT=2925 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for call_forwards
-- ----------------------------
DROP TABLE IF EXISTS `call_forwards`;
CREATE TABLE `call_forwards` (
  `caller_number` int(11) NOT NULL COMMENT 'caller cli number',
  `agent_extension` int(11) DEFAULT NULL COMMENT 'agent extension number',
  `time_stamp` varchar(100) NOT NULL DEFAULT '0' COMMENT 'Unix timestamp ',
  `created_date` datetime DEFAULT NULL COMMENT 'Current date time',
  `state` int(11) DEFAULT '1',
  PRIMARY KEY (`caller_number`,`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for call_queue
-- ----------------------------
DROP TABLE IF EXISTS `call_queue`;
CREATE TABLE `call_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for loop_monitor
-- ----------------------------
DROP TABLE IF EXISTS `loop_monitor`;
CREATE TABLE `loop_monitor` (
  `timestamp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of loop_monitor
-- ----------------------------
INSERT INTO `loop_monitor` VALUES ('1517290822');

-- ----------------------------
-- Table structure for web_presence
-- ----------------------------
DROP TABLE IF EXISTS `web_presence`;
CREATE TABLE `web_presence` (
  `ext` varchar(32) NOT NULL,
  `state` varchar(16) NOT NULL,
  `cidnum` varchar(64) DEFAULT NULL,
  `cidname` varchar(64) DEFAULT NULL,
  `inorout` varchar(1) DEFAULT NULL,
  `callstart` int(11) DEFAULT NULL,
  PRIMARY KEY (`ext`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_presence
-- ----------------------------
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2010', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2020', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2030', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2040', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2050', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2060', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2070', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2080', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2090', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2100', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2110', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2120', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2130', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2140', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2150', '0', NULL, NULL, NULL, NULL);
INSERT INTO `callevents`.`web_presence` (`ext`, `state`, `cidnum`, `cidname`, `inorout`, `callstart`) VALUES ('2220', '0', NULL, NULL, NULL, NULL);
