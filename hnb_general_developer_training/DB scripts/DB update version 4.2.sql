# HNB General Db update version 4.2
# @target DB2 pbxcoovoxdb_replica
# @since 2018-2-22
# @author Sandun

# This script creates a table structure same as the cdr table in the PBX

/*
Navicat MySQL Data Transfer

Source Server         : HNB General prod PBX db
Source Server Version : 50547
Source Host           : 10.100.21.238:3306
Source Database       : coovox_db

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2018-02-22 12:35:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cdr
-- ----------------------------
DROP TABLE IF EXISTS `cdr`;
CREATE TABLE `cdr` (
  `AcctId` int(11) NOT NULL,
  `ccsrc` varchar(80) DEFAULT NULL,
  `clid` varchar(80) DEFAULT NULL,
  `src` varchar(80) DEFAULT NULL,
  `dst` varchar(80) DEFAULT NULL,
  `calleenum` varchar(80) DEFAULT NULL,
  `dcontext` varchar(80) DEFAULT NULL,
  `channel` varchar(80) DEFAULT NULL,
  `dstchannel` varchar(80) DEFAULT NULL,
  `lastapp` varchar(80) DEFAULT NULL,
  `lastdata` varchar(80) DEFAULT NULL,
  `start` varchar(80) DEFAULT NULL,
  `answer` varchar(80) DEFAULT NULL,
  `end` varchar(80) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `billsec` int(11) DEFAULT NULL,
  `disposition` int(11) DEFAULT NULL,
  `amaflags` int(11) DEFAULT NULL,
  `accountcode` varchar(150) DEFAULT NULL,
  `calltype` varchar(150) DEFAULT NULL,
  `uniqueid` varchar(80) DEFAULT NULL,
  `rcfile` varchar(80) DEFAULT NULL,
  `recordingfilename` varchar(256) DEFAULT NULL,
  `recordtimelen` int(11) DEFAULT NULL,
  PRIMARY KEY (`AcctId`)
) ENGINE=InnoDB AUTO_INCREMENT=31101 DEFAULT CHARSET=utf8;


# Grants the hnbcc_user the CRUD access to the cdr replica db
GRANT SELECT, INSERT, UPDATE, DELETE ON `pbxcoovoxdb\_replica`.* TO 'hnbcc_user'@'localhost';

