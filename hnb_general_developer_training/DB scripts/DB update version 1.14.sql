# washroom break type will be removed and short type will be added
ALTER TABLE `agent_requests` 
CHANGE COLUMN `request_type` `request_type` ENUM('lunch', 'short', 'sick', 'other') NULL DEFAULT NULL ;
