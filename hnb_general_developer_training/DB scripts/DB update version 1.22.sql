# unique id column data type will be changed to varchar
ALTER TABLE `call_records` 
CHANGE COLUMN `cdr_unique_id` `cdr_unique_id` VARCHAR(50) NULL DEFAULT NULL ;
