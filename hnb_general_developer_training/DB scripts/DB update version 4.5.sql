# HNBGI Call Center System - DB Update Version 4.5
# @author Vinothan
# @since 2018-08-15

-- Creates a new column, Status added to record active status of queues 

ALTER TABLE `extension_queue` ADD `status` ENUM('active','inactive','deleted') NOT NULL DEFAULT 'active' AFTER `supervisor_id`;

-- ---------- END OF SCRIPT ----------------------------------