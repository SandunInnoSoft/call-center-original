package DTO;

import java.sql.Date;

public class PbxCdr { 
	private int acctId,duration,billable,amaflags; // int fields   
	private Date dateTime; // datetime fields
	private String clid, src, dst, dcontext, srctrunk, dstrunk, lastapp, lastdata, disposition, calltype, accountcode, uniqueid, monitorpath, 
	extfld1, extfld2, extfld3; // All varchar type fields
	
	public PbxCdr(int acctId, int duration, int billable, int amaflags, Date dateTime, String clid, String src,
			String dst, String dcontext, String srctrunk, String dstrunk, String lastapp, String lastdata,
			String disposition, String calltype, String accountcode, String uniqueid, String monitorpath,
			String extfld1, String extfld2, String extfld3) {
		super();
		this.acctId = acctId;
		this.duration = duration;
		this.billable = billable;
		this.amaflags = amaflags;
		this.dateTime = dateTime;
		this.clid = clid;
		this.src = src;
		this.dst = dst;
		this.dcontext = dcontext;
		this.srctrunk = srctrunk;
		this.dstrunk = dstrunk;
		this.lastapp = lastapp;
		this.lastdata = lastdata;
		this.disposition = disposition;
		this.calltype = calltype;
		this.accountcode = accountcode;
		this.uniqueid = uniqueid;
		this.monitorpath = monitorpath;
		this.extfld1 = extfld1;
		this.extfld2 = extfld2;
		this.extfld3 = extfld3;
	}
	public int getAcctId() {
		return acctId;
	}
	public void setAcctId(int acctId) {
		this.acctId = acctId;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getBillable() {
		return billable;
	}
	public void setBillable(int billable) {
		this.billable = billable;
	}
	public int getAmaflags() {
		return amaflags;
	}
	public void setAmaflags(int amaflags) {
		this.amaflags = amaflags;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getClid() {
		return clid;
	}
	public void setClid(String clid) {
		this.clid = clid;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getDst() {
		return dst;
	}
	public void setDst(String dst) {
		this.dst = dst;
	}
	public String getDcontext() {
		return dcontext;
	}
	public void setDcontext(String dcontext) {
		this.dcontext = dcontext;
	}
	public String getSrctrunk() {
		return srctrunk;
	}
	public void setSrctrunk(String srctrunk) {
		this.srctrunk = srctrunk;
	}
	public String getDstrunk() {
		return dstrunk;
	}
	public void setDstrunk(String dstrunk) {
		this.dstrunk = dstrunk;
	}
	public String getLastapp() {
		return lastapp;
	}
	public void setLastapp(String lastapp) {
		this.lastapp = lastapp;
	}
	public String getLastdata() {
		return lastdata;
	}
	public void setLastdata(String lastdata) {
		this.lastdata = lastdata;
	}
	public String getDisposition() {
		return disposition;
	}
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	public String getCalltype() {
		return calltype;
	}
	public void setCalltype(String calltype) {
		this.calltype = calltype;
	}
	public String getAccountcode() {
		return accountcode;
	}
	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}
	public String getUniqueid() {
		return uniqueid;
	}
	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}
	public String getMonitorpath() {
		return monitorpath;
	}
	public void setMonitorpath(String monitorpath) {
		this.monitorpath = monitorpath;
	}
	public String getExtfld1() {
		return extfld1;
	}
	public void setExtfld1(String extfld1) {
		this.extfld1 = extfld1;
	}
	public String getExtfld2() {
		return extfld2;
	}
	public void setExtfld2(String extfld2) {
		this.extfld2 = extfld2;
	}
	public String getExtfld3() {
		return extfld3;
	}
	public void setExtfld3(String extfld3) {
		this.extfld3 = extfld3;
	}
	

}
