package DTO;

public class DbConfig {
	private String hostName, tableName, tablePrefix,dbName, port, userName, pWord;

	
	public DbConfig(String hostName, String tableName, String tablePrefix, String dbName, String port, String userName,
			String pWord) {
		super();
		this.hostName = hostName;
		this.tableName = tableName;
		this.tablePrefix = tablePrefix;
		this.dbName = dbName;
		this.port = port;
		this.userName = userName;
		this.pWord = pWord;
	}
	public DbConfig() {
		
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTablePrefix() {
		return tablePrefix;
	}

	public void setTablePrefix(String tablePrefix) {
		this.tablePrefix = tablePrefix;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getpWord() {
		return pWord;
	}

	public void setpWord(String pWord) {
		this.pWord = pWord;
	}
	
}
