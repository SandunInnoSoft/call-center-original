/**
 * 
 */
package DAO;

/**
 * @author Vikum Sam
 * @since 09/04/2018
 *
 */
public class CdrMapping {

	private String column1, column2; // 2 column names that match each other
	private String dataType1, dataType2; // data types of the column names that match each other
	
	/**
	 * Parameterized constructor.                           (1)
	 * <p>
	 * Can pass parameters through the constructor.
	 * </p>
	 * 
	 *
	 * @param  column1 Column1 Name .          
	 * @param  column2 Column1 Name .          
	 * @param  dataType1 Column1 datatype .          
	 * @param  dataType2 Column2 datatype .          
	 * @return 
	 * @since	1.0
	 * 
	 */
	public CdrMapping(String column1, String dataType1, String column2, String dataType2) {
		
		this.column1 = column1;
		this.column2 = column2;
		this.dataType1 = dataType1;
		this.dataType2 = dataType2;
	}
	
	public String getColumn1() {
		return column1;
	}
	public void setColumn1(String column1) {
		this.column1 = column1;
	}
	public String getColumn2() {
		return column2;
	}
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	public String getDataType1() {
		return dataType1;
	}
	public void setDataType1(String dataType1) {
		this.dataType1 = dataType1;
	}
	public String getDataType2() {
		return dataType2;
	}
	public void setDataType2(String dataType2) {
		this.dataType2 = dataType2;
	}
	
}
