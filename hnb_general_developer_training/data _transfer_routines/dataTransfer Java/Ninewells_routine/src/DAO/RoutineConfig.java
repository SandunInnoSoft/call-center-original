/**
 * 
 */
package DAO;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import DTO.DbConfig;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;

/**
 * @author Vikum Sam
 * @since 09/04/2018
 *
 */

public class RoutineConfig {
	private static RoutineConfig instance = null;

	private static String hostName, dbName, uName, pWord;
	private static ArrayList<CdrMapping> mapList; // mapping list
	private Document doc; // xml doc
	private DbConfig pbxConfig, localConfig;

	protected RoutineConfig() {
		try {
			File fxmlFile = new File("Settings.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fxmlFile);

			doc.getDocumentElement().normalize();
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("columns");

			mapList = new ArrayList();
			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					mapList.add(new CdrMapping(eElement.getElementsByTagName("name1").item(0).getTextContent(),
							eElement.getElementsByTagName("type1").item(0).getTextContent(),
							eElement.getElementsByTagName("name2").item(0).getTextContent(),
							eElement.getElementsByTagName("type2").item(0).getTextContent()));
				}

			}
			NodeList configList = doc.getElementsByTagName("db");
			for (int temp = 0; temp < configList.getLength(); temp++) {
				Node xNode = configList.item(temp);
				
				if (xNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element confElement = (Element) xNode;
					
					if (confElement.getAttribute("id").equals("1")) {
						System.out.println("Host Name "+confElement.getElementsByTagName("hostname").item(0).getTextContent());
						pbxConfig = new DbConfig(confElement.getElementsByTagName("hostname").item(0).getTextContent(),
								confElement.getElementsByTagName("tablename").item(0).getTextContent(),
								confElement.getElementsByTagName("tableprefix").item(0).getTextContent(),
								confElement.getElementsByTagName("dbname").item(0).getTextContent(),
								confElement.getElementsByTagName("port").item(0).getTextContent(),
								confElement.getElementsByTagName("uname").item(0).getTextContent(),
								confElement.getElementsByTagName("pword").item(0).getTextContent());
					} else if (confElement.getAttribute("id").equals("2")) {
						localConfig = new DbConfig(
								confElement.getElementsByTagName("hostname").item(0).getTextContent(),
								confElement.getElementsByTagName("tablename").item(0).getTextContent(),
								confElement.getElementsByTagName("tableprefix").item(0).getTextContent(),
								confElement.getElementsByTagName("dbname").item(0).getTextContent(),
								confElement.getElementsByTagName("port").item(0).getTextContent(),
								confElement.getElementsByTagName("uname").item(0).getTextContent(),
								confElement.getElementsByTagName("pword").item(0).getTextContent());
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// Exists only to defeat instantiation.
	}

	// returns the only object from this class
	public static RoutineConfig getInstance() {
		if (instance == null) {
			instance = new RoutineConfig();
		}
		return instance;
	}

	// getCdrMapping will return all cdr mapping records
	public ArrayList<CdrMapping> getCdrMappings() {
		return mapList;
	}

	// getXmlDocument will return the whole xml object read by dom
	public Document getXmlDocument() {
		return doc;
	}

	public DbConfig getPbxDbConfig() {
		return pbxConfig;
	}
	public DbConfig getLocalDbConfig() {
		return localConfig;
	}

}
