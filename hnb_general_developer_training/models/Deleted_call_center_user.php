<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is to interact with the deleted_call_center_user database table
 *
 * @author Sandun
 * @since 2018-1-10
 */
class Deleted_call_center_user extends ActiveRecord{
    
    public static function insertDeletedUserRecord($userInformationArray){
        $deletedUser = new Deleted_call_center_user();
        $deletedUser->id = $userInformationArray['id'];
        $deletedUser->name = $userInformationArray['name'];
        $deletedUser->password = $userInformationArray['password'];
        $deletedUser->fullname = $userInformationArray['fullname'];
        $deletedUser->role_id = $userInformationArray['role_id'];
        $deletedUser->user_profile_pic = $userInformationArray['user_profile_pic'];
        $deletedUser->status = $userInformationArray['status'];
        $deletedUser->created_date = $userInformationArray['created_date'];
        $deletedUser->contact_number = $userInformationArray['contact_number'];
        $deletedUser->voip_extension = $userInformationArray['voip_extension'];
        $deletedUser->user_email = $userInformationArray['user_email'];
        $deletedUser->webphone = $userInformationArray['webphone'];
        $deletedUser->created_user_id = $userInformationArray['created_user_id'];
        $deletedUser->deleted_date = date("Y-m-d H:i:s");
        $deletedUser->deleted_user_id = Yii::$app->session->get("user_id");
        return $deletedUser->insert();
    }
    
}
