<?php


namespace app\models;
use yii;
use yii\db\ActiveRecord;
use app\models\Extqueue_callchannel_intermediate;

/**
 * This model class interacts with Call_channel table in DB1
 *
 * @author Sandun
 * @since 2018-02-16
 */
class Call_channel extends ActiveRecord{
    
    /**
     * <b></b>
     * 
     * @author Sandun
     * @since 2018-2-19
     */
    public static function getChannelInfoFomId($channelId){
        return call_channel::find()
                ->where("id = $channelId")
                ->one();
    }

    // public function getQueueids(){
    //     return $this->hasOne(Extqueue_callchannel_intermediate::classname(), ['extqueue_id' => 'id']);
    // }
    
    public static function getChannelInfoFromChannelNumber($channelNumber){
        return call_channel::find()
                ->where("channel_number = $channelNumber")
                ->one();
    }

            /**
     * <b>Check if the channel is already registered or not</b>
     * <p>This function checks the channel number passed into function is already exists in the channel table.
     * If exists, returns false, if not exists returns true</p>'
     * 
     * @param int $channelNumber
     * @return boolean
     * 
     * @since 2018-8-17
     * @author Vinothan
     */
    public static function isChannelAvailable($channelNumber){
        $extension = call_channel::find()
                ->where("channel_number = '$channelNumber'")
                ->one();
        if(!$extension){
            return TRUE;
        }else{
            return FALSE;
        }
    }

        /**
     * <b>Inserts a new Channel to the web presence table</b>
     * @param type $newChannel
     * @return type
     * 
     * @since 2018-08-17
     * @author Vinothan
     */
    public static function insertNewChannelInfo($data){
        $call_channel = new Call_channel();
        $call_channel->channel_number =$data['channelNumber'];
        $call_channel->channel_name =$data['channelName'];;
        $call_channel->channel_description =$data['channelDesc'];;

        if ($call_channel->save()) {
            return $call_channel->id;
        }



    }
}
