<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of help_requests
 *
 * @author Sandun
 * @since 2017-07-12
 */
class Voicemail_extension extends ActiveRecord {
    
    /**
     * 
     * @param int $queueId
     * @return boolean|array false if no VM extension found for the queue, 1D array of voice mail numbers for the queue
     * 
     * @since 2018-1-10
     * @author Sandun
     */
    public static function getVoicemailNumbersOfQueue($queueId){
            $voiceMailNumbersQuery = Voicemail_extension::find();
                                $voiceMailNumbersQuery->select("voicemail_extension");
                                if($queueId != NULL){
                                    $voiceMailNumbersQuery->where("ext_queue_id = $queueId");
                                }
            $voiceMailNumbers = $voiceMailNumbersQuery->all();

        $voiceMailNumbersRemade = array();
        if(count($voiceMailNumbers) > 0){
            for($x = 0; $x < count($voiceMailNumbers); $x++){
                array_push($voiceMailNumbersRemade, $voiceMailNumbers[$x]['voicemail_extension']);
            }
            return $voiceMailNumbersRemade;
        }else{
            return FALSE;
        }
    }

}
