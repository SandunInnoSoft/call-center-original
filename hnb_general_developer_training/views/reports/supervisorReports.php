<?php

use yii\helpers\Url;
use yii\helpers\Html;
$ftpParams = include __DIR__ . '/../../config/pbx_audio_ftp_config.php';
?>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>-->
<!-- <script src="js/jquery-3.2.1.js"></script> -->
<script type="text/javascript" src="js/chartjs.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<style>
    thead{
        font-weight: bold;
    }

    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }
    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .missed-call-history-tbody td, .missed-call-history-thead th {
        float: left;
        width: 25%;
    }    

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<style>
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }

    .channelLabel{
        margin-right: 10px;
    }
</style>



<div  style="font-family: 'Lato' , sans-serif;">

    <div class="col-xs-2" >
        <?php 
            if(Yii::$app->session->get("user_role") != "1" && Yii::$app->session->get("user_role") != "5"){
                // user is a supervisor
        ?>
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Agents</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 650px;overflow-y: scroll">
                    <ul id="agentListGroup" class="list-group" style="">
                        <a id="0" style="cursor: pointer" onclick="loadQueueDataForSupervisor(this)" class="list-group-item">All records</a>
                        <a id="<?= Yii::$app->session->get('voip') ?>" style="cursor: pointer" onclick="callAgentReports(<?= Yii::$app->session->get('user_id') ?>,<?= Yii::$app->session->get('voip') ?>, '<?= Yii::$app->session->get('full_name') ?>', this.id)" class="list-group-item"><?= Yii::$app->session->get('full_name')." : ".Yii::$app->session->get('voip') ?></a>
                        <?php for ($index = 0; $index < count($agents); $index++) { ?>
                            <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['fullname'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['fullname']." : ".$agents[$index]['voip_extension'] ?></a>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
        <?php
        }else{
            // user is an admin
        ?>
        
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Agents</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 325px;overflow-y: scroll">
                    <ul id="agentListGroup" class="list-group" style="">
                        <a id="0" style="cursor: pointer" onclick="callAgentReports(0, 0, 0, 0)" class="list-group-item">All Agents</a>
                        <?php for ($index = 0; $index < count($agents); $index++) { ?>
                            <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['fullname'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['fullname']." : ".$agents[$index]['voip_extension'] ?></a>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Extension Queues</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 325px;overflow-y: scroll">
                    <ul id="queueListGroup" class="list-group" style="">
                        <?php for ($index = 0; $index < count($queues); $index++) { ?>
                        <a id="<?= $queues[$index]['id'] ?>" style="cursor: pointer" onclick="callQueueReports(this.innerHTML, this.id)" class="list-group-item"><?= $queues[$index]['name'] ?></a>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        
        <?php  
        }
        ?>
    </div>
    <div class="col-xs-10" style="padding: 0%">
        <div class="row">
            <div class="col-xs-3">
                <label for="fromdate">Date from*</label>
                <!--<input class="form-control activateFields" type="date" id="fromdate">-->       
                <div class="input-group date" id="fromdate">
                    <input type="text" class="form-control" id="dateFromValue"/>	<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <label for="fromtime">Time from*</label>
                <div class="form-group" id="fromtime">
                <select id="timeFromSelect" class="form-control">
                    <?php
                    for ($i=0; $i < 24; $i++) { 
                        if($i < 10){
                            ?>
                            <option value="<?= '0'.$i.':00:00' ?>"><?= '0'.$i.':00' ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?= $i.':00:00' ?>"><?= $i.':00' ?></option>
                            <?php
                        }
                    }
                    ?>
                    <option value="23:59:59">End of the day</option>
                </select>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="todate">Date to*</label>                
                <!--<input class="form-control activateFields" type="date" id="todate">-->
                <div class="input-group date" id="todate">
                    <input type="text" class="form-control" id="dateToValue"/>	<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <label for="fromtime">Time to*</label>
                <div class="form-group" id="totime">
                <select id="timeToSelect" class="form-control">
                    <?php
                    for ($i=0; $i < 24; $i++) { 
                        if($i < 10){
                            ?>
                            <option value="<?= '0'.$i.':00:00' ?>"><?= '0'.$i.':00' ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?= $i.':00:00' ?>"><?= $i.':00' ?></option>
                            <?php
                        }
                    }
                    ?>
                    <option value="23:59:59" selected>End of the day</option>
                </select>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="contactNum">Contact</label>
                <input class="form-control activateFields" type="tel" id="contactNum">
            </div>
            <div class="col-xs-3" style="padding-top: 23px">
                <div class="row" style="padding-bottom: 1%">
                    <div class="col-xs-12">
                        <a class="disabled btn btn-block btn-primary" id="btnSummeryView" onclick="filterRecordsSummery()">View Summery Report</a>
                    </div>
                </div>
<!--                 <div class="row" style="padding-top: 1%">
                    <div class="col-xs-12">
                       <a class="disabled btn btn-block btn-primary" id="btnView" onclick="filterRecords()">View Full Report</a> 
                    </div>
                </div> -->
                <div class="row" style="padding-top: 1%">
                    <div class="col-xs-12">
                       <a class=" btn btn-block btn-primary" id="btnView" onclick='filterRecordsPagination(1,"All")'>View Full Report</a> 
                    </div>
                </div>                
            </div>
            <input type="hidden" id="hiddenAgentId" />
            <input type="hidden" id="hiddenAgentExt" />
            <input type="hidden" id="hiddenAgentName" />
            <input type="hidden" id="hiddenQueueId" />
        </div>

        <ul class="nav nav-tabs" id="selectList" role="tablist">
          <li class="nav-item">
            <a class="nav-link" id="channelList-tab" data-toggle="tab" href="#channelList" role="tab" aria-controls="channelList" aria-selected="false">Incoming Channels</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="cdrList-tab" data-toggle="tab" href="#cdrList" role="tab" aria-controls="cdrList" aria-selected="false">Call Detailed Reports</a>
        </li>
    </ul>

    <div class="tab-content" id="selectListContent">
      <div class="tab-pane fade" id="channelList" role="tabpanel" aria-labelledby="channelList-tab">
        <br>
        <div class="col-md-12">
            <div class="well" id="channelsListDiv">
                <?php 
                if($channelsInfo != NULL){
                    foreach($channelsInfo as $key){
                        ?>
                        <label data-toggle="tooltip" data-placement="left" title="<?=$key->channel_description?>"><input type="checkbox" value="<?= $key->id ?>" class="channelCheckBoxesClass" id="channel<?=$key->channel_number?>">&nbsp<?= $key->channel_name ?> &nbsp</label>
                        <?php
                    }
                }else{
                    ?>
                    <label>No channels available</label>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <div class="tab-pane fade" id="cdrList" role="tabpanel" aria-labelledby="cdrList-tab">
        <br>
      <div class="col-md-12">
        <div class="well" id="channelsListDiv">
            <label data-toggle="tooltip" data-placement="left" title="Answered-calls"><input type="checkbox" value="1" id="checkedCdrs">&nbsp Answered Calls &nbsp</label>
            <label data-toggle="tooltip" data-placement="left" title="Outgoing-calls"><input type="checkbox" value="2" id="checkedCdrs">&nbsp Outgoing Calls &nbsp</label>
            <label data-toggle="tooltip" data-placement="left" title="Missed-calls"><input type="checkbox" value="3" id="checkedCdrs">&nbsp Missed Calls &nbsp</label>
            <label data-toggle="tooltip" data-placement="left" title="Agent Abandoned Calls" id="agentCdr"><input type="checkbox" value="4" id="checkedCdrs">&nbsp Agent Abandoned Calls &nbsp</label>
            <label data-toggle="tooltip" data-placement="left" title="Agent Login Details" id="agentCdr"><input type="checkbox" value="5" id="checkedCdrs">&nbsp Agent Login Details &nbsp</label>
            <label data-toggle="tooltip" data-placement="left" title="Agent DND Records" id="agentCdr"><input type="checkbox" value="6" id="checkedCdrs">&nbsp Agent DND Records &nbsp</label>
        </div>
      </div>
    </div>
  </div>        

        <span id="searchTimeSpan"></span>
        <br>
        <img class="img-responsive" src="images/loading.gif" id="loadingGifImg" style="display: none">

<!-- this container to render CDR full report details -------------------------------------------------------------------------------------------------------------------------------->
    <style>
                .tableElementGapExtenderAllAgents{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingReceiverAllAgents{
                    width: 10% !important;
                    text-align: left;
                }
                .outgoingAgentNameAllAgents{
                    width: 14% !important;
                    text-align: left;
                }
                .outgoingStartTimeAllAgents{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingAnsweredTimeAllAgents{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingEndTimeAllAgents{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingDurationAllAgents{
                    width: 10% !important;
                    text-align: left;
                }
                .outgoingCallPlaybackAllAgents{
                    width: 13% !important;
                    text-align: left;
                }
                .tableElementGapExtenderAgent{
                    width: 20% !important;
                    text-align: left;
                }
                .outgoingReceiverAgent{
                    width: 15% !important;
                    text-align: left;
                }
                .outgoingAgentNameAgent{
                    width: 14% !important;
                    text-align: left;
                }
                .outgoingStartTimeAgent{
                    width: 17% !important;
                    text-align: left;
                }
                .outgoingAnsweredTimeAgent{
                    width: 17% !important;
                    text-align: left;
                }
                .outgoingEndTimeAgent{
                    width: 17% !important;
                    text-align: left;
                }
                .outgoingDurationAgent{
                    width: 13% !important;
                    text-align: left;
                }
                .outgoingCallPlaybackAgent{
                    width: 18% !important;
                    text-align: left;
                }
                .missed-calls-table th{
                    width: 110px;
                }
                .tableElementVMGapExtender{
                    width: 50%;
                }                
</style>


<div class="row" id="agentReportsDiv" style="padding-left: 1%"></div>
<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;display: none;"  id="main_container" >
    <div class="row" style="padding-top: 0%">
        <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2" id="main_container_heading"></div>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div>
    <br>
    <div id="answered_calls_container" style="display: none;">
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
            <div class="navbar-header">
                <a class="navbar-brand" >Answered Calls</a>
                <a class="btn btn-md btn-info" id="countOfAnsweredCalls" style="margin-top: 3%"></a>
            </div>
            <div class="navbar-collapse collapse">
                <span class="nav navbar-nav navbar-right">
                    <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvAnswered" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>
                    </span>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <table id="answerCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                    <thead class="call-history-thead" id="content_answered_table_head">
                    </thead>
                    <tbody class="call-history-tbody" id="content_answered_table_body">
                    </tbody>
                </table>
            </div>
            <div id="pagination_answered"></div>
        </div>
        <div id="outgoing_calls_container" style="display: none;">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Outgoing Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%" id="countOfOutgoingCalls"></a>
                </div>
                <div class="navbar-collapse collapse">
                    <span class="nav navbar-nav navbar-right">
                        <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvOutgoing" onclick="generateCsv(this)" >
                            Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                            <a>&nbsp;</a>                        
                        </span>
                    </div>
                </div>
                <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                    <table id="outgoingCallsTable" class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
                        <thead class="call-history-thead" id="content_outgoing_table_head">
                            <tr>

                            </tr>
                        </thead>
                        <tbody class="call-history-tbody" id="content_outgoing_table_body">

                        </tbody>
                    </table>
                </div>
                <div id="pagination_outgoing"></div>
            </div>
            <div class="row">
                <div class="col-xs-6" id="missed_calls_container" style="display: none;">
                    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold" >
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">Missed Calls</a>
                            <a class="btn btn-md btn-info" href="#" style="margin-top: 3%" id="countOfMissedCalls"></a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <span class="nav navbar-nav navbar-right">
                                <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvMissed" onclick="generateCsv(this)" >
                                    Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                                    <a>&nbsp;</a>                        
                                </span>
                            </div>
                        </div>
                        <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                            <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                                <thead class="missed-call-history-thead" id="content_missed_table_head">
                                    <tr>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Time
                                        </th>
                                        <th>
                                            Caller
                                        </th>
                                        <th>
                                            Duration
                                        </th>                         
                                    </tr>
                                </thead>
                                <tbody class="missed-call-history-tbody" id="content_missed_table_body">
                                </tbody>
                            </table>
                        </div>
                        <div id="pagination_missed"></div>
                    </div>
                    <div class="col-xs-6" id="ivr_calls_container" style="display: none;">
                        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold" >
                            <div class="navbar-header">
                                <a class="navbar-brand" href="#">IVR Received Calls</a>
                                <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"></a>
                            </div>    
                        </div>
                        <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                            <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                                <thead class="abandoned-call-history-thead">
                                    <tr>
                                        <th>
                                            Date Time
                                        </th>
                                        <th>
                                            IVR
                                        </th>
                                        <th>
                                            Caller
                                        </th>                        
                                    </tr>
                                </thead>
                                <tbody class="abandoned-call-history-tbody">
                                </tbody>
                            </table>
                        </div>
                        <div id="pagination_ivr"></div>
                    </div>
                </div>
                <div id="vm_calls_container" style="display: none;">
                <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold" >
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Received Voice Mail Calls</a>
                        <a class="btn btn-md btn-info" href="#" style="margin-top: 3%" id="countOfVmCalls"></a>
                    </div>
                </div>
                <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                    <table id="vmCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="call-history-thead"  id="content_missed_table_head">
                            <tr>
                                <th class="tableElementVMGapExtender">
                                    Caller Number
                                </th> 
                                <th class="tableElementVMGapExtender">
                                    Voice Mail Number
                                </th>                      
                                <th class="tableElementVMGapExtender">
                                    Answered Time
                                </th>
                                <th class="tableElementVMGapExtender">
                                    End Time
                                </th>
                                <th class="tableElementVMGapExtender">
                                    Duration
                                </th>
                            </tr>
                        </thead>
                        <tbody class="call-history-tbody" id="content_missed_table_body">
                        </tbody>
                    </table>
                </div>
                <div id="pagination_vm"></div>
                </div>
                <div class="row">
                    <div class="col-xs-6" style="padding-right: 2%;display: none;" id="abandoned_calls_container">
                        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold" >
                            <div class="navbar-header">
                                <a class="navbar-brand" href="#">Agent Abandoned Calls</a>
                                <a class="btn btn-md btn-info" href="#" style="margin-top: 3%" id="countOfAbandonedCalls"></a>
                                <a>&nbsp;</a>                    
                            </div>
                            <div class="navbar-collapse collapse">
                                <span class="nav navbar-nav navbar-right">
                                    <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvAbandoned" onclick="generateCsv(this)" >
                                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                                        <a>&nbsp;</a>                        
                                    </span>
                                </div>
                            </div>
                            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                                <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block ">
                                    <thead class="abandoned-call-history-thead">
                                        <tr>
                                            <th>
                                                Caller Number
                                            </th>
                                            <th>
                                                Abandoned Date
                                            </th>
                                            <th>
                                                Time
                                            </th>                        
                                        </tr>
                                    </thead>
                                    <tbody class="abandoned-call-history-tbody" id="content_abandoned_table_body">

                                    </tbody>
                                </table>
                            </div>
                            <div id="pagination_abandoned"></div>
                        </div>

                        <!--<div class="col-xs-1"></div>-->
                        <div id="login_records_container" style="display: none;">
                        <div class="col-xs-6" >
                            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold" >
                                <div class="navbar-header">
                                    <a class="navbar-brand" href="#">Agent Login Details</a>
                                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%" id="countOfLoginRecords"></a>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <span class="nav navbar-nav navbar-right">
                                        <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportloginrecords" onclick="generateCsv(this)" >
                                            Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                                            <a>&nbsp;</a>                        
                                        </span>
                                    </div>
                                </div>
                                <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                                    <?php //if ($agent_abandoned_calls) {     ?>        
                                        <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px; ">
                                            <thead class="abandoned-call-history-thead">
                                                <tr>
                                                    <th>
                                                        Logged Date
                                                    </th>
                                                    <th>
                                                        Logged Time
                                                    </th>
                                                    <th>
                                                        Logout Time
                                                    </th>                        
                                                </tr>
                                            </thead>
                                            <tbody class="abandoned-call-history-tbody" id="content_login_table_body">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="pagination_login"></div>
                                </div>
                                </div>
                            </div>
                            <br>
                        <div class="row" id="dnd_records_container" style="display: none;">
                            <div class="col-xs-6">
                                <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold" >
                                    <div class="navbar-header">
                                        <a class="navbar-brand" href="#">Agent DND Records</a>
                                        <a class="btn btn-md btn-info" href="#" style="margin-top: 3%" id="countOfDndRecords"></a>
                                    </div>
                                    <div class="navbar-collapse collapse">
                                        <span class="nav navbar-nav navbar-right">
                                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportcsvdnd" onclick="generateCsv(this)" >
                                                Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                                                <a>&nbsp;</a>                        
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                                        <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px; ">
                                            <thead class="abandoned-call-history-thead">
                                                <tr>
                                                    <th>
                                                        Date
                                                    </th>
                                                    <th>
                                                        Time
                                                    </th>
                                                    <th>
                                                        Mode
                                                    </th>                        
                                                </tr>
                                            </thead>
                                            <tbody class="abandoned-call-history-tbody" id="content_dnd_table_body">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="pagination_dnd"></div>                
                                </div>
                            </div>                    
        <br>
    </div>

<!-- END of CDR container-------------------------------------------------------------------------------------------------------------------------------------------->

    </div>
</div>

<script>
    $('#fromdate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#todate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });


    $("#queueListGroup > a").click(function(){
        $("#channelList-tab").click();
    });

    $('#agentListGroup > a').not(document.getElementById("0")).click(function(){
        $("#channelsListDiv > #agentCdr").css("display","inline-block");
        $('#channelsListDiv input:checked').removeAttr('checked');
    });


    function callAgentReports(agent_id, agent_ex, agent_name, id) {
        //active view button
        <?php 
            if(Yii::$app->session->get("user_role") == "1"){
                // user is admin
        ?>
        $("#channelsListDiv").empty();                            
        $("#channelsListDiv").append("<label>No channels available</label>");
        <?php 
            }
        ?>
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(agent_ex);
        $('#hiddenAgentName').val(agent_name);
        $('#hiddenAgentId').val(agent_id);
        $('#hiddenQueueId').val("");
        $("#channelsListDiv > #agentCdr").css("display","none");
        $('#channelsListDiv input:checked').removeAttr('checked');
        
        var url = "<?= Url::to(['reports/performanceoverviewbyagent']) ?>";

        //Start : set agents panel css
        {
            var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
            
            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");
        }
    }
    function filterRecords() {

        $("#main_container").css('display','none');    
           
        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
			    var pattern = /[^0-9]/;
			    var result = contactNumber.match(pattern);
			    if(result){
		           swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
		           return false;
			    }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var url = "<?= Url::to(['reports/performanceoverviewbydate']) ?>";
        }else{
           var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

        var checkedCdrs = getSelectedCdrTypes();

        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    fromdate: fromDate, 
                    todate: toDate, 
                    agent_id: agentId, 
                    agentex: agentEx, 
                    agentname: agentName, 
                    contactNum: contactNumber, 
                    queue_id : queueId, 
                    fromTime : fromTime, 
                    toTime : toTime,
                    channels: selectedChannels,
                    checkedCdrs : checkedCdrs
                },
                success: function (data)
                {
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
                    console.log('filter records : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
//                    alert(xhr.responseText);
//                    alert(xhr.status);
//                    alert(thrownError);
//                    alert(xhr.responseText);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }


    }
    
    /**
     * 
     * @returns {undefined}
     * 
     * @since 2017-10-30
     * @author Sandun
     * 
     */
    function filterRecordsSummery(){

        $("#main_container").css('display','none');  

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        
        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
            var url = "<?= Url::to(['reports/summery']) ?>";
        }else{
            var url = "<?= Url::to(['reports/summerybyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

//        alert(fromDate + " +++ " + toDate);
        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
        
        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    fromdate: fromDate, 
                    todate: toDate, 
                    agent_id: agentId, 
                    agentex: agentEx, 
                    agentname: agentName, 
                    contactNum: contactNumber, 
                    queue_id : queueId, 
                    fromTime : fromTime, 
                    toTime : toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
//                    alert(data);
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
//                    alert(xhr.responseText);
                    console.log('filter records summery : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }
        
    }
    
    function showLoadingScreen(){
        $("#agentReportsDiv").empty();
        $("#loadingGifImg").css("display", "inline");
        $("#searchTimeSpan").html(""); // clears the search time diplay span   
        return true;
    }

    function hideLoadingScreen(){
        $("#loadingGifImg").css("display", "none");
        return true;
    }
    
    function showErrorMessage(){
        $("#agentReportsDiv").empty();
        $("#agentReportsDiv").append("ERROR Occured. Please try again!");
    }
    
    
    function callQueueReports(queue_name, id) {
        //active view button
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val(queue_name);
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val(id);
        $("#channelsListDiv > #agentCdr").css("display","none");        
        

        //Start : set agents panel css
        {
            var items = document.getElementById('queueListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
            
            $("#agentListGroup > a").css("backgroundColor", "white");
            $("#agentListGroup > a").css("color", "black");
        }

        loadChannelsInfoOfTheQueue(id);
     
    }



    function loadChannelsInfoOfTheQueue(extQueueId){
        if(extQueueId){
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label>Loading channels...</label>");        
            $.ajax({
                type : "GET",
                url : "<?= Url::to(['reports/findchannelsforqueue'])?>",
                data : {
                    queueId : extQueueId
                },
                success : function(data){
                    $("#channelsListDiv").empty();
                    if(data != "0"){
                        var queueChannelsObj = JSON.parse(data);
                        for(var x = 0; x < queueChannelsObj.length; x++){
                            var channelTickBox = $("<input>");
                            $(channelTickBox).attr("type", "checkbox");
                            $(channelTickBox).val(queueChannelsObj[x]['id']);
                            $(channelTickBox).addClass("channelCheckBoxesClass");
                            $(channelTickBox).attr("id", "channel"+queueChannelsObj[x]['channel_number']);
                            
                            var channelName = " "+queueChannelsObj[x]['channel_name']+" ";

                            var channelNameLabel = $("<label></label>");
                            $(channelNameLabel).attr("data-toggle", "tooltip");
                            $(channelNameLabel).attr("data-placement", "left");
                            $(channelNameLabel).addClass("channelLabel");                           
                            $(channelNameLabel).attr("title", queueChannelsObj[x]['channel_description']);
                            
                            $(channelNameLabel).append(channelTickBox);
                            $(channelNameLabel).append(channelName);
                            

                            $("#channelsListDiv").append(channelNameLabel);
                        }
                    }else{
                        $("#channelsListDiv").append("<label>No channels available</label>");                            
                    }
                },
                error : function(error){
                    console.log("load channels error = "+error.responseText);
                    $("#channelsListDiv").empty();
                    $("#channelsListDiv").append("<label style='color:red'>Error loading channels</label>");    
                }
            });
        }else{
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label style='color:red'>Queue ID is incorrect. Please select the queue again</label>"); 
        }
    }
    
    function loadQueueDataForSupervisor(element){
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val("");
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val("");
        var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            element.style.backgroundColor = "grey";
            element.style.color = "white";
            
            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");
            
    }

    function getSelectedChannelIds(){
        if($(".channelCheckBoxesClass:checked").length > 0){
            // channels selected
            var selectedChannelsCsvString = "";
            $(".channelCheckBoxesClass:checked").each(function(){
                var channelNumber = $(this).attr('id');
                channelNumber = channelNumber.replace("channel", "");
                selectedChannelsCsvString = selectedChannelsCsvString+channelNumber+",";
            });
            return selectedChannelsCsvString;
        }else{
            // no channels selected
            return "0";
        }
    }

    function showSearchTimeDuration (start, end){
        var diff = end - start;
        $("#searchTimeSpan").html("Search time = "+ (diff/1000)+" seconds");
        return true;
    }

    
     function getSelectedCdrTypes() {
        var selectedCdrCsvString = "";
        $("#checkedCdrs:checked").each(function (index) {
            selectedCdrCsvString = selectedCdrCsvString.concat(this.value, ",");
        });
        return selectedCdrCsvString;
    }



    function filterRecordsPagination(num,paginationCdrType) {
        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var url = "<?= Url::to(['reports/performanceoverviewbydatewithpagination']) ?>";
        }else{
           var url = "<?= Url::to(['reports/performanceoverviewbyqueuewithpagination']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

        var checkedCdrs = getSelectedCdrTypes();

        var  num = num;
        var paginationCdrType  = paginationCdrType;

        if (paginationCdrType == "All") {
            $("#main_container").css('display','none');  
            $("#answered_calls_container").css('display','none');  
            $("#outgoing_calls_container").css('display','none');               
            $("#missed_calls_container").css('display','none');               
            $("#vm_calls_container").css('display','none');               
            $("#abandoned_calls_container").css('display','none');               
            $("#login_records_container").css('display','none');               
            $("#dnd_records_container").css('display','none');               
        }

        if (fromDateConverted < toDateConverted) {           
        if (paginationCdrType == "All") {
                showLoadingScreen();              
        }            
            $.ajax({
                type: "POST",
                url: url,
                dataType : "json",
                data: {
                    fromdate: fromDate, 
                    todate: toDate, 
                    agent_id: agentId, 
                    agentex: agentEx, 
                    agentname: agentName, 
                    contactNum: contactNumber, 
                    queue_id : queueId, 
                    fromTime : fromTime, 
                    toTime : toTime,
                    channels: selectedChannels,
                    checkedCdrs : checkedCdrs,
                    page_num:num,
                    paginationCdrType:paginationCdrType,
                },
                success: function (data)
                {
                    if (paginationCdrType == "All") {
                            hideLoadingScreen();              
                    }

                    $("#agentReportsDiv").empty();
                    $("#main_container").css('display','block');

                    if (data.answeredData.countOfResults > 0) {
                    $("#answered_calls_container").css('display','block');                        
                    var tableTdClass = "tableElementGapExtenderAllAgents";

                    if (data.answeredData.agent_ex != 0) {
                        tableTdClass = "tableElementGapExtenderAgent";
                        $("#main_container_heading").html(agentName+":"+agentEx);                        
                    }else{
                        $("#main_container_heading").html("All Agents");
                    }

                    $("#content_answered_table_head").empty();                    

                    var newHeaderRow = $('<tr></tr>');
                    var thNumber = $('<th></th>').html("Caller Number").addClass(tableTdClass);
                    newHeaderRow.append(thNumber);

                    var thAnsweredTime = $('<th></th>').html("Answered Time").addClass(tableTdClass);
                    newHeaderRow.append(thAnsweredTime);

                    if (data.answeredData.agent_ex == 0) {
                    var thAgent = $('<th></th>').html("Agent Name").addClass(tableTdClass);
                    newHeaderRow.append(thAgent);
                    }                    

                    var thEnd = $('<th></th>').html("End Time").addClass(tableTdClass);
                    newHeaderRow.append(thEnd);

                    var thDuration = $('<th></th>').html("Duration").addClass(tableTdClass);
                    newHeaderRow.append(thDuration);

                    var thPlayBack = $('<th></th>').html("Call Playback").addClass(tableTdClass);
                    newHeaderRow.append(thPlayBack);

                    $("#content_answered_table_head").append(newHeaderRow);

                    $("#content_answered_table_body").empty();
                    $('#pagination_answered').empty();                
                    $("#main_container").css("display", "block");
                    $('#countOfAnsweredCalls').html(data.answeredData.countOfAnsweredSetRows);

                        var paginationStart = 0;
                        var paginationEnd = 0;                         

                            var paginationStart = parseInt(num) - 3;
                            if (paginationStart<=1) {paginationStart = 1}

                            var paginationEnd = parseInt(num) + 3;
                            if (paginationEnd>=data.answeredData.pagesOfAnsweredSetRows) {paginationEnd = parseInt(data.answeredData.pagesOfAnsweredSetRows)}

                    $.each(data.answeredData.results,function(key,value){

                        var newRow = $('<tr></tr>');
                        var tdSrc  = $('<td></td>').html(value.src).addClass(tableTdClass);
                        newRow.append(tdSrc);
                        var tdAnswer  = $('<td></td>').html(value.answer).addClass(tableTdClass);
                        newRow.append(tdAnswer);
                        if (data.answeredData.agent_ex == 0) {
                            var result = value.name +":"+value.voip;
                            var isnum = /^\d+$/.test(value.name);
                            if (result.length<=13) {
                                if (isnum) {
                                result = value.voip;                                    
                                }else{
                                result = value.name +":"+value.voip;                                    
                                }
                            }else{
                                result = result.substr(0,12)+"..";
                            }

                            var tdName  = $('<td></td>').html(result).addClass(tableTdClass).attr('title',value.name +":"+value.voip);
                            newRow.append(tdName);
                        }  
                        var tdEnd  = $('<td></td>').html(value.end).addClass(tableTdClass);
                        newRow.append(tdEnd);
                        var tdDuration  = $('<td></td>').html(value.duration + " seconds").addClass(tableTdClass);
                        newRow.append(tdDuration);
                        var tdPlay  = $('<td></td>').addClass(tableTdClass);
                        newRow.append(tdPlay);
                        var str = value.start;
                        var playbackPath = 'this'+','+'I'+','+value.voip+','+value.src+','+ str.replace(' ','+',)
                        var anchorPlay = $('<a></a>').addClass("btn btn-md btn-success btn-block").html("Play")
                                                     .attr('data-toggle','modal')
                                                     .attr('data-target','#audioPlaybackModal')
                                                     .attr("onclick","playCallRecord("+playbackPath+")");
                        tdPlay.append(anchorPlay);                        
                        $("#content_answered_table_body").append(newRow);
                    })
                        var nav = $('<nav></nav>');
                        var ul = $('<ul></ul>').addClass("pagination");

                        var First = $('<li></li>');
                        var anchor1 = $('<a></a>').attr('aria-label','Previous').html("First").addClass("answerPaginationAnchorClassFirst");
                        First.append(anchor1);
                        ul.append(First);

                        var Last = $('<li></li>');
                        var anchor3 = $('<a></a>').attr('aria-label','Next').html("Last").addClass("answerPaginationAnchorClassLast");;
                        Last.append(anchor3);

                        for (var i= paginationStart;  i<= paginationEnd ; i++) {
                            var lidata = $('<li></li>')
                            if (num == i) {
                                lidata.addClass('active');
                            }
                            var anchor2 = $('<a></a>').html(i).attr('data-page',i).addClass("answerPaginationAnchorClass");
                            lidata.append(anchor2);                        
                            ul.append(lidata);                            
                        }

                        ul.append(Last);                                            
                        nav.append(ul);
                    $('#pagination_answered').append(nav);

                    $('.answerPaginationAnchorClass').unbind();
                    $('.answerPaginationAnchorClass').bind("click",function(){
                        $("#content_answered_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination($(this).attr("data-page"),"Answered");
                    });

                    $('.answerPaginationAnchorClassFirst').unbind();
                    $('.answerPaginationAnchorClassFirst').bind("click",function(){
                        $("#content_answered_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(1,"Answered");
                    });

                    $('.answerPaginationAnchorClassLast').unbind();
                    $('.answerPaginationAnchorClassLast').bind("click",function(){
                        $("#content_answered_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(data.answeredData.pagesOfAnsweredSetRows,"Answered");
                    });                                        

                }

                if (data.outgoingData.countOfResults > 0) {
                    $("#outgoing_calls_container").css('display','block');  

                        var tableTdoutgoingReceiverClass = "outgoingReceiverAllAgents";
                        var tableTdoutgoingAgentNameClass = "outgoingAgentNameAllAgents";
                        var tableTdoutgoingStartTimeClass = "outgoingStartTimeAllAgents";
                        var tableTdoutgoingAnsweredTimeClass = "outgoingAnsweredTimeAllAgents";
                        var tableTdoutgoingEndTimeClass = "outgoingEndTimeAllAgents";
                        var tableTdoutgoingDurationClass = "outgoingDurationAllAgents";
                        var tableTdoutgoingCallPlaybackClass = "outgoingCallPlaybackAllAgents";                    

                    if (data.outgoingData.agent_ex != 0) {

                        tableTdoutgoingReceiverClass = "outgoingReceiverAgent";
                        tableTdoutgoingAgentNameClass = "outgoingAgentNameAgent";
                        tableTdoutgoingStartTimeClass = "outgoingStartTimeAgent";
                        tableTdoutgoingAnsweredTimeClass = "outgoingAnsweredTimeAgent";
                        tableTdoutgoingEndTimeClass = "outgoingEndTimeAgent";
                        tableTdoutgoingDurationClass = "outgoingDurationAgent";
                        tableTdoutgoingCallPlaybackClass = "outgoingCallPlaybackAgent";

                        $("#main_container_heading").html(agentName+":"+agentEx);                        
                    }else{
                        $("#main_container_heading").html("All Agents");
                    }

                    $("#content_outgoing_table_head").empty();

                    var newHeaderRow = $('<tr></tr>');
                    var thReceiver = $('<th></th>').html("Receiver").addClass(tableTdoutgoingReceiverClass);
                    newHeaderRow.append(thReceiver);

                    if (data.outgoingData.agent_ex == 0) {
                    var thAgent = $('<th></th>').html("Agent Name").addClass(tableTdoutgoingAgentNameClass);
                    newHeaderRow.append(thAgent);
                    }                    

                    var thStartTime = $('<th></th>').html("Start Time").addClass(tableTdoutgoingStartTimeClass);
                    newHeaderRow.append(thStartTime);

                    var tdAnsweredTime = $('<th></th>').html("Answered Time").addClass(tableTdoutgoingAnsweredTimeClass);
                    newHeaderRow.append(tdAnsweredTime);                    

                    var thEnd = $('<th></th>').html("End Time").addClass(tableTdoutgoingEndTimeClass);
                    newHeaderRow.append(thEnd);

                    var thDuration = $('<th></th>').html("Duration").addClass(tableTdoutgoingDurationClass);
                    newHeaderRow.append(thDuration);

                    var thPlayBack = $('<th></th>').html("Call Playback").addClass(tableTdoutgoingCallPlaybackClass);
                    newHeaderRow.append(thPlayBack);

                    $("#content_outgoing_table_head").append(newHeaderRow);

                    $("#content_outgoing_table_body").empty();
                    $('#pagination_outgoing').empty();                
                    $("#main_container").css("display", "block");
                    $('#countOfOutgoingCalls').html(data.outgoingData.countOfOutgoingSetRows);

                        var paginationStart = 0;
                        var paginationEnd = 0;                         

                            var paginationStart = parseInt(num) - 3;
                            if (paginationStart<=1) {paginationStart = 1}

                            var paginationEnd = parseInt(num) + 3;
                            if (paginationEnd>=data.outgoingData.pagesOfOutgoingSetRows) {paginationEnd = parseInt(data.outgoingData.pagesOfOutgoingSetRows)}

                    $.each(data.outgoingData.results,function(key,value){

                        var newRow = $('<tr></tr>');
                        var tdSrc  = $('<td></td>').html(value.dst).addClass(tableTdoutgoingReceiverClass);
                        newRow.append(tdSrc);
                        if (data.outgoingData.agent_ex == 0) {

                            var result = value.name +":"+value.voip;
                            var isnum = /^\d+$/.test(value.name);
                            if (result.length<=13) {
                                if (isnum) {
                                result = value.voip;                                    
                                }else{
                                result = value.name +":"+value.voip;                                    
                                }
                            }else{
                                result = result.substr(0,12)+"..";
                            }

                            var tdName  = $('<td></td>').html(result).addClass(tableTdoutgoingAgentNameClass).attr('title',value.name +":"+value.voip);
                            newRow.append(tdName);
                        } 
                        var tdStart  = $('<td></td>').html(value.start).addClass(tableTdoutgoingStartTimeClass);
                        newRow.append(tdStart);                         
                        var tdAnswer  = $('<td></td>').html(value.answer).addClass(tableTdoutgoingAnsweredTimeClass);
                        newRow.append(tdAnswer);
                        var tdEnd  = $('<td></td>').html(value.end).addClass(tableTdoutgoingEndTimeClass);
                        newRow.append(tdEnd);
                        var tdDuration  = $('<td></td>').html(value.duration + " seconds").addClass(tableTdoutgoingDurationClass);
                        newRow.append(tdDuration);
                        var tdPlay  = $('<td></td>').addClass(tableTdoutgoingCallPlaybackClass);
                        newRow.append(tdPlay);
                        var str = value.start;
                        var playbackPath = 'this'+','+'O'+','+value.voip+','+value.dst+','+ str.replace(' ','+',)
                        var anchorPlay = $('<a></a>').addClass("btn btn-md btn-success btn-block").html("Play")
                                                     .attr('data-toggle','modal')
                                                     .attr('data-target','#audioPlaybackModal')
                                                     .attr("onclick","playCallRecord("+playbackPath+")");
                        tdPlay.append(anchorPlay);                        
                        $("#content_outgoing_table_body").append(newRow);
                    })

                        var nav = $('<nav></nav>');
                        var ul = $('<ul></ul>').addClass("pagination");

                        var First = $('<li></li>');
                        var anchor1 = $('<a></a>').attr('aria-label','Previous').html("First").addClass("outgoingPaginationAnchorClassFirst");;
                        First.append(anchor1);
                        ul.append(First);

                        var Last = $('<li></li>');
                        var anchor3 = $('<a></a>').attr('aria-label','Last').html("Last").addClass("outgoingPaginationAnchorClassLast");;
                        Last.append(anchor3);

                        for (var i= paginationStart;  i<= paginationEnd ; i++) {
                            var lidata = $('<li></li>')
                            if (num == i) {
                                lidata.addClass('active');
                            }
                            var anchor2 = $('<a></a>').html(i).attr('data-page',i).addClass("outgoingPaginationAnchorClass");
                            lidata.append(anchor2);                        
                            ul.append(lidata);                            
                        }

                        ul.append(Last);                                            
                        nav.append(ul);
                    $('#pagination_outgoing').append(nav);

                    $('.outgoingPaginationAnchorClass').unbind();
                    $('.outgoingPaginationAnchorClass').bind("click",function(){
                        $("#content_outgoing_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination($(this).attr("data-page"),"Outgoing");
                    });

                    $('.outgoingPaginationAnchorClassFirst').unbind();
                    $('.outgoingPaginationAnchorClassFirst').bind("click",function(){
                        $("#content_outgoing_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(1,"Outgoing");
                    });

                    $('.outgoingPaginationAnchorClassLast').unbind();
                    $('.outgoingPaginationAnchorClassLast').bind("click",function(){
                        $("#content_outgoing_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(data.outgoingData.pagesOfOutgoingSetRows,"Outgoing");
                    });                                     

                }

                if (data.missedData.countOfResults > 0) {
                    $("#missed_calls_container").css('display','block');                        

                    if (data.missedData.agent_ex != 0) {
                        $("#main_container_heading").html(agentName+":"+agentEx);                        
                    }else{
                        $("#main_container_heading").html("All Agents");
                    }

                    $("#content_missed_table_body").empty();
                    $('#pagination_missed').empty();                
                    $("#main_container").css("display", "block");
                    $('#countOfMissedCalls').html(data.missedData.countOfMissedSetRows);

                        var paginationStart = 0;
                        var paginationEnd = 0;                         

                            var paginationStart = parseInt(num) - 3;
                            if (paginationStart<=1) {paginationStart = 1}

                            var paginationEnd = parseInt(num) + 3;
                            if (paginationEnd>=data.missedData.pagesOfMissedSetRows) {paginationEnd = parseInt(data.missedData.pagesOfMissedSetRows)}

                    $.each(data.missedData.results,function(key,value){
                            var startTime = new Date(value.start);
                            var endTime = new Date(value.end);       
                            var diff = (endTime - startTime)/1000;                        

                        var newRow = $('<tr></tr>');
                        var tdDate  = $('<td></td>').html(value.date);
                        newRow.append(tdDate);
                        var tdTime  = $('<td></td>').html(value.time);
                        newRow.append(tdTime);
                        var tdNumber  = $('<td></td>').html(value.caller_num);
                        newRow.append(tdNumber);
                        var tdDiff  = $('<td></td>').html(diff+ " seconds");
                        newRow.append(tdDiff);
                        $("#content_missed_table_body").append(newRow);                        
                    })

                    var nav = $('<nav></nav>');
                        var ul = $('<ul></ul>').addClass("pagination");

                        var First = $('<li></li>');
                        var anchor1 = $('<a></a>').attr('aria-label','Previous').html("First").addClass("missedPaginationAnchorClassFirst");
                        First.append(anchor1);
                        ul.append(First);

                        var Last = $('<li></li>');
                        var anchor3 = $('<a></a>').attr('aria-label','Next').html("Last").addClass("missedPaginationAnchorClassLast");;
                        Last.append(anchor3);

                        for (var i= paginationStart;  i<= paginationEnd ; i++) {
                            var lidata = $('<li></li>')
                            if (num == i) {
                                lidata.addClass('active');
                            }
                            var anchor2 = $('<a></a>').html(i).attr('data-page',i).addClass("missedPaginationAnchorClass");
                            lidata.append(anchor2);                        
                            ul.append(lidata);                            
                        }

                        ul.append(Last);                                            
                        nav.append(ul);

                    $('#pagination_missed').append(nav);

                    $('.missedPaginationAnchorClass').unbind();
                    $('.missedPaginationAnchorClass').bind("click",function(){
                        $("#content_missed_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination($(this).attr("data-page"),"Missed");
                    });

                    $('.missedPaginationAnchorClassFirst').unbind();
                    $('.missedPaginationAnchorClassFirst').bind("click",function(){
                        $("#content_missed_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(1,"Missed");
                    });

                    $('.missedPaginationAnchorClassLast').unbind();
                    $('.missedPaginationAnchorClassLast').bind("click",function(){
                        $("#content_missed_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(data.missedData.pagesOfMissedSetRows,"Missed");
                    });                         
                }

                if (data.abandonedData.countOfResults > 0) {
                    $("#abandoned_calls_container").css('display','block');                        

                    if (data.abandonedData.agent_ex != 0) {
                        $("#main_container_heading").html(agentName+":"+agentEx);                        
                    }else{
                        $("#main_container_heading").html("All Agents");
                    }

                    $("#content_abandoned_table_body").empty();
                    $('#pagination_abandoned').empty();                
                    $("#main_container").css("display", "block");
                    $('#countOfAbandonedCalls').html(data.abandonedData.countOfAbandonedSetRows);

                    var paginationStart = 0;
                    var paginationEnd = 0;                         

                    var paginationStart = parseInt(num) - 3;
                    if (paginationStart<=1) {paginationStart = 1}

                        var paginationEnd = parseInt(num) + 3;
                    if (paginationEnd>=data.abandonedData.pagesOfAbandonedSetRows) {paginationEnd = parseInt(data.abandonedData.pagesOfAbandonedSetRows)}


                        $.each(data.abandonedData.results,function(key,value){

                            var newRow = $('<tr></tr>');
                            var tdSrc  = $('<td></td>').html(value.src);
                            newRow.append(tdSrc);

                            str = value.end;
                            var tdDate  = $('<td></td>').html(str.substring(0,10));
                            newRow.append(tdDate);

                            var tdTime  = $('<td></td>').html(str.substring(11,20));
                            newRow.append(tdTime);


                            $("#content_abandoned_table_body").append(newRow);                        
                        })

                    var nav = $('<nav></nav>');
                    var ul = $('<ul></ul>').addClass("pagination");

                    var First = $('<li></li>');
                    var anchor1 = $('<a></a>').attr('aria-label','Previous').html("First").addClass("abandonedPaginationAnchorClassFirst");
                    First.append(anchor1);
                    ul.append(First);

                    var Last = $('<li></li>');
                    var anchor3 = $('<a></a>').attr('aria-label','Next').html("Last").addClass("abandonedPaginationAnchorClassLast");;
                    Last.append(anchor3);

                    for (var i= paginationStart;  i<= paginationEnd ; i++) {
                        var lidata = $('<li></li>')
                        if (num == i) {
                            lidata.addClass('active');
                        }
                        var anchor2 = $('<a></a>').html(i).attr('data-page',i).addClass("abandonedPaginationAnchorClass");
                        lidata.append(anchor2);                        
                        ul.append(lidata);                            
                    }

                    ul.append(Last);                                            
                    nav.append(ul);

                    $('#pagination_abandoned').append(nav);

                    $('.abandonedPaginationAnchorClass').unbind();
                    $('.abandonedPaginationAnchorClass').bind("click",function(){
                        $("#content_abandoned_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination($(this).attr("data-page"),"Abandoned");
                    });

                    $('.abandonedPaginationAnchorClassFirst').unbind();
                    $('.abandonedPaginationAnchorClassFirst').bind("click",function(){
                        $("#content_abandoned_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(1,"Abandoned");
                    });

                    $('.abandonedPaginationAnchorClassLast').unbind();
                    $('.abandonedPaginationAnchorClassLast').bind("click",function(){
                        $("#content_abandoned_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(data.abandonedData.pagesOfAbandonedSetRows,"Abandoned");
                    });                         
                }

                if (data.loginRecordsData.countOfResults > 0) {
                    $("#login_records_container").css('display','block');                        

                    if (data.loginRecordsData.agent_ex != 0) {
                        $("#main_container_heading").html(agentName+":"+agentEx);                        
                    }else{
                        $("#main_container_heading").html("All Agents");
                    }

                    $("#content_login_table_body").empty();
                    $('#pagination_login').empty();                
                    $("#main_container").css("display", "block");
                    $('#countOfLoginRecords').html(data.loginRecordsData.countOfLoginRecordsSetRows);

                    var paginationStart = 0;
                    var paginationEnd = 0;                         

                    var paginationStart = parseInt(num) - 3;
                    if (paginationStart<=1) {paginationStart = 1}

                        var paginationEnd = parseInt(num) + 3;
                    if (paginationEnd>=data.loginRecordsData.pagesOfLoginRecordsSetRows) {paginationEnd = parseInt(data.loginRecordsData.pagesOfLoginRecordsSetRows)}

                        $.each(data.loginRecordsData.results,function(key,value){                     

                            var newRow = $('<tr></tr>');
                            var tdDate  = $('<td></td>').html(value.Logged_Date);
                            newRow.append(tdDate);
                            var tdTime  = $('<td></td>').html(value.Logged_Time);
                            newRow.append(tdTime);
                            var tdOutTime  = $('<td></td>').html(value.Logout_Time);
                            newRow.append(tdOutTime);

                            $("#content_login_table_body").append(newRow);                        
                        })

                    var nav = $('<nav></nav>');
                    var ul = $('<ul></ul>').addClass("pagination");

                    var First = $('<li></li>');
                    var anchor1 = $('<a></a>').attr('aria-label','Previous').html("First").addClass("loginRecordsPaginationAnchorClassFirst");
                    First.append(anchor1);
                    ul.append(First);

                    var Last = $('<li></li>');
                    var anchor3 = $('<a></a>').attr('aria-label','Next').html("Last").addClass("loginRecordsPaginationAnchorClassLast");;
                    Last.append(anchor3);

                    for (var i= paginationStart;  i<= paginationEnd ; i++) {
                        var lidata = $('<li></li>')
                        if (num == i) {
                            lidata.addClass('active');
                        }
                        var anchor2 = $('<a></a>').html(i).attr('data-page',i).addClass("loginRecordsPaginationAnchorClass");
                        lidata.append(anchor2);                        
                        ul.append(lidata);                            
                    }

                    ul.append(Last);                                            
                    nav.append(ul);

                    $('#pagination_login').append(nav);

                    $('.loginRecordsPaginationAnchorClass').unbind();
                    $('.loginRecordsPaginationAnchorClass').bind("click",function(){
                        $("#content_login_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination($(this).attr("data-page"),"LoginRecords");
                    });

                    $('.loginRecordsPaginationAnchorClassFirst').unbind();
                    $('.loginRecordsPaginationAnchorClassFirst').bind("click",function(){
                        $("#content_login_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(1,"LoginRecords");
                    });

                    $('.loginRecordsPaginationAnchorClassLast').unbind();
                    $('.loginRecordsPaginationAnchorClassLast').bind("click",function(){
                        $("#content_login_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination(data.loginRecordsData.pagesOfLoginRecordsSetRows,"LoginRecords");
                    });                         
                }


                if (data.dndRecordsData.countOfResults > 0) {
                    $("#dnd_records_container").css('display','block');                        

                    if (data.dndRecordsData.agent_ex != 0) {
                        $("#main_container_heading").html(agentName+":"+agentEx);                        
                    }else{
                        $("#main_container_heading").html("All Agents");
                    }

                    $("#content_dnd_table_body").empty();
                    $('#pagination_dnd').empty();                
                    $("#main_container").css("display", "block");
                    $('#countOfDndRecords').html(data.dndRecordsData.countOfDndRecordsSetRows);

                    var paginationStart = 0;
                    var paginationEnd = 0;                         

                    var paginationStart = parseInt(num) - 3;
                    if (paginationStart<=1) {paginationStart = 1}

                        var paginationEnd = parseInt(num) + 3;
                    if (paginationEnd>=data.dndRecordsData.pagesOfDndRecordsSetRows) {paginationEnd = parseInt(data.dndRecordsData.pagesOfDndRecordsSetRows)}

                        $.each(data.dndRecordsData.results,function(key,value){                     

                            var newRow = $('<tr></tr>');
                            var str = value.timestamp;
                            var tdDate  = $('<td></td>').html(str.substring(0,10));
                            newRow.append(tdDate);
                            var tdTime  = $('<td></td>').html(str.substring(11,20));
                            newRow.append(tdTime);
                            var tdMode  = $('<td></td>').html(value.dnd_mode);
                            newRow.append(tdMode);

                            $("#content_dnd_table_body").append(newRow);                        
                        })

                    var nav = $('<nav></nav>');
                    var ul = $('<ul></ul>').addClass("pagination");

                    var First = $('<li></li>');
                    var anchor1 = $('<a></a>').attr('aria-label','Previous').html("First").addClass("dndRecordsPaginationAnchorClassFirst");
                    First.append(anchor1);
                    ul.append(First);

                    var Last = $('<li></li>');
                    var anchor3 = $('<a></a>').attr('aria-label','Next').html("Last").addClass("dndRecordsPaginationAnchorClassLast");;
                    Last.append(anchor3);

                    for (var i= paginationStart;  i<= paginationEnd ; i++) {
                        var lidata = $('<li></li>')
                        if (num == i) {
                            lidata.addClass('active');
                        }
                        var anchor2 = $('<a></a>').html(i).attr('data-page',i).addClass("dndRecordsPaginationAnchorClass");
                        lidata.append(anchor2);                        
                        ul.append(lidata);                            
                    }

                    ul.append(Last);                                            
                    nav.append(ul);

                    $('#pagination_dnd').append(nav);

                    $('.dndRecordsPaginationAnchorClass').unbind();
                    $('.dndRecordsPaginationAnchorClass').bind("click",function(){
                        $("#content_dnd_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");
                        filterRecordsPagination($(this).attr("data-page"),"DndRecords");
                    });

                    $('.dndRecordsPaginationAnchorClassFirst').unbind();
                    $('.dndRecordsPaginationAnchorClassFirst').bind("click",function(){
                        $("#content_dnd_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");                        
                        filterRecordsPagination(1,"DndRecords");
                    });

                    $('.dndRecordsPaginationAnchorClassLast').unbind();
                    $('.dndRecordsPaginationAnchorClassLast').bind("click",function(){
                        $("#content_dnd_table_body").html("<h5>&nbsp;Loading...<i class='fa fa-spinner'></i></h5>");                        
                        filterRecordsPagination(data.dndRecordsData.pagesOfDndRecordsSetRows,"DndRecords");
                    });                         
                } 
                                     

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
                    console.log('filter records : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }


    }



    
</script>


    <script>
        function playCallRecord(playAudioAnchor, direction, extension, customerNumber, datetime) {
                    // $("#audioPlaybackModal").modal("toggle");
                    var row = playAudioAnchor.parentNode.parentNode;
                    var id = row.id;

                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
                    //                    $('#callRecordPlaybackSource').attr("src", audioFilePath);
                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();
                    //                    changeCallPlayBackAudioSRC(audioFilePath);
                    //                    $("#audioPlayerSource").attr("src", audioFilePath);
                    //                    $("#audioPlayer")[0].play();
                }
                function pauseAudio() {
    //                    alert($('#audioPlayer')[0].paused);
    if ($('.audioPlayerClass')[0].paused == false) {
        $('.audioPlayerClass')[0].pause();
    }
}

$('#audioPlaybackModal').on('hidden.bs.modal', function () {
  pauseAudio();
});

function downloadCurrentlyPlayingAudioFile(){
    var url = $("#audioPlayerSource").attr("src");
    if(url != ""){
        console.log("downloading audio file from "+url);
        $("#downloaderIframe").attr("src", url);
    }else{
        console.log("No audio url to download");
    }
}

</script>

<!-- audio playback modal -->
<div id="audioPlaybackModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
        </div>
        <div class="modal-body">
            <!-- <p>Some text in the modal.</p> -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div id="audioPlayerDiv">
                            <audio id="audioPlayer" controls class="audioPlayerClass">
                                <source id="audioPlayerSource" src="" type="audio/wav">
                                </audio>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end of audio playback modal -->
<!-- downloader iframe -->
<iframe src="" id="downloaderIframe" style="display: none">

</iframe>
<!-- end of downloader iframe-->
<script> 

    function generateCsv(event){

        var eventId = event.id;

        var reportName = "";

        if (eventId == "exportCsvAnswered") {
           reportName = "Answered"; 
           $('#exportCsvAnswered').addClass("disabled");
           $('#exportCsvAnswered').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");
       }else if (eventId == "exportCsvOutgoing") {
        reportName = "outgoing";
        $('#exportCsvOutgoing').addClass("disabled"); 
        $('#exportCsvOutgoing').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportCsvMissed") {
        reportName = "missed"; 
        $('#exportCsvMissed').addClass("disabled");
        $('#exportCsvMissed').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportCsvAbandoned") {
        reportName = "abandoned"; 
        $('#exportCsvAbandoned').addClass("disabled");
        $('#exportCsvAbandoned').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportloginrecords") {
        reportName = "loginrecords"; 
        $('#exportloginrecords').addClass("disabled");
        $('#exportloginrecords').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportcsvdnd") {
        reportName = "dndreport"; 
        $('#exportcsvdnd').addClass("disabled");
        $('#exportcsvdnd').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }


    var searchStartDate = new Date();

    var fromDate = $('#dateFromValue').val();
    var toDate = $('#dateToValue').val();
    var agentId = $('#hiddenAgentId').val();
    var agentEx = $('#hiddenAgentExt').val();
    var agentName = $('#hiddenAgentName').val();
    var contactNumber = $('#contactNum').val();
    contactNumber = contactNumber.trim();

    if(contactNumber != ""){
        var pattern = /[^0-9]/;
        var result = contactNumber.match(pattern);
        if(result){
           swal(
            'Oops!!!',
            'Contact number is incorrect. Please remove non numeric characters',
            'error'
            );
           return false;
       }
   }

   var queueId = $('#hiddenQueueId').val();
   if(queueId == ""){
       var url = "<?= Url::to(['reports/performanceoverviewbydateexportcsv']) ?>";
   }else{
       var url = "<?= Url::to(['reports/performanceoverviewbyqueueexportcsv']) ?>";
   }

   var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
   var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

   var fromDateConverted = new Date(fromDate+" "+fromTime);
   var toDateConverted = new Date(toDate+" "+toTime);

   var selectedChannels = getSelectedChannelIds();

   var checkedCdrs = getSelectedCdrTypes();


   if (fromDateConverted < toDateConverted) {

            var winops = window.open(url+"&fromdate="+fromDate
           +"&todate="+toDate
           +"&agent_id="+agentId
           +"&agentex="+agentEx
           +"&agentname="+agentName
           +"&contactNum="+contactNumber
           +"&queue_id="+queueId
           +"&fromTime="+fromTime
           +"&toTime="+toTime
           +"&channels="+selectedChannels
           +"&checkedCdrs="+checkedCdrs
           +"&reportName="+reportName); 

                if (eventId == "exportCsvAnswered") {
                    $('#exportCsvAnswered').removeClass("disabled");
                    $('#exportCsvAnswered').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");
                }else if (eventId == "exportCsvOutgoing") {
                    $('#exportCsvOutgoing').removeClass("disabled"); 
                    $('#exportCsvOutgoing').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportCsvMissed") {
                    $('#exportCsvMissed').removeClass("disabled");
                    $('#exportCsvMissed').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportCsvAbandoned") {
                    $('#exportCsvAbandoned').removeClass("disabled");
                    $('#exportCsvAbandoned').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportloginrecords") {
                    $('#exportloginrecords').removeClass("disabled");
                    $('#exportloginrecords').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportcsvdnd") {
                    $('#exportcsvdnd').removeClass("disabled");
                    $('#exportcsvdnd').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");
                }
   }

}
</script>