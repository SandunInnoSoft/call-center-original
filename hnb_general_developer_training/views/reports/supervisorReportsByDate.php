<?php

use yii\helpers\Url;
use yii\helpers\Html;

if ($ftpParams['fileExtension'] == "wav") {
    $audioType = "audio/wav";
} else if ($ftpParams['fileExtension'] == "gsm") {
    $audioType = "audio/x-gsm";
}

?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
.panel-heading{
    font-size: 90%;
}
thead{
    font-weight: bold;
}
.panel-heading{
    background-color: #10297d !important;
    color: white !important;
}
</style>


<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;"  id="agentReportsDIV">
    <div class="row" style="padding-top: 0%">
        <?php
        if ($agent_extension != 0) {
            ?>
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2"><?= $agent_name ?> : <?= $agent_extension ?></div>
        <?php } else { ?>
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2">All agents</div>
        <?php } ?>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div>
    <br>
    <style>
    <?php if ($agent_id == 0) { ?>
        .tableElementGapExtender{
            width: 16% !important;
            text-align: left;
        }
            /*.tableElementGapExtenderOutgoing{
                width: 14% !important;
                text-align: left;
                }*/

                .outgoingReceiver{
                    width: 10% !important;
                    text-align: left;
                }
                .outgoingAgentName{
                    width: 14% !important;
                    text-align: left;
                }
                .outgoingStartTime{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingAnsweredTime{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingEndTime{
                    width: 16% !important;
                    text-align: left;
                }
                .outgoingDuration{
                    width: 10% !important;
                    text-align: left;
                }
                .outgoingCallPlayback{
                    width: 13% !important;
                    text-align: left;
                }


            <?php } else { ?>
                .tableElementGapExtender{
                    width: 20% !important;
                    text-align: left;
                }
                .outgoingReceiver{
                    width: 15% !important;
                    text-align: left;
                }
                .outgoingAgentName{
                    width: 14% !important;
                    text-align: left;
                }
                .outgoingStartTime{
                    width: 17% !important;
                    text-align: left;
                }
                .outgoingAnsweredTime{
                    width: 17% !important;
                    text-align: left;
                }
                .outgoingEndTime{
                    width: 17% !important;
                    text-align: left;
                }
                .outgoingDuration{
                    width: 13% !important;
                    text-align: left;
                }
                .outgoingCallPlayback{
                    width: 18% !important;
                    text-align: left;
                }
            <?php } ?>

        </style>
        <?php if ($answered_calls) { ?>        
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Answered Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($answered_calls) ?></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <span class="nav navbar-nav navbar-right">
                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvAnswered" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>
                      </span>
                  </div>
              </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <table id="answerCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                    <thead class="call-history-thead">
                        <tr>
                            <th class="tableElementGapExtender">
                                Caller Number
                            </th>
    <!--                        <th>
                            Start Time
                        </th>-->
                        <th class="tableElementGapExtender">
                            Answered Time
                        </th>
                        <?php if ($agent_id == 0) { ?>
                            <th class="tableElementGapExtender">
                                Agent Name
                            </th>
                        <?php } ?>
                        <th class="tableElementGapExtender">
                            End Time
                        </th>
                        <th class="tableElementGapExtender">
                            Duration
                        </th>
                        <th class="tableElementGapExtender">
                            Call Playback
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    for ($index = 0; $index < count($answered_calls); $index++) {

//                        $seconds = $answered_calls[$index]['duration'];
//
//                        $hours = floor($seconds / 3600);
//                        $mins = floor($seconds / 60 % 60);
//                        $secs = floor($seconds % 60);
//
//                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        $timeFormat = 0;
                        $timeFirst = strtotime($answered_calls[$index]['answer']);
                        $timeSecond = strtotime($answered_calls[$index]['end']);
                        if ($timeFirst != '0000-00-00 00:00:00' && $timeSecond != '0000-00-00 00:00:00') {
                            $timeFormat = $timeSecond - $timeFirst;
                            if ($timeFormat < 0) {
                                $timeFormat = 0;
                            }
                        }
                        ?>
                        <tr id="<?= $answered_calls[$index]['id'] ?>">
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['src'] ?></td>
                            <!--<td><? = $answered_calls[$index]['start'] ?></td>-->
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['answer'] ?></td>
                            <?php if ($agent_id == 0) { ?>
                                <td class="tableElementGapExtender" title="<?= $answered_calls[$index]['name'].' : '.$answered_calls[$index]['voip'] ?>">
                                    <?php 
                                    $result = $answered_calls[$index]['name'].':'.$answered_calls[$index]['voip'];
                                    if (strlen($result)<=13) {
                                        if (!ctype_digit($answered_calls[$index]['name']) && $answered_calls[$index]['name'] != $answered_calls[$index]['voip']) {
                                            echo $result; 
                                        }else{
                                            echo $answered_calls[$index]['name'];
                                        }
                                    }else {
                                        if (!ctype_digit($answered_calls[$index]['name']) && $answered_calls[$index]['name'] != $answered_calls[$index]['voip']) {
                                            echo substr($result, 0,13).'..';
                                        }else{
                                            echo $answered_calls[$index]['name'];
                                        }
                                    } ?>
                                </td>
                            <?php } ?>
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['end'] ?></td>
                            <td class="tableElementGapExtender"><?= $timeFormat ?> seconds</td>
                            <td class="tableElementGapExtender">
                                <a data-toggle="modal" data-target="#audioPlaybackModal" class="btn btn-md btn-success btn-block" onclick="playCallRecord(this, 'I', '<?= $answered_calls[$index]['voip'] ?>', '<?= $answered_calls[$index]['src'] ?>', '<?= str_replace(' ', '+', $answered_calls[$index]['start']) ?>')">Play</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } ?>
    <br>
    <script>

        var audioType = "<?= $audioType ?>";



        function playCallRecord(playAudioAnchor, direction, extension, customerNumber, datetime) {
                    // $("#audioPlaybackModal").modal("toggle");
                    var row = playAudioAnchor.parentNode.parentNode;
                    var id = row.id;

                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
                    //                    $('#callRecordPlaybackSource').attr("src", audioFilePath);
                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();
                    //                    changeCallPlayBackAudioSRC(audioFilePath);
                    //                    $("#audioPlayerSource").attr("src", audioFilePath);
                    //                    $("#audioPlayer")[0].play();
                }
                function pauseAudio() {
    //                    alert($('#audioPlayer')[0].paused);
    if ($('.audioPlayerClass')[0].paused == false) {
        $('.audioPlayerClass')[0].pause();
    }
}

$('#audioPlaybackModal').on('hidden.bs.modal', function () {
  pauseAudio();
});

function downloadCurrentlyPlayingAudioFile(){
    var url = $("#audioPlayerSource").attr("src");
    if(url != ""){
        console.log("downloading audio file from "+url);
        $("#downloaderIframe").attr("src", url);
    }else{
        console.log("No audio url to download");
    }
}

</script>

<script> 

    function generateCsv(event){

        var eventId = event.id;

        var reportName = "";

        if (eventId == "exportCsvAnswered") {
           reportName = "Answered"; 
           $('#exportCsvAnswered').addClass("disabled");
           $('#exportCsvAnswered').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");
       }else if (eventId == "exportCsvOutgoing") {
        reportName = "outgoing";
        $('#exportCsvOutgoing').addClass("disabled"); 
        $('#exportCsvOutgoing').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportCsvMissed") {
        reportName = "missed"; 
        $('#exportCsvMissed').addClass("disabled");
        $('#exportCsvMissed').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportCsvAbandoned") {
        reportName = "abandoned"; 
        $('#exportCsvAbandoned').addClass("disabled");
        $('#exportCsvAbandoned').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportloginrecords") {
        reportName = "loginrecords"; 
        $('#exportloginrecords').addClass("disabled");
        $('#exportloginrecords').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }else if (eventId == "exportcsvdnd") {
        reportName = "dndreport"; 
        $('#exportcsvdnd').addClass("disabled");
        $('#exportcsvdnd').html("Exporting CSV &nbsp; <i class='fa fa-spinner fa-spin'></i>");            
    }


    var searchStartDate = new Date();

    var fromDate = $('#dateFromValue').val();
    var toDate = $('#dateToValue').val();
    var agentId = $('#hiddenAgentId').val();
    var agentEx = $('#hiddenAgentExt').val();
    var agentName = $('#hiddenAgentName').val();
    var contactNumber = $('#contactNum').val();
    contactNumber = contactNumber.trim();

    if(contactNumber != ""){
        var pattern = /[^0-9]/;
        var result = contactNumber.match(pattern);
        if(result){
           swal(
            'Oops!!!',
            'Contact number is incorrect. Please remove non numeric characters',
            'error'
            );
           return false;
       }
   }

   var queueId = $('#hiddenQueueId').val();
   if(queueId == ""){
       var url = "<?= Url::to(['reports/performanceoverviewbydateexportcsv']) ?>";
   }else{
       var url = "<?= Url::to(['reports/performanceoverviewbyqueueexportcsv']) ?>";
   }

   var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
   var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

   var fromDateConverted = new Date(fromDate+" "+fromTime);
   var toDateConverted = new Date(toDate+" "+toTime);

   var selectedChannels = getSelectedChannelIds();

   var checkedCdrs = getSelectedCdrTypes();


   if (fromDateConverted < toDateConverted) {

            var winops = window.open(url+"&fromdate="+fromDate
           +"&todate="+toDate
           +"&agent_id="+agentId
           +"&agentex="+agentEx
           +"&agentname="+agentName
           +"&contactNum="+contactNumber
           +"&queue_id="+queueId
           +"&fromTime="+fromTime
           +"&toTime="+toTime
           +"&channels="+selectedChannels
           +"&checkedCdrs="+checkedCdrs
           +"&reportName="+reportName); 

                if (eventId == "exportCsvAnswered") {
                    $('#exportCsvAnswered').removeClass("disabled");
                    $('#exportCsvAnswered').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");
                }else if (eventId == "exportCsvOutgoing") {
                    $('#exportCsvOutgoing').removeClass("disabled"); 
                    $('#exportCsvOutgoing').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportCsvMissed") {
                    $('#exportCsvMissed').removeClass("disabled");
                    $('#exportCsvMissed').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportCsvAbandoned") {
                    $('#exportCsvAbandoned').removeClass("disabled");
                    $('#exportCsvAbandoned').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportloginrecords") {
                    $('#exportloginrecords').removeClass("disabled");
                    $('#exportloginrecords').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");            
                }else if (eventId == "exportcsvdnd") {
                    $('#exportcsvdnd').removeClass("disabled");
                    $('#exportcsvdnd').html("Export CSV &nbsp;<i class='glyphicon glyphicon-save'></i>");
                }   

   }

}


</script>

<button id="downloadCsvButton" onclick="getDownloadCsv()" style="display: none;"></button>

<!-- audio playback modal -->
<div id="audioPlaybackModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
        </div>
        <div class="modal-body">
            <!-- <p>Some text in the modal.</p> -->
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div id="audioPlayerDiv">
                            <audio id="audioPlayer" controls class="audioPlayerClass">
                                <source id="audioPlayerSource" src="" type="audio/wav">
                                </audio>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- end of audio playback modal -->
<!-- downloader iframe -->
<iframe src="" id="downloaderIframe" style="display: none">

</iframe>
<!-- end of downloader iframe-->

<?php if ($outgoing_calls) { ?>
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Outgoing Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($outgoing_calls) ?></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <span class="nav navbar-nav navbar-right">
                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvOutgoing" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>                        
                      </span>
                  </div>
              </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <table id="outgoingCallsTable" class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
            <thead class="call-history-thead">
                <tr>
                    <th class="outgoingReceiver">
                        Receiver
                    </th>
                    <?php if ($agent_id == 0) { ?>
                        <th class="outgoingAgentName">
                            Agent Name
                        </th>
                    <?php } ?>
                    <th class="outgoingStartTime">
                        Start Time
                    </th>
                    <th class="outgoingAnsweredTime">
                        Answered Time
                    </th>
                    <th class="outgoingEndTime">
                        End Time
                    </th>
                    <th class="outgoingDuration">
                        Duration
                    </th>
                    <th class="outgoingCallPlayback">
                        Call Playback
                    </th>
                </tr>
            </thead>
            <tbody class="call-history-tbody">
                <?php
                for ($index = 0; $index < count($outgoing_calls); $index++) {

                    $seconds = $outgoing_calls[$index]['duration'];

                    $hours = floor($seconds / 3600);
                    $mins = floor($seconds / 60 % 60);
                    $secs = floor($seconds % 60);

                    $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                    ?>
                    <tr>
                        <td class="outgoingReceiver"><?= $outgoing_calls[$index]['dst'] ?></td>
                        <?php if ($agent_id == 0) { ?>
                            <td class="outgoingAgentName" title="<?= $outgoing_calls[$index]['name'].' : '.$outgoing_calls[$index]['voip'] ?>">
                                <?php 
                                $result = $outgoing_calls[$index]['name'].':'.$outgoing_calls[$index]['voip'];
                                if (strlen($result)<=13) {
                                    if (!ctype_digit($outgoing_calls[$index]['name']) && $outgoing_calls[$index]['name'] != $outgoing_calls[$index]['voip']) {
                                        echo $result; 
                                    }else{
                                        echo $outgoing_calls[$index]['name'];
                                    }
                                }else {
                                    if (!ctype_digit($outgoing_calls[$index]['name']) && $outgoing_calls[$index]['name'] != $outgoing_calls[$index]['voip']) {
                                        echo substr($result, 0,13).'..';
                                    }else{
                                        echo $outgoing_calls[$index]['name'];
                                    }
                                } ?>
                            </td>
                        <?php } ?>                          
                        <td class="outgoingStartTime"><?= $outgoing_calls[$index]['start'] ?></td>
                        <td class="outgoingAnsweredTime"><?= $outgoing_calls[$index]['answer'] ?></td>
                        <td class="outgoingEndTime"><?= $outgoing_calls[$index]['end'] ?></td>
                        <td class="outgoingDuration" style="text-align:center"><?= $timeFormat ?></td>
                        <td class="outgoingCallPlayback">
                            <?php
                            if($outgoing_calls[$index]['answer'] != NULL && $outgoing_calls[$index]['answer'] != ''){
                                ?>
                                <a data-toggle="modal" data-target="#audioPlaybackModal" class="btn btn-md btn-success btn-block" onclick="playCallRecord(this, 'O', '<?= $outgoing_calls[$index]['voip'] ?>', '<?= $outgoing_calls[$index]['dst'] ?>', '<?= str_replace(' ', '+', $outgoing_calls[$index]['start']) ?>')">Play</a>
                                <?php
                            }else{
                                ?>
                                <a class="btn btn-md btn-success btn-block disabled">Play</a>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>
<br>
<?php if ($agent_id != 0) { ?>
    <div class="row">
                <?php if ($agent_abandoned_calls) { ?>        
        <div class="col-xs-6" style="padding-right: 2%">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Abandoned Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($agent_abandoned_calls) ?></a>
                    <a>&nbsp;</a>                    
                    </div>
                    <div class="navbar-collapse collapse">
                        <span class="nav navbar-nav navbar-right">
                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvAbandoned" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>                        
                      </span>
                  </div>
              </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Caller Number
                                </th>
                                <th>
                                    Abandoned Date
                                </th>
                                <th>
                                    Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

                                $datetime = $agent_abandoned_calls[$index]['end'];
                                $dateTimeExploded = explode(" ", $datetime);
                                ?>
                                <tr>
                                    <td><?= $agent_abandoned_calls[$index]['src'] ?></td>
                                    <td><?= $dateTimeExploded[0] ?></td>
                                    <td><?= $dateTimeExploded[1] ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
            </div>
        </div>
                <?php } ?>

        <!--<div class="col-xs-1"></div>-->
                    <?php if ($login_records) { ?>        
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Login Details</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($login_records) ?></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <span class="nav navbar-nav navbar-right">
                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportloginrecords" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>                        
                      </span>
                  </div>
              </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php //if ($agent_abandoned_calls) {     ?>        
                        <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                            <thead class="abandoned-call-history-thead">
                                <tr>
                                    <th>
                                        Logged Date
                                    </th>
                                    <th>
                                        Logged Time
                                    </th>
                                    <th>
                                        Logout Time
                                    </th>                        
                                </tr>
                            </thead>
                            <tbody class="abandoned-call-history-tbody">
                                <?php
                                for ($index = 0; $index < count($login_records); $index++) {

                                    $login_time_sig = $login_records[$index]['logged_in_time'];
                                    $login_date = date('Y-m-d', $login_time_sig);
                                    $login_time = date('H:i:s', $login_time_sig);

                                    $logged_time_sig = $login_records[$index]['time_signature'];
                                    $logged_date = date('Y-m-d', $logged_time_sig);
                                    $logged_time = date('H:i:s', $logged_time_sig);
                                    ?>
                                    <tr>
                                        <td><?= $login_date ?></td>
                                        <td><?= $login_time ?></td>
                                        <td><?= $logged_time ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>
                    <?php } ?>
        </div><br>
        <div class="row">
                    <?php if ($agent_dnd_reocrds) { ?>        
            <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent DND Records</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($agent_dnd_reocrds) ?></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <span class="nav navbar-nav navbar-right">
                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportcsvdnd" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>                        
                      </span>
                  </div>
              </div>
                <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                        <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                            <thead class="abandoned-call-history-thead">
                                <tr>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Time
                                    </th>
                                    <th>
                                        Mode
                                    </th>                        
                                </tr>
                            </thead>
                            <tbody class="abandoned-call-history-tbody">
                                <?php
                                for ($index = 0; $index < count($agent_dnd_reocrds); $index++) {
                                    $date_time = $agent_dnd_reocrds[$index]['timestamp'];
                                    $explods = explode(" ", $date_time);
                                    ?>
                                    <tr>
                                        <td><?= $explods[0]; ?></td>
                                        <td><?= $explods[1]; ?></td>
                                        <td><?= $agent_dnd_reocrds[$index]['dnd_mode']; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>
                    <?php } ?>
        </div>
    <?php }
    if ($agent_id == 0) {
        ?>
        <style>
        .missed-calls-table th{
            width: 150px;
        }
    </style>
    <div class="row">
        <?php if ($missed_calls) { ?>        
            <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Missed Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($missed_calls) ?></a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <span class="nav navbar-nav navbar-right">
                            <a class="btn btn-md btn-warning" style="margin-top: 5%;" id="exportCsvMissed" onclick="generateCsv(this)" >
                        Export CSV &nbsp;<i class="glyphicon glyphicon-save"></i></a>
                        <a>&nbsp;</a>                        
                      </span>
                  </div>
              </div>
                <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                    <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Caller Number
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php for($i = 0; $i < count($missed_calls); $i++){?>
                                <tr>
                                    <td><?=$missed_calls[$i]['date']?></td>
                                    <td><?=$missed_calls[$i]['time']?></td>
                                    <td><?=$missed_calls[$i]['caller_num']?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php } ?>
        <?php if ($ivrCallsData) { ?>        
            <div class="col-xs-6">
                <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">IVR Received Calls</a>
                        <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($ivrCallsData) ?></a>
                    </div>    
                </div>
                <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                    <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date Time
                                </th>
                                <th>
                                    IVR
                                </th>
                                <th>
                                    Caller Number
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php for($i = 0; $i < count($ivrCallsData); $i++){?>
                                <tr>
                                    <td><?=$ivrCallsData[$i]['start']?></td>
                                    <td><?=$ivrCallsData[$i]['dst']?></td>
                                    <td><?=$ivrCallsData[$i]['src']?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
    <br>
    <style type="text/css">
    .tableElementVMGapExtender{
        width: 50%;
    }
</style>
<div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Received Voice Mail Calls</a>
        <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($vmData) ?></a>
    </div>
</div>
<?php if ($vmData) { ?>        
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <table id="vmCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
            <thead class="call-history-thead">
                <tr>
                    <th class="tableElementVMGapExtender">
                        Caller Number
                    </th> 
                    <th class="tableElementVMGapExtender">
                        Voice Mail Number
                    </th>                      
                    <th class="tableElementVMGapExtender">
                        Answered Time
                    </th>
                    <th class="tableElementVMGapExtender">
                        End Time
                    </th>
                    <th class="tableElementVMGapExtender">
                        Duration
                    </th>
                </tr>
            </thead>
            <tbody class="call-history-tbody">
                <?php
                for ($index = 0; $index < count($vmData); $index++) {

                   $seconds = $vmData[$index]['duration'];

                   $hours = floor($seconds / 3600);
                   $mins = floor($seconds / 60 % 60);
                   $secs = floor($seconds % 60);

                   $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                   ?>
                   <tr>
                    <td class="tableElementVMGapExtender"><?= $vmData[$index]['src'] ?></td>
                    <td class="tableElementVMGapExtender"><?= $vmData[$index]['dst'] ?></td>
                    <td class="tableElementVMGapExtender"><?= $vmData[$index]['answer'] ?></td>
                    <td class="tableElementVMGapExtender"><?= $vmData[$index]['end'] ?></td>
                    <td class="tableElementVMGapExtender"><?= $timeFormat ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php } ?>


<?php }?>




</div>


