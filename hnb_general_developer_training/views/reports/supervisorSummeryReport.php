<?php
/*
 * 
 * @author Sandun
 * @since 2017-10-30
 */

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div id="agentReportsDIV" class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;border-bottom: 0;">
    <div class="row" style="padding-top: 0%">
        <?php
        if ($agent_extension != 0) {
            ?>
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2"><?= $agent_name ?> : <?= $agent_extension ?></div>
        <?php } else { ?>
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2">All agents</div>
        <?php } ?>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div><br>    

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Overall summery of <!-- <?= $agentInfo['agentName'] ?> --> performance from <?= $_GET['fromdate']?> to <?= $_GET['todate']?></a>
            <!-- <button class="btn btn-danger navbar-right">Download PDF&nbsp;<i class="fa fa-file-pdf-o"></i></button> -->
        </div>
        <div class="navbar-collapse collapse">
            <span class="nav navbar-nav navbar-right">
                <a class="btn btn-md btn-danger" style="margin-top: 5%;" id="downloadBtn" data-html2canvas-ignore="true" onclick="downloadPDF()" >
                    Export PDF&nbsp;<i class="fa fa-file-pdf-o"></i></a>
                    <a>&nbsp;</a>
                </span>
            </div>        
        </div>    

    <br>

        <style>
        h3{
            text-align: center;
        }

        .tiles{
            text-align: center;
        }
    </style>

    <div class="row" >
        <div class="col-xs-3 tiles" style="border: 1px solid #10297d; border-left: 0; border-right: 0;">
            Total Calls Received <!-- BOTH : total calls received to call center-->
            <br>
            <?= $calls['totalCallsReceived'] ?>
        </div>
        <div class="col-xs-3 tiles" style="border: 1px solid #10297d; border-left: 0; border-right: 0;">
            Total Calls Answered 
            <br>
            <?= $calls['totalCallsAnswered'] ?>
        </div>
        <?php 
            if($_GET['agent_id'] == 0){
                // All agents
                ?>
                    <div class="col-xs-3 tiles" style="border: 1px solid #10297d; border-left: 0; border-right: 0;">
                        Total Calls Missed
                        <br>
                        <?= $calls['totalCallsMissed'] ?>
                    </div>
                <?php
            }else{
                // single agent
                ?>
                    <div class="col-xs-3 tiles" style="border: 1px solid #10297d; border-left: 0; border-right: 0;">
                        Total Calls Abandoned
                        <br>
                        <?= $calls['totalCallsAbandoned'] ?>
                    </div>
                <?php
            }
        ?>
        <div class="col-xs-3 tiles" style="border: 1px solid #10297d;border-right: 0">
            Total Outgoing Calls
            <br>
            <?= $calls['totalCallsOutgoing'] ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3 tiles">

        </div>
        <div class="col-xs-3 tiles">
            
            <br>
            
        </div>
        <div class="col-xs-3 tiles">
            
            <br>
            
        </div>
        <div class="col-xs-3 tiles">
            
            <br>
            
        </div>
    </div>
<!--    <hr>
    <div class="row">
        <div class="col-xs-3 tiles">
            Total Time worked
            <br>
            <? = $work['totalTimeWorked'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Total time spent for breaks
            <br>
            <? = $work['totalBreakTime'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Number of times turned ON DND
            <br>
            <? = $work['dndOnCount'] ?>
        </div>
        <div class="col-xs-3 tiles">
            Total number of agents registered
            <br>
            <? = $agentInfo['noOfAgents']?>
        </div>
    </div>-->
<!-- </div> -->



<?php
/*
*@modified Vinothan
*@since 23-08-2018
*<b> Added this block to prepare data to generate graphs
*/

$chartLables = array();

$data_breaksFullData = $breakFullData;
$data_breaksFullData = json_encode($data_breaksFullData);

$data_breaksDailyData = $breakDailyData;
$data_breaksDailyData = json_encode($data_breaksDailyData);

// Calls Answered in given time Data
$callsAnsweredGivenPeriodAgent = 0;
$callsAnsweredGivenPeriodAgentPercentage = 0;
$callsAnsweredGivenPeriodOthers = 0;
$callsAnsweredGivenPeriodOthersPercentage = 0;
if ($callAnswerCounts[0] != null && $callAnswerCounts[1] != null) {

//    if($callAnswerCounts[0] == 0){
//        $callAnswerCounts[0] = 1;
//    }
//    
//    if($callAnswerCounts[1] == 0){
//       $callAnswerCounts[1] = 1; 
//    }

    if($agent_extension != '0'){
        // single agent
        $callsAnsweredGivenPeriodAgent = $callAnswerCounts[0];
        $callsAnsweredGivenPeriodOthers = $callAnswerCounts[1] - $callAnswerCounts[0];
        $callsAnsweredGivenPeriodAgentPercentage = ($callAnswerCounts[0] > 0 && $callAnswerCounts[1] > 0 ? ($callAnswerCounts[0] / $callAnswerCounts[1]) * 100 : 0);
        $callsAnsweredGivenPeriodOthersPercentage = ($callAnswerCounts[1] > 0 && $callAnswerCounts[0] > 0 ? (($callAnswerCounts[1] - $callAnswerCounts[0]) / $callAnswerCounts[1]) * 100 : 100);
    }else{
        // all agents
        $callsAnsweredGivenPeriodAgent = $callAnswerCounts[1];
        $callsAnsweredGivenPeriodOthers = $callAnswerCounts[0];
        $callsAnsweredGivenPeriodAgentPercentage = ($callAnswerCounts[0] > 0 && $callAnswerCounts[1] > 0 ? ($callAnswerCounts[0] / $callAnswerCounts[1]) * 100 : 0);
        $callsAnsweredGivenPeriodOthersPercentage = ($callAnswerCounts[1] > 0 && $callAnswerCounts[0] > 0 ? (($callAnswerCounts[1] - $callAnswerCounts[0]) / $callAnswerCounts[1]) * 100 : 100);
        
    }
//$totalCallsFigure
}

// =================================

$agent_answered_percnt = 0;
$others_answered_percnt = 0;
$others_answered_count = 0;
if ($all_answered_calls_count != 0) {
    if($agent_extension != "0"){
        // single agent
        $agent_answered_percnt = ($agent_answered_calls_count / $all_answered_calls_count) * 100;
        $others_answered_percnt = ($others_answered_count / $all_answered_calls_count) * 100;
        $others_answered_count = $all_answered_calls_count - $agent_answered_calls_count;
    }else{
        $others_answered_count = $todayMissedCallsCount;
    }
}

$agent_answered_percnt = bcadd($agent_answered_percnt, '0', 2);
$others_answered_percnt = bcadd($others_answered_percnt, '0', 2);
// ====================================================================
$dailyBreakPercentage = 0;
$dailyWorkPercentage = 0;
$dailyBreakTime = '00 hours 00 minutes';
$dailyWorkTime = '00 hours 00 minutes';
if ($breakDailyData[0] != null) {
    $dailyBreakPercentage = ($breakDailyData[0] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
}
if ($breakDailyData[1] != null) {
    $dailyWorkPercentage = ($breakDailyData[1] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
}
if ($breakDailyData[2] != null) {
    $dailyBreakTime = $breakDailyData[2];
}
if ($breakDailyData[3] != null) {
    $dailyWorkTime = $breakDailyData[3];
}

// Break and work times of the given period
$timePeriodBreakPercentage = 0;
$timePeriodWorkPercentage = 0;
$timePeriodBreakValue = '00 hours 00 minutes';
$timePeriodWorkValue = '00 hours 00 minutes';
if ($breakFullData[0] != null) {
    $timePeriodBreakPercentage = ($breakFullData[0] / ($breakFullData[0] + $breakFullData[1])) * 100;
}
if ($breakFullData[1] != null) {
    $timePeriodWorkPercentage = ($breakFullData[1] / ($breakFullData[0] + $breakFullData[1])) * 100;
}
if ($breakFullData[2] != null) {
    $timePeriodBreakValue = $breakFullData[2];
}
if ($breakFullData[3] != null) {
    $timePeriodWorkValue = $breakFullData[3];
}
?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
.panel-heading{
    font-size: 90%;
}
thead{
    font-weight: bold;
}
.panel-heading{
    background-color: #10297d !important;
    color: white !important;
}
</style>

<style>
.answered-calls-history-table {
    width: 100%;
}

.outgoing-calls-history-table {
    width: 100%;
}

.abandoned-calls-history-table {
    width: 100%;
}

.call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
    display: block;
}

.call-history-thead th {
    height: 30px;
}

.call-history-tbody {
    overflow-y: auto;
    height: 300px;
    font-size: 90%;
}

.call-history-tbody td, .call-history-thead th {
    float: left;
    width: 20%;
}

.abandoned-call-history-tbody td, .abandoned-call-history-thead th {
    float: left;
    width: 33.33%;
}

.call-history-thead tr:after, call-history-tbody tr:after {
    clear: both;
    content: ' ';
    display: block;
    visibility: hidden;
}
</style>

<!-- <div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;border-top: 0;"  id="agentReportsDIV"> -->

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold; ">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Break Reports</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Daily break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($dailyWorkPercentage, 2) ?>%</td>
                                                <td><?= round($dailyBreakPercentage, 2) ?>%</td>
                                            </tr>
                                            <tr>
                                                <td><?= $dailyWorkTime ?></td>
                                                <td><?= $dailyBreakTime ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_breaksDailyData ?>,
                                        backgroundColor: [
                                        'rgba(244, 157, 0, 0.8)',
                                        'rgba(66, 139, 202, 0.8)'
                                        ]
                                    }]
                                },
                                options: {
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-5" style="padding-top: 15px">
                        <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                            <div class="panel-heading" style="height: 60px">
                                Total break times comparison
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Worked</td>
                                                    <td>Breaks</td>
                                                </tr>                                            
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?= round($timePeriodWorkPercentage, 2) . ' %' ?></td>
                                                    <td><?= round($timePeriodBreakPercentage, 2) . ' %' ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $timePeriodWorkValue ?></td>
                                                    <td><?= $timePeriodBreakValue ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    </div>                            
                    <div class="col-xs-7" style="padding-bottom: 5px">
                        <canvas id="breakTimeFullChart" height="250px"></canvas>
                        <script>
                            var ctx = document.getElementById("breakTimeFullChart").getContext('2d');
                            var myPieChart = new Chart(ctx, {
                                type: 'pie',
                                data: {
                                    labels: ["Breaks", "Worked"],
                                    datasets: [{
//                                        label: '# of Votes',
data: <?= $data_breaksFullData ?>,
backgroundColor: [
'rgba(244, 157, 0, 0.8)',
'rgba(66, 139, 202, 0.8)'                                        ]
}]
},
options: {
}
});
</script>                
</div>            
</div>
</div>
</div><br>

<div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
    <div class="navbar-header">
        <a class="navbar-brand" href="#">Service Level Figures</a>
    </div>
</div>
<div class="row navbar-default" style="border: 1px solid #cdcdcd">
    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-5" style="padding-top: 15px">
                <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                    <div class="panel-heading" style="height: 60px">
                        Today Answered Calls Result
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <?php if($agent_extension != "0"){ 
                                            // single agent
                                    $chartLables = array("Today Agent Answered Calls", "Today Others Answered calls");


                                    ?>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= $others_answered_percnt . '%' ?></td>
                                                <td><?= $agent_answered_percnt . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $others_answered_count . ' calls' ?></td>
                                                <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>                                                                        
                                <?php } else { 
                                        // all agents
                                    $chartLables = array("Today All answered calls", "Today All missed calls");
                                    $totalCallsToday = $todayMissedCallsCount + $agent_answered_calls_count;
                                    $missedCallsPersentage = ($todayMissedCallsCount == 0 || $totalCallsToday == 0 ? 0 : $todayMissedCallsCount / $totalCallsToday * 100);
                                    $answeredCallsPersentage = ($agent_answered_calls_count == 0 || $totalCallsToday == 0 ? 0 : $agent_answered_calls_count / $totalCallsToday * 100);                                     
                                    ?>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Missed</td>
                                                <td>Answered</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($missedCallsPersentage, 2) . '%' ?></td>
                                                <td><?= round($answeredCallsPersentage, 2) . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $others_answered_count . ' calls' ?></td>
                                                <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>                                       
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
            <div class="col-xs-7" style="padding-bottom: 5px">
                <canvas id="serviceLevelDailyChart" height="250px"></canvas>
                <script>
                    var ctx = document.getElementById("serviceLevelDailyChart").getContext('2d');
                    var myPieChart = new Chart(ctx, {
                        type: 'pie',
                        data: {
                            labels: ["<?=$chartLables[0]?>", "<?=$chartLables[1]?>"],
                            datasets: [{
                                        //                            label: '# of Votes',
//                                        data: <? = $data_breaksFullData ?>,
data: [<?= $agent_answered_calls_count ?>,<?= ($agent_extension != "0" ? $all_answered_calls_count - $agent_answered_calls_count : $todayMissedCallsCount) ?>],
backgroundColor: [
'rgba(54, 156, 56, 0.8)',
'rgba(214, 84, 40, 0.8)'
]
}]
},
options: {
}
});
</script>
</div>
</div>
</div>
<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-5" style="padding-top: 15px">
            <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                <div class="panel-heading" style="height: 60px">
                    Total Answered Calls Result
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <?php if($agent_extension != "0") {
                                        //single agent
                                $chartLables = array("Total Agent Answered Calls", "Total Others Answered calls");

                                ?>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td>Others</td>
                                            <td>Agent</td>
                                        </tr>                                            
                                    </thead>
                                    <tbody>
                                        <tr>
    <!--                                                <td><? = $others_answered_percnt . '%' ?></td>
        <td><? = $agent_answered_percnt . '%' ?></td>-->
        <td><?= round($callsAnsweredGivenPeriodOthersPercentage, 2) . '%' ?></td>
        <td><?= round($callsAnsweredGivenPeriodAgentPercentage, 2) . '%' ?></td>
    </tr>
    <tr>
    <!--                                                <td><? = $others_answered_count ?></td>
        <td><? = $agent_answered_calls_count ?></td>-->
        <td><?= $callsAnsweredGivenPeriodOthers . ' calls' ?></td>
        <td><?= $callsAnsweredGivenPeriodAgent . ' calls' ?></td>
    </tr>
</tbody>
</table>                                    
<?php }else{ 
                                       // all agents
    $chartLables = array("Total Answered Calls", "Total Missed calls"); 
    $noOfMissedCalls = count($missed_calls);
    $totalCallsCount = $noOfMissedCalls + $callsAnsweredGivenPeriodAgent;
    $missedCallsPercentage = ($noOfMissedCalls == 0 || $totalCallsCount == 0 ? 0 : $noOfMissedCalls / $totalCallsCount * 100);
    $answeredCallsPercentage = ($callsAnsweredGivenPeriodAgent == 0 || $totalCallsCount == 0 ? 0 : $callsAnsweredGivenPeriodAgent / $totalCallsCount * 100);
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>Missed</td>
                <td>Answered</td>
            </tr>                                            
        </thead>
        <tbody>
            <tr>
    <!--                                                <td><? = $others_answered_percnt . '%' ?></td>
        <td><? = $agent_answered_percnt . '%' ?></td>-->
        <td><?= round($missedCallsPercentage, 2) . '%' ?></td>
        <td><?= round($answeredCallsPercentage, 2) . '%' ?></td>
    </tr>
    <tr>
    <!--                                                <td><? = $others_answered_count ?></td>
        <td><? = $agent_answered_calls_count ?></td>-->
        <td><?= $callsAnsweredGivenPeriodOthers . ' calls' ?></td>
        <td><?= $callsAnsweredGivenPeriodAgent . ' calls' ?></td>
    </tr>
</tbody>
</table>                                    

<?php
}
?>
</div>
</div>
</div>
</div>                                        
</div>                            
<div class="col-xs-7" style="padding-bottom: 5px">
    <canvas id="serviceLevelFullChart" height="250px"></canvas>
    <script>
        var ctx = document.getElementById("serviceLevelFullChart").getContext('2d');
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["<?=$chartLables[0]?>", "<?=$chartLables[1]?>"],
                datasets: [{
//                                        label: '# of Votes',
//                                        data: <? = $data_breaksDailyData ?>,
data: [<?= $callsAnsweredGivenPeriodAgent ?>,<?= $callsAnsweredGivenPeriodOthers ?>],
backgroundColor: [
'rgba(54, 156, 56, 0.8)',
'rgba(214, 84, 40, 0.8)'
]
}]
},
options: {
}
});
</script>
</div>            
</div>
</div>
</div>

<!-- Below scripts used to generate PDF report -->
<script type="text/javascript">

    function downloadPDF() { 
        $('#downloadBtn').addClass("disabled");
        $('#downloadBtn').html("Exporting PDF &nbsp; <i class='fa fa-spinner fa-spin'></i>");    

        var deferreds = [];
        var doc = new jsPDF();
        var deferred = $.Deferred();
        deferreds.push(deferred.promise());
        generateCanvas(doc, deferred);

        <?php if ($agent_extension !=0 ) {  ?> 
            var filename = "<?= $_GET['fromdate'].'_'.$_GET['todate'].'_'.$agent_name.'_'.$agent_extension.'_Summery';  ?>";
        <?php }else{ ?>
            var filename = "<?= $_GET['fromdate'].'_'.$_GET['todate'].'_All_Agents_Summery';  ?>";        
        <?php } ?>


        $.when.apply($, deferreds).then(function () {
          doc.save(filename+'.pdf');
          $('#downloadBtn').removeClass("disabled");
          $('#downloadBtn').html("Export PDF &nbsp;<i class='fa fa-file-pdf-o'></i>");
      })  
    }


    function generateCanvas(doc, deferred){
        html2canvas(document.getElementById("agentReportsDIV"), {
            onrendered: function (canvas) {
                var img = canvas.toDataURL();
                doc.addImage(img, 'PNG',10,20,190,200);
                deferred.resolve();
            }
        });
    }

  
</script>

