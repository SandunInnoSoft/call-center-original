<?php
/* @var $this \yii\web\View */
/* @var $content string */

date_default_timezone_set("Asia/Colombo");

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- External resources -->
    <script src="js/jquery-3.2.1.js"></script>
    <script src="js/sweetalert2.js"></script>
    <script src="js/jspdf.min.js"></script>
    <script src="js/html2canvas.js"></script>
    <!--<script src="js/bootstrap-switch.js"></script>-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.js"></script>-->
    <link rel="icon" href="hnb_images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/sweetalert2.css"></link>
    <link rel="stylesheet" href="css/font-awesome.min.css"></link>
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap2/bootstrap-switch.css"></link>-->
    <!--<link rel="stylesheet" href="css/bootstrap-switch.css"></link>-->
    <!-- /External resources -->
    <?= Html::csrfMetaTags() ?>
    <title>HNB General Insurance Call Center</title>
    <?php $this->head() ?>
    <style>
    .navbar-inverse{
        background-color: #10297d; /* hnb blue color */
        border-top-color: #faa61a; /* hnb yellow color */
    }

    .navbar-fixed-top{
        color: #faa61a;
    }

    .navbar-inverse .navbar-nav > li > a {
        color: #faa61a;
        font-weight: bold;
    }

    .navbar-inverse .navbar-brand {
        color: #faa61a;
        /*background-image: url('hnb_images/logo_min.png');*/
        background-repeat: no-repeat;
        background-position: center;
        width: 60px;
    }

    .logout-label > a{
        background-color: #faa61a;                
        border: 1px solid #10297d;
        border-radius: 5px;                
    }

    .dropdown-label > a{
        background-color: #d9edf7;                
        border: 1px solid #10297d;
        border-radius: 5px;                
    }            

    .navbar-inverse .navbar-nav > .logout-label > a {
        color: #10297d;
    } 

    .navbar-inverse .navbar-nav > .dropdown-label > a {
        color: #10297d;
    }                         

</style>

</head>
<body style="">
    <?php $this->beginBody() ?>

    <?php 
    $id = "";
    $full_name = "";
    $voip = "";

    if (isset($_SESSION["user_id"])) {
        $id = $_SESSION["user_id"];
    }                        
    if (isset($_SESSION["voip"])){
        $voip = $_SESSION["voip"];
    }
    if (isset($_SESSION["full_name"])) {
        $full_name = $_SESSION["full_name"];
    }
    ?>

    <div class="wrap" style="border: 5px solid #10297d;">
        <?php
        NavBar::begin([
            'brandLabel' => '',
            'brandUrl' => ['/user/login_view'],
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Home', 'url' => ['/user/login_view']],
                ['label' => 'HNB GI CC Dashboard', 'url' => ['/supevisor/dashboard']],
//                    ['label' => 'Test', 'options' => ['class' => 'headerFonts'], 'url' => ['/user/login_view']],
                ['label' => $full_name.':'.$voip, 'options' => ['class' => 'dropdown-toggle'], 'url' =>'#',
                'items'=>[
                    '<li><a href="#" id="changePassword"><span class="glyphicon glyphicon-wrench"></span>&nbsp;&nbsp;Change Password</a></li>',
             ],
         ],                  

         ['label' => 'Logout', 'options' => ['class' => 'logout-label'], 'url' => ['/user/doomout']],
     ],
 ]);
        NavBar::end();
        ?>

        <div class="container">
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
            <?= $content ?>
        </div>
    </div>

        <!--<footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <? = date('Y') ?></p>
        
                <p class="pull-right"><? = Yii::powered() ?></p>
            </div>
        </footer>-->
        <script>
            if (typeof (EventSource) !== "undefined") {
                var source = new EventSource("<?= Url::to(['events/event']) ?>");
                source.onmessage = function (event) {
                    console.log("timestamp online hit" + "+++ " + event.data);
                    if (event.data == 1) {
                        window.location.replace("<?= Url::to(['user/doomout']) ?>&from=autoLogout"); 
                    }
                };
            } else {
                document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
            }
        </script>
        <script>
            /**
             * <b>This function calls and ajax to the server function which checks the endless loop execution</b>
             * @author Sandun
             * @since 2017-10-02
             * @returns {}
             */
             function checkEndlessLoopAJAX(){
                console.log("Check endless loop invoked : "+getTodayDate());
                $.ajax({
                    url: "<?= Url::to(['asterisks/check_endless_loop_execution']) ?>",
                    type: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        console.log("Check endless loop Success returned: "+getTodayDate()+" : data "+data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Check endless loop error : "+jqXHR.responseText);
                    }
                });
            }
            
            function checkAnswerCallEndlessLoopAJAX(){
                console.log("Check answer call endless loop invoked : "+getTodayDate());
                $.ajax({
                    url: "<?= Url::to(['asterisks/check_answercall_endless_loop_execution']) ?>",
                    type: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        console.log("Check answer call endless loop Success returned: "+getTodayDate()+" : data "+data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Check answer call endless loop error : "+jqXHR.responseText);
                    }
                });
            }
            
            function checkHungupCallEndlessLoopAJAX(){
                console.log("Check hungup call endless loop invoked : "+getTodayDate());
                $.ajax({
                    url: "<?= Url::to(['asterisks/check_hungupcall_endless_loop_execution']) ?>",
                    type: 'GET',
                    success: function (data, textStatus, jqXHR) {
                        console.log("Check hung up call endless loop Success returned: "+getTodayDate()+" : data "+data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Check hung up call endless loop error : "+jqXHR.responseText);
                    }
                });
            }
            /**
             * <b>Invokes when the page loads and invokes check endless loop ajax function every 1 minute time</b>
             * 
             * @author Sandun
             * @since 2017-10-02
             * @type type             
             * */
             $(document).ready(function(){
                // checkEndlessLoopAJAX();
                // checkAnswerCallEndlessLoopAJAX();
                // checkHungupCallEndlessLoopAJAX();
                // setInterval(function(){
                //     checkEndlessLoopAJAX();
                //     checkAnswerCallEndlessLoopAJAX();
                //     checkHungupCallEndlessLoopAJAX();                    
                // }, 60000);
            });

            /**
             * <b>Returns current time in Y-m-d h:i:s format</b>
             * <p>This function returns the current time in Y-m-d h:i:s format</p>
             * 
             * @author Sandun
             * @since 2017-10-02
             * @returns {String} 
             */
             function getTodayDate(){
                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                return date+' '+time;
            }
        </script>

        <script>
            $("#changePassword").click(function () {
                $('#idPassword').val('');
                $('#idConfirmPassword').val('');
                $('#alertMessage').removeClass();
                $('#alertMessage').html('');              
                $('#changePasswordModal').modal({
                    show: 'true'
                });
            });

        </script>                
        <!-- Modal -->
        <div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title text-center" id="exampleModalLabel">Change Password</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body form-group" style="">
                    <label>Password</label>
                    <input type="Password" class="form-control"  id="idPassword"> <br>
                    <label>Confirm Password</label>                        
                    <input type="Password" class="form-control" id="idConfirmPassword"> <br>
                    <input type="hidden" id="idUserId" value="<?=$id?>"/>
                </div>
                <div id="alertMessage"></div>
            </div>
            <div class="text-center">
                <button type="button" class="btn btn-primary" onclick="changePassword()">Update</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"  onclick="resetModel()">Close</button>
                <br><br><br>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">



    function changePassword(){
        var user_id = $('#idUserId').val();
        var password = $('#idPassword').val();
        var confirmPassword = $('#idConfirmPassword').val();

        if (password != confirmPassword) {
            $('#alertMessage').removeClass();            
            $('#alertMessage').html('Password does not match the confirm password');              
            $('#alertMessage').addClass("alert alert-danger");              
        }


        if (password != '' && confirmPassword != '' && password == confirmPassword) {
            $.ajax({
                url: "<?= Url::to(['user/editpassword']) ?>",
                type: 'POST',
                data: {user_id: user_id, password: password},
                success: function (data, textStatus, jqXHR) {
                    if (data == '1') {
                                $('#alertMessage').removeClass();
                                $('#alertMessage').html('Password Changed Successfully');              
                                $('#alertMessage').addClass("alert alert-success");                        
                        }else{
                            console.log("data submission failed: " + data);
                                $('#alertMessage').removeClass();
                                $('#alertMessage').html('Please re-submit the form');              
                                $('#alertMessage').addClass("alert alert-danger");                            
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error" + jqXHR.rex);
                                $('#alertMessage').removeClass();
                                $('#alertMessage').html('Some error occured');              
                                $('#alertMessage').addClass("alert alert-danger");                                                     
                    }
                });
        }
    }
</script>


<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
