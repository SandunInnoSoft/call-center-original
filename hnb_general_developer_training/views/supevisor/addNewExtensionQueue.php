<?php

use yii\helpers\Url;
?>

<body>
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-md-12">
                <a href = "<?= Url::to(['supevisor/managequeues']) ?>" class = "btn btn-info btn-sm" type = "button">< Back</a>
                <h3 class = "text-primary text-center">
                    <?php
                    if (isset($_GET['queue'])) {
                        ?>
                        <b> Update VOIP Extension Queue </b>
                        <?php
                    } else {
                        ?>
                        <b> Add New VOIP Extension Queue </b>
                        <?php
                    }
                    ?>
                </h3>
                <form role = "form" id = "idNewQueue">
                    <div class = "form-group">

                        <label>
                            Queue Name<span style="color: tomato">*</span>
                        </label>
                        <input type = "text" class = "form-control" id = "queueNameInput" name = "queueNameInput"
                        <?php if (isset($_GET['queue'])) { ?>
                           value = "<?= $queue_data[0]['name']; ?>";
                       <?php } ?>
                       />
                       <?php if (isset($_GET['queue'])) { ?>
                        <input type = "hidden" class = "form-control" id = "editQueueId" name = "editQueueId" value = "<?=$_GET['queue']?>"/>
                    <?php }else{ ?>
                        <input type = "hidden" class = "form-control" id = "editQueueId" name = "editQueueId" value = ""/>
                    <?php } ?>
                </div>
                <?php if (isset($_GET['queue'])) { ?>
                    <div class = "form-group">
                        <label>
                            Extensions<span style="color: tomato">*</span>
                        </label>
                        <div class="row" style="padding-left: 1%; padding-right: 1%">
                            <div class="col-md-12" style="border: 1px solid #337ab7">                                    
                                <?php
                                if ($allExtensions) {
                                        // unassigned extensions available
                                    $count = 1;
                                    $remaining = count($allExtensions);
                                    echo "<div class='row'>";
                                    for ($x = 0; $x < count($allExtensions); $x++) {
                                        if ($count == 12) {
                                            ?>
                                            <div class="col-md-1">
                                                <input class="extensionsCheckbox" type="checkbox" value="<?= $allExtensions[$x] ?>"
                                                <?php
                                                foreach ($assignedExtensions as $item) {
                                                    if ($item == $allExtensions[$x]) {
                                                        echo 'checked="checked"';
                                                    } else {
                                                        continue;
                                                    }
                                                }
                                                ?>                                                        
                                                >
                                                <label><?= $allExtensions[$x] ?></label>
                                            </div>
                                            <?php
                                            echo "</div>";
                                            echo "<div class='row'>";
                                            $count = 1;
                                        } else {
                                            ?>
                                            <div class="col-md-1">
                                                <input class="extensionsCheckbox" type="checkbox" value="<?= $allExtensions[$x] ?>"
                                                <?php
                                                foreach ($assignedExtensions as $item) {
                                                    if ($item == $allExtensions[$x]) {
                                                        echo 'checked="checked"';
                                                    } else {
                                                        continue;
                                                    }
                                                }
                                                ?>                                                                                                  
                                                >
                                                <label><?= $allExtensions[$x] ?></label>
                                            </div>
                                            <?php
                                            $count++;
                                        }
                                    }
                                } else {
                                        // all extensions assigned
                                    ?>
                                    <div class="row">
                                        <strong style="padding-left: 10%">No VOIP extensions available to assign</strong>
                                    </div>                                                   
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class = "form-group">
                        <label>
                            Extensions<span style="color: tomato">*</span>
                        </label>
                        <div class="row" style="padding-left: 1%; padding-right: 1%">
                            <div class="col-md-12" style="border: 1px solid #337ab7">                                    
                                <?php
                                if ($unasignedExtensions) {
                                        // unassigned extensions available
                                    $count = 1;
                                    $remaining = count($unasignedExtensions);
                                    echo "<div class='row'>";
                                    for ($x = 0; $x < count($unasignedExtensions); $x++) {
                                        if ($count == 12) {
                                            ?>
                                            <div class="col-md-1">
                                                <input class="extensionsCheckbox" type="checkbox" value="<?= $unasignedExtensions[$x] ?>">
                                                <label><?= $unasignedExtensions[$x] ?></label>
                                            </div>
                                            <?php
                                            echo "</div>";
                                            echo "<div class='row'>";
                                            $count = 1;
                                        } else {
                                            ?>
                                            <div class="col-md-1">
                                                <input class="extensionsCheckbox" type="checkbox" value="<?= $unasignedExtensions[$x] ?>">
                                                <label><?= $unasignedExtensions[$x] ?></label>
                                            </div>
                                            <?php
                                            $count++;
                                        }
                                    }
                                } else {
                                        // all extensions assigned
                                    ?>
                                    <div class="row">
                                        <strong style="padding-left: 10%">No VOIP extensions available to assign</strong>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <br>
                <div class = "form-group">

                    <label>
                        Queue Supervisor<span style="color: tomato">*</span>
                    </label>
                    <?php
                    if ($unassignedSupervisors) {
                        ?>
                        <select id="queueSupervisorSelect" name="queueSupervisorSelect" class="form-control">

                            <?php if (isset($_GET['queue'])) { ?>
                                <option value="" selected="selected"><?= $queue_data[0]['fullname']; ?></option>                                
                            <?php } ?>
                            <?php
                            for ($x = 0; $x < count($unassignedSupervisors); $x++) {
                                echo "<option value='" . $unassignedSupervisors[$x]['id'] . "'>" . $unassignedSupervisors[$x]['name'] . "</option>";
                            }
                            ?>
                        </select>                                      
                        <?php
                    } else {
                        ?>

                        <?php if (isset($_GET['queue'])) { ?>
                            <select id="queueSupervisorSelect" name="queueSupervisorSelect" class="form-control" disabled="">
                                <option value="" selected="selected"><?= $queue_data[0]['fullname']; ?></option>
                            </select>                                 
                        <?php } else { ?>


                            <select id="queueSupervisorSelect" name="queueSupervisorSelect" class="form-control" disabled="">
                                <option value="0">No Available Supervisors to assign</option>
                            </select> 
                        <?php } ?>                           

                        <?php
                    }
                    ?>
                </div>
                <button type = "button" id = "btnSaveQueue" class = "btn btn-success" onclick="validateForm()">
                    Save
                </button>
                <button type = "reset" class = "btn btn-info" onclick="clearForm()">
                    Clear
                </button>

            </form>
        </div>
    </div>
</div>
</body>
<script>
    function addNewExtensionQueueAjax() {
        var queueName = $("#queueNameInput").val();
        var editQueueId = $("#editQueueId").val();
        var selectedExtensionsCsvString = getSelectedExtensions();
        var selectedSupervisorId = $("#queueSupervisorSelect").val();

        $.ajax({
            url: "<?= Url::to(['supevisor/addnewextensionqueue']) ?>",
            type: 'POST',
            data: {name: queueName, selectedExtensions: selectedExtensionsCsvString, selectedSupervisor: selectedSupervisorId, editQueueId:editQueueId},
            success: function (data, textStatus, jqXHR) {
                swal({
                    title: 'Added!',    
                    text: 'New extensions queue added successfully',
                    type: "success"
                });

                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal({
                    title: 'Some problem!',
                    text: 'An error occured, Please re submit your new extensions queue!',
                    type: "danger"
                });
            }
        });

    }

    function getSelectedExtensions() {
        var selectedExtensionsCsvString = "";
        $(".extensionsCheckbox:checked").each(function (index) {
            selectedExtensionsCsvString = selectedExtensionsCsvString.concat(this.value, ",");
        });

        return selectedExtensionsCsvString;
    }

    function validateForm() {
        var validationSuccess = true;
        if ($("#queueNameInput").val() == "") {
            // Queue name is empty
            if (validationSuccess == true) {
                validationSuccess = false;
                swal({
                    title: 'Oops!',
                    text: 'Queue name is empty!',
                    type: "warning"
                });
            }
        }

        if (getSelectedExtensions() == "") {
            // no extensions were selected
            if (validationSuccess == true) {
                validationSuccess = false;
                swal({
                    title: 'Oops!',
                    text: 'No extensions selected!',
                    type: "warning"
                });
            }
        }

        if ($("#queueSupervisorSelect").val() == '0') {
            // no supervisor selected
            if (validationSuccess == true) {
                validationSuccess = false;
                swal({
                    title: 'Oops!',
                    text: 'No supervisor selected!',
                    type: "warning"
                });
            }
        }

        if (validationSuccess == true) {
            // successfully validated
            addNewExtensionQueueAjax();
        }

        function clearForm() {
            $("#queueNameInput").val("");
            $("#queueSupervisorSelect").val("0");
            $(".extensionsCheckbox:checked").each(function (index) {
                $(this).removeAttr("checked");
            });
        }
    }


</script>