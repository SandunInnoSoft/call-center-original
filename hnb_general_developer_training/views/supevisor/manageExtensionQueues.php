<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .btn-default{
                background-color: #10297d !important;
                color: #faa61a !important;
            }
            .text-success{
                color: #10297d !important;
            }

        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="margin-top: 1%;">
                <div class="col-md-12">
                    <h3 class="text-left text-success"><strong>
                            Manage Queues
                        </strong>
                    </h3>
                </div>
            </div>
            <div class="row">
                <?php
                if(Yii::$app->session->get("user_role") == 1){
                    // user is admin
                ?>    
                    <div class="col-md-3">
                        <a name="btnAddNewExtensionQueue" id="btnAddNewExtensionQueue" class="btn btn-default btn-lg" href="<?= Url::to(['supevisor/queuecreate'])?>">Add New Extensions Queue</a>
                    </div>
                    <div class="col-md-5">

                    </div>
                <?php    
                }else{
                    // user is not admin
                ?>
                    <div class="col-md-2">
                        <a name="btnAddNewUser" id="btnAddNewUser" class="btn btn-default btn-lg" href="<?= Url::to(['supevisor/insert']) ?>">Add New User</a>
                    </div>
                    <div class="col-md-10">

                    </div>                
                
                <?php
                }
                ?>

            </div>
            <br>
            <br>


        </div>
        <div class="row" style="margin-top: 3%;" id="userTableFrame">
            Loading queue list...

        </div>
        <script>
            $(document).ready(function () {
                $('#userTableFrame').load("<?php echo Url::to(['supevisor/managetableofqueues']) ?>");
            });
        </script>
    </body>
</html>
