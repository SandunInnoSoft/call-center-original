<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
?>
<script type="text/javascript">
    function Pager(tableName, itemsPerPage) {
        this.tableName = tableName;
        this.itemsPerPage = itemsPerPage;
        this.currentPage = 1;
        this.pages = 0;
        this.inited = false;

        this.showRecords = function (from, to) {
            var rows = document.getElementById(tableName).rows;
            // i starts from 1 to skip table header row
            for (var i = 1; i < rows.length; i++) {
                if (i < from || i > to)
                    rows[i].style.display = 'none';
                else
                    rows[i].style.display = '';
            }
        }

        this.showPage = function (pageNumber) {
            if (!this.inited) {
//                alert("not inited");
                return;
            }

            var oldPageAnchor = document.getElementById('pg' + this.currentPage);
            oldPageAnchor.className = 'pg-normal';

            this.currentPage = pageNumber;
            var newPageAnchor = document.getElementById('pg' + this.currentPage);
            newPageAnchor.className = 'pg-selected';

            var from = (pageNumber - 1) * itemsPerPage + 1;
            var to = from + itemsPerPage - 1;
            this.showRecords(from, to);
        }

        this.prev = function () {
            if (this.currentPage > 1)
                this.showPage(this.currentPage - 1);
        }

        this.next = function () {
            if (this.currentPage < this.pages) {
                this.showPage(this.currentPage + 1);
            }
        }

        this.init = function () {
            var rows = document.getElementById(tableName).rows;
            var records = (rows.length - 1);
            this.pages = Math.ceil(records / itemsPerPage);
            this.inited = true;
        }

        this.showPageNav = function (pagerName, positionId) {
            if (!this.inited) {
//                alert("not inited");
                return;
            }
            var element = document.getElementById(positionId);

            var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
            for (var page = 1; page <= this.pages; page++)
                pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
            pagerHtml += '<span onclick="' + pagerName + '.next();" class="pg-normal"> Next &#187;</span>';

            element.innerHTML = pagerHtml;
        }
    }




</script>

<style>
    .pg-normal{
        cursor: pointer !important;
    }

</style>

<div class="container-fluid">
    <div class="col-md-12" id="adminUserManageTable">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="userManageTable">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>

                            </th>
                            <th>
                                Queue
                            </th>                            
                            <th>
                                Supervisor
                            </th>
                            <th>
                                Edit
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for ($i = 0; $i < count($user_data); $i++) {
                            $color = 'success';
                            $role = 'Supervisor';
                            $status = '';
                            $queue = $user_data[$i]['id'];

                            if ($user_data[$i]['status'] == 'active') {
                                $status = 'Active';
                            } else if ($user_data[$i]['status'] == 'inactive') {
                                $status = 'In-active';
                            } else if ($user_data[$i]['status'] == 'deleted') {
                                $status = 'Deleted';
                            }
                            ?>
                            <tr id="<?= $user_data[$i]['id'] ?>" class="<?= $color ?>">
                                <td>
                                    <?= $i + 1 ?>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-success" onclick="deleteQueue('<?= $user_data[$i]['id'] ?>', this)">Delete</a>
                                    <!--<input type="checkbox" id="<? = $user_data[$i]['id'] ?>" value="<? = $user_data[$i]['id'] ?>">-->
                                </td>
                                <td>
                                    <?= $user_data[$i]['name'] ?>
                                </td>
                                <td>
                                    <?= $user_data[$i]['fullname'] ?>
                                </td>                                
                                <td>
                                    <?php
                                    $urlSet = Url::to(['supevisor/queuecreate']);
                                    $urlSet = $urlSet . '&queue=' . $queue;
                                    ?>
                                    <!--<a href="<?//= Url::to(['supevisor/insert&user='.$user.'']) ?>" class="btn btn-sm btn-info">Edit</a>-->
                                    <a href="<?= $urlSet ?>" class="btn btn-sm btn-info">Edit</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>                        

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" id="pagerDiv">
            <script type="text/javascript">
                
                function enableAgentWebphone(element, agentId){
                    $(element).addClass("disabled");
                    $(element).html("Enabling..");
                    var state = 1;
                    $.ajax({
                        url: "<?= Url::to(['supevisor/changeagentwebphonestate'])?>",
                        type: 'GET',
                        data: {state : state, agentId : agentId},
                        success: function (data, textStatus, jqXHR) {
//                            alert(data);
                            $(element).removeClass("disabled");
                            if (data == 1) {
                                // success
                                $(element).html("Disable");
                                $(element).removeClass("btn-success");
                                $(element).addClass("btn-danger");
                                element.setAttribute("onClick", "disableAgentWebphone(this,"+agentId+")");
//                                location.reload();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
//                            alert(jqXHR.responseText);
                        }
                    });
                }
                
                function disableAgentWebphone(element, agentId){
                    $(element).addClass("disabled");
                    $(element).html("Disabling..");
                    var state = 2;
                    $.ajax({
                        url: "<?= Url::to(['supevisor/changeagentwebphonestate'])?>",
                        type: 'GET',
                        data: {state : state, agentId : agentId},
                        success: function (data, textStatus, jqXHR) {
//                            alert(data);
                            $(element).removeClass("disabled");
                            if (data == 1) {
                                // success
                                $(element).html("Enable");
                                $(element).removeClass("btn-danger");
                                $(element).addClass("btn-success");
                                element.setAttribute("onClick", "enableAgentWebphone(this,"+agentId+")");
//                                location.reload();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
//                            alert(jqXHR.responseText);
                        }
                    });                    
                }
                
                var pager = new Pager('userManageTable', 10);
                pager.init();
                pager.showPageNav('pager', 'pagerDiv');
                pager.showPage(1);


                function deleteQueue(queueId, deleteAnchorObj) {
                    $(deleteAnchorObj).addClass("disabled");
                    $(deleteAnchorObj).html("Deleting");
                    $.ajax({
                        url: "<?= Url::to(['supevisor/deletequeue']) ?>",
                        type: 'GET',
                        data: {qId: queueId},
                        success: function (data, textStatus, jqXHR) {
                            if (data == 1) {
                                $("#" + queueId).remove();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                    });
                }

                function changeAgentRole(clickedAnchorElement, promoteOrDemote) {
                    var row = clickedAnchorElement.parentNode.parentNode;
                    if (promoteOrDemote == 1) {
                        // promote agent
                        promoteAgent(row);
                    } else {
                        // demote super agent
                        demoteSuperAgent(row);
                    }
                }

                function promoteAgent(row) {
                    var userId = row.id;
                    var promoteButton = row.cells[6].children[0];
                    $(promoteButton).addClass("disabled");
                    $(promoteButton).html("Promoting..");
                    $.ajax({
                        url: "<?= Url::to(['supevisor/promoteagent']) ?>",
                        type: 'GET',
                        data: {userId: userId},
                        success: function (data, textStatus, jqXHR) {
//                            alert(data);
                            $(promoteButton).removeClass("disabled");
                            if (data == 1) {
                                // success
                                row.cells[3].innerHTML = "Senior Agent";
                                $(promoteButton).html("Demote");
                                $(promoteButton).removeClass("btn-danger");
                                $(promoteButton).addClass("btn-warning");
                                promoteButton.setAttribute("onClick", "changeAgentRole(this, 0)");
                                $(row).removeClass("success");
                                $(row).addClass("info");
//                                location.reload();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
//                            alert(jqXHR.responseText);
                        }
                    });
                }

                function demoteSuperAgent(row) {
                    var userId = row.id;
                    var demoteButton = row.cells[6].children[0];
                    $(demoteButton).addClass("disabled");
                    $(demoteButton).html("Demoting..");
                    $.ajax({
                        url: "<?= Url::to(['supevisor/demotesuperagent']) ?>",
                        type: 'GET',
                        data: {userId: userId},
                        success: function (data, textStatus, jqXHR) {
//                            alert(data);
                            $(demoteButton).removeClass("disabled");
                            if (data == 1) {
                                // success
//                                location.reload();
                                row.cells[3].innerHTML = "Agent";
                                $(demoteButton).html("Promote");
                                $(demoteButton).removeClass("btn-warning");
                                $(demoteButton).addClass("btn-danger");
                                demoteButton.setAttribute("onClick", "changeAgentRole(this, 1)");
                                $(row).removeClass("info");
                                $(row).addClass("success");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
//                            alert(jqXHR.responseText);
                        }
                    });
                }
                function logoutUser(element, id) {
                    if (id != '') {
                        $.ajax({
                            url: "<?= Url::to(['supevisor/logoutuser']) ?>",
                            type: 'POST',
                            data: {userId: id},
                            success: function (data, textStatus, jqXHR) {
                                if (data == 1) {
                                    $(element).addClass("disabled");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log('jqXHR is: ' + jqXHR);
                                console.log('jqXHR is: ' + jqXHR.responseText);
                                console.log('textStatus is: ' + textStatus);
                                console.log('errorThrown is: ' + errorThrown);
                            }
                        });
                    }
                }

            </script>
        </div>
    </div>
</div>