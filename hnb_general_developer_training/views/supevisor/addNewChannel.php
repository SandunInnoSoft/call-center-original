<?php

use yii\helpers\Url;
?>

<body onload="channelupdate()">
    <div class = "container-fluid">
        <div class = "row">
            <div class = "col-md-12">
                <a href = "<?= Url::to(['supevisor/managechannels']); ?>" class = "btn btn-info btn-sm" type = "button">< Back</a>

                <?php 
                if (isset($_GET['channel'])) { ?>
                    <h3 class = "text-primary text-center">
                        <b> Update Channel Infomation </b>
                    </h3>
                <?php } else { ?>
                    <h3 class = "text-primary text-center">
                        <b> Add New Channel </b>
                    </h3>
                <?php } ?>
                <!-- <form action="#" role = "form" id = "idNewChannelForm"> -->
                    <div class = "form-group">

                        <label>
                            Channel Number<span style="color: tomato">*</span>
                        </label>
                        <input type = "number" class = "form-control" id = "channelNumberInput" name = "channelNumberInput" 


                        <?php 
                        if (isset($_GET['channel'])) { ?>
                            value = "<?= $oneChannel['channel_number'] ?>";
                        <?php } ?>

                        />
                        <span id="channelValidationMessage"></span>
                        <?php if (isset($_GET['channel'])) { ?>
                            <input type = "hidden" class = "form-control" id = "editChannelId" name = "editChannelId" value = "<?= $oneChannel['id'] ?> "/>
                        <?php }else{ ?>
                            <input type = "hidden" class = "form-control" id = "editChannelId" name = "editChannelId" value = ""/>
                        <?php } ?>                        
                    </div>
                    <div class = "form-group">

                        <label>
                            Channel Name<span style="color: tomato">*</span>
                        </label>
                        <input type = "text" class = "form-control" id = "channelNameInput" name = "channelNameInput" 
                        <?php 
                        if (isset($_GET['channel'])) { ?>
                            value = "<?= $oneChannel['channel_name'] ?>";
                        <?php } ?>
                        />
                    </div>

                    <div class = "form-group">
                        <label>
                            Extension Queue<span style="color: tomato">*</span>
                        </label>
                        <select id="selectExtensionQueue" name="selectExtensionQueue" class="form-control">
                            <?php 
                            if (isset($_GET['channel'])) { ?>
                                <option value="<?=$extensionQueue['id']?>" selected = "selected"><?=$extensionQueue['name']?></option>
                            <?php }else{ ?>
                                <option value="0">Select Extension</option>
                            <?php } ?>                             
                            <?php
                            for ($x = 0; $x < count($extensionQueues); $x++) {
                                echo "<option value='" . $extensionQueues[$x]['id'] . "'>" . $extensionQueues[$x]['name'] . "</option>";
                            }
                            ?>
                        </select>
                        <span id="channelValidationMessage"></span>
                    </div>

                    <div class = "form-group">
                        <label>
                            Channel Description<span style="color: tomato">*</span>
                        </label>
                        <div>
                            <textarea rows="4" class = "form-control"  id = "channelDescInput" name = "channelDescInput"><?php 
                            if (isset($_GET['channel'])) { ?><?= $oneChannel['channel_description'] ?><?php } ?></textarea>
            </div>
        </div>    
        <?php if (isset($_GET['channel'])) { ?>
        <a id = "btnSaveChannel" class = "btn btn-success btn-md" onclick="validateForm()">Update</a>    
        <?php } else {?>                            
        <a id = "btnSaveChannel" class = "btn btn-success btn-md disabled" onclick="validateForm()">
            Save
        </a>
    <?php } ?>
        <a class = "btn btn-info" onclick="clearForm()">
            Clear
        </a>
        <!-- </form> -->

    </div>
</div>
<br>
<div class="row" id="extInsertNotifDiv">
</div>
</div>
</body>
<script>
    var isChannelExists = true;

    $("#channelNumberInput").keypress(function (event) {
        pressEnter(event);
    });

    function pressEnter(event) {
        if (event.keyCode == 13) {
            $("#btnSaveChannel").click();
        }
    }

    function showSaveSuccessMessage(channel) {
        $("#extInsertNotifDiv").empty();
        var successMsgDiv = $("<div></div>");
        $(successMsgDiv).addClass("alert alert-success");
        $(successMsgDiv).append("<strong> " + channel + " Channel Saved Successfully!</strong>");
        $("#extInsertNotifDiv").append(successMsgDiv);
    }

    function showSaveFailedMessage(channel) {
        $("#extInsertNotifDiv").empty();
        var failMsgDiv = $("<div></div>");
        $(failMsgDiv).addClass("alert alert-danger");
        $(failMsgDiv).append("<strong> " + channel + " Channel Saving Failed!</strong> Please try saving again");
        $("#extInsertNotifDiv").append(failMsgDiv);
    }

    function addNewChannelAjax() {
        var channelNumber = $("#channelNumberInput").val();
        var channelName = $("#channelNameInput").val();
        var channelDesc = $("#channelDescInput").val();
        var extensionQueue = $("#selectExtensionQueue").val();
        var editChannelId = $("#editChannelId").val();

        $.ajax({
            url: "<?= Url::to(['supevisor/addnewchannelajax']) ?>",
            type: 'POST',
            data: {channelNumber: channelNumber,channelName:channelName,channelDesc:channelDesc,extensionQueue:extensionQueue,editChannelId:editChannelId},
            success: function (data, textStatus, jqXHR) {
                if (data == "1") {
                    swal({
                        title: 'Added!',
                        text: 'New channel added successfully',
                        type: "success"
                    });
                    // showSaveSuccessMessage(channelNumber);
                    clearForm();
                } else if(data == "2"){
                    swal({
                        title: 'Done!',
                        text: 'Channel updated successfully',
                        type: "success"
                    });
                    // showSaveSuccessMessage(channelNumber);
                    }else{
                    swal({
                        title: 'Some problem!',
                        text: 'An error occured, Please re submit your new channel!',
                        type: "danger"
                    });
                    // showSaveFailedMessage(channelNumber);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // alert(jqXHR.responseText);
                // swal({
                //     title: 'Some problem!',
                //     text: 'An error occured, Please re submit your new channel!',
                //     type: "danger"
                // });
                showSaveFailedMessage(channelNumber);
            }
        });

    }


    function validateForm() {
        var validationSuccess = true;
        if ($("#channelNumberInput").val() == "") {
            // channel number is empty
            if (validationSuccess == true) {
                validationSuccess = false;
                swal({
                    title: 'Oops!',
                    text: 'Channel Number is empty!',
                    type: "warning"
                });
            }
        }

        if ($("#channelNameInput").val() == "") {
            // channel name is empty
            if (validationSuccess == true) {
                validationSuccess = false;
                swal({
                    title: 'Oops!',
                    text: 'Channel Name is empty!',
                    type: "warning"
                });
            }
        }

        if ($("#channelDescInput").val() == "") {
            // channel desc is empty
            if (validationSuccess == true) {
                validationSuccess = false;
                swal({
                    title: 'Oops!',
                    text: 'Channel Description is empty!',
                    type: "warning"
                });
            }
        }


        if (validationSuccess == true) {
            if (isChannelExists == false) {
                // successfully validated
                addNewChannelAjax();
            } else {
                swal({
                    title: 'Oops!',
                    text: 'Channel Number already exists!',
                    type: "warning"
                });
            }
        }

        function clearForm() {
            $("#channelNumberInput").val("");
        }
    }


        <?php 
        if (isset($_GET['channel'])) { ?>
            function channelupdate(){
            isChannelExists = false;    
            }
        <?php } ?>


     <?php if (isset($_GET['channel'])) { ?>


   $("#channelNumberInput").keyup(function () {

            var typingText = $(this).val();

            if (typingText== "<?= $oneChannel['channel_number'] ?>") {
                        $("#btnSaveChannel").removeClass("disabled");
            }            

            else if (typingText != "" && typingText.length > 2 && typingText!= "<?= $oneChannel['channel_number'] ?>") {
            // typing text is not empty
            $("#channelValidationMessage").html("Checking..");
            $("#channelValidationMessage").css("color", "black");
            $("#btnSaveChannel").addClass("disabled");

            $.ajax({
                url: "<?= Url::to(['admin/checkchannelavailability']) ?>",
                data: {typingChannel: typingText},
                type: 'GET',
                success: function (data, textStatus, jqXHR) {
                    if (data == "1") {
                        // channel is available
                        $("#channelValidationMessage").html("Channel is available");
                        $("#channelValidationMessage").css("color", "green");
                        $("#btnSaveChannel").removeClass("disabled");
                        isChannelExists = false;
                    } else {
                        // channel is not available
                        $("#channelValidationMessage").html("Channel is not available");
                        $("#channelValidationMessage").css("color", "red");
                        $("#btnSaveChannel").addClass("disabled");
                        isChannelExists = true;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("channel validation error : " + jqXHR.responseText);
                    isChannelExists = true;
                }
            });
        } else {
            // typing text is empty
            $("#channelValidationMessage").html("");
            $("#channelValidationMessage").css("color", "red");
            $("#btnSaveChannel").addClass("disabled");
        }
});        
     
     <?php } else { ?>   

    $("#channelNumberInput").keyup(function () {

            var typingText = $(this).val();

            if (typingText != "" && typingText.length > 2 ) {
            // typing text is not empty
            $("#channelValidationMessage").html("Checking..");
            $("#channelValidationMessage").css("color", "black");
            $("#btnSaveChannel").addClass("disabled");

            $.ajax({
                url: "<?= Url::to(['admin/checkchannelavailability']) ?>",
                data: {typingChannel: typingText},
                type: 'GET',
                success: function (data, textStatus, jqXHR) {
                    if (data == "1") {
                        // channel is available
                        $("#channelValidationMessage").html("Channel is available");
                        $("#channelValidationMessage").css("color", "green");
                        $("#btnSaveChannel").removeClass("disabled");
                        isChannelExists = false;
                    } else {
                        // channel is not available
                        $("#channelValidationMessage").html("Channel is not available");
                        $("#channelValidationMessage").css("color", "red");
                        $("#btnSaveChannel").addClass("disabled");
                        isChannelExists = true;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("channel validation error : " + jqXHR.responseText);
                    isChannelExists = true;
                }
            });
        } else {
            // typing text is empty
            $("#channelValidationMessage").html("");
            $("#channelValidationMessage").css("color", "red");
            $("#btnSaveChannel").addClass("disabled");
        }
});

<?php } ?>


</script>