<!DOCTYPE html>

<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<script>
    function checkEndlessloopAjax(){
        $.ajax({
            url: "<?=Url::to(['asterisks/check_endless_loop_execution'])?>",
            type: "GET",
            success: function(dataObj){
                var dateObj = new Date();
                var isoConvertedDateString = dateObj.toISOString();
                if(dataObj == "1"){
                    // Loop invoke called
                    $("#responseMessagesDisplayDiv").html("Routine invoke called - "+isoConvertedDateString);
                    $("#responseSuccessMessagesDisplayDiv").append("Routine invoke called - "+isoConvertedDateString+" <br> <hr>");
                }else{
                    // loop did not invoked
                    $("#responseMessagesDisplayDiv").html("Routine did not invoke - "+isoConvertedDateString);
                }
            },
            error:  function(errorObj){
                $("#responseMessagesDisplayDiv").html("Error occured - "+errorObj.responseText);
                $("#responseerrorsDisplayDiv").append("Error occured - "+errorObj.responseText+" <br> <hr>");
            }
        });
    }

    $(document).ready(function(){
        checkEndlessloopAjax();

        setInterval(function(){ 
            checkEndlessloopAjax();
         }, 30000);
    });
</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12" style="text-align: center">
            <label>PBX call events monitor<label>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12" style="text-align: left">
            <div class="well well-lg" id="responseMessagesDisplayDiv"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="well well-md" id="responseSuccessMessagesDisplayDiv"></div>
        </div>
        <div class="col-md-6">
            <div class="well well-md" id="responseerrorsDisplayDiv"></div>
        </div>
    </div>
</div>
