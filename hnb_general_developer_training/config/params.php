<?php

return [
    'adminEmail' => 'admin@example.com',
    'fullreport_maxVisibleButttons' => 10,
    'fullreport_records' => 10,
];
