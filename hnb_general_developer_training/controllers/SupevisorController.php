<?php

/*
* This controller handle all supevisor wise controls
* @author: Vikum
* @date: 27/06/2017
* 
*/

namespace app\controllers;

//defined('BASEPATH') OR exit('No direct script access allowed');


use Yii;
use yii\db\ActiveRecord;
use yii\web\Controller;
use app\models\Customer;
use app\models\call_center_user;
use app\models\Agent_requests;
use app\models\agent_notifications;
use app\models\web_presence;
use app\models\Miss_calls_email_log;
use app\models\cdr;
use app\models\Call_records;
use app\models\Contact_list;
use app\models\Logged_in_users;
use app\models\Ext_queue_intermediate;
use app\models\Extension_queue;
use app\models\Allowed_exec_extqueues;
use app\models\Call_channel;
use app\models\Extqueue_callchannel_intermediate;

class SupevisorController extends Controller {

/**
 * <b>Overrides parent class controller constructor</b>
 * 
 * @param type $id
 * @param type $module
 * @param type $config
 * 
 * @author Sandun
 * @since 2017-09-25
 */
public function __construct($id, $module, $config = array()) {
    parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        } else if (Yii::$app->session->get('user_role') == '2' || Yii::$app->session->get('user_role') == '4') {
//            // user is and agent or super agent
//            $this->redirect('index.php?r=user/login_view');
//        }
}

//    public function __construct() {
//
//        parent::__construct();
//    }

public function actionOverview() {
    $session = Yii::$app->session;
    if (!$session->has('user_id')) {
        $this->redirect('index.php?r=user/login_view');
    } else {
        $user_role = $session->get('user_role');
        if ($user_role == '1' || $user_role == '3' || $user_role == '5') {
            $query_customers = Customer::find();
            $liveCalls = $query_customers->all();
            $webphoneParams = require(__DIR__ . '/../config/webphone.php');
            $webphoneParams['username'] = Yii::$app->session->get('voip');
            $webphoneParams['phoneState'] = call_center_user::getAgentWebphoneState($session->get('user_id'));
            $contacts = $this->allcontacts();

            $consumedAgentRequest = Agent_requests::getConsumedAgentRequestsBySupervisor();
            $requestData = array();
            $i = 0;
            foreach ($consumedAgentRequest as $key) {
                $agentInfoRequest = call_center_user::getUser($key['agent_id']);
//                $type = $key['request_type'];
                $tempAgentData = array(
                    'type' => $key['request_type'],
                    'id' => $key['id'],
                    'name' => $agentInfoRequest[0]['name'],
                );
                array_push($requestData, $tempAgentData);
//                    $requestData[$i]['type'] = $key['request_type'];
//                    $requestData[$i]['id'] = $key['id'];
//                    $requestData[$i]['name'] = ;
//                    $requestData[$i]['time'] = $key['approved_time_period'];
                $i++;
            }
            return $this->render('coordinatorOverview', ['liveCalls' => $liveCalls, 'webphoneParams' => $webphoneParams, 'contacts' => $contacts, 'agentRequests' => $requestData]);
        } else {
            $this->redirect('index.php?r=user/login_view');
        }
    }
}

public function actionNotifyagent() {
    $notify_type = $_POST['dial_type'];
    $agent_id = $_POST['agent_id'];

    $session = Yii::$app->session;
    $supervisor_id = $session->get('user_id');

    $time = date("Y-m-d H:i:s");
    $notif_status = 'active';

    $agent_notif = new agent_notifications();
    $agent_notif->agent_id = $agent_id;
    $agent_notif->supervisor_id = $supervisor_id;
    $agent_notif->notif_type = $notify_type;
    $agent_notif->notif_status = $notif_status;
    $agent_notif->notif_time = $time;
    $insert = $agent_notif->insert();

    echo $insert;
}

public function actionGetuser() {
    $customerId = $_POST['q'];
    $customerInfo = Customer::getCustomerInformation($customerId);
    $fullDate = $customerInfo[0]['dob'];
    $pieces = explode(" ", $fullDate);
    // piece1
    $customerData = array(
        'customer_name' => $customerInfo[0]['customer_name'],
        'email' => $customerInfo[0]['email'],
        'contact' => $customerInfo[0]['contact_number'],
        'address' => $customerInfo[0]['address'],
        'dob' => $pieces[0],
        'gender' => $customerInfo[0]['gender'],
    );


    echo json_encode($customerData);
}

// This function loads Agent data according to the supervisor rank
public function actionManage() {
    return $this->render('manageUsers');
}

public function actionManagetable() {
    $session = Yii::$app->session;
    $user_role = $session->get('user_role');
    if (!$session->has('user_id') || $user_role == '2' || $user_role == '4') {
//            echo $user_role . ' ++++';
//            echo $user_role.' ';            
        $this->redirect('index.php?r=user/login_view');
    } else {

        $userInfo = call_center_user::getUserData($user_role);
        $liveAgents = Logged_in_users::getLoggedAgents();
//            print_r($userInfo);
        $result = array();
        for ($i = 0; $i < count($userInfo); $i++) {
//                echo 'Hello '.$userInfo[$i]['name'];
            $result[$i]['id'] = $userInfo[$i]['id'];
            $result[$i]['name'] = $userInfo[$i]['name'];
            $result[$i]['fullname'] = $userInfo[$i]['fullname'];
            $result[$i]['voip'] = $userInfo[$i]['voip_extension'];
            $result[$i]['role_id'] = $userInfo[$i]['role_id'];
            $result[$i]['status'] = $userInfo[$i]['status'];
            $result[$i]['webphone'] = $userInfo[$i]['webphone'];
        }
        $onlineUsers = array();
        if (count($liveAgents) > 0) {
            foreach ($liveAgents as $key) {
                array_push($onlineUsers, $key['user_id']);
            }
        }


        return $this->render('manageUsersTable', ['user_data' => $result, 'liveAgents' => $onlineUsers]);
    }
//        return $this->render('manageUsersTable');
}

public function actionInsert() {
    $this->checksession();
    $user = 'null';
    $userInfo = array();
    $availableExtensions = $this->getAvailableVOIPExtensions();
    //to get all queues
    $availableQueus = Extension_queue::getAllAvailableQueuesData();
    if (isset($_GET['user'])) {
        $id = $_GET['user'];
        $allowedQueues = Extension_queue::getAllAvailableQueuesDataOfSelectedExecutive($id);
        $QueueOfSupervisor = Extension_queue::getExtensionQueueIdOfTheSupervisor($id);
        if (Yii::$app->session->get("user_role") != "1") {
            // not admin
            if (Yii::$app->session->get("user_role") == "3") {
                // is a supervisor
                $agentIdsOfQueue = call_center_user::getAgentIdsFromQueueId(Yii::$app->session->get("extQueueId"));
                if (in_array($id, $agentIdsOfQueue) == FALSE) {
                    // the agent details not belong to the viewing person, redirects to manage users page
                    return $this->redirect("index.php?r=supevisor/manage");
                }
            } else {
                // is not a supervisor
                return $this->redirect("index.php?r=user/login_view");
            }
        }
        $userInfo = call_center_user::getUser($id);
        $result = array(
            'id' => $userInfo[0]['id'],
            'name' => $userInfo[0]['name'],
            'fullname' => $userInfo[0]['fullname'],
            'role_id' => $userInfo[0]['role_id'],
            'extension' => $userInfo[0]['voip_extension'],
            'user_profile_pic' => $userInfo[0]['user_profile_pic']
        );

//            if (count($availableExtensions) > 0) {
        array_push($availableExtensions, $userInfo[0]['voip_extension']);
        sort($availableExtensions);
//            }else{
//                $availableExtensions = array();
//            }

        return $this->render('addNewUser', ['user_data' => $result, 'availableExtensions' => $availableExtensions, 'availableQueus' => $availableQueus,
            'allowedQueues' => $allowedQueues, 'QueueOfSupervisor' => $QueueOfSupervisor ]);
    } else {
        return $this->render('addNewUser', ['user_data' => array('id' => '', 'name' => '', 'fullname' => '', 'role_id' => '', 'extension' => '', 'user_profile_pic' => ''), 'availableExtensions' => $availableExtensions, 'availableQueus' => $availableQueus, 'allowedQueues' => [],'QueueOfSupervisor' => []]);
    }
//        return $this->render('addNewUser');
}

/**
 * <b>Get all available extension numbers</b>
 * <p>This function returns all the available extension numbers sorted by assending order.
 * If no available extensions, will return 0 </p>
 * 
 * @return array available VOIP Extension numbers / 0
 * @since 2017-08-04
 * @author Sandun
 */
private function getAvailableVOIPExtensions() {
//    public function actionGetavailableextensions() {
    $allExtensions = web_presence::getAllVoipExtensions();
    $takenExtensions = call_center_user::getAllAssignedVOIPExtensions();

    $availableExtentions = array_diff($allExtensions, $takenExtensions);
    if (count($availableExtentions) > 0) {
        sort($availableExtentions);
        return $availableExtentions;
    } else {
        return array();
    }
}

private function checksession() {
    $session = Yii::$app->session;
    if (!$session->has('user_id')) {
        $this->redirect('index.php?r=user/login_view');
    } else {
        return true;
    }
}

public function actionAdduser() {
    $userId = $_POST['editUserId'];
    $recordType = 'save';
    if ($userId != '') {
        $recordType = 'update';
    }

    $data = array(
        'recordState' => 0, // 1 for password changed, 2 for profile pic changed, 3 for both changed, 0 both not changed
        'name' => $_POST['txtUserName'],
        'userId' => $userId,
        'role_id' => $_POST['selectUserRole'],
        'fullname' => $_POST['txtName'],
        'extension' => $_POST['selectVoipExtension'],
        'status' => 'active',
        'created_date' => date('Y-m-d H:i:s')
    );


    $password = $_POST['txtPassword'];
    if ($password != '') {
        $hashedPassword = md5($password);
        $data['hashedPassword'] = $hashedPassword;
        $data['recordState'] = 1;
    }
    $picChanged = $_POST['picChanged'];
    if ($picChanged == '1') {
        $data['user_profile_pic'] = $this->do_upload();
        if ($data['recordState'] == 1) {
            $data['recordState'] = 3;
        } else if ($data['recordState'] == 0) {
            $data['recordState'] = 2;
        }
    }

    $result = call_center_user::saveCallCenterUser($data, $recordType);

    if ($result) {
        if ($recordType == 'save') {
            if (isset($_POST['selectQueues'])) {
                foreach ($_POST['selectQueues'] as $selectedQueue) {
                    $dataQueue = array(
                        'user_id' => $result,
                        'allowed_ext_queue_id' => $selectedQueue,
                        'granted_by' => Yii::$app->session->get("user_id")
                    );
                    Allowed_exec_extqueues::addNewAllowedQueuesOfExecutive($dataQueue);
                }
            }
        } else {
            if (isset($_POST['selectQueues'])) {
                Allowed_exec_extqueues::deleteAllowedQueuesOfExecutive($userId);
                foreach ($_POST['selectQueues'] as $selectedQueue) {
                    $dataQueue = array(
                        'user_id' => $userId,
                        'allowed_ext_queue_id' => $selectedQueue,
                        'granted_by' => Yii::$app->session->get("user_id")
                    );
                    Allowed_exec_extqueues::addNewAllowedQueuesOfExecutive($dataQueue);
                }
            }
        }
        echo 1;
    } else {
        echo 0;
    }
}

private function do_upload() {
    $type = explode('.', $_FILES["imgUserProfile"]["name"]);
    $type = strtolower($type[count($type) - 1]);
    $url = "hnb_images/user_profile_pics/" . uniqid(rand()) . '.' . $type;
    $thumbUrl = "hnb_images/user_profile_pics/thumbs/" . uniqid(rand()) . '.' . $type;
    if (in_array($type, array("jpg", "jpeg", "gif", "png")))
        if (is_uploaded_file($_FILES["imgUserProfile"]["tmp_name"])) {
            if (move_uploaded_file($_FILES["imgUserProfile"]["tmp_name"], $url)) {
                $this->generate_image_thumbnail($url, $thumbUrl);
                return $url;
            }
        }
        return "";
    }

/*
 * PHP function to resize an image maintaining aspect ratio
 *
 * Creates a resized (e.g. thumbnail, small, medium, large)
 * version of an image file and saves it as another file
 */

public function generate_image_thumbnail($source_image_path, $thumbnail_image_path) {

    $thumb_max_height = 250;
    $thumb_max_width = 250;

    list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
    switch ($source_image_type) {
        case IMAGETYPE_GIF:
        $source_gd_image = imagecreatefromgif($source_image_path);
        break;
        case IMAGETYPE_JPEG:
        $source_gd_image = imagecreatefromjpeg($source_image_path);
        break;
        case IMAGETYPE_PNG:
        $source_gd_image = imagecreatefrompng($source_image_path);
        break;
    }
    if ($source_gd_image === false) {
        return false;
    }
    $source_aspect_ratio = $source_image_width / $source_image_height;
    $thumbnail_aspect_ratio = $thumb_max_width / $thumb_max_height;
    if ($source_image_width <= $thumb_max_width && $source_image_height <= $thumb_max_height) {
        $thumbnail_image_width = $source_image_width;
        $thumbnail_image_height = $source_image_height;
    } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
        $thumbnail_image_width = (int) ($thumb_max_height * $source_aspect_ratio);
        $thumbnail_image_height = $thumb_max_height;
    } else {
        $thumbnail_image_width = $thumb_max_width;
        $thumbnail_image_height = (int) ($thumb_max_width / $source_aspect_ratio);
    }
    $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
    imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
    imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
    imagedestroy($source_gd_image);
    imagedestroy($thumbnail_gd_image);
    return true;
}

/*
 * This function will response agent requests
 * @author: Vikum
 * @since: 19/07/2017
 *     
 */

public function actionAgentresponse() {
    $requestId = $_POST['q'];
    $response = $_POST['res'];
    $approveTime = $_POST['time'];
    $supervisorId = Yii::$app->session->get("user_id");
    $requestType = Agent_requests::getAgentRequest($requestId);
    if ($requestType == 'lunch') {
        $approveTime = '45';
    } else if ($requestType == 'short') {
        $approveTime = '5';
    } else if ($requestType == 'other') {
        $approveTime = '15';
    }
    $data = array(
        'responded_time' => date("Y-m-d H:i:s"),
        'approved_time_period' => $approveTime,
        'request_status' => $response,
        'supervisor_id' => $supervisorId,
        'consumed' => 0
    );
    $requestResponce = Agent_requests::setAgentRequestResponse($data, $requestId);
    if ($requestResponce) {
        return 1;
    } else {
        return 0;
    }
}

public function actionSend_misscall_email() {
    $lastRecord = Miss_calls_email_log::getLastEmailTimestamp();
    $lastTimestamp = $lastRecord->timestamp;
    $cdrCallRecords = cdr::getMissedCalls($lastTimestamp);
    $cdrCalls = array();
    foreach ($cdrCallRecords as $record) {
        $val = $record['uniqueid'];
        array_push($cdrCalls, "$val");
    }
    $callRecordsCalls = Call_records::selectLatestCallNumbers($lastTimestamp);
    $callRecordMissCalls = array();
    foreach ($callRecordsCalls as $callRecordsCall) {
        $val = $callRecordsCall['cdr_unique_id'];
        array_push($callRecordMissCalls, "$val");
    }
    $result = array_diff($cdrCalls, $callRecordMissCalls);
    $missedRecords = cdr::getCallsFromSelectedList($result);
    // Start email section

    if (count($missedRecords) > 0) {
        $adminEmail = call_center_user::getSupervisorEmail();
        $lastTime = gmdate("Y-m-d H:i:s", $lastTimestamp); // Last email log time
        $currentTime = date("Y-m-d H:i:s"); // Current email log time
        $body = "Hi <br> "
        . "This email contains missed calls reported from $lastTime to $currentTime <br>";
        for ($i = 0; $i < count($missedRecords); $i++) {
            $body = $body . $missedRecords[$i]['src'] . "<br>";
        }
        $timezone = "Asia/Colombo";
        date_default_timezone_set($timezone);
        $body = $body . " Regards,<br>";
        $dataSet = array(
            'sentTo' => $adminEmail,
            'start_time' => $lastTime,
            'end_time' => $currentTime,
            'body' => $body
        );
        $this->sendMisscallEmail($dataSet);
//            print_r($dataSet);
    }
    // End email section
//        print_r($missedRecords);
}

private function sendMisscallEmail($data) {
    $timeRangeStart = $data['start_time'];
    $timeRangeEnd = $data['end_time'];
    Yii::$app->mailer->compose("@app/mail/layouts/html", ['contactForm' => $data['body']])
    ->setFrom('HNB General Insuerence Call Center')
    ->setTo($data['sentTo'])
    ->setTextBody($data['body'])
    ->setSubject("Call Center Application Miss Calls List within $timeRangeStart to $timeRangeEnd")
    ->send();
    return true;
}

/**
 * 
 * @return type
 * 
 * @modified Sandun 2017-12-20
 * @description Passes available extension queues information to the view
 */
public function actionDashboard() {
    $extensionQueuesInformation = Extension_queue::getAllAvailableQueuesData();
    return $this->render('dashboradView', ['extensionQueues' => $extensionQueuesInformation]);
}

public function actionSavecontact() {
    $phone_number = $_POST['contact_number'];
    $contact_name = $_POST['contact_name'];
    $data = array(
        'number' => $phone_number,
        'name' => $contact_name,
        'created_date_time' => date("Y-m-d H:i:s")
    );
    $contactRecord = Contact_list::saveContact($data);
    if ($contactRecord) {
        echo 1;
    } else {
        echo 0;
    }
}

private function allcontacts() {

    $contactRecord = Contact_list::loadAllContact();
    $returnData = array();
    if ($contactRecord) {
        foreach ($contactRecord as $key) {
            $val = array(
                'id' => $key['id'],
                'number' => $key['contact_number'],
                'name' => $key['contact_name']
            );
            array_push($returnData, $val);
        }

        return json_encode($returnData);
    } else {
        return 0;
    }
}

public function actionRefreshcontacts() {
    echo $this->allcontacts();
}

public function actionSearchcontact() {
    $search = $_POST['q'];
    if ($search != '') {
        $contactRecord = Contact_list::searchContact($search);
    } else {
        $contactRecord = Contact_list::loadAllContact();
    }
    if ($contactRecord) {
        $returnData = array();
        foreach ($contactRecord as $key) {
            $val = array(
                'id' => $key['id'],
                'number' => $key['contact_number'],
                'name' => $key['contact_name']
            );
            array_push($returnData, $val);
        }
        echo json_encode($returnData);
    } else {
        echo 0;
    }
}

public function actionDelete_contact() {
    $id = $_POST['id'];
    $deleted = Contact_list::deleteContact($id);
    if ($deleted) {
        echo 1;
    } else {
        echo 0;
    }
}

public function actionPromoteagent() {
    $userId = $_GET['userId'];
    return call_center_user::setUserRole($userId, 4);
}

public function actionDemotesuperagent() {
    $userId = $_GET['userId'];
    return call_center_user::setUserRole($userId, 2);
}

public function actionLogoutuser() {
    $userId = $_POST['userId'];
    $updateUser = Logged_in_users::setOldRecordInactive($userId);
    if ($updateUser) {
        echo 1;
    } else {
        echo 0;
    }
}

public function actionChangeagentwebphonestate() {
    $agentId = $_GET['agentId'];
    $webphoneState = $_GET['state'];

    if (call_center_user::setAgentWebphoneState($agentId, $webphoneState)) {
        echo 1;
    } else {
        echo 0;
    }
}

/**
 * <b>Shows the new extensions queue create page</b>
 * <p>This function shows the new extensions create page, only if the viewing user is an admin. Othervise returns to login page.
 * When loading the page, gets all unassigned supervisors and unassigned extensions to any of the queues.</p>
 * 
 * @return addNewExtensionQueue page
 * 
 * @author Sandun
 * @since 2017-12-148
 *
 * @modified Vinothan
 * @since 2018-12-15
 * added modification to make admin to update queues of supervisors 
 *
 */
public function actionQueuecreate() {
    $this->checksession();
    $queue = 'null';
    $queueInfo = array();

    if (Yii::$app->session->get("user_role") != 1) {
        // user is not admin
        $this->redirect('index.php?r=user/login_view');
    }

    $unasignedExtensions = $this->getAllUnassignedExtensions();
    sort($unasignedExtensions);
    $unassignedSupervisors = $this->getAllUnassignedSupervisors();

    if (isset($_GET['queue'])) {
        $id = $_GET['queue'];
        $queueData = call_center_user::getSupervisorOfQueue($id);
        $assignedExtensions = Ext_queue_intermediate::getAllExtensionsOfTheQueue($id);

        $allExtensions = array();
        foreach ($assignedExtensions as $item) {
            array_push($allExtensions, "$item");
        }

        for ($i = 0; $i < count($unasignedExtensions); $i++) {
            array_push($allExtensions, $unasignedExtensions[$i]);
        }

        sort($allExtensions);

        $result = array();
        for ($i = 0; $i < count($queueData); $i++) {
            $result[$i]['id'] = $queueData[$i]['id'];
            $result[$i]['fullname'] = $queueData[$i]['fullname'];
            $result[$i]['name'] = $queueData[$i]['name'];
            $result[$i]['voip'] = $queueData[$i]['voip_extension'];
            $result[$i]['status'] = $queueData[$i]['status'];
        }

        return $this->render("addNewExtensionQueue", ['allExtensions' => $allExtensions, 'assignedExtensions' => $assignedExtensions, 'queue_data' => $result, 'unasignedExtensions' => $unasignedExtensions, 'unassignedSupervisors' => $unassignedSupervisors]);
    }


    return $this->render("addNewExtensionQueue", ['unasignedExtensions' => $unasignedExtensions, 'unassignedSupervisors' => $unassignedSupervisors]);
}

/**
 * <b>Get all unassigned extensions</b>
 * <p>This private function gets all unassigned extensions to any of the queues available</p>
 * 
 * @return array unassigned extensions / empty array
 * 
 * @author Sandun
 * @since 2017-12-14
 */
private function getAllUnassignedExtensions() {
    $allExtensions = web_presence::getAllVoipExtensions();
    $QueueAssignedExtensions = Ext_queue_intermediate::getAllVoipExtensions();
    $unassignedExtensions = array();
    for ($x = 0; $x < count($allExtensions); $x++) {
        $found = FALSE;
        for ($y = 0; $y < count($QueueAssignedExtensions); $y++) {
            if ($allExtensions[$x] == $QueueAssignedExtensions[$y]['voip_extension']) {
                // match found
                $found = TRUE;
                break;
            }
        }

        if ($found == FALSE) {
            array_push($unassignedExtensions, $allExtensions[$x]);
        }
    }
    return $unassignedExtensions;
}

/**
 * <b>Get all unassigned supervisors</b>
 * <p>This private function gets all unassigned supervisors to any of the available queues.</p>
 * 
 * @return array unassigned supervisors / empty array
 * 
 * @author Sandun
 * @since 2017-12-14
 * 
 */
private function getAllUnassignedSupervisors() {
    $allRegisteredSupervisors = call_center_user::getAllSupervisorIdsAndNames();
    $allAssignedSupervisorIds = Extension_queue::getAllAssignedSupervisorIds();
    $unassignedSupervisors = array();

    foreach ($allRegisteredSupervisors as $key) {
        $found = FALSE;
        foreach ($allAssignedSupervisorIds as $value) {
            if ($key->id == $value->supervisor_id) {
                $found = TRUE;
            }
        }

        if ($found == FALSE) {
            // not an assigned supervisor
            $unassignedSupervisorInformation = array(
                "id" => $key->id,
                "name" => $key->name,
            );
            array_push($unassignedSupervisors, $unassignedSupervisorInformation);
        }
    }

    return $unassignedSupervisors;
}

/**
 * <b>Adds new extension queue to the system</b>
 * <p>This function adds new extension queue to the system.
 * First the supervisor id and queue name gets inserted to the extentions queue table and gets the queue id.
 * Then records the VOIP extensions in the Ext_queue_intermediate table with the queue id </p>
 * 
 * @author Sandun
 * @since 2017-12-14
 *
 * @modified Vinothan
 * @since 2018-07-16
 * <b>Update the exisiting queue information to system</b>
 * <p>This will update the queuename,supervisor in the extension queue table and voip extentions in the 
 * Ext_queue_intermediate table 
 */
public function actionAddnewextensionqueue() {

    $editQueueId  = $_POST['editQueueId'];

    if (Yii::$app->session->get("user_role") != "1") {
        // not admin
        return $this->redirect('index.php?r=user/login_view');
    }

    if ($editQueueId == "") { // insert operation           
        $queueName = $_POST['name'];
        $selectedExtensionsCsvString = $_POST['selectedExtensions'];
        $selectedSupervisorId = $_POST['selectedSupervisor'];

        $extensionsArray = explode(",", $selectedExtensionsCsvString);

        $transaction = Yii::$app->db->beginTransaction();
        
        try{
            $newQueueId = Extension_queue::insertNewExtensionQueue($queueName, $selectedSupervisorId);
            Ext_queue_intermediate::insertExtensionsOfNewQueue($newQueueId, $extensionsArray);
            $transaction->commit();
        }catch (Exception $e) {

            $transaction->rollBack();
        }

    }else{ //update operation
        $queueName = $_POST['name'];
        $selectedExtensionsCsvString = $_POST['selectedExtensions'];
        $selectedSupervisorId = $_POST['selectedSupervisor'];
        $extensionsArray = explode(",", $selectedExtensionsCsvString);

        $transaction = Yii::$app->db->beginTransaction();
        
        try{
        // Change of Queue Name
            $newQueue = Extension_queue::findOne($editQueueId);
            $newQueue->name = $queueName;
            $newQueue->update();

        //Change of supervisor or/and Extensions
            if($selectedSupervisorId != ""){
                $newSupervisor = Extension_queue::findOne($editQueueId);
                $newSupervisor->supervisor_id = $selectedSupervisorId;
                $newSupervisor->update();
            }        

        //Deleting existing queues
            $allQueuesToDelete = Ext_queue_intermediate::find()->where("queueid = $editQueueId")->all();

            var_dump($allQueuesToDelete);
            foreach ($allQueuesToDelete as $model) {
                $model->delete();
            }
            Ext_queue_intermediate::insertExtensionsOfNewQueue($editQueueId, $extensionsArray);
            $transaction->commit();
        }catch (Exception $e) {

            $transaction->rollBack();
        }
    }
    
}

/**
 * 
 * @author Sandun
 * @since 2017-12-21
 */
public function actionAddnewextensionajax() {
    if (Yii::$app->session->get("user_role") != "1") {
        // not admin
        return $this->redirect('index.php?r=user/login_view');
    }
    $newExtension = Yii::$app->request->post("extension");
    echo web_presence::insertNewExtensionNumber($newExtension);
}

/**
 * 
 * @return type
 * 
 * @since 2017-12-21
 * @author Sandun
 */
public function actionAddextension() {
    if (Yii::$app->session->get("user_role") != "1") {
        // not admin
        return $this->redirect('index.php?r=user/login_view');
    } else {
        return $this->render("addNewExtension");
    }
}

/**
 * <b>Checks if the typing username is available or not</b>
 * <p></p>
 * @since 2018-1-3
 * @author Sandun
 */
public function actionCheckusernameavailability() {
    $typingUsername = Yii::$app->request->get("typingUsername");
    if (call_center_user::isUsernameAvailable($typingUsername) == TRUE) {
        echo 1;
    } else {
        echo 0;
    }
}

/**
 * <b>Allows admin to manage extension queues</b>
 * <p></p>
 * @since 2018-8-15
 * @author Vinothan
 */
public function actionManagequeues() {
    return $this->render('manageExtensionQueues');
}

public function actionManagetableofqueues() {
    $session = Yii::$app->session;
    $user_role = $session->get('user_role');
    if (!$session->has('user_id') || $user_role == '2' || $user_role == '4') {
        $this->redirect('index.php?r=user/login_view');
    } else {
        $allUsersWithQueus = call_center_user::getAllSupervisorsWithQueues();

        $result = array();
        for ($i = 0; $i < count($allUsersWithQueus); $i++) {
            $result[$i]['id'] = $allUsersWithQueus[$i]['id'];
            $result[$i]['fullname'] = $allUsersWithQueus[$i]['fullname'];
            $result[$i]['name'] = $allUsersWithQueus[$i]['name'];
            $result[$i]['voip'] = $allUsersWithQueus[$i]['voip_extension'];
            $result[$i]['status'] = $allUsersWithQueus[$i]['status'];
        }

        return $this->render('manageExtensionQueuesTable', ['user_data' => $result]);
    }
}

/**
 * <b>Allows admin to delete extension queues</b>
 * <p></p>
 * @since 2018-8-15
 * @author Vinothan
 * @description Allows admin to delete queues in frontend which changes the queue status to INACTIVE. 
 */
public function actionDeletequeue() {
    if (!Yii::$app->session->has('user_id')) {
        $this->redirect('index.php?r=user/login_view');
    }
    $queueId = Yii::$app->request->get("qId");
    if ($queueId != NULL) {
        if ($this->updateQueueStatus($queueId) == TRUE) {
            echo 1;
        } else {
            echo 0;
        }
    }
}

private function updateQueueStatus($queueId) {
    $queue = Extension_queue::findOne($queueId);
    $queue->status = "Inactive";
    if ($queue->update() != false) {
        return TRUE;
    } else {
        return FALSE;
    }
}



/**
 * <b>Allows admin to manage channels and its descriptions</b>
 * <p></p>
 * @since 2018-8-17
 * @author Vinothan
 */
public function actionManagechannels() {
    return $this->render('manageChannels');
}

public function actionManagetableofchannels() {
    $session = Yii::$app->session;
    $user_role = $session->get('user_role');
    if (!$session->has('user_id') || $user_role == '2' || $user_role == '4') {
        $this->redirect('index.php?r=user/login_view');
    } else {

        $allChannels = Call_channel::find()->all();

        $result = array();
        for ($i = 0; $i < count($allChannels); $i++) {
            $result[$i]['id'] = $allChannels[$i]['id'];
            $result[$i]['channel_number'] = $allChannels[$i]['channel_number'];
            $result[$i]['channel_description'] = $allChannels[$i]['channel_description'];
            $result[$i]['channel_name'] = $allChannels[$i]['channel_name'];
        }

        return $this->render('manageChannelsTable', ['user_data' => $result]);
    }
}


/**
 * <b>Shows the new channels create page</b>
 * <p>This function shows the new channels create page, only if the viewing user is an admin. Othervise returns to login page.
 * 
 * @return add new channels
 * 
 * @author Vinothan
 * @since 2017-12-148
 *
 */
public function actionChannelcreate() {
    $this->checksession();
    $queue = 'null';
    $queueInfo = array();

    if (Yii::$app->session->get("user_role") != 1) {
        // user is not admin
        $this->redirect('index.php?r=user/login_view');
    }

    $extensionQueues = Extension_queue::find()->all();
    sort($extensionQueues);

    if (isset($_GET['channel'])) {
        $id = $_GET['channel'];

        $oneChannel = Call_channel::find()->where(['id'=>$id])->one();
        $existingQueue = Extqueue_callchannel_intermediate::find()->where(['channel_id'=>$oneChannel['id']])->one();
        $extensionQueue = Extension_queue::find()->where(['id'=>$existingQueue['extqueue_id']])->one();


        return $this->render("addNewChannel", ['extensionQueues' => $extensionQueues, 'oneChannel'=> $oneChannel, 'extensionQueue' => $extensionQueue]);
    }


    return $this->render("addNewChannel", ['extensionQueues' => $extensionQueues]);
}

/**
 * 
 * @author Vinothan
 * @since 2018-08-17
 */
public function actionAddnewchannelajax() {

    $editChannelId = $_POST['editChannelId'];
    if (Yii::$app->session->get("user_role") != "1") {
        // not admin
        return $this->redirect('index.php?r=user/login_view');
    }

    if ($editChannelId == "") { // inset operation
        $channelData = array(
            'channelNumber' => $_POST['channelNumber'],
            'channelName' => $_POST['channelName'],
            'channelDesc' => $_POST['channelDesc']
        );

        $queueData = array(
            'channelNumber' => $_POST['channelNumber'],
            'extensionQueue' => $_POST['extensionQueue'],
        );

        $transaction = Yii::$app->db->beginTransaction();
        
        try{

        $savedChannelId =  Call_channel::insertNewChannelInfo($channelData);

        $extqueue_callchannel_intermediate = new Extqueue_callchannel_intermediate();
        $extqueue_callchannel_intermediate->channel_id = $savedChannelId;
        $extqueue_callchannel_intermediate->extqueue_id = $_POST['extensionQueue'];
        $extqueue_callchannel_intermediate->save();
        
        $transaction->commit();
        }catch (Exception $e) {
            $transaction->rollBack();
        }

        echo 1;

    }else { // update operation

    // change of channel info
        $transaction = Yii::$app->db->beginTransaction();
        
        try{    

        $channel = Call_channel::findOne($editChannelId);
        $channel->channel_number = $_POST['channelNumber'];
        $channel->channel_name = $_POST['channelName'];
        $channel->channel_description = trim($_POST['channelDesc']);
        $channel->update();

        $extqueue = Extqueue_callchannel_intermediate::find()->where(['channel_id'=>$editChannelId])->one();
        $extqueue->extqueue_id= $_POST['extensionQueue'];
        $extqueue->update();
        $transaction->commit();
        }catch (Exception $e) {

            $transaction->rollBack();
        }
        echo 2;

    }

}

}
