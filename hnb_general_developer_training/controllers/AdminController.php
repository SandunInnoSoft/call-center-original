<?php

namespace app\controllers;

/**
 * This controller is to perform administrator related action
 *
 * @author Sandun
 * @since 2018-1-3
 */
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\web_presence;
use app\models\cdr;
use app\models\Call_channel;

class AdminController extends Controller {
    
    /**
     * 
     * @since 2018-1-3
     * @author Sandun
     */
    public function actionCheckextensionavailability(){
        $typingExtension = Yii::$app->request->get("typingExtension");
        if($typingExtension != NULL && web_presence::isExtensionAvailable($typingExtension) == TRUE){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    /**
     * 
     * @author Sandun
     * @since 2018-07-11
     */
    public function actionCdrnullrecords(){
        $dateFrom = Yii::$app->request->get("dateFrom");
        $dateTo = Yii::$app->request->get("dateTo");
        
        if($dateFrom != NULL && $dateTo != NULL){
            // show null records
            $dateFrom = trim($dateFrom);
            $dateTo = trim($dateTo);
            $dstNullRecords = cdr::getDstNullRecordsBetweenDateRange($dateFrom, $dateTo);
            if(count($dstNullRecords) == 0){
                // No null records
                echo "No destination extension is empty records found between $dateFrom 00:00:00 and $dateTo 23:59:59";
            }else{
                echo "<h5>".count($dstNullRecords)." of destination empty call records were found between $dateFrom 00:00:00 and $dateTo 23:59:59</h5>";
                echo '<br>';
                echo '<table>';
                echo "<thead>";
                    echo "<tr>";
                        echo "<th style='text-align: center'>Caller Number</th>";
                        echo "<th style='text-align: center'>Start Date and Time</th>";
                        echo "<th style='text-align: center'>End Date and Time</th>";
                        echo "<th style='text-align: center'>Duration (Seconds)</th>";
                        echo "<th style='text-align: center'>Recieved channel</th>";
                    echo "</tr>";
                echo "<thead>";

                echo "<tbody>";
                    for($x = 0; $x < count($dstNullRecords) ; $x++){
                        if($dstNullRecords[$x]['calleenum'] != NULL && $dstNullRecords[$x]['calleenum'] != ""){
                            $channelInfo = Call_channel::getChannelInfoFromChannelNumber($dstNullRecords[$x]['calleenum']);   
                        }else{
                            $channelInfo['channel_name'] = "-";
                        }
                        echo "<tr>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['src']."</td>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['start']."</td>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['end']."</td>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['duration']."</td>";
                            echo "<td style='text-align: center'>".$channelInfo['channel_name']."</td>";
                        echo "<tr>";
                    }
                echo "<tbody>";
                echo '</table>';
            }
        }else{
            echo "Invalid datetime period!";
        }
    }

    

    /**
     * 
     * @since 2018-08-17
     * @author Vinothan
     */
    public function actionCheckchannelavailability(){
        $typingChannel = Yii::$app->request->get("typingChannel");
        if($typingChannel != NULL && call_channel::isChannelAvailable($typingChannel) == TRUE){
            echo 1;
        }else{
            echo 0;
        }
    }

}
