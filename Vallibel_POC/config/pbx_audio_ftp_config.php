<?php

return [
    'serverIpAddress' => '10.100.21.238:5060', // Ip address of audio file ftp server
	'serverUrl'=>'https://10.100.21.238:9999/audioRecordingDirectoryScanner.php?ext=', // Path to get audio files 
    'ftp_path' => '/var/spool/asterisk/monitor/recording', // Path to audio file directory
    'username' => 'root', // FTP login user name
    'password' => 'Password123', // FTP login password
    'fileExtension' => 'wav', // audio file extension
    'audioFilePath' =>'https://10.100.21.238:9999/', // PBX audio file path url
    'NAS_audio_file_Location' =>'F:/Zycoo/<date>/monitor/recording/<extension>/', // Audio file save location in the NAS
    'NAS_audio_file_Location_part_2' =>'/monitor/recording/', // Audio file save location in the NAS after the date directory
    'JRE_HOME' =>'E:\\jre-10.0.2\\bin\\java.exe', // JRE exe file path
    'onDemandDownloaderJar' =>'E:\\xampp\\htdocs\\hnbGeneralCC\\web\\onDemandFetch\\SampleTestJarApp.jar', // On demand audio file downloader jar file path
];
