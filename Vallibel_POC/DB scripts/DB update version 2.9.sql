# HNB Call center DB update version 2.9
# @author Sandun
# @since 2017-09-27

-- This script creates a new table with 1 field to log the timestamp of the endless loop's each interation
CREATE TABLE `callevents`.`loop_monitor` (
`timestamp`  varchar(100) NULL DEFAULT NULL 
);

INSERT INTO `loop_monitor` (`timestamp`) VALUES ("1506507808");

