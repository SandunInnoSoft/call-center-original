
--
-- Database: `hnbpbx`
--

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `ticket_number` varchar(50) NOT NULL COMMENT 'this will be some prefix with pk',
  `source` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `customer_id` int(11) DEFAULT NULL,
  `priority` enum('high','mid','low') NOT NULL DEFAULT 'mid',
  `assignee` int(11) NOT NULL,
  `reporter` int(11) DEFAULT NULL,
  `description` varchar(500) NOT NULL,
  `ticket_status` enum('new','inprogress','closed') NOT NULL DEFAULT 'new',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);
