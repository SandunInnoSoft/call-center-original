--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `contact_m` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `id_number` varchar(20) DEFAULT NULL,
  `contact_o` varchar(50) DEFAULT NULL,
  `pref_language` enum('english','sinhala','tamil') DEFAULT 'sinhala',
  `acc_card_number` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);	


--
-- Alter Table `Call center user`
--  
  
ALTER TABLE `call_center_user` 
ADD COLUMN `Manager` INT(11) NULL DEFAULT NULL AFTER `created_user_id`;
  