ALTER TABLE `call_center_user` 
ADD COLUMN `status` ENUM('active', 'inactive', 'deleted') NULL AFTER `role_id`,
ADD COLUMN `created_date` DATETIME NULL AFTER `status`;

ALTER TABLE `call_center_user` 
CHANGE COLUMN `status` `status` ENUM('active','inactive','deleted') NULL DEFAULT 'active' ,
ADD COLUMN `user_profile_pic` VARCHAR(200) NULL AFTER `role_id`;
