package home;

/**
 * @author Vikum Samaranayake
 * @since 04/06/2018
 *
 */
public class ConfigData {
	private String hostPbxIp, hostPbxPort, hostPbxUname, hostPbxPword, hostPbxAudioDirectory, nasBackupDirectory, nasAudioOriginalDirectory;

	public String getHostPbxIp() {
		return hostPbxIp;
	}

	public void setHostPbxIp(String hostPbxIp) {
		this.hostPbxIp = hostPbxIp;
	}

	public String getHostPbxPort() {
		return hostPbxPort;
	}

	public void setHostPbxPort(String hostPbxPort) {
		this.hostPbxPort = hostPbxPort;
	}

	public String getHostPbxUname() {
		return hostPbxUname;
	}

	public void setHostPbxUname(String hostPbxUname) {
		this.hostPbxUname = hostPbxUname;
	}

	public String getHostPbxPword() {
		return hostPbxPword;
	}

	public void setHostPbxPword(String hostPbxPword) {
		this.hostPbxPword = hostPbxPword;
	}

	public String getHostPbxAudioDirectory() {
		return hostPbxAudioDirectory;
	}

	public void setHostPbxAudioDirectory(String hostPbxAudioDirectory) {
		this.hostPbxAudioDirectory = hostPbxAudioDirectory;
	}

	public String getNasBackupDirectory() {
		return nasBackupDirectory;
	}

	public void setNasBackupDirectory(String nasAudioDirectory) {
		this.nasBackupDirectory = nasAudioDirectory;
	}

	public String getNasAudioOriginalDirectory() {
		return nasAudioOriginalDirectory;
	}

	public void setNasAudioOriginalDirectory(String nasAudioOriginalDirectory) {
		this.nasAudioOriginalDirectory = nasAudioOriginalDirectory;
	}
	

}
