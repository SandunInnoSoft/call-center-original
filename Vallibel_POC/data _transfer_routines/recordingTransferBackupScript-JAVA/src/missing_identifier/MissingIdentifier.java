package missing_identifier;

import java.io.File;
import java.time.Duration;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.Session;

import home.ConfigData;
import home.ManageConfig;

public class MissingIdentifier {
	private String SFTP_HOST, SFTP_USER, SFTP_PASS, SFTP_WORKING_DIR, BACKUP_FILE_DIRECTORY, AUDIO_FILE_NAS_DIRECTORY;
	private ManageConfig configObj;
	private int SFTP_PORT;
	private ConfigData configData;
	private Session session;
	private Channel channel;

	public MissingIdentifier() {

		configObj = new ManageConfig();
		configData = configObj.getConfigData();

		BACKUP_FILE_DIRECTORY = configData.getNasBackupDirectory();
		AUDIO_FILE_NAS_DIRECTORY = configData.getNasAudioOriginalDirectory();

	}

	/**
	 * @param args
	 */
	public void test() {
		Set<String> ret = this.getYesterdayBackupFiles();
		Iterator iterator = ret.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		System.out.println("Size: " + ret.size());

		// Set<String> ret = this.getYesterdayBackupFiles();

	}

	public Set<String> getYesterdayBackupFiles() {
		Set<String> returnSet = new HashSet<String>();
		Set<String> ret = this.getAllDirectories(this.BACKUP_FILE_DIRECTORY);
		Iterator iterator = ret.iterator();
		while (iterator.hasNext()) {
			returnSet.addAll(this.getFilesFromDirectory(this.BACKUP_FILE_DIRECTORY + iterator.next().toString(),true));
		}

		return returnSet;

	}

	public Set<String> getYesterdayRoutineFiles() {
		Set<String> returnSet = new HashSet<String>();
		String directoryPath = this.AUDIO_FILE_NAS_DIRECTORY;
		
		Set<String> ret = this.getAllDirectories(this.AUDIO_FILE_NAS_DIRECTORY);
		Iterator iterator = ret.iterator();
		while (iterator.hasNext()) {
			returnSet.addAll(this.getFilesFromDirectory(this.BACKUP_FILE_DIRECTORY + iterator.next().toString(), false));
		}

		return returnSet;
	}

	private Set<String> getAllDirectories(String directory) {
		Set<String> returnSet = new HashSet<String>();
		File folder = new File(directory);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isDirectory()) {
				returnSet.add(listOfFiles[i].getName());
			}
		}
		return returnSet;
	}

	/**
	 * @param directory String
	 * @param checkYesterday boolean 
	 * 
	 */
	private Set<String> getFilesFromDirectory(String directory, boolean checkYesterday) {
		Set<String> returnSet = new HashSet<String>();
		File folder = new File(directory + "/");
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {

				if (checkYesterday) {
					// Check if file belongs yesterday
					if (this.isYesterday(listOfFiles[i].getName()))
						// File is yesterday
						returnSet.add(listOfFiles[i].getName());
				} else {					
					returnSet.add(listOfFiles[i].getName());
				}

			}
		}
		return returnSet;
	}

	/**
	 * @param fileName 
	 * 
	 */
	public boolean isYesterday(String fileName) {
		String[] audioFileNameSplitted = fileName.split("-");
		String dateArea = audioFileNameSplitted[0];

		int year = Integer.parseInt(dateArea.substring(0, 4));
		int month = Integer.parseInt(dateArea.substring(4, 6));
		int date = Integer.parseInt(dateArea.substring(6, 8));

		LocalDate yesterday = LocalDate.now().minusDays(1);
		LocalDate fileDate = LocalDate.of(year, month, date);

		if (Duration.between(yesterday.atStartOfDay(), fileDate.atStartOfDay()).toDays() == 0) {
			return true;
		} else {
			return false;
		}

	}

}
