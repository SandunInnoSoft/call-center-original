<?php

use yii\helpers\Url;
use yii\helpers\Html;

if ($ftpParams['fileExtension'] == "wav") {
    $audioType = "audio/wav";
} else if ($ftpParams['fileExtension'] == "gsm") {
    $audioType = "audio/x-gsm";
}

$chartLables = array();

$data_breaksFullData = $breakFullData;
$data_breaksFullData = json_encode($data_breaksFullData);

$data_breaksDailyData = $breakDailyData;
$data_breaksDailyData = json_encode($data_breaksDailyData);
$data_ticketsData = $ticketsData;
//print_r($ticketsData);
$data_ticketsData = json_encode($data_ticketsData);
// Calls Answered in given time Data
$callsAnsweredGivenPeriodAgent = 0;
$callsAnsweredGivenPeriodAgentPercentage = 0;
$callsAnsweredGivenPeriodOthers = 0;
$callsAnsweredGivenPeriodOthersPercentage = 0;
if ($callAnswerCounts[0] != null && $callAnswerCounts[1] != null) {
    
//    if($callAnswerCounts[0] == 0){
//        $callAnswerCounts[0] = 1;
//    }
//    
//    if($callAnswerCounts[1] == 0){
//       $callAnswerCounts[1] = 1; 
//    }
    
    if($agent_extension != '0'){
        // single agent
        $callsAnsweredGivenPeriodAgent = $callAnswerCounts[0];
        $callsAnsweredGivenPeriodOthers = $callAnswerCounts[1] - $callAnswerCounts[0];
        $callsAnsweredGivenPeriodAgentPercentage = ($callAnswerCounts[0] > 0 && $callAnswerCounts[1] > 0 ? ($callAnswerCounts[0] / $callAnswerCounts[1]) * 100 : 0);
        $callsAnsweredGivenPeriodOthersPercentage = ($callAnswerCounts[1] > 0 && $callAnswerCounts[0] > 0 ? (($callAnswerCounts[1] - $callAnswerCounts[0]) / $callAnswerCounts[1]) * 100 : 100);
    }else{
        // all agents
        $callsAnsweredGivenPeriodAgent = $callAnswerCounts[1];
        $callsAnsweredGivenPeriodOthers = $callAnswerCounts[0];
        $callsAnsweredGivenPeriodAgentPercentage = ($callAnswerCounts[0] > 0 && $callAnswerCounts[1] > 0 ? ($callAnswerCounts[0] / $callAnswerCounts[1]) * 100 : 0);
        $callsAnsweredGivenPeriodOthersPercentage = ($callAnswerCounts[1] > 0 && $callAnswerCounts[0] > 0 ? (($callAnswerCounts[1] - $callAnswerCounts[0]) / $callAnswerCounts[1]) * 100 : 100);
        
    }
//$totalCallsFigure
}

// =================================

$agent_answered_percnt = 0;
$others_answered_percnt = 0;
$others_answered_count = 0;
if ($all_answered_calls_count != 0) {
    if($agent_extension != "0"){
        // single agent
        $agent_answered_percnt = ($agent_answered_calls_count / $all_answered_calls_count) * 100;
        $others_answered_percnt = ($others_answered_count / $all_answered_calls_count) * 100;
        $others_answered_count = $all_answered_calls_count - $agent_answered_calls_count;
    }else{
        $others_answered_count = $todayMissedCallsCount;
    }
}

$agent_answered_percnt = bcadd($agent_answered_percnt, '0', 2);
$others_answered_percnt = bcadd($others_answered_percnt, '0', 2);
// ====================================================================
$dailyBreakPercentage = 0;
$dailyWorkPercentage = 0;
$dailyBreakTime = '00 hours 00 minutes';
$dailyWorkTime = '00 hours 00 minutes';
//pendingTicketsPercentage
$pendingTicketsPercentageVal = 0;
if ($ticketsData[0] != 0 ){
    $pendingTicketsPercentageVal = ($ticketsData[0] / ($ticketsData[0] + $ticketsData[1] + $ticketsData[2])) *100;
}
//openTicketsPercentage
$openTicketsPercentageVal = 0;
if ($ticketsData[1] != 0 ){
    $openTicketsPercentageVal = ($ticketsData[1] / ($ticketsData[0] + $ticketsData[1] + $ticketsData[2])) *100;
}
//closedTicketsPercentage
$closedTicketsPercentageVal = 0;
if ($ticketsData[2] != 0 ){
    $closedTicketsPercentageVal = ($ticketsData[2] / ($ticketsData[0] + $ticketsData[1] + $ticketsData[2])) *100;
}
if ($breakDailyData[0] != null) {
    $dailyBreakPercentage = ($breakDailyData[0] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
}
if ($breakDailyData[1] != null) {
    $dailyWorkPercentage = ($breakDailyData[1] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
}
if ($breakDailyData[2] != null) {
    $dailyBreakTime = $breakDailyData[2];
}
if ($breakDailyData[3] != null) {
    $dailyWorkTime = $breakDailyData[3];
}

// Break and work times of the given period
$timePeriodBreakPercentage = 0;
$timePeriodWorkPercentage = 0;
$timePeriodBreakValue = '00 hours 00 minutes';
$timePeriodWorkValue = '00 hours 00 minutes';
if ($breakFullData[0] != null) {
    $timePeriodBreakPercentage = ($breakFullData[0] / ($breakFullData[0] + $breakFullData[1])) * 100;
}
if ($breakFullData[1] != null) {
    $timePeriodWorkPercentage = ($breakFullData[1] / ($breakFullData[0] + $breakFullData[1])) * 100;
}
if ($breakFullData[2] != null) {
    $timePeriodBreakValue = $breakFullData[2];
}
if ($breakFullData[3] != null) {
    $timePeriodWorkValue = $breakFullData[3];
}
?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
    .panel-heading{
        font-size: 90%;
    }
    thead{
        font-weight: bold;
    }
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }

    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;"  id="agentReportsDIV">
    <div class="row" style="padding-top: 0%">
        <?php
        if ($agent_extension != 0) {
            ?>
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2"><?= $agent_name ?> : <?= $agent_extension ?></div>
        <?php } else { ?>
            <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2">All agents</div>
        <?php } ?>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div><br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Break Reports</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
    <div class="tickets-status-chart">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-4" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Ticket Status
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>New</td>
                                                <td>Inprogress</td>
                                                <td>Closed</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($pendingTicketsPercentageVal, 2) ?>%</td>
                                                <td><?= round($openTicketsPercentageVal, 2) ?>%</td>
                                                <td><?= round($closedTicketsPercentageVal, 2) ?>%</td>
                                            </tr>
                                            <tr>
                                                <td><?=$ticketsData[0]  ?></td>
                                                <td><?= $ticketsData[1]  ?></td>
                                                <td><?= $ticketsData[2]  ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4" style="padding-bottom: 5px">
                    <canvas id="ticketsDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("ticketsDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["New", "Inprogress", "Closed"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_ticketsData ?>,
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)',
                                            'rgba(66, 169, 0, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Daily break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($dailyWorkPercentage, 2) ?>%</td>
                                                <td><?= round($dailyBreakPercentage, 2) ?>%</td>
                                            </tr>
                                            <tr>
                                                <td><?= $dailyWorkTime ?></td>
                                                <td><?= $dailyBreakTime ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_breaksDailyData ?>,
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($timePeriodWorkPercentage, 2) . ' %' ?></td>
                                                <td><?= round($timePeriodBreakPercentage, 2) . ' %' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $timePeriodWorkValue ?></td>
                                                <td><?= $timePeriodBreakValue ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: <?= $data_breaksFullData ?>,
                                        backgroundColor: [
                                            'rgba(244, 157, 0, 0.8)',
                                            'rgba(66, 139, 202, 0.8)'                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>                
                </div>            
            </div>
        </div>
    </div><br>

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Service Level Figures</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Today Answered Calls Result
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <?php if($agent_extension != "0"){ 
                                            // single agent
                                        $chartLables = array("Today Agent Answered Calls", "Today Others Answered calls");
                                        
                                        
                                    ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Others</td>
                                                    <td>Agent</td>
                                                </tr>                                            
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?= $others_answered_percnt . '%' ?></td>
                                                    <td><?= $agent_answered_percnt . '%' ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?= $others_answered_count . ' calls' ?></td>
                                                    <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                                </tr>
                                            </tbody>
                                        </table>                                                                        
                                    <?php } else { 
                                        // all agents
                                        $chartLables = array("Today All answered calls", "Today All missed calls");
                                        $totalCallsToday = $todayMissedCallsCount + $agent_answered_calls_count;
                                        $missedCallsPersentage = ($todayMissedCallsCount == 0 || $totalCallsToday == 0 ? 0 : $todayMissedCallsCount / $totalCallsToday * 100);
                                        $answeredCallsPersentage = ($agent_answered_calls_count == 0 || $totalCallsToday == 0 ? 0 : $agent_answered_calls_count / $totalCallsToday * 100);                                     
                                    ?>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <td>Missed</td>
                                                        <td>Answered</td>
                                                    </tr>                                            
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><?= round($missedCallsPersentage, 2) . '%' ?></td>
                                                        <td><?= round($answeredCallsPersentage, 2) . '%' ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><?= $others_answered_count . ' calls' ?></td>
                                                        <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>                                       
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["<?=$chartLables[0]?>", "<?=$chartLables[1]?>"],
                                datasets: [{
                                        //                            label: '# of Votes',
//                                        data: <? = $data_breaksFullData ?>,
                                        data: [<?= $agent_answered_calls_count == 0 ? 1 : $agent_answered_calls_count //$agent_answered_calls_count ?>,<?= ($agent_extension != "0" ? $all_answered_calls_count - $agent_answered_calls_count : $todayMissedCallsCount) ?>],
                                        backgroundColor: [
                                            'rgba(54, 156, 56, 0.8)',
                                            'rgba(214, 84, 40, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total Answered Calls Result
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <?php if($agent_extension != "0") {
                                        //single agent
                                    $chartLables = array("Total Agent Answered Calls", "Total Others Answered calls");

                                    ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Others</td>
                                                    <td>Agent</td>
                                                </tr>                                            
                                            </thead>
                                            <tbody>
                                                <tr>
    <!--                                                <td><? = $others_answered_percnt . '%' ?></td>
                                                    <td><? = $agent_answered_percnt . '%' ?></td>-->
                                                    <td><?= round($callsAnsweredGivenPeriodOthersPercentage, 2) . '%' ?></td>
                                                    <td><?= round($callsAnsweredGivenPeriodAgentPercentage, 2) . '%' ?></td>
                                                </tr>
                                                <tr>
    <!--                                                <td><? = $others_answered_count ?></td>
                                                    <td><? = $agent_answered_calls_count ?></td>-->
                                                    <td><?= $callsAnsweredGivenPeriodOthers . ' calls' ?></td>
                                                    <td><?= $callsAnsweredGivenPeriodAgent . ' calls' ?></td>
                                                </tr>
                                            </tbody>
                                        </table>                                    
                                    <?php }else{ 
                                       // all agents
                                    $chartLables = array("Total Answered Calls", "Total Missed calls"); 
                                    $noOfMissedCalls = count($missed_calls);
                                    $totalCallsCount = $noOfMissedCalls + $callsAnsweredGivenPeriodAgent;
                                    $missedCallsPercentage = ($noOfMissedCalls == 0 || $totalCallsCount == 0 ? 0 : $noOfMissedCalls / $totalCallsCount * 100);
                                    $answeredCallsPercentage = ($callsAnsweredGivenPeriodAgent == 0 || $totalCallsCount == 0 ? 0 : $callsAnsweredGivenPeriodAgent / $totalCallsCount * 100);
                                    ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Missed</td>
                                                    <td>Answered</td>
                                                </tr>                                            
                                            </thead>
                                            <tbody>
                                                <tr>
    <!--                                                <td><? = $others_answered_percnt . '%' ?></td>
                                                    <td><? = $agent_answered_percnt . '%' ?></td>-->
                                                    <td><?= round($missedCallsPercentage, 2) . '%' ?></td>
                                                    <td><?= round($answeredCallsPercentage, 2) . '%' ?></td>
                                                </tr>
                                                <tr>
    <!--                                                <td><? = $others_answered_count ?></td>
                                                    <td><? = $agent_answered_calls_count ?></td>-->
                                                    <td><?= $callsAnsweredGivenPeriodOthers . ' calls' ?></td>
                                                    <td><?= $callsAnsweredGivenPeriodAgent . ' calls' ?></td>
                                                </tr>
                                            </tbody>
                                        </table>                                    
                                    
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>                                        
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'pie',
                            data: {
                                labels: ["<?=$chartLables[0]?>", "<?=$chartLables[1]?>"],
                                datasets: [{
//                                        label: '# of Votes',
//                                        data: <? = $data_breaksDailyData ?>,
                                        data: [<?= $callsAnsweredGivenPeriodAgent ?>,<?= $callsAnsweredGivenPeriodOthers ?>],
                                        backgroundColor: [
                                            'rgba(54, 156, 56, 0.8)',
                                            'rgba(214, 84, 40, 0.8)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>            
            </div>
        </div>
    </div>
    <br>
    <style>
<?php if ($agent_id == 0) { ?>
            .tableElementGapExtender{
                width: 16% !important;
                text-align: left;
            }
            /*.tableElementGapExtenderOutgoing{
                width: 14% !important;
                text-align: left;
            }*/

            .outgoingReceiver{
                width: 10% !important;
                text-align: left;
            }
            .outgoingAgentName{
                width: 14% !important;
                text-align: left;
            }
            .outgoingStartTime{
                width: 16% !important;
                text-align: left;
            }
            .outgoingAnsweredTime{
                width: 16% !important;
                text-align: left;
            }
            .outgoingEndTime{
                width: 16% !important;
                text-align: left;
            }
            .outgoingDuration{
                width: 10% !important;
                text-align: left;
            }
            .outgoingCallPlayback{
                width: 13% !important;
                text-align: left;
            }


<?php } else { ?>
            .tableElementGapExtender{
                width: 20% !important;
                text-align: left;
            }
            .outgoingReceiver{
                width: 15% !important;
                text-align: left;
            }
            .outgoingAgentName{
                width: 14% !important;
                text-align: left;
            }
            .outgoingStartTime{
                width: 17% !important;
                text-align: left;
            }
            .outgoingAnsweredTime{
                width: 17% !important;
                text-align: left;
            }
            .outgoingEndTime{
                width: 17% !important;
                text-align: left;
            }
            .outgoingDuration{
                width: 13% !important;
                text-align: left;
            }
            .outgoingCallPlayback{
                width: 18% !important;
                text-align: left;
            }
<?php } ?>

    </style>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Answered Calls</a>
            <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($answered_calls) ?></a>
            <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($answered_calls) { ?>        
            <table id="answerCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th class="tableElementGapExtender">
                            Caller Number
                        </th>
    <!--                        <th>
                            Start Time
                        </th>-->
                        <th class="tableElementGapExtender">
                            Answered Time
                        </th>
                        <?php if ($agent_id == 0) { ?>
                            <th class="tableElementGapExtender">
                                Agent Name
                            </th>
                        <?php } ?>
                        <th class="tableElementGapExtender">
                            End Time
                        </th>
                        <th class="tableElementGapExtender">
                            Duration
                        </th>
                        <th class="tableElementGapExtender">
                            Call Playback
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    for ($index = 0; $index < count($answered_calls); $index++) {

//                        $seconds = $answered_calls[$index]['duration'];
//
//                        $hours = floor($seconds / 3600);
//                        $mins = floor($seconds / 60 % 60);
//                        $secs = floor($seconds % 60);
//
//                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        $timeFormat = 0;
                        $timeFirst = strtotime($answered_calls[$index]['answer']);
                        $timeSecond = strtotime($answered_calls[$index]['end']);
                        if ($timeFirst != '0000-00-00 00:00:00' && $timeSecond != '0000-00-00 00:00:00') {
                            $timeFormat = $timeSecond - $timeFirst;
                            if ($timeFormat < 0) {
                                $timeFormat = 0;
                            }
                        }
                        ?>
                        <tr id="<?= $answered_calls[$index]['id'] ?>">
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['src'] ?></td>
                            <!--<td><? = $answered_calls[$index]['start'] ?></td>-->
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['answer'] ?></td>
                            <?php if ($agent_id == 0) { ?>
                                <td class="tableElementGapExtender" title="<?= $answered_calls[$index]['name'].' : '.$answered_calls[$index]['voip'] ?>"><?= $answered_calls[$index]['name'] ?></td>
                            <?php } ?>
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['end'] ?></td>
                            <td class="tableElementGapExtender"><?= $timeFormat ?> seconds</td>
                            <td class="tableElementGapExtender">
                                <a data-toggle="modal" data-target="#audioPlaybackModal" class="btn btn-md btn-success btn-block" onclick="playCallRecord(this, 'I', '<?= '2100'//$answered_calls[$index]['voip'] ?>', '<?= '815462400'//$answered_calls[$index]['src'] ?>', '<?= str_replace(' ', '+', '2019-01-09 10:01:48')//str_replace(' ', '+', $answered_calls[$index]['start']) ?>')">Play</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <br>
    <script>

                var audioType = "<?= $audioType ?>";



                function playCallRecord(playAudioAnchor, direction, extension, customerNumber, datetime) {
                    var row = playAudioAnchor.parentNode.parentNode;
                    var id = row.id;

                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
                    
                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();
                    
                    createShowTransfersButton(direction, extension, customerNumber, datetime, id);
                }

                function pauseAudio() {
    //                    alert($('#audioPlayer')[0].paused);
                    if ($('.audioPlayerClass')[0].paused == false) {
                        $('.audioPlayerClass')[0].pause();
                    }
                }

                $('#audioPlaybackModal').on('hidden.bs.modal', function () {
                    $("#transfersTable").css("display", "none");
                    $("#transferDataLoadingGifImg").css("display", "none");
                    pauseAudio();
                });

                function downloadCurrentlyPlayingAudioFile(){
                    var url = $("#audioPlayerSource").attr("src");
                    if(url != ""){
                        console.log("downloading audio file from "+url);
                        $("#downloaderIframe").attr("src", url);
                    }else{
                        console.log("No audio url to download");
                    }
                }

                function getTransferCallsInfo(direction, extension, customerNumber, datetime, acctId){
                    showTransferCallsLoadingScreen();
                    $.ajax({
                        url : "<?= Url::to(['ftp/get_call_tranfer_information']) ?>",
                        type : "GET",
                        data: {
                            datetime : datetime,
                            callType : direction,
                            ext: extension,
                            phoneNumber: customerNumber,
                            acctId : acctId
                        },
                        success: function(str){
                            hideTransferCallsLoadingScreen();
                            console.log("get transfer calls info success = "+str);
                            var jsonDecodedTransferDataSet = JSON.parse(str);
                            generateTransferCallsTableData(jsonDecodedTransferDataSet);
                        },
                        error: function(err){
                            hideTransferCallsLoadingScreen();
                            console.log("get transfer calls info error - "+err.responseText);
                        }
                    });
                }

                function showTransferCallsLoadingScreen(){
                    $("#transferDataLoadingGifImg").css("display", "inline");
                    $("#transfersTable").css("display", "none");
                    $("#transfersTable > tbody").empty();
                    hideNoNextTransfersAvailableMessage();
                    return true;
                }

                function hideTransferCallsLoadingScreen(){
                    $("#transferDataLoadingGifImg").css("display", "none");
                    return true;
                }

                function showNoNextTransfersAvailableMessage(){
                    $("#noTransfersAvailableMsgDiv").css("display", "inline");
                    return true;
                }

                function hideNoNextTransfersAvailableMessage(){
                    $("#noTransfersAvailableMsgDiv").css("display", "none");
                    return true;
                }

                function generateTransferCallsTableData(jsonDecodedTransferData){                    
                    if(jsonDecodedTransferData['nextTransferedCallsCount'] > 0){
                        // has transfered calls
                        hideNoNextTransfersAvailableMessage();
                        $("#transfersTable").css("display", "initial");
                        $("#transfersTable").css("width", "100%");                        
                        populateTransferTable(jsonDecodedTransferData['nextTransferedCalls']);
                    }else{
                        showNoNextTransfersAvailableMessage();
                    }
                }

                function loadCallTransfersTable(direction, extension, customerNumber, datetime, id){
                    $("#showCallTransfersAnchor").remove();
                    getTransferCallsInfo(direction, extension, customerNumber, datetime, id);
                    return true;
                }

                function createShowTransfersButton(direction, extension, customerNumber, datetime, id){
                    $(".showTransfersBtnClass").remove();
                    var anchorBtn = $("<a></a>");
                    $(anchorBtn).addClass("btn btn-primary btn-block showTransfersBtnClass");
                    $(anchorBtn).attr("id", "showCallTransfersAnchor");
                    var onclickUrl = "loadCallTransfersTable('"+direction+"', "+extension+", "+customerNumber+", '"+datetime+"', "+id+")";
                    $(anchorBtn).attr("onclick", onclickUrl);
                    $(anchorBtn).html("Show call transfers");

                    $("#transfersTable").parent().prepend(anchorBtn);
                }

                function populateTransferTable(transferTableDataArray){
                    for (var x = 0; x < transferTableDataArray.length; x++) {
                        var newRow = $("<tr></tr>");
                        
                        var index = $("<b></b>").html(x + 1);
                        var indexCell = $("<td></td>").html(index);
                        var callerNumberCell = $("<td></td>").html(transferTableDataArray[x]['callerNumber']);
                        var answeredTimeCell = $("<td></td>").html(transferTableDataArray[x]['answeredTime']);
                        var agentNameCell = $("<td></td>").html(transferTableDataArray[x]['agentName']+" : "+transferTableDataArray[x]['dst']);
                        var endTimeCell = $("<td></td>").html(transferTableDataArray[x]['endTime']);
                        var durationCell = $("<td></td>").html(transferTableDataArray[x]['duration']);

                        /* alert("playTransferCallAudio('I', "+transferTableDataArray[x]['dst']+", "+transferTableDataArray[x]['callerNumber']+", "+transferTableDataArray[x]['startedTime']+")"); */
                        var startTime = transferTableDataArray[x]['startedTime'];
                        var startTimereplaced = startTime.replace(" ", "+");
                        var playButton = $("<a></a>").addClass("btn btn-block btn-primary").attr("href", "#").html("Play");
                        $(playButton).attr("onclick", "playTransferCallAudio('I', '"+transferTableDataArray[x]['dst']+"', '"+transferTableDataArray[x]['callerNumber']+"', '"+transferTableDataArray[x]['startedTime']+"')");
                        var playButtonCell = $("<td></td>").html(playButton);

                        $(newRow).append(indexCell);
                        $(newRow).append(callerNumberCell);
                        $(newRow).append(answeredTimeCell);
                        $(newRow).append(agentNameCell);
                        $(newRow).append(endTimeCell);
                        $(newRow).append(durationCell);
                        $(newRow).append(playButtonCell);                        
                        
                        $("#transfersTable > tbody").append(newRow);

                    }
                }

                function playTransferCallAudio(direction, extension, customerNumber, datetime){
                    var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber;
                    console.log("Playing audio file path = "+audioFilePath);
                    $(".audioPlayerClass").remove();
                    var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
                    $("#audioPlayerDiv").append(newAudioElement);
                    $(newAudioElement)[0].play();
                
                }

                
    </script>
    <style>
        .audioPlayerClass{
            width: 100% !important;
        }
    </style>
    <!-- audio playback modal -->
        <div id="audioPlaybackModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
                </div>
              <div class="modal-body">
                <!-- <p>Some text in the modal.</p> -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="audioPlayerDiv">
                                <audio id="audioPlayer" controls class="audioPlayerClass">
                                    <source id="audioPlayerSource" src="" type="audio/wav">
                                </audio>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                        </div>
                    </div>
                    <!-- Transfers section -->
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title" style="text-align:center" id = "transfersH4Id">Call Transfers</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            
                            <table id="transfersTable" class="table table-fixed table-striped" style="display: none">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Caller Number
                                        </th>
                                        <th>
                                            Answered Time
                                        </th>
                                        <th>
                                            Agent Name
                                        </th>
                                        <th>
                                            End Time
                                        </th>
                                        <th>
                                            Duration
                                        </th>
                                        <th>
                                            Call Playback
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End of Transfers section -->
                    <!-- Loading screen section -->
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <img class="img-responsive" src="images/loading.gif" id="transferDataLoadingGifImg" style="display: none">
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row" id="noTransfersAvailableMsgDiv" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-warning" style="text-align: center">
                                <strong>No transfers available</strong>
                            </div>
                        </div>
                    </div>
                    <!-- End of Loading screen section -->                    
                </div>
              </div>
            </div>

          </div>
        </div>
    <!-- end of audio playback modal -->
    <!-- downloader iframe -->
    <iframe src="" id="downloaderIframe" style="display: none">
        
    </iframe>
    <!-- end of downloader iframe-->

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Outgoing Calls </a>
            <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($outgoing_calls) ?></a>
            <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
        </div>                    
    </div>

    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($outgoing_calls) { ?>
            <table id="outgoingCallsTable" class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th class="outgoingReceiver">
                            Receiver
                        </th>
                        <?php if ($agent_id == 0) { ?>
                            <th class="outgoingAgentName">
                                Agent Name
                            </th>
                        <?php } ?>
                        <th class="outgoingStartTime">
                            Start Time
                        </th>
                        <th class="outgoingAnsweredTime">
                            Answered Time
                        </th>
                        <th class="outgoingEndTime">
                            End Time
                        </th>
                        <th class="outgoingDuration">
                            Duration
                        </th>
                        <th class="outgoingCallPlayback">
                            Call Playback
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    for ($index = 0; $index < count($outgoing_calls); $index++) {

                        $seconds = $outgoing_calls[$index]['duration'];

                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);

                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        ?>
                        <tr>
                            <td class="outgoingReceiver"><?= $outgoing_calls[$index]['dst'] ?></td>
                            <?php if ($agent_id == 0) { ?>
                                <td class="outgoingAgentName" title="<?= $outgoing_calls[$index]['name'].' : '.$outgoing_calls[$index]['voip'] ?>"><?= $outgoing_calls[$index]['name'] ?></td>
                            <?php } ?>                          
                            <td class="outgoingStartTime"><?= $outgoing_calls[$index]['start'] ?></td>
                            <td class="outgoingAnsweredTime"><?= $outgoing_calls[$index]['answer'] ?></td>
                            <td class="outgoingEndTime"><?= $outgoing_calls[$index]['end'] ?></td>
                            <td class="outgoingDuration" style="text-align:center"><?= $timeFormat ?></td>
                            <td class="outgoingCallPlayback">
                            <?php
                                if($outgoing_calls[$index]['answer'] != NULL && $outgoing_calls[$index]['answer'] != ''){
                            ?>
                                <a data-toggle="modal" data-target="#audioPlaybackModal" class="btn btn-md btn-success btn-block" onclick="playCallRecord(this, 'O', '<?= '2100'//$outgoing_calls[$index]['voip'] ?>', '<?= '0715381110'//$outgoing_calls[$index]['dst'] ?>', '<?= str_replace(' ', '+', '2019-01-09 11:25:26')//str_replace(' ', '+', $outgoing_calls[$index]['start']) ?>')">Play</a>
                            <?php
                            }else{
                            ?>
                                <a class="btn btn-md btn-success btn-block disabled">Play</a>
                            <?php
                            }
                            ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
    <br>
    <?php if ($agent_id != 0) { ?>
    <div class="row">
        <div class="col-xs-6" style="padding-right: 2%">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Abandoned Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($agent_abandoned_calls) ?></a>
                    <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($agent_abandoned_calls) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Caller Number
                                </th>
                                <th>
                                    Abandoned Date
                                </th>
                                <th>
                                    Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

                                $datetime = $agent_abandoned_calls[$index]['end'];
                                $dateTimeExploded = explode(" ", $datetime);
                                ?>
                                <tr>
                                    <td><?= $agent_abandoned_calls[$index]['src'] ?></td>
                                    <td><?= $dateTimeExploded[0] ?></td>
                                    <td><?= $dateTimeExploded[1] ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>

            </div>

        </div>

        <!--<div class="col-xs-1"></div>-->
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Login Details</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($login_records) ?></a>
                    <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php //if ($agent_abandoned_calls) {     ?>        
                <?php if ($login_records) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Logged Date
                                </th>
                                <th>
                                    Logged Time
                                </th>
                                <th>
                                    Logout Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            for ($index = 0; $index < count($login_records); $index++) {

                                $login_time_sig = $login_records[$index]['logged_in_time'];
                                $login_date = date('Y-m-d', $login_time_sig);
                                $login_time = date('H:i:s', $login_time_sig);

                                $logged_time_sig = $login_records[$index]['time_signature'];
                                $logged_date = date('Y-m-d', $logged_time_sig);
                                $logged_time = date('H:i:s', $logged_time_sig);
                                ?>
                                <tr>
                                    <td><?= $login_date ?></td>
                                    <td><?= $login_time ?></td>
                                    <td><?= $logged_time ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent DND Records</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($agent_dnd_reocrds) ?></a>
                    <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($agent_dnd_reocrds) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Mode
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            for ($index = 0; $index < count($agent_dnd_reocrds); $index++) {
                                $date_time = $agent_dnd_reocrds[$index]['timestamp'];
                                $explods = explode(" ", $date_time);
                                ?>
                                <tr>
                                    <td><?= $explods[0]; ?></td>
                                    <td><?= $explods[1]; ?></td>
                                    <td><?= $agent_dnd_reocrds[$index]['dnd_mode']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php }
        if ($agent_id == 0) {
        ?>
    <style>
        .missed-calls-table th{
            width: 150px;
        }
    </style>
    <div class="row">
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Missed Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($missed_calls) ?></a>
                    <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($missed_calls) { ?>        
                    <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Caller Number
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php for($i = 0; $i < count($missed_calls); $i++){?>
                                <tr>
                                    <td><?=$missed_calls[$i]['date']?></td>
                                    <td><?=$missed_calls[$i]['time']?></td>
                                    <td><?=$missed_calls[$i]['caller_num']?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">IVR Received Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($ivrCallsData) ?></a>
                    <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($ivrCallsData) { ?>        
                    <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date Time
                                </th>
                                <th>
                                    IVR
                                </th>
                                <th>
                                    Caller Number
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php for($i = 0; $i < count($ivrCallsData); $i++){?>
                                <tr>
                                    <td><?=$ivrCallsData[$i]['start']?></td>
                                    <td><?=$ivrCallsData[$i]['dst']?></td>
                                    <td><?=$ivrCallsData[$i]['src']?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
    <br>
    <style type="text/css">
        .tableElementVMGapExtender{
            width: 50%;
        }
    </style>
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Received Voice Mail Calls</a>
                <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($vmData) ?></a>
                <a class="btn btn-md btn-primary" style="margin-top: 3%;"><span class="glyphicon glyphicon-download-alt"></span> Download CSV</a>
            </div>
        </div>
        <div class="row navbar-default" style="border: 1px solid #cdcdcd">
            <?php if ($vmData) { ?>        
                <table id="vmCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                    <thead class="call-history-thead">
                        <tr>
                            <th class="tableElementVMGapExtender">
                                Caller Number
                            </th> 
                            <th class="tableElementVMGapExtender">
                                Voice Mail Number
                            </th>                      
                            <th class="tableElementVMGapExtender">
                                Answered Time
                            </th>
                            <th class="tableElementVMGapExtender">
                                End Time
                            </th>
                            <th class="tableElementVMGapExtender">
                                Duration
                            </th>
                        </tr>
                    </thead>
                    <tbody class="call-history-tbody">
                        <?php
                        for ($index = 0; $index < count($vmData); $index++) {

                           $seconds = $vmData[$index]['duration'];
    
                           $hours = floor($seconds / 3600);
                           $mins = floor($seconds / 60 % 60);
                           $secs = floor($seconds % 60);
    
                           $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                            ?>
                            <tr>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['src'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['dst'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['answer'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['end'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $timeFormat ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>


    <?php }?>
</div>

