  <?php
    use yii\helpers\Url;
  ?>
        <div class="ticketcontent" id="ticketcontent">
          <i class='fa fa-info-circle' style="font-size: 30px"></i>
          <span class="nav-text">&nbsp;&nbsp;&nbsp;&nbsp;<?=$ticketsData[0]['ticket_number'];?> : <?=substr($ticketsData[0]['description'], 0,25)."...";?></span>
          <span class="nav-text pull-right">Date:<?=substr($ticketsData[0]['created_date'],0,10);?></span>
          <hr>
          <div>
            <?php if ($ticketsData[0]['ticket_status'] == "new" || $ticketsData[0]['ticket_status'] == "inprogress") { ?>
              <a href="#" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</a>
              <a  class="btn btn-success btn-sm pull-right" onclick="resolveTicket(<?=$ticketsData[0]['id'];?>)"><span class="glyphicon glyphicon-ok" ></span>&nbsp;Resolve</a>
            <?php }else{  ?>
              <a href="#" class="btn btn-default btn-sm" disabled><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</a>
              <a href="#" class="btn btn-success btn-sm pull-right" disabled><span class="glyphicon glyphicon-ok"></span>&nbsp;Already Resolved</a>
            <?php } ?>
            <?php if ($ticketsData[0]['priority'] == 'high') { ?>
              <a class="btn btn-sm pull-right text-danger" ><span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;<?=ucfirst($ticketsData[0]['priority']);?></a>
            <?php }else{  ?>
              <a class="btn btn-sm pull-right text-primary" ><span class="glyphicon glyphicon-exclamation-sign"></span>&nbsp;<?=ucfirst($ticketsData[0]['priority']);?></a>
            <?php } ?>
          </div><br><br>
          <span class="nav-text">Details</span>
          <div class="row" style="padding-top: 15px">
            <div class="col-xs-6">
              <table class="table">
                <tr>
                  <td>Type <span class="pull-right">:</span></td>
                  <td><?=$ticketsData[0]['source'];?></td>
                </tr>
                <tr>
                  <td>Status<span class="pull-right">:</span></td>
                  <td>
                    <?php if ($ticketsData[0]['ticket_status'] == "new") { ?>
                      <span class="label label-danger"><?=ucfirst($ticketsData[0]['ticket_status']);?></span>
                    <?php }else if($ticketsData[0]['ticket_status'] == "inprogress"){ ?>
                      <span class="label label-warning"><?=ucfirst($ticketsData[0]['ticket_status']);?></span>
                    <?php }else{ ?>
                      <span class="label label-success"><?=ucfirst($ticketsData[0]['ticket_status']);?></span>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td>Created By<span class="pull-right">:</span></td>
                  <td><?=$createdUser->fullname;?></td>
                </tr>                                                
              </table>
            </div>
            <div class="col-xs-6">
              <table class="table">
                <tr>
                  <td>Customer Name<span class="pull-right">:</span></td>
                  <td><?=$ticketsData[0]['customer_name'];?></td>
                </tr>
                <tr>
                  <td>Contact Number<span class="pull-right">:</span></td>
                  <td><?=$ticketsData[0]['contact_m'];?></td>
                </tr> 
                <tr>
                  <td>Assigned To<span class="pull-right">:</span></td>
                  <td><?=$assignedUser->fullname;?></td>
                </tr>                                                
              </table>
            </div>
          </div>
          <span class="nav-text">Description</span>
          <hr>
          <div><?=$ticketsData[0]['description'];?>
        </div>
            <br>
            <span class="nav-text">Recorded Audio</span>
            <hr>
            <div><a data-toggle="modal" data-target="#audioPlaybackModal" style="cursor: pointer" onclick="playCallRecord(this, 'I', '2095', '773799777', '2019-01-09 11:33:37')"><?=substr($ticketsData[0]['created_date'],0,10);?>_call_record</a></div>
        <br>  <br>




            <!-- audio playback modal -->
            <div id="audioPlaybackModal" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
                        </div>
                        <div class="modal-body">
                            <!-- <p>Some text in the modal.</p> -->
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="audioPlayerDiv">
                                            <audio id="audioPlayer" controls class="audioPlayerClass">
                                                <source id="audioPlayerSource" src="" type="audio/wav">
                                            </audio>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- end of audio playback modal -->

            <iframe id="downloaderIframe" style="display: none"></iframe>

            <script>

                function pauseAudio() {
                    //                    alert($('#audioPlayer')[0].paused);
                    if ($('.audioPlayerClass')[0].paused == false) {
                        $('.audioPlayerClass')[0].pause();
                    }
                }

                $('#audioPlaybackModal').on('hidden.bs.modal', function () {
                    $("#transfersTable").css("display", "none");
                    $("#transferDataLoadingGifImg").css("display", "none");
                    pauseAudio();
                });

                function downloadCurrentlyPlayingAudioFile(){
                    var url = $("#audioPlayerSource").attr("src");
                    if(url != ""){
                        console.log("downloading audio file from "+url);
                        $("#downloaderIframe").attr("src", url);
                    }else{
                        console.log("No audio url to download");
                    }
                }
            </script>



        <span class="nav-text">Comments</span>
        <hr>
        <div >
          <div class="panel panel-white post panel-shadow">
            <div class="post-heading">
              <div>
                <a><b>Add New Comment</b></a>                            
              </div>
            </div> 
            <div class="post-description"> 
              <textarea class="form-control" rows="1" id="commentArea" onkeypress="return addNewComment(event,<?=$ticketsData[0]['id'];?>)"></textarea>
            </div>
          </div>
        </div>





        <?php if (count($ticketComments)>0) { ?>
          <?php for ($i=0; $i < count($ticketComments) ; $i++) {  ?>
            <div >
              <div class="panel panel-white post panel-shadow">
                <div class="post-heading">
                  <div>
                    <a><b><?=$ticketComments[$i]['fullname'] ?></b></a>&nbsp;<span class="text-muted"><?=$ticketComments[$i]['created_date'] ;?></span>                            
                    <a class="btn btn-default btn-xs pull-right" onclick="deleteComment(<?=$ticketComments[$i]['id'] ?>,<?=$ticketsData[0]['id'];?>)"><span class="fa fa-times"></span></a>
                    <a href="#" class="btn btn-default btn-xs pull-right"><span class="glyphicon glyphicon-pencil"></span></a>
                  </div>
                </div> 
                <div class="post-description"> 
                  <p><?=$ticketComments[$i]['comment'] ?></p>
                </div>
              </div>
            </div>
        <?php  } ?>
      <?php } ?>
          </div>




