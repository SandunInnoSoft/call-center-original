<div id="customerTicketsDiv">

  <?php if (count($ticketsData) > 0 ){ ?>
    <ul class="list-group">

      <?php for ($i=0; $i < count($ticketsData); $i++) {  ?>
      <li class="list-group-item d-flex justify-content-between align-items-center" style="cursor:pointer;" onclick="getTicketsData(<?=$ticketsData[$i]['id'];?>)">
        <?php echo $ticketsData[$i]['ticket_number'].' - '. substr($ticketsData[$i]['description'],0,5).'...' ?>

        <?php if ($ticketsData[$i]['ticket_status'] == "new") { ?>
        <span class="label label-danger pull-right">New</span>
        <?php }else if ($ticketsData[$i]['ticket_status'] == "inprogress"){ ?>
        <span class="label label-warning pull-right">In Progress</span>
         <?php }else{ ?> 
        <span class="label label-success pull-right">Closed</span>
         <?php } ?>          
      </li>
      <?php } ?>                  
    </ul>
  <?php }else{ ?>
    <div style="margin: 50px"><h4 class="text-muted">No Tickets Found</h4></div>
  <?php } ?>  
</div>
