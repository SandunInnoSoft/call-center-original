<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>

<style>
thead{
  font-weight: bold;
}

.panel-heading{
  background-color: #10297d !important;
  color: white !important;
}

.panel-heading-sub{
  background-color: #337ab7 !important;
  color: white !important;
}

.ticket-content{
  margin: 20px
}

.nav-text {
 vertical-align: 5px;
 font-size: 18px
}
</style>


<style type="text/css">


.panel-shadow {
  box-shadow: rgba(0, 0, 0, 0.2) 7px 7px 7px;
}
.panel-white {
  border: 1px solid #dddddd;
}
.panel-white  .panel-heading {
  color: #333;
  background-color: #fff;
  border-color: #ddd;
}

.post .post-heading {
  padding-left:20px;
  padding-right:20px;
  padding-top:20px;
}

.post .post-description {
  padding: 20px;
  padding-top: 5px;
}

</style>

<?php 

$customerNic = null;
if (isset($customerData)) {
  $customerNic = $customerData->id_number;
}
?>

<a href = "<?= Url::to(['agent/callerinformation']) ?>" class = "btn" type = "button">< Back</a>
<div  style="font-family: 'Lato' , sans-serif;">
  <div class="col-xs-12" >
    <div class="row">
      <div class="panel panel-default" style="border: 1px solid #10297d;">
        <div class="panel-heading"  style="height: 40px">
          <h4 class="panel-title">
            <a>Search</a>
          </h4>
        </div>
        <div id="collapse1" class="" style="height: 50px;padding: 5px">
          <div class="row">
            <div class="col-xs-3">
              <div class="input-group">
                <input class="form-control" type="text" id="searchByIdText" placeholder="Ticket #">
                <span class="input-group-addon" id="searchById" ><i class="fa fa-search"></i></span>
              </div>
            </div>
            <div class="col-xs-3">
              <div class="input-group">
                <input class="form-control" type="text" id="searchByNicText" placeholder="Customer Nic..">
                <span class="input-group-addon" id="searchByNic"><i class="fa fa-search"></i></span>
              </div>
            </div>             
            <div class="col-xs-3">
              <div class="input-group">
                <input class="form-control" type="text" id="searchByNameText" placeholder="Customer Name..">
                <span class="input-group-addon" id="searchByName"><i class="fa fa-search"></i></span>
              </div>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>    

  <div class="col-xs-3" >
    <div class="row">
      <div class="panel panel-default" style="border: 1px solid #10297d;">
        <div class="panel-heading"  style="height: 40px">
          <h4 class="panel-title" >
            <?php if (isset($customerData)) { ?>
              <a id="ticketsHead">Tickets of <?=$customerData->customer_name; ?> (<?=$customerData->id_number;?>)</a>
            <?php }else{ ?>
              <a id="ticketsHead2">Tickets</a>
            <?php } ?>
          </h4>
        </div>
        <div id="customerTicketsDiv" style="height: 200px;overflow-y: scroll">
        </div>
      </div>
      <div class="panel panel-default" style="border: 1px solid #10297d;">
        <div class="panel-heading"  style="height: 40px">
          <h4 class="panel-title">
            <a>All Tickets of <?=Yii::$app->session->get('user_name')  ?></a>
          </h4>
        </div>
        <div id="collapse1" class="" style="height: 350px;overflow-y: scroll">
          <div id="collapse1">
           <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading panel-heading-sub">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseX">
                  Open</a>
                </h4>
              </div>
              <div id="collapseX" class="panel-collapse collapse in">
                <?php if (count($openticketsDataOfUser) > 0 ){ ?>
                  <ul class="list-group">

                    <?php for ($i=0; $i < count($openticketsDataOfUser); $i++) {  ?>
                      <li class="list-group-item d-flex justify-content-between align-items-center" style="cursor:pointer;" onclick="getTicketsData(<?=$openticketsDataOfUser[$i]['id'];?>)">
                        <?php echo $openticketsDataOfUser[$i]['ticket_number'].' - '. substr($openticketsDataOfUser[$i]['description'],0,5).'...' ?>

                        <?php if ($openticketsDataOfUser[$i]['ticket_status'] == "new") { ?>
                          <span class="label label-danger pull-right">New</span>
                        <?php }else if ($openticketsDataOfUser[$i]['ticket_status'] == "inprogress"){ ?>
                          <span class="label label-warning pull-right">In Progress</span>
                        <?php }else{ ?> 
                          <span class="label label-success pull-right">Closed</span>
                        <?php } ?>  
                      </li>
                    <?php } ?>
                  </ul>
                <?php }else{ ?>
                  <div style="margin: 50px"><h4 class="text-muted">No Tickets Found</h4></div>
                <?php } ?>  
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading panel-heading-sub">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                Recently closed</a>
              </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse in">
              <?php if (count($closedticketsDataOfUser) > 0 ){ ?>
                <ul class="list-group">

                  <?php for ($i=0; $i < count($closedticketsDataOfUser); $i++) {  ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center" style="cursor:pointer;" onclick="getTicketsData(<?=$closedticketsDataOfUser[$i]['id'];?>)">
                      <?php echo $closedticketsDataOfUser[$i]['ticket_number'].' - '. substr($closedticketsDataOfUser[$i]['description'],0,5).'...' ?>
                      <?php if ($closedticketsDataOfUser[$i]['ticket_status'] == "new") { ?>
                        <span class="label label-danger pull-right">New</span>
                      <?php }else if ($closedticketsDataOfUser[$i]['ticket_status'] == "inprogress"){ ?>
                        <span class="label label-warning pull-right">In Progress</span>
                      <?php }else{ ?> 
                        <span class="label label-success pull-right">Closed</span>
                      <?php } ?> 
                    </li>
                  <?php } ?>
                </ul>
              <?php }else{ ?>
                <div style="margin: 50px"><h4 class="text-muted">No Tickets Found</h4></div>
              <?php } ?> 
            </div>
          </div>
        </div> 
      </div>
    </div>
  </div>
</div>
</div>

<div class="col-xs-1" ></div>
<div class="col-xs-8" >
  <div class="row">
    <div id="collapse1" class="" style="height: 50px">
      <button class="btn btn-primary" type="button" data-target="#myModal" data-toggle="modal">
        <i class="glyphicon glyphicon-plus"></i> &nbsp;Create New Ticket  
      </button>
    </div>
  </div>
</div>
<div class="col-xs-1" ></div>
<div class="col-xs-8" >
  <div class="row">
    <div class="panel panel-default" style="border: 1px solid #10297d;">
      <div style="height: 600px;overflow-y: scroll">
        <div class="ticket-content" Id="ticket-content">
          <div class="text-center" style="margin-top: 100px"><h4 class="text-muted">Please Select a Ticket..</h4></div>
        </div>
      </div>  
    </div>
  </div>
</div>

<!-- Modal -->

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog" style="width: 650px">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style=" padding:9px;background-color:#10297d;color: white ">
        <div class="heading" style="height:10px">
          <div class="row">
            <div class="col-lg-11" style="text-align:center "> 
              <h1 style="margin:5px 0">Create Issues</h1> 
            </div>   
            <div class="col-lg-1" style="text-align:center ">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>                              
          </div>
        </div>                  

      </div>
      <div class="main-body">
        <form name="myForm2" id="myForm2" class="form-horizontal" style="max-width:600px;margin:auto">
          <div class="main-login main-center">
            <div class="modal-body">

              <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">
                  <input class="form-control" name="hidden" id="hidden" type="hidden" required autofocus />
                </div> 
              </div>
              <div class="user-section">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">
                    <input class="form-control" name="id" id="id" placeholder="NIC/Passport" type="text" required autofocus />
                    <span id="identiyNumberValidationMessage"></span>
                  </div> 

                  <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">                                        
                    <select class="form-control" id="comp_type" placeholder="Type">
                      <option value="">Complaint Type</option>
                      <option>Lead</option>
                      <option>Technical</option>


                    </select>                                        
                  </div>  

                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                    <input class="form-control" name="name" id="name" placeholder="Name" type="text" required />
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 12px;">
                    <input class="form-control" name="address" id="address" placeholder="Address" type="text" required />
                  </div>
                </div> 
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 12px;">
                    <input class="form-control" name="email" id="email" placeholder="Email" type="text" required autofocus />
                  </div> 
                </div> 
              </div>

              <div class="row">                                    
                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">                                        
                  <select class="form-control" id="source_comp">
                    <option value="">Source of Information</option>
                    <option>Phone Call</option>
                    <option>Direct Contact</option>
                    <option>Other</option>


                  </select>                                       
                </div>  
                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">                                        
                  <select class="form-control" id="language">
                    <option value="">Pref.Language</option>
                    <option>Sinhala</option>
                    <option>English</option>
                    <option>Tamil</option>

                  </select>                                       
                </div>   
              </div>    
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">
                  <input class="form-control" name="mobile" id="mobile" placeholder="Mobile" type="text" required autofocus />
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 12px;">
                  <input class="form-control" name="other" id="other" placeholder="Other" type="text" required autofocus />
                </div> 

              </div>
              <div class="row">                                    
                <div class="col-lg-4 col-md-4 col-sm-6" style="padding-bottom: 12px;">                                        
                  <select class="form-control" id="ticket_status">
                    <option value="">Status</option>
                    <option value="new" selected="">New</option>
                    <option value="inprogress">Inprogress</option>
                    <option value="closed">Closed</option>

                  </select>                                        
                </div>   
                <div class="col-lg-4 col-md-4 col-sm-6" style="padding-bottom: 12px;">                                        
                  <select class="form-control" id="priority">
                    <option value="">Priority</option>
                    <option>High</option>
                    <option>Mid</option>
                    <option>Low</option>

                  </select>                                       
                </div>  
                <div class="col-lg-4 col-md-3 col-sm-6" style="padding-bottom: 28px;">                                        
                  <select class="form-control" id="assignee">

                  </select>                                       
                </div>   
              </div>

              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <textarea style="resize:vertical;" class="form-control" placeholder="Description.." id="description" rows="6" name="comment" required></textarea>
                </div>
              </div>
              <div class="panel-footer" style="margin-bottom:-14px; padding: 10px 0px">
                <input type="button" class="btn btn-success" onclick="createNewUser()" value="Save"/>
                <!--<span class="glyphicon glyphicon-ok"></span>-->
                <input type="reset" class="btn btn-danger" value="Clear" />
                <!--<span class="glyphicon glyphicon-remove"></span>-->
                <button style="float: right;" type="button" class="btn btn-warning btn-close" data-dismiss="modal">Close</button>
              </div>
            </div>                      

          </div>

        </form>
      </div>
    </div>
  </div>
</div>



<script >
  function createNewUser() {

//            var hidden = $('#hidden').val();
//            var Id = $('#id').val();
//            var Comp_type = $('#comp_type').val();
//            var Name = $('#name').val();
//            var Address = $('#address').val();
//            var Source_comp = $('#source_comp').val();
//            var Language = $('#language').val();
//            var Mobile = $('#mobile').val();
//            var Other = $('#other').val();
//            var Email = $('#email').val();
//            var Ticket_status = $('#ticket_status').val();
//            var Priority = $('#priority').val();
//            var Assignee = $('#assignee').val();
//            var Description = $('#description').val();


var data = {

  id: $('#id').val(),
  name: $('#name').val(),
  address: $('#address').val(),
  language: $('#language').val(),
  mobile: $('#mobile').val(),
  other: $('#other').val(),
  email: $('#email').val(),
  comp_type: $('#comp_type').val(),
  source: $('#source_comp').val(),
  ticket_status: $('#ticket_status').val(),
  priority: $('#priority').val(),
  assignee: $('#assignee').val(),
  description: $('#description').val(),
  customer_id: '',
  reporter: ''

};

var isValid=formValidate(data);
if (isValid) {
  $.ajax({
    url: "<?= Url::to(['tickets/save']) ?>",
    type: "POST",
    data: data,
    success: function (res) {
      swal({
        title: "Success",
        text: "Data saved successfully",
        type: "success"
      });
      $("#myForm2")[0].reset();
      location.reload();
    }
  });
}

}

function formValidate(data) {

  if (data.id === "" || data.name === "" || data.address === "" || data.language === "" || data.mobile === "" || data.other === "" || data.email === "" || data.comp_type === "" || data.source === "" || data.ticket_status === "" || data.priority === "" || data.assignee === "" || data.description === "") {
    swal({
      title: "Warning",
      text: "Fill all Data",
      type: "warning"
    });
    return false;
  } else {
    return true;
  }
}

$('document').ready(function(){
  getInitialData();
})
</script>



<script>

  $("#searchById").click(function(){
    var searchText = $("#searchByIdText").val();

    if (searchText == "" || searchText == null) {
      $("#searchByNicText").val(""); 
    }else{
      var url = "<?= Url::to(['tickets/searchbyticket']) ?>"
      $.ajax({
        type: "GET",
        url: url,
        data: {searchText: searchText},
        success: function(data){
          var result = $(data).find('#customerTicketsDiv');
          $("#customerTicketsDiv").html(result);
          $("#ticketsHead").html('Search Results..');
          $("#ticketsHead2").html('Search Results..');

        }
      });
    }
  });



  $("#searchByNic").click(function(){
    var searchText = $("#searchByNicText").val();

    if (searchText == "" || searchText == null) {
      $("#searchByNicText").val("");      
    }else{
      var url = "<?= Url::to(['tickets/searchbynic']) ?>"
      $.ajax({
        type: "GET",
        url: url,
        data: {searchText: searchText},
        success: function(data){
          var result = $(data).find('#customerTicketsDiv');
          $("#customerTicketsDiv").html(result);
          $("#ticketsHead").html('Search Results..');
          $("#ticketsHead2").html('Search Results..');          
        }
      });
    }
  });


  $("#searchByName").click(function(){
    var searchText = $("#searchByNameText").val();

    if (searchText == "" || searchText == null) {
      $("#searchByNicText").val(""); 
    }else{
      var url = "<?= Url::to(['tickets/searchbyname']) ?>"
      $.ajax({
        type: "GET",
        url: url,
        data: {searchText: searchText},
        success: function(data){
          var result = $(data).find('#customerTicketsDiv');
          $("#customerTicketsDiv").html(result);
          $("#ticketsHead").html('Search Results..');
          $("#ticketsHead2").html('Search Results..');          
        }
      });
    }
  });


  function getInitialData(){
    var nic = '<?=$customerNic;?>';
    $.ajax({
      url: "<?= Url::to(['tickets/customertickets']) ?>",
      data: {nic:nic},
      success : function(data){
        var result = $(data).find('#customerTicketsDiv');
        $("#customerTicketsDiv").html(result);
      }
    });

    $.ajax({
      url: "<?= Url::to(['tickets/getuseragent']) ?>",
      data: {},
      success: function (data) {
        var arrayData = JSON.parse(data);
        var str ='';
        str+=`<option value=''>Assignee</option>`;
        arrayData.forEach(function(key){    
         str+= `<option value=${key['id']}>${key['name']}</option>`;   
       });
        $('html #assignee').append(str);


      }
    });
  }


  function getTicketsData(id){
    var url = "<?= Url::to(['tickets/loadticketinfo']) ?>"
    $.ajax({
      type: "GET",
      url: url,
      data : {id:id},
      success: function(data){
        var result = $(data).find('#ticketcontent');
        $("#ticket-content").html(result);
      },
        error: function(err){
          console.log("Get tickets data ajax error");
          console.log(err.responseText);
        }
    });
  }


  function addNewComment(e,ticketId) {
    if (e.keyCode == 13) {
      var comment = document.getElementById("commentArea");
      var ticket_id = ticketId;
      var createdDate = "<?=date('Y-m-d H:i:s'); ?>";
      var url = "<?= Url::to(['tickets/addnewcomment']) ?>";
      $.ajax({
        type: "POST",
        url: url,
        data : {comment:comment.value,ticket_id:ticket_id,createdDate:createdDate},
        success: function(data){
          getTicketsData(ticket_id);
          return false;
        } 
      });
    }
  }


  function deleteComment(id,ticketId){
    var url = "<?= Url::to(['tickets/deletecomment']) ?>";
    var ticket_id = ticketId;
    $.ajax({
      type: "GET",
      url: url,
      data : {id:id},
      success: function(data){
        getTicketsData(ticket_id);
      }
    });
  }


  function resolveTicket(id){
    var url = "<?= Url::to(['tickets/resolveticket']) ?>";
    var ticket_id = id;
    $.ajax({
      type: "GET",
      url: url,
      data : {ticket_id:ticket_id},
      success: function(data){
        location.reload();
      }
    });
  }


  $("#id").keyup(function(){
   $("#identiyNumberValidationMessage").removeAttr("data-value");
   $("#identiyNumberValidationMessage").removeAttr( 'style' );
   var typingText = $(this).val();
   if(typingText != "" && typingText.length > 0){
          // typing text is not empty
          $("#identiyNumberValidationMessage").html("Checking..");
          $("#identiyNumberValidationMessage").css("color", "grey");  

          $.ajax({
            url: "<?= Url::to(['tickets/checkopenticketsofid'])?>",
            data: {typingText : typingText},
            type: 'GET',
            success: function (data, textStatus, jqXHR) {
              if(data == "0"){
                      // no open tickets is available
                      $("#identiyNumberValidationMessage").html("No Open Tickets Found");
                      $("#identiyNumberValidationMessage").css("color", "green");
                    }else{
                      // open tickets are available
                      $("#identiyNumberValidationMessage").html(data+" Open Tickets Found");
                      $("#identiyNumberValidationMessage").css("color", "red");
                      $("#identiyNumberValidationMessage").attr("data-value", typingText);                      
                      $("#identiyNumberValidationMessage").css("cursor",'pointer');                
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                    console.log("extension validation error : "+jqXHR.responseText);
                  }
                });    
        }else{
          // typing text is empty
          $("#identiyNumberValidationMessage").html("");
          $("#identiyNumberValidationMessage").css("color", "red");  
        }
      });


  $('#identiyNumberValidationMessage').on('click', function(e){
   var searchText = e.target.dataset.value;
   if (searchText == "" || searchText == null) {
    $("#searchByNicText").val("");      
  }else{
    var url = "<?= Url::to(['tickets/searchbynic']) ?>"
    $.ajax({
      type: "GET",
      url: url,
      data: {searchText: searchText},
      success: function(data){
        var result = $(data).find('#customerTicketsDiv');
        $("#customerTicketsDiv").html(result);
        $("#ticketsHead").html('Search Results..');
        $("#ticketsHead2").html('Search Results..');
        $('#myModal').modal('hide');            
      }
    });
  }
});


  function playCallRecord(playAudioAnchor, direction, extension, customerNumber, datetime) {
      var row = playAudioAnchor.parentNode.parentNode;
      var id = row.id;

      var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime="+datetime+"&callType="+direction+"&ext="+extension+"&phoneNumber="+customerNumber;
      console.log("Playing audio file path = "+audioFilePath);
      $(".audioPlayerClass").remove();
      var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");

      $("#audioPlayerDiv").append(newAudioElement);
      $(newAudioElement)[0].play();

      // createShowTransfersButton(direction, extension, customerNumber, datetime, id);
  }






</script>


