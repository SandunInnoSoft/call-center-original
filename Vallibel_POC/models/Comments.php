<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model is to interact with the tickets table in the database
 *
 * @author Vikum
 * @since 2018-12-02
 */
class Comments extends ActiveRecord {

	public function saveComment($data){
		$comment = new Comments();
		$comment->ticket_id = $data['ticket_id'];
		$comment->created_date = $data['createdDate'];
		$comment->comment = $data['comment'];
		$comment->createdBy = $data['createdBy'];
		return $comment->insert();
	}


	public function getTicketComments($id){
		$connection = Yii::$app->db;
		$command = $connection->createCommand("SELECT comments.id,comments.created_date,comments.comment,ccuser.fullname from comments,call_center_user as ccuser where comments.createdBy = ccuser.id and comments.ticket_id = '$id' ORDER BY comments.created_date DESC");
		$result = $command->queryAll();
		return $result;		
	}

}
