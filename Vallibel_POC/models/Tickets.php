<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tickets
 *
 * @since 03/12/2018
 * @author Prabath
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class Tickets extends ActiveRecord {

    /**
     * <b>Save Tickets</b>
     * <p>This function save call ticket data</p>
     * 
     * @param int $data     
     * @return boolean
     * @since 2018-12-03
     * @author Vikum
     */
    public static function saveTicket($data) {
        $ticketRequest = new Tickets();
        $ticketRequest->ticket_number = $data['ticket_number'];
        $ticketRequest->source = $data['source'];
        $ticketRequest->status = $data['status'];
        $ticketRequest->customer_id = $data['customer_id'];
        $ticketRequest->priority = $data['priority'];
        $ticketRequest->assignee = $data['assignee'];
        $ticketRequest->reporter = $data['reporter'];
        $ticketRequest->description = $data['description'];
        $ticketRequest->ticket_status = $data['ticket_status'];
        $ticketRequest->created_date = $data['created_date'];      
        return $ticketRequest->insert();
    }


    /**
     * <b>Search Tickets</b>
     * <p>This function search tickets</p>
     * 
     * @return array
     * @since 2018-12-03
     * @author Vinothan
     */
    public function searchByTicketNumber($text){

        $connection = Yii::$app->db;
        $command = $connection->createCommand("SELECT * from tickets where ticket_number Like '%$text%' ");

        $result = $command->queryAll();
        return $result;             
    }



    /**
     * <b>Search Tickets</b>
     * <p>This function search tickets</p>
     * 
     * @return array
     * @since 2018-12-03
     * @author Vinothan
     */
    public function searchByCustomerNic($text){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("SELECT tickets.id,tickets.ticket_number,tickets.source,tickets.status,tickets.customer_id,tickets.priority,tickets.assignee,tickets.reporter,tickets.description,tickets.ticket_status,tickets.created_date from tickets,customer where customer.id_number Like '$text%' and customer.id = tickets.customer_id");

        $result = $command->queryAll();
        return $result;              
    }    


    /**
     * <b>Search Tickets</b>
     * <p>This function search tickets</p>
     * 
     * @return array
     * @since 2018-12-03
     * @author Vinothan
     */
    public function searchByCustomerName($text){

        $connection = Yii::$app->db;
        $command = $connection->createCommand("SELECT tickets.id,tickets.ticket_number,tickets.source,tickets.status,tickets.customer_id,tickets.priority,tickets.assignee,tickets.reporter,tickets.description,tickets.ticket_status,tickets.created_date from tickets,customer where customer.customer_name Like '$text%' and customer.id = tickets.customer_id");

        $result = $command->queryAll();
        return $result;                  
    }    


    /**
     * <b>Search Tickets</b>
     * <p>This function search tickets</p>
     * 
     * @return array
     * @since 2018-12-03
     * @author Vinothan
     */
    public function getTicketInformation($text){

        $connection = Yii::$app->db;
        $command = $connection->createCommand("SELECT tickets.id,tickets.ticket_number,tickets.source,tickets.status,tickets.customer_id,tickets.priority,tickets.assignee,tickets.reporter,tickets.description,tickets.ticket_status,tickets.created_date,customer.customer_name,customer.contact_m From tickets,customer where customer.id = tickets.customer_id and tickets.id = '$text' ");

        $result = $command->queryAll();
        return $result;                  
    }   
     /**
     * <b>Search Tickets</b>
     * <p>This function will search for tcikets </p>
     * 
     * @param int $data     
     * @return boolean
     * @since 2018-12-03
     * @author Vikum
     */
    public static function searchForAgentTickets($agentId, $ticketStatus) {
        $ticketAgentObj = new Tickets();
        $query = $ticketAgentObj->find()
        
        ->where("ticket_status = '$ticketStatus'")
        ->andWhere("assignee  = '$agentId'")
        ->count();
        return $query;
    }


      public function getOpenTickets($nic){
        $connection = Yii::$app->db;
        $command = $connection->createCommand("SELECT count(tickets.ticket_number) as opentickets from tickets,customer where customer.id = tickets.customer_id and ticket_status in ('new','inprogress') and customer.id_number = '$nic' ");
        $result = $command->queryAll();
        return $result;        
      }      
  


}
