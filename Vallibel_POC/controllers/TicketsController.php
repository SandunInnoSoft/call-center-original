<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicketsController
 * 
 * @author Vino
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Tickets;
use app\models\Customer;
use app\models\Comments;
use app\models\Call_center_user;

class TicketsController extends Controller {
    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Vinothan
     * @since 2018-11-30
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        set_time_limit(300);
    }
    

    /**
     * <b>Renders index page of ticket component</b>
     * 
     * @author Vinothan
     * @since 2018-11-30
     */    
    public function actionTickets() {

        $session = Yii::$app->session;
        if (!$session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        } else {        

        $loggedUser = Yii::$app->session->get('user_id');

        $openticketsDataOfUser = Tickets::find()
            ->where(['reporter' => $loggedUser])
            ->andWhere(['in','ticket_status' , ['new','inprogress']])
            ->orderBy(['created_date' => SORT_DESC])
            ->all();

        $closedticketsDataOfUser = Tickets::find()
            ->where(['reporter' => $loggedUser])
            ->andWhere(['ticket_status' => 'closed'])
            ->limit(5)
            ->orderBy(['created_date' => SORT_DESC])
            ->all();            

        $nicFromReq = Yii::$app->request->get('nic');
        if ($nicFromReq != "") {
            $customerData = Customer::find()
            ->where(['id_number' => $nicFromReq])
            ->one();

            $ticketsData = Tickets::find()
            ->where(['customer_id' => $customerData->id])
            ->all();

            $commentsData = Comments::find()
            ->where(['ticket_id' => $ticketsData[0]['id']])
            ->all();
            return $this->render('dashboard', ['customerData' => $customerData, 'ticketsData' => $ticketsData, 'commentsData' => $commentsData,'openticketsDataOfUser'=>$openticketsDataOfUser,'closedticketsDataOfUser'=>$closedticketsDataOfUser]);
        }else{
            return $this->render('dashboard', ['openticketsDataOfUser'=>$openticketsDataOfUser,'closedticketsDataOfUser'=>$closedticketsDataOfUser]);
        }
      }
    }

    /**
     * <b>Save ticket</b>
     * 
     * @author Vikum
     * @since 2018-12-03
     */    
        public function actionSave() {

        
        $checkUserAvailability= Customer::getCustomerByNicNumber($_POST['id']);
        if (!$checkUserAvailability) {

            $data = array(
                'customer_name' => $_POST["name"],
                'contact_m' => $_POST["mobile"],
                'email' => $_POST["email"],
                'address' => $_POST["address"],
                'id_number' => $_POST["id"],
                'contact_o' => $_POST["other"],
                'pref_language' => $_POST["language"],
            );

            $insertCustomer = Customer::saveCustomer($data);
            $_POST['customer_id'] = $insertCustomer ;
            $this->insertTicket($_POST);
        } else {

            $this->insertTicket($_POST);
        }
    }

    public function insertTicket($ticketData) {

        $data = array(
            'ticket_number' => 'T-' . time(),
            'source' => $ticketData['source'],
            'status' => 'active',
            'priority' => $ticketData['priority'],
            'customer_id' => $ticketData['customer_id'],
            'assignee' => $ticketData['assignee'],
            'reporter' => Yii::$app->session->get('user_id'),
            'description' => $ticketData['description'],
            'ticket_status' => $ticketData['ticket_status'],
            'created_date' => date("Y-m-d H:i:s.")
        );

        $insert = Tickets::saveTicket($data);

        if ($insert) {
            echo $insert;
        } else {
            echo 0;
        }
    }

    public function actionCustomertickets(){
        $nicFromReq = Yii::$app->request->get('nic');
        if ($nicFromReq != "") {

            $customerData = Customer::find()
            ->where(['id_number' => $nicFromReq])
            ->one();

            $ticketsData = Tickets::find()
            ->where(['customer_id' => $customerData->id])
            ->orderBy(['created_date' => SORT_DESC])
            ->all();

            return $this->render('customerTicketsDiv', ['ticketsData' => $ticketsData]);
        }else{
            return $this->render('customerTicketsDiv', ['ticketsData' => []]);
        }
    }

    public function actionSearchbyticket(){
      $text = Yii::$app->request->get('searchText');
      $ticketsData = Tickets::searchByTicketNumber($text);
      return $this->render('customerTicketsDiv', ['ticketsData' => $ticketsData]);
    }


    public function actionSearchbynic(){
      $text = Yii::$app->request->get('searchText');
      $ticketsData = Tickets::searchByCustomerNic($text);
      return $this->render('customerTicketsDiv', ['ticketsData' => $ticketsData]);
    }


    public function actionSearchbyname(){
      $text = Yii::$app->request->get('searchText');
      $ticketsData = Tickets::searchByCustomerName($text);
      return $this->render('customerTicketsDiv', ['ticketsData' => $ticketsData]);
    }


    public function actionLoadticketinfo(){
      $id = Yii::$app->request->get('id');
      $ticketsData = Tickets::getTicketInformation($id);
      $createdUser = Call_center_user::getUserInformationById($ticketsData[0]['reporter']); 
      $assignedUser = Call_center_user::getUserInformationById($ticketsData[0]['assignee']);
      $ticketComments = Comments::getTicketComments($ticketsData[0]['id']);

      return $this->render('ticketContent',['ticketsData'=>$ticketsData,'createdUser'=>$createdUser,'assignedUser'=>$assignedUser,'ticketComments'=>$ticketComments]);
    }


    public function actionAddnewcomment(){
      $data = array(
        'ticket_id'=>Yii::$app->request->post('ticket_id'),
        'comment'=>Yii::$app->request->post('comment'),
        'createdDate'=>Yii::$app->request->post('createdDate'),
        'createdBy'=>Yii::$app->session->get('user_id')
      );
        $insert = Comments::saveComment($data);
        if ($insert) {
            echo $insert;
        } else {
            echo 0;
        }      
    }

    public function actionDeletecomment(){
      $commentId = $_GET['id'];
        $models = Comments::find()->where(['id'=> $commentId])->all();
        foreach ($models as $model) {
            $model->delete();
        }
        echo 1;      
    }


    public function actionResolveticket(){
      $ticketId = $_GET['ticket_id'];
            $ticket = Tickets::findOne($ticketId);
            $ticket->ticket_status = 'closed';
            $ticket->update();
        echo 1;           
    }
	 public function actionGetuseragent() {
        $array = array();
        $callCenterUser = new Call_center_user();
        $agent = $callCenterUser::getAllAgents();
        foreach ($agent as $row) {
            array_push($array,array(
            'id' => $row['id'],
            'name' => $row['name']
            ));
        }
        echo json_encode($array);
    }

    public function actionCheckopenticketsofid(){
      $nic = Yii::$app->request->get('typingText');
      $countopentickets = Tickets::getOpenTickets($nic);
      echo $countopentickets[0]['opentickets'];

    }       

}