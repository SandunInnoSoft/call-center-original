<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\Call_records;
use app\models\cdr;
use app\models\call_center_user;

/**
 * This controller is used to perform FTP operations with another remote server
 * 
 * @author Sandun
 * @since 2017-08-31
 */
class FtpController extends Controller {

    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        }
    }

    public function actionGetaudiofile() {
        $recordId = $_GET['id'];
        $direction = $_GET['direction'];
        $agentExtension = $_GET['extension'];
        $cdrUniqueId = Call_records::getCallRecordUniqueIdbyCallId($recordId);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';


//        $agentExtension = "2020";
//        $agentExtension = "800";

        if ($direction == "incoming") {
            $direction = 1;
        } else if ($direction == "outgoing") {
            $direction = 2;
        } else {
            $direction = 0;
        }

//        $fileName = $this->audioFileNameOfTheCall($cdrUniqueId, $agentExtension, $direction);
        $fileName = $this->getAudioFileName($cdrUniqueId, $agentExtension);
//        $fileName = "20170727-192929-2010-2020-1501163969.11-4";
//        $fileName = "20170919-174325-117221980-800-1505823203.75";
//        $fileName = "Linkin_Park_-_Battle_Symphony_lyrics";
        header('Content-Type: application/octet-stream');
        if ($ftpParams['fileExtension'] == "wav") {
            header('Content-Type: audio/wav');
        }if ($ftpParams['fileExtension'] == "gsm") {
            header('Content-Type: audio/x-gsm');
        }

//
//
        header('Cache-Control: no-cache');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
//        header('Content-Type: audio/x-gsm');
//        header("Content-disposition: attachment; filename=file.gsm");
//        ini_set("zlib.output_compression", "Off");
        $fileData = $this->getAudioFile($agentExtension, $fileName);
    }

    /**
     * <b>Returns the audio file from the remote server directory through SFTP</b>
     * <p></p>
     * 
     * @param type $agentExtension
     * @param type $fileName
     * @return type
     * @author Sandun
     * @since 2017-09-20
     */
    private function getAudioFile($agentExtension, $fileName) {
        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

//        $ipAddress = "10.100.21.238";
        $ipAddress = $ftpParams['serverIpAddress']; //"192.168.1.254";
        $ftpDirectory = $ftpParams['ftp_path'];
        $user = $ftpParams['username']; //"root";
        $pass = $ftpParams['password']; //"Password123";
//        $extension = "gsm";
        $extension = $ftpParams['fileExtension']; //"wav";
//        $filePath = "$ftpDirectory/$agentExtension/$fileName.$extension";
        $filePath = "$ftpDirectory/$agentExtension/$fileName";

        $c = curl_init("sftp://$user:$pass@$ipAddress:22/$filePath");
        curl_setopt($c, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }

    public function actionAudio() {
        return $this->render("testAudio");
    }

    private function audioFileNameOfTheCall($cdrUniqueId, $agentExtension, $direction) {
        if ($direction == 1) {
// This means incomming call
            $cdrData = cdr::getIncommingCdrDataByUniqueIdAndExtension($cdrUniqueId, $agentExtension);
        } else if ($direction == 2) {
// This means outgoing call
            $cdrData = cdr::getOutgoingCdrDataByUniqueIdAndExtension($cdrUniqueId, $agentExtension);
        }

        if ($cdrData != Null) {
// has the data array
            $fileName = "";
            $dateAndTime = $cdrData['start'];
            $dateAndTime = str_replace('-', "", $dateAndTime);
            $dateAndTime = str_replace(':', "", $dateAndTime);
            $dateAndTime = str_replace(' ', "-", $dateAndTime);
            $fileName = $dateAndTime; // Date and time

            $fileName .= "-" . $cdrData['src']; // source number of the call

            $fileName .= "-" . $cdrData['dst']; // destination number of the call

            $fileName .= "-" . $cdrData['uniqueid']; // unique ID

            $fileName .= "-" . $cdrData['duration']; // call duration



            return $fileName;
        } else {
// doesnt have the data array
            return false;
        }
    }

    public function actionFilename() {
        $uniqueId = '1505297916.49';
// 1505972540.88 , 805 = Test data 1 
// 1505825433.90 , 805 = Test data 2
// 1504240332.10, 807 = Test data 3
        $extension = '807';
        $caller = '117221980';
        echo 'File name will be: ' . $this->getAudioFileName($uniqueId, $extension);
    }

    /**
     * <b>Get Call audio file name</b>
     * 
     * @param type $uniqueId
     * @param type $exension     
     * 
     * @author Vikum
     * @since 2017-09-25
     */
    private function getAudioFileName($uniqueId, $exension) {
        if ($uniqueId != null && $exension != null) {
            $fileNames = $this->getFileNames($exension);
            $cdrRecord = cdr::getIncommingCdrDataByUniqueIdAndExtension($uniqueId, $exension);
            $cdrStartDate = $cdrRecord['start'];

            $dateStart = strtotime($cdrStartDate);
            $cdrStartDate = date('Y-m-d H:i:s', $dateStart);

            $cdrEndDate = $cdrRecord['end'];
            $cdrEndDate = strtotime($cdrEndDate);
            $cdrEndDate = date('Y-m-d H:i:s', $cdrEndDate);

            $fileName = '';
//            $cdrEndDate = $cdrRecord['end'];
            if (count($fileNames) > 0) {
                foreach ($fileNames as $key) {

                    $splitName = explode("-", $key);
                    if (count($splitName) > 2) {
                        $date = substr($splitName[0], 0, -4) . '-' . substr($splitName[0], 4, -2) . '-' . substr($splitName[0], 6);
                        $time = substr($splitName[1], 0, -4) . ':' . substr($splitName[1], 2, -2) . ':' . substr($splitName[1], 4);

                        $createdDate = $date . ' ' . $time; // Find Date from file name
                        $createdDateTime = strtotime($createdDate);
                        $createdDate = date('Y-m-d H:i:s', $createdDateTime);

                        if ($createdDate > $cdrStartDate && $createdDate < $cdrEndDate) {
                            $fileName = $key;
                            break;
                        }
                    } else {
                        continue;
                    }
                }
            }

            return $fileName;
        }
    }

    private function getFileNames($extension) { // This function suppose to return all file names from remote directory which named extension passed through parameters
//    public function actionGetaud() { // This function suppose to return all file names from remote directory which named extension passed through parameters
//        return array('20170921-103738-117221980-805-1505970454.68.wav', '20170921-105704-803-805-1505971624.79-4.wav', '20170921-111227-117221980-805-1505972541.89.wav', '20170921-111833-117221980-805-1505972905.117.wav');
//        $extension = '801';
        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

//        $ipAddress = "10.100.21.238";
        $url = $ftpParams['serverUrl']; //"192.168.1.254";
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $dataset = file_get_contents($url . $extension, false, stream_context_create($arrContextOptions));
//        print_r(json_decode($dataset, true));
        return json_decode($dataset, true);
    }
    
    private function getAudioFilePathFromNAS($callStartDatetime, $callType, $extensionNumber, $customerNumber){
       $dateTimeExplodedArray = explode(" ", $callStartDatetime);
       $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';
       $audioFileDirectoryfile = $ftpParams['NAS_audio_file_Location'];
       $date = $dateTimeExplodedArray[0];
       $date = str_replace("-", "_", $date);
       $audioFileDirectoryfile = str_replace("<date>", $date, $audioFileDirectoryfile);
       $audioFileDirectoryfile = str_replace("<extension>", $extensionNumber, $audioFileDirectoryfile);
       // echo "Audio directory file = $audioFileDirectoryfile";
       if(file_exists($audioFileDirectoryfile) == true){
            // directory exitsts
           $audioFileNamesArray = scandir($audioFileDirectoryfile);

            $callStartDatetime = str_replace("-", "", $callStartDatetime);
            $callStartDatetime = str_replace(":", "", $callStartDatetime);        
            $callStartDatetime = str_replace(" ", "-", $callStartDatetime);
            if($callType == "I"){
                // call type is incoming
                $audioFileNamePattern = "-".$customerNumber."-".$extensionNumber."-";
            }else{
                // call type is outgoing
                $audioFileNamePattern = "-".$extensionNumber."-".$customerNumber."-";
            }
            
            $matchingFileNamesArray = array();
            // echo "Pattern to search = $audioFileNamePattern <br>";
            foreach ($audioFileNamesArray as $key) {
                if(preg_match("/($audioFileNamePattern)/", $key) == 1){
                    // matching file name exists
                    array_push($matchingFileNamesArray, $audioFileDirectoryfile.$key);
                }
            }

            if(count($matchingFileNamesArray) == 0){
                // echo "No match found<br>";
                return false;
            }else{
                return $matchingFileNamesArray;
            }

       }else{
            // echo "directory does not exist<br>";
            // echo "<br>";
            // echo $audioFileDirectoryfile;
            // echo "<br>";
             return false;
       }
        
    }
    
    private function streamAudioFile($audioFilePath){
        header('Content-Type: audio/wav');


        header('Cache-Control: no-cache');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($audioFilePath));
        header('Accept-Ranges: bytes');
        header('Content-Disposition: inline; filename="HNBGI_recorded_audio.wav"');

        readfile($audioFilePath);
        // echo file_get_contents($audioFilePath);

    }

    /**
     * <b>Play audio file</b>
     * <p>Streams the audio file from the server to be captured and played by the HTML5 player in the client browser</p>
     * 
     * @author Sandun
     * @modified Sandun 2018-8-21
     * @description if GET parameter returnType is present and set to "Text", a text of "Found" will be 
     * displayed otherwise the audio file content will be returned
     * 
     */
    public function actionPlayaudiofile(){
        $dateTime = $_GET['datetime'];//Yii::$app->request->get("datetime");
        $dateTime = str_replace("+", " ", $dateTime);
        $callType = $_GET['callType'];//Yii::$app->request->get("callType");
        $extensionNumber = $_GET['ext'];//Yii::$app->request->get("ext");
        $customerNumber = $_GET['phoneNumber'];//Yii::$app->request->get("phoneNumber");
        $returnType = Yii::$app->request->get("returnType");
        $acctId = Yii::$app->request->get("acctId");
        
        $fromTimeUnix = strtotime("-2 hours");
        $currentTimeUnix = strtotime("now");
        $audioFileTimeUnix = strtotime($dateTime);
        
        if($audioFileTimeUnix >= $fromTimeUnix && $audioFileTimeUnix <= $currentTimeUnix){
            // is within 2 hours time
            $callStartDateTimeExploded = explode(" ", $dateTime);
            $callStartDate = $callStartDateTimeExploded[0];
            $callStartTime = $callStartDateTimeExploded[1];
            if($callType == "I"){
                $callTypeForDownload = "incoming";
            }else{
                $callTypeForDownload = "outgoing";
            }
            $downloadResponse = $this->downloadAudioFileFromPbx($callStartDate, $callStartTime, $callTypeForDownload, $extensionNumber, $customerNumber);
        }
        
        

        $audioFilePathArray = $this->getAudioFilePathFromNAS($dateTime, $callType, $extensionNumber, $customerNumber);
        if($audioFilePathArray != false){
            // audio file exists
                $selectedAudioFilePath = $this->locateMatchingFilePath($audioFilePathArray, $dateTime);
                if($selectedAudioFilePath != false){
                    if($returnType == NULL){
                        // echo "File found in NAS";
                        $this->streamAudioFile($selectedAudioFilePath);
                    }else{
                        if($returnType == "Text"){
                            echo "Available";
                        }
                    }
                }else{
                    echo "Unable to find the match";
                }
            // }
        }else{
            //audio file doesnt exist in NAS
            if($returnType == NULL){
                $callStartDateTimeExploded = explode(" ", $dateTime);
                $callStartDate = $callStartDateTimeExploded[0];
                $callStartTime = $callStartDateTimeExploded[1];
                if($callType == "I"){
                    $callTypeForDownload = "incoming";
                }else{
                    $callTypeForDownload = "outgoing";
                }
                $downloadResponse = $this->downloadAudioFileFromPbx($callStartDate, $callStartTime, $callTypeForDownload, $extensionNumber, $customerNumber);
                if(strpos($downloadResponse, "Audio_file_downloaded") !== false){
                    // file downloaded
                    $audioFilePathArray = $this->getAudioFilePathFromNAS($dateTime, $callType, $extensionNumber, $customerNumber);
                    if($audioFilePathArray != false){
                        // audio file exists
                        if(count($audioFilePathArray) == 1){
                            $this->streamAudioFile($audioFilePathArray[0]);
                        }else{
                            // multiple matches exists
                            $selectedAudioFilePath = $this->locateMatchingFilePath($audioFilePathArray, $dateTime);
                            if($selectedAudioFilePath != false){                            
                                    $this->streamAudioFile($selectedAudioFilePath);
                            }else{
                                echo "Unable to find the match after downloading";
                            }
                        }
                    }else{
                        echo "Audio file doest exists after download";
                    }

                }
            }else{
                if($returnType == "Text"){
                    echo "Not_found_in_NAS";
                }
            }
        }
    }

    public function actionGet_call_tranfer_information(){
        $acctId = Yii::$app->request->get("acctId");
        $cdrInfo = cdr::getCdrInfoById($acctId);
        if($cdrInfo){
            $uniqueId = $cdrInfo["uniqueid"];
            $startTime = $cdrInfo["start"];
            $endTime = $cdrInfo["end"];
            $sourceNumber = $cdrInfo["src"];
            $dstNumber = $cdrInfo["dst"];

            // $transferedCallsArray = cdr::nextAnsweredExtensionOfuniqieId($uniqueId, $endTime);
            $transferedCallsArray = cdr::allAnsweredExtensionsInfoOfSameCall($uniqueId);


            if(count($transferedCallsArray) > 0){
                // has next transfered calls
                $transferedCallsProcessedArray = [];
                for($x = 0; $x < count($transferedCallsArray); $x++){

                    $agentInfo = call_center_user::getUserInformationFromVOIPextension($transferedCallsArray[$x]['dst']);

                    if(isset($agentInfo['voip'])){
                        $agentName = $agentInfo['name'];
                    }else{
                        $agentName = "-";
                    }

                    $transferRecordInfo = [
                        "callerNumber" => $transferedCallsArray[$x]['src'],
                        "startedTime" => $transferedCallsArray[$x]['start'],                                            
                        "answeredTime" => $transferedCallsArray[$x]['answer'],
                        "dst" => $transferedCallsArray[$x]['dst'],
                        "agentName" => $agentName,
                        "endTime" => $transferedCallsArray[$x]['end'],
                        "duration" => $transferedCallsArray[$x]['duration'],
                    ];

                    array_push($transferedCallsProcessedArray, $transferRecordInfo);
                }

                $responseDataArray = [
                    'callState' => "Available",
                    'nextTransferedCalls' => $transferedCallsProcessedArray,
                    "nextTransferedCallsCount" => count($transferedCallsProcessedArray)
                ];
            }else{
                // no next transfered calls
                $previousAnsweredCalls = cdr::previousAnsweredExtensionOfUniqueId($uniqueId, $startTime);
                if(count($previousAnsweredCalls) > 0){
                    // last one of a transfered call
                    $responseDataArray = [
                        'callState' => "Available",
                        "transferState" => "Last one of a transfered call",
                        'nextTransferedCalls' => [],
                        "nextTransferedCallsCount" => 0
                    ];
                }else{
                    // no next or previous transfered calls records, may have possible files

                    $transferredCalls = $this->checkFilesForPossibleTransfers($cdrInfo["src"], $cdrInfo["dst"], $cdrInfo["end"]);


                    $responseDataArray = [
                        'callState' => "Available",
                        "transferState" => "no next or previous transfer call records, files scan",
                        'nextTransferedCalls' => $transferredCalls,
                        "nextTransferedCallsCount" => count($transferredCalls)
                    ];


                }

            }
            echo Json::encode($responseDataArray);
        }else{
            $responseDataArray = [
                'callState' => "Available",
                "transferState" => "has no cdr info",
                'audioFilePath' => $selectedAudioFilePath,
            ];
            echo Json::encode($responseDataArray);
        }
    }


    private function checkFilesForPossibleTransfers($externalNumber, $answeredExtension,  $callEndedTime){
        $searchToTime = date("Y-m-d H:i:s", strtotime("+10 minutes", strtotime($callEndedTime)));

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';
        $audioFileDirectoryfile = $ftpParams['NAS_audio_file_Location'];
        $fromDateExploded = explode(" ", $callEndedTime);
        $toDateExploded = explode(" ", $searchToTime);

        $returnPossibleTransfersInfo = array();

        // if($fromDateExploded[0] == $toDateExploded[0]){
            // both from and to times are in the same day
            
            $date = $fromDateExploded[0];
            $date = str_replace("-", "_", $date);
            $audioFileDirectoryfile = str_replace("<date>", $date, $audioFileDirectoryfile);
            $audioFileDirectoryfile = str_replace("<extension>/", "", $audioFileDirectoryfile);

            if(file_exists($audioFileDirectoryfile) == true){
                // directory exitsts
               $audioFileNamesArray = scandir($audioFileDirectoryfile);
                
                for($x = 0; $x < count($audioFileNamesArray); $x++){
                    $extension = $audioFileNamesArray[$x];
                    if(strlen($extension) > 2){
                        // its an extension folder
                        $audioFileDirectoryExtensionPath = $audioFileDirectoryfile.$extension;
                        $audioFileInExtension = scandir($audioFileDirectoryExtensionPath);
                        for($y = 0; $y < count($audioFileInExtension); $y++){
                            $audioFileName = $audioFileInExtension[$y];
                            if(strlen($audioFileName) > 2){
                                $audioFileNameExploded = explode("-", $audioFileName);
                                if(strpos($externalNumber, $audioFileNameExploded[2]) == TRUE){
                                    // has a match with an extension call
                                    if($audioFileNameExploded[2] != $answeredExtension){
                                        // not the same extension as currently playing file

                                        $fileDate = substr($audioFileNameExploded[0], 0, -4) . '-' . substr($audioFileNameExploded[0], 4, -2) . '-' . substr($audioFileNameExploded[0], 6);
                                        $fileTime = substr($audioFileNameExploded[1], 0, -4) . ':' . substr($audioFileNameExploded[1], 2, -2) . ':' . substr($audioFileNameExploded[1], 4);
                                        
                                        $dateFromTimeUnixStamp = strtotime($callEndedTime);
                                        $dateToTimeUnixStamp = strtotime($searchToTime);
                                        $audioFileDateUnixStamp = strtotime($filedate." ".$fileTime); 
                                        
                                        if($dateFromTimeUnixStamp <= $audioFileDateUnixStamp && $dateToTimeUnixStamp >= $audioFileDateUnixStamp){
                                            // audio file match found
                                            $audioFileDataArray = [
                                                "callerNumber" => $externalNumber,
                                                "startedTime" => date("Y-m-d H:i:s", $audioFileDateUnixStamp),                                            
                                                "answeredTime" => "-",
                                                "dst" => $audioFileNameExploded[3],
                                                "agentName" => "-",
                                                "endTime" => "-",
                                                "duration" => "-",
                                            ];

                                            array_push($returnPossibleTransfersInfo, $audioFileDataArray);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }    
           }else{
                 return false;
           }

        // }else{
            // from and to dates are in different dates
        // }

        return $returnPossibleTransfersInfo;

    }

    private function locateMatchingFilePath($audioFilePathArray, $datetimeToMatch){
        $explodedDateTime = explode(" ", $datetimeToMatch);
        $dateToMatch = $explodedDateTime[0];
        $timeToMatch = $explodedDateTime[1];
        $unixTimestampToMatch = strtotime($datetimeToMatch);
        $unixTimestampCompareFrom = $unixTimestampToMatch - 5;
        $unixTimestampCompareTo = $unixTimestampToMatch + 60;
        $foundFiles = array();
        for($x = 0; $x < count($audioFilePathArray); $x++){
            $explodedAudioFileFullPath = explode("/", $audioFilePathArray[$x]);
            $audioFileName = end($explodedAudioFileFullPath);
            $fileDateTime = $this->extractDatetimeFromAudioFileName($audioFileName);
            $unixTimestampOfFileDateTime = strtotime($fileDateTime);
            if($unixTimestampOfFileDateTime >= $unixTimestampCompareFrom && $unixTimestampOfFileDateTime <= $unixTimestampCompareTo){
                // match found
                array_push($foundFiles, $audioFilePathArray[$x]);
//                return $audioFilePathArray[$x];
            }
        }
        
        if(count($foundFiles) == 1){
            // one match found
            return $foundFiles[0];
        }else if(count($foundFiles) > 1){
            // has more than 1 match
            $largestFile = $foundFiles[0];
            for($x = 1; $x < count($foundFiles) - 1; $x++){
                if(filesize($foundFiles[$x]) > filesize($largestFile)){
                    $largestFile = $foundFiles[$x];
                }
            }
            
            return $largestFile;
        }else{
            return false;   
        }
    }
    

    private function extractDatetimeFromAudioFileName($audioFileName){
            $audioFileNameExploded = explode("-", $audioFileName);
            $dateSection = $audioFileNameExploded[0];
            $timeSection = $audioFileNameExploded[1];

            $dateSplittedString = str_split($dateSection, 2);
            $dateTime = $dateSplittedString[0].$dateSplittedString[1]."-".$dateSplittedString[2]."-".$dateSplittedString[3]." ";

            $timeSplittedString = str_split($timeSection, 2);
            $dateTime = $dateTime.$timeSplittedString[0].":".$timeSplittedString[1].":".$timeSplittedString[2];
            return $dateTime;
    }

    public function actionTestaudiofile(){
        $audioFileName = "20180112-140319-112586104-4380-1515745997.23958.wav";
        echo $this->extractDatetimeFromAudioFileName($audioFileName);
    }
    
    public function actionTestcmd(){
//        exec("java -version 2>&1", $output);
//        print_r($output);
        
        echo "<br>";
        
        echo shell_exec("cd F:\\xampp\\htdocs\\hnbtestgeneralcc\\web\\onDemandFetch && java -jar findfilePBXSftp.jar '2018-08-15' '10:40:54' 'incoming' '2080' '114389212' 2>&1");
    }

    /**
     * <b>Invokes ondemand audio fetch jar</b>
     * <p>This function invokes the on demand audio fetch jar with parameters to download the file to the NAS and to return the shell response to identify
     *    if the download was a success</p>
     * 
     * @author Sandun
     * @since 2018-08-17
     * 
     * @param type $callStartDate
     * @param type $callStartTime
     * @param type $callType
     * @param type $extension
     * @param type $outSideNumber
     * @return String shell execution response
     */
    private function downloadAudioFileFromPbx($callStartDate, $callStartTime, $callType, $extension, $outSideNumber){
        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

        $command = $ftpParams['JRE_HOME']." -jar ".$ftpParams['onDemandDownloaderJar']." ";        
        $command .= "$callStartDate $callStartTime $callType $extension $outSideNumber 2>&1";

        return shell_exec($command);
    }
}
