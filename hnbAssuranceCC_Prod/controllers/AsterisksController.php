<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\web_presence;
use app\models\call_queue;
use app\models\call_forwards;
use app\models\abandon_calls;
use app\models\Call_records;
use app\models\cdr;
use app\models\loop_monitor;

/**
 * This cotroller is used to perform the PBX asterisk operations by listening to the responses made from the PBX console
 *
 * @author Sandun
 * @since 2017-09-13
 */
class AsterisksController extends Controller {

    public function actionResponses() {

        set_time_limit(-1);

        ini_set("default_socket_timeout", -1);

        $pbxConsoleLoginParams = require(__DIR__ . '/../config/pbxConsoleLogin.php');

        $socket = fsockopen($pbxConsoleLoginParams['serverAddress'], $pbxConsoleLoginParams['port']);
        fputs($socket, "Action: Login\r\n");
        fputs($socket, "Username: " . $pbxConsoleLoginParams['username'] . "\r\n");
        fputs($socket, "Secret: " . $pbxConsoleLoginParams['password'] . "\r\n\r\n");

        //-----------------------------------------------------------------------------

        $event = "";
        while ($ret = fgets($socket)) {
            if (substr($ret, 0, 6) == "Event:") {
                $e = explode(':', $ret);
                $event = trim($e[1]);
            }



            //--------------------------------------DeviceStateChange ----------------------------------------------------	

            if ($event == "DeviceStateChange") {
                $data = explode(':', $ret);
                $device = "";
                
                if ($data[0] == "Timestamp") {
                    $ts = floor(trim($data[1]));
                }
                //$device = "";
                if ($data[0] == "Device" && substr(trim($data[1]), 0, 3) == 'SIP') {
                    $d = explode('/', trim($data[1]));
                    $dev = trim($d[1]);
                    $device = "";

                    if (is_numeric($dev)) {
                        $device = $dev;
                    }
                }

                if ($data[0] == "State" && $device != "") {
                    $state = trim($data[1]);

                    if ($state == "NOT_INUSE") {
                        //Clear CID fields and update presence state
//                    $sql = "update callevents.web_presence set state='$state',cidnum = NULL, cidname = NULL, inorout = NULL, callstart = NULL where ext='$device'";
//                    mysqli_query($web, $sql);
                        web_presence::updatePresenceStateForNotInUse($state, $device);
                    } else {
                        //Update presence state
//                    $sql = "update callevents.web_presence set state='$state' where ext='$device'";
//                    mysqli_query($web, $sql);
                        web_presence::updatePresenceStateForOtherStates($state, $device);
                    }


                    $event = "";
                    $device = "";
                }
            }

            //-------------------------------------- End of DeviceStateChange ----------------------------------------------------	
            //---------------------Call Queue ------------------------------------------------------

            if ($event == "NewCallerid") {
                $data = explode(':', $ret);
                $cidnum = "";

                if ($data[0] == "ChannelState") {
                    $cState = trim($data[1]);
                }

                if ($data[0] == "CallerIDNum") {
                    $cidnum = trim($data[1]);
                }


                //   if($cState == "6" && $cidnum != "")

                if ($cidnum != "" && $cState == "6") {
//		 if(!checkQueueRecordExist($cidnum)){
//                $sql = "insert into callevents.call_queue (number) values ('$cidnum')";
//                mysqli_query($web, $sql);
                    call_queue::addToCallQueue($cidnum);
//		}
                }
                //$cState = "";
            }

            // ------------------- End of Call Queue -------------------------
            //---------------------------------------------Hangup------------------------------------------------

            if ($event == "Hangup") {
                $data = explode(':', $ret);


                // if($data[0] == "ChannelState"){
                //     $cState = trim($data[1]);		
                //	}
                $cidnamee = "";
                $cidnum = "";
                //if($data[0] == "CallerIDNum"){
                // $cidnum = trim($data[1]);
                //}
                if ($data[0] == "CallerIDName") {
                    $cidnamee = trim($data[1]);
                }
                if ($data[0] == "Timestamp") {
                    $ts = floor(trim($data[1]));
                    $ts = trim($data[1]);
                }

                if ($data[0] == "CallerIDNum") {
                    $cidnum = trim($data[1]);
                }

                if ($data[0] == "ConnectedLineNum") {
                    $connum = trim($data[1]);
                }

                if ($data[0] == "Cause-txt") {
                    $cause = trim($data[1]);

                    $numbersCount = strlen($connum);
                    if ($cause == "Call Rejected" && $connum != "" && $numbersCount > "6") {

//                    $sql = "insert into callevents.abandon_calls (agent_id,caller_number,timestamp) values ('$cidnum','$connum','$ts')";
//                    mysqli_query($web, $sql);
                        abandon_calls::insertAnAbandantCall($cidnum, $connum, $ts);
                    }
                }



                //   $cState == 6 && $cidnum != "" && 

                if ($cidnum == $cidnamee) {
//                $sql = "DELETE FROM callevents.call_queue where number='$cidnum'";
//                mysqli_query($web, $sql);
                    call_queue::deleteCallRecordFromQueue($cidnum);
                }
                //$cidnum = "";
            }

            // ---------------- End of hungup ----------------------
            //-------------------------------------- DialBegin ----------------------------------------------------  

            if ($event == "DialBegin") {
                $data = explode(':', $ret);
                //$cidnum = "";

                if ($data[0] == "Timestamp") {
                    $ts = floor(trim($data[1]));
                }

                if ($data[0] == "Channel") {
                    $c = explode('/', trim($data[1]));
                    $c2 = explode('-', trim($c[1]));
                    $channel = trim($c2[0]);
                }

                if ($data[0] == "CallerIDNum") {
                    $cidnum = trim($data[1]);
                }

                if ($data[0] == "CallerIDName") {
                    $cidname = trim($data[1]);
                }
                if ($data[0] == "ChannelState") {
                    $chnldesc = trim($data[1]);
                }

                if ($data[0] == "DialString") {
                    if (substr(trim($data[1]), 0, 3) == 'SIP' || is_numeric(trim($data[1]))) {
                        if (is_numeric(trim($data[1]))) {
                            $exten = trim($data[1]);
                        } else {
                            $e = explode('/', trim($data[1]));
                            $exten = trim($e[1]);
                        }

                        //Update inbound presence call
//                    $sql = "update callevents.web_presence set cidnum = '$cidnum', cidname = '$cidname', inorout='I', callstart='$ts' where ext='$exten' and cidnum is null";
//                    mysqli_query($web, $sql);
//
//
//                    $sql = "update callevents.web_presence set cidnum = '$exten', inorout='O', callstart='$ts' where ext='$channel' and cidnum is null";
//                    mysqli_query($web, $sql);
                        web_presence::updateInboundPresenceCall($cidnum, $cidname, $ts, $exten, $channel);




//----------------------------------------Delete Queue Answered calls from the list-------------------------------------------------
//                    $sql = "DELETE FROM callevents.call_queue where number='$cidnum'";
//                    mysqli_query($web, $sql);
                        call_queue::deleteCallRecordFromQueue($cidnum);

//-----------------------------------------------------------------------------------------------------------------------------------				
                    } else {
                        $e = explode('@', trim($data[1]));
                        $dialed = trim($e[0]);

                        if ($channel != 'gateway') {
                            //Update outbound presence call
//                        $sql = "update callevents.web_presence set cidnum = '$dialed', inorout='O', callstart='$ts' where ext='$channel'";
//                        mysqli_query($web, $sql);
                            web_presence::updateOutboundPresenceCall($dialed, $ts, $channel);

//                        $sql = "DELETE FROM callevents.call_queue where number='$cidnum'";
//                        mysqli_query($web, $sql);
                            call_queue::deleteCallRecordFromQueue($cidnum);
                        }
                    }
                    $numbersCount = strlen($cidnum);           //Characters more than 7 
                    if ($cidnum != "" && $numbersCount > "6") {

                        //if($cidnum != ""){								//For Any number of Characters 	

                        $time = date("Y-m-d H-i-s");
//                    $sql = "insert into callevents.call_forwards (caller_number,agent_extension,time_stamp,created_date,state) values ('$cidnum','$exten','$ts','$time','1')";
//                    mysqli_query($web, $sql);
                        call_forwards::insertForwardedCall($cidnum, $exten, $ts, $time);
                    }

                    $event = "";
                    $exten = "";
                }
            }
            // ------------ End of Dial begin -----------------------
            //-------------------------------------- UnParkedCall ----------------------------------------------------   

            if ($event == "UnParkedCall") {
                $data = explode(':', $ret);

                if ($data[0] == "Timestamp") {
                    $ts = floor(trim($data[1]));
                }

                if ($data[0] == "RetrieverChannel") {
                    $c = explode('/', trim($data[1]));
                    $c2 = explode('-', trim($c[1]));
                    $channel = trim($c2[0]);
                }

                if ($data[0] == "ParkeeCallerIDNum") {
                    $cidnum = trim($data[1]);
                }

                if ($data[0] == "ParkeeCallerIDName") {
                    $cidname = trim($data[1]);
                }

                if ($data[0] == "ParkingSpace") {
                    $dialed = trim($data[1]);

                    $pickup = "$cidnum ($dialed)";

                    //Update outbound presence call
//                    $sql = "update callevents.web_presence set cidnum = '$pickup', cidname='$cidname', inorout='O', state='INUSE', callstart='$ts' where ext='$channel'";
//                    mysqli_query($web, $sql);
                    web_presence::updateOutboundPresenceCallToInuse($pickup, $cidname, $ts, $channel);

//----------------------------------------Delete Queue Answered calls form the list----------------------------------------------------
//                    $sql = "DELETE FROM callevents.call_queue where number='$cidnum'";
//                    mysqli_query($web, $sql);
                    call_queue::deleteCallRecordFromQueue($cidnum);

//-------------------------------------------------------------------------------------------------------------------------------------							

                    $event = "";
                    $channel = "";
                }
            }



            // end of socket while loop
        }

        sleep(5);
        set_time_limit(30);
        exit;

        // end of action responses function
    }
    
    /*
     * This function will record call record in db
     * @author: Vikum
     * @since: 01/08/2017   
     * 
     * @modified Sandun 2017-09-26
     * @description Duplicated here from agent controller to be used with the endless loop,
     * AgentId is and caller number are recieved from GET variables
     */
        public function actionSavecallrecord() {
        $number = $_GET['callerNumber'];
        $agentId = $_GET['user_id'];
        if(isset($_GET['voipextension'])){
            // from hungup
            $voipExtension = $_GET['voipextension'];
            
            $cdrData = cdr::getMostRecentCdrRecordDataWithUniqueId($number, $voipExtension);
            $fullCallDuration = cdr::getRecordsDurationByUniqueId($cdrData[0]['uniqueid']);
            $callDuration = $cdrData[0]['duration'] - 17;
            $entireDuration = $fullCallDuration[0]['num'] - $callDuration;
            if ($entireDuration < 0) {
                $entireDuration = 0;
            }
            
            $time = microtime(true);
            $data = array(
                'call_date' => date("Y-m-d H:i:s"), // This need to change to get called date
                'call_time' => date("Y-m-d H:i:s"), // This need to change to get called date
                'caller_number' => $number,
                'cli_number' => $number,
                'user_id' => $agentId,
                'active' => 1,
                'created_date_time' => date("Y-m-d H:i:s."),
                'timestamp' => $time,
                'uniqueid' => $cdrData[0]['uniqueid'],
                'call_waiting_duration' => $entireDuration,
                'call_end_time' => $cdrData[0]['end']
            );
            $insert = Call_records::insertNewCallRecordForHungUpCall($data);
            if ($insert) {
                echo $insert;
            } else {
                echo 0;
            }        
            
            
        }else{
            // from answer
            if($this->checkIfAnotherRecordNotExists($number, $agentId) == TRUE){
                $time = microtime(true);
                $data = array(
                    'call_date' => date("Y-m-d H:i:s"),
                    'call_time' => date("Y-m-d H:i:s"),
                    'caller_number' => $number,
                    'cli_number' => $number,
                    'user_id' => $agentId,
                    'active' => 1,
                    'created_date_time' => date("Y-m-d H:i:s."),
                    'timestamp' => $time
                );
                $insert = Call_records::insertNewCallRecord($data);
                if ($insert) {
                    echo $insert;
                } else {
                    echo 0;
                }                
            }else{
                $this->writeToCallLog("More Records were found for caller : $number, agent : $agentId and date : ".date("Y-m-d H:i:s"));
            }
        }

    }
    
    private function checkIfAnotherRecordNotExists($callerNumber, $userId){
        $sameRecords = Call_records::find()
                ->where("caller_number = '$callerNumber'")
                ->andWhere("user_id = $userId")
                ->andWhere("UNIX_TIMESTAMP(call_date) > ".time() - 5)
                ->andWhere("UNIX_TIMESTAMP(call_date) < ".time())
                ->count();
        if($sameRecords == 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    /*
     * This function will record call hung up in a call record
     * @author: Vikum
     * @since: 01/08/2017  
     * 
     * @modified Sandun 2017-09-26
     * @description Duplicated here from agent controller to be used with the endless loop   
     */
    public function actionHungupcallrecord() {
        $id = $_GET['callerId'];
        $agentExtension = $_GET['voipExtension'];
        $callerRecord = Call_records::getCallRecordFormId($id);
        $answeredTimestamp = $callerRecord['timestamp'];
        $callerNumber = $callerRecord['cli_number'];
        $callDuration = microtime(true) - $answeredTimestamp;
//        sleep(2);
        $uniqueId = cdr::getMostRecentUniqueId($callerNumber, $agentExtension);
        if ($uniqueId) {
            $fullCallDuration = cdr::getRecordsDurationByUniqueId($uniqueId[0]['uniqueid']);
            $entireDuration = $fullCallDuration[0]['num'] - $callDuration;
            if ($entireDuration < 0) {
                $entireDuration = 0;
            }
            $data = array(
                'cdr_unique_id' => $uniqueId[0]['uniqueid'],
                'call_waiting_duration' => $entireDuration,
                'call_end_time' => date("Y-m-d H:i:s")
            );
            $update = Call_records::updateCallRecord($data, $id);
            if ($update) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            $this->writeToCallLog("Unique id find missed for caller - $callerNumber : agent - $agentExtension");
            echo 0;
        }
    }
    
    /**
     * <b>Checks if the last saved timestamp is null or the time interval is greater than 5 seconds, if so invokes the endless loop</b>
     * @author Sandun
     * @since 2017-09-27
     * @modified Sandun 2017-10-02
     * @description Changed the interval check to see if the function stopped for more than 3 minutes
     */
    public function actionCheck_endless_loop_execution(){
        $timestamp = loop_monitor::getLastTimestamp();
        $currentTimestamp = time();
        $interval = $currentTimestamp - $timestamp;
        if($timestamp == NULL || $interval > 60){
            // timestamp is null or time interval is greater than 60 seconds
            echo 1;
            $this->invoke_endless_loop();
        }else{
            echo 0;
        }
    }
    
    public function actionCheck_answercall_endless_loop_execution(){
        $timestamp = loop_monitor::getLastAnswerLoopTimestamp();
        $currentTimestamp = time();
        $interval = $currentTimestamp - $timestamp;
        if($timestamp == NULL || $interval > 60){
            // timestamp is null or time interval is greater than 60 seconds
            echo 1;
            $this->invoke_answercall_endless_loop();
        }else{
            echo 0;
        }
    }
    
    public function actionCheck_hungupcall_endless_loop_execution(){
        $timestamp = loop_monitor::getLastHungupLoopTimestamp();
        $currentTimestamp = time();
        $interval = $currentTimestamp - $timestamp;
        if($timestamp == NULL || $interval > 60){
            // timestamp is null or time interval is greater than 60 seconds
            echo 1;
            $this->invoke_hungupcall_endless_loop();
        }else{
            echo 0;
        }
    }    
    
    
    /**
     * <b>Invokes the endless loop</b>
     * <p>This function invokes the endless loop used to monitor the events triggered from the PBX</p>
     * 
     * @author Sandun
     * @since 2017-09-27
     * 
     */
    private function invoke_endless_loop(){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://localhost:90/hnbassurancecc/asterisknew/index.php",
                CURLOPT_USERAGENT => 'Endless loop cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        
        echo $resp;
    }
    
    private function invoke_answercall_endless_loop(){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://localhost:90/hnbassurancecc/asterisknew/answerCallDetector.php",
                CURLOPT_USERAGENT => 'Endless loop cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        
        echo $resp;
    }
    
    private function invoke_hungupcall_endless_loop(){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "http://localhost:90/hnbassurancecc/asterisknew/hungUpCallDetector.php",
                CURLOPT_USERAGENT => 'Endless loop cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        
        echo $resp;
    }    


    private function writeToCallLog($string){
        $logEntry = $string;
        $myfile = fopen("calllog.txt", "a") or die("Unable to open file!");
        fwrite($myfile, date("Y-m-d H:i:s"));
        fwrite($myfile, "\n");
        fwrite($myfile, $logEntry);
        fwrite($myfile, "\n");
        fwrite($myfile, "---------------------------------------------------------------------------");
        fwrite($myfile, "\n");
        fclose($myfile);
        return true;
    }

    // end of asterisks controller class
}
