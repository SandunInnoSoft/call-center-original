<?php
use yii\helpers\Url;
namespace app\controllers;

/**
 * This controller is to perform administrator related action
 *
 * @author Sandun
 * @since 2018-1-3
 */
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\Json;
use app\models\web_presence;
use app\models\cdr;
use app\models\Call_channel;

class AdminController extends Controller {
    
    /**
     * 
     * @since 2018-1-3
     * @author Sandun
     */
    public function actionCheckextensionavailability(){
        $typingExtension = Yii::$app->request->get("typingExtension");
        if($typingExtension != NULL && web_presence::isExtensionAvailable($typingExtension) == TRUE){
            echo 1;
        }else{
            echo 0;
        }
    }
    
    /**
     * 
     * @author Sandun
     * @since 2018-07-11
     */
    public function actionCdrnullrecords(){
        $dateFrom = Yii::$app->request->get("dateFrom");
        $dateTo = Yii::$app->request->get("dateTo");
        
        if($dateFrom != NULL && $dateTo != NULL){
            // show null records
            $dateFrom = trim($dateFrom);
            $dateTo = trim($dateTo);
            $dstNullRecords = cdr::getDstNullRecordsBetweenDateRange($dateFrom, $dateTo);
            if(count($dstNullRecords) == 0){
                // No null records
                echo "No destination extension is empty records found between $dateFrom 00:00:00 and $dateTo 23:59:59";
            }else{
                echo "<h5>".count($dstNullRecords)." of destination empty call records were found between $dateFrom 00:00:00 and $dateTo 23:59:59</h5>";
                echo '<br>';
                echo '<table>';
                echo "<thead>";
                    echo "<tr>";
                        echo "<th style='text-align: center'>Caller Number</th>";
                        echo "<th style='text-align: center'>Start Date and Time</th>";
                        echo "<th style='text-align: center'>End Date and Time</th>";
                        echo "<th style='text-align: center'>Duration (Seconds)</th>";
                        echo "<th style='text-align: center'>Recieved channel</th>";
                    echo "</tr>";
                echo "<thead>";

                echo "<tbody>";
                    for($x = 0; $x < count($dstNullRecords) ; $x++){
                        if($dstNullRecords[$x]['calleenum'] != NULL && $dstNullRecords[$x]['calleenum'] != ""){
                            $channelInfo = Call_channel::getChannelInfoFromChannelNumber($dstNullRecords[$x]['calleenum']);   
                        }else{
                            $channelInfo['channel_name'] = "-";
                        }
                        echo "<tr>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['src']."</td>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['start']."</td>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['end']."</td>";
                            echo "<td style='text-align: center'>".$dstNullRecords[$x]['duration']."</td>";
                            echo "<td style='text-align: center'>".$channelInfo['channel_name']."</td>";
                        echo "<tr>";
                    }
                echo "<tbody>";
                echo '</table>';
            }
        }else{
            echo "Invalid datetime period!";
        }
    }

    /**
     * <b>Shows the information of answered calls which doesnt have audio files available in the NAS</b>
     * <p>This funtion displays a table with call record information of the answered call records within less than 3 days time
     * of the whole PBX which doesnt have an associated audio file in the NAS</p>
     * 
     * @author Sandun
     * @since 2018-08-09
     */
    public function actionMissingaudiofiles(){
        $dateFrom = Yii::$app->request->get("dateFrom");
        $dateTo = Yii::$app->request->get("dateTo");

        $dateFromObj = date("Y-m-d", strtotime($dateFrom));
        $dateToObj = date("Y-m-d", strtotime($dateTo));

        if($dateFrom == $dateTo){
            $difference = "1";
        }else{
            $dateDiff = date_diff(date_create($dateFrom." 00:00:00"), date_create($dateTo." 23:59:59"));
            $difference = $dateDiff->format("%d");
        }

        if($difference == false){
            echo "Incorrect date range";
        }else{
            if($difference > 3){
                echo "Time difference is too long. Make sure the date range is less than 3 days";
            }else{

                if($dateToObj == date("Y-m-d")){
                    // to date is today
                    $dateTo = date("Y-m-d H:i:s", strtotime("-1 hour"));
                    $dateTimeExploded = explode(" ", $dateTo);
                    $dateTo = $dateTimeExploded[0];
                    $timeTo = $dateTimeExploded[1];
                }else{
                    $timeTo = "23:59:59";
                }
                $incomingOrOutgoing = Yii::$app->request->get("direction");     
        
                if($incomingOrOutgoing == "O"){
                    // out going calls
                    echo "Outgoing calls";
                }else{
                    // incoming calls
        
                    if($dateFrom != NULL && $dateTo != NULL){
                        $dateFrom = trim($dateFrom);
                        $dateTo = trim($dateTo);
                        
                        $answeredCallrecords = cdr::getAgentAnsweredCallsDataBetweenDates($dateFrom, $dateTo, "00:00:00", $timeTo, 0, NULL, "Data", NULL, NULL);
                        
                        
                        if(count($answeredCallrecords) > 0){
                            ini_set("max_execution_time", 300);
                            $answeredMissingAudioFileRecords = $this->audioRecordsWithoutaudioFile($answeredCallrecords);
                            
            
                            echo "<h5>".count($answeredMissingAudioFileRecords)." of answered but missing audio file call records were found, out of ".count($answeredCallrecords)." answered calls between $dateFrom 00:00:00 and $dateTo 23:59:59</h5>";
                            echo '<br>';
                            echo '<table>';
                            echo "<thead>";
                                echo "<tr>";
                                    echo "<th style='text-align: center'>Caller Number</th>";
                                    echo "<th style='text-align: center'>Answered Extension</th>";                            
                                    echo "<th style='text-align: center'>Start Date and Time</th>";
                                    echo "<th style='text-align: center'>End Date and Time</th>";
                                    echo "<th style='text-align: center'>Duration (Seconds)</th>";
                                    echo "<th style='text-align: center'>Recieved channel</th>";
                                    echo "<th style='text-align: center'>Trunk</th>";                                    
                                    echo "<th style='text-align: center'>Response</th>";                        
                                echo "</tr>";
                            echo "<thead>";
            
                            echo "<tbody>";
                                for($x = 0; $x < count($answeredMissingAudioFileRecords) ; $x++){
                                    if($answeredMissingAudioFileRecords[$x]['calleenum'] != NULL && $answeredMissingAudioFileRecords[$x]['calleenum'] != ""){
                                        $channelInfo = Call_channel::getChannelInfoFromChannelNumber($answeredMissingAudioFileRecords[$x]['calleenum']);   
                                    }else{
                                        $channelInfo['channel_name'] = "-";
                                    }
                                    echo "<tr>";
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['src']."</td>";
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['dst']."</td>";                                
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['start']."</td>";
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['end']."</td>";
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['duration']."</td>";
                                        echo "<td style='text-align: center'>".$channelInfo['channel_name']."</td>";
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['trunk']."</td>";                                        
                                        echo "<td style='text-align: center'>".$answeredMissingAudioFileRecords[$x]['response']."</td>";                            
                                    echo "<tr>";
                                    ob_flush();
                                    flush();
                                }
                            echo "<tbody>";
                            echo '</table>';
                        }else{
                            echo "No answered call records between $dateFrom to $dateTo";
                        }
                    }else{
                        echo "Invalid parameters";
                    }
                }

            }
        }

        
    }

    /**
     * <b>Filters and selects only the answered call records which doesnt have audio files in NAS</b>
     * <p>This function searches the answered call records information passed as an array in the NAS to check if an audio file associated to it
     * is available in the NAS or not, if no audio file found for a record, adds to the returning data array</p>
     * 
     * @author Sandun
     * @since 2018-8-21
     * 
     * @param array $answeredcallRecordsArray 
     * @return array answeredButNoAudioFilesArray
     */
    private function audioRecordsWithoutaudioFile($answeredcallRecordsArray){
        $answeredButNoAudioFilesArray = array();
        for ($x = 0; $x < count($answeredcallRecordsArray); $x++){

            $dateTime = $answeredcallRecordsArray[$x]['start'];
            $dateTime = str_replace(" ", "+", $dateTime);
            $callType = "I";

            $extension = $answeredcallRecordsArray[$x]['dst'];
            $customerNumber = $answeredcallRecordsArray[$x]['src'];

            $response = $this->callPlayAudio($dateTime, $callType, $extension, $customerNumber);

            if($response != false){
                // dont have audio file in NAS and PBX
                $channelInfo = explode("-", $answeredcallRecordsArray[$x]['channel']);
                $trunk = (isset($channelInfo[0]) ? $channelInfo[0] : "-");
                $newArray = [
                    "response" => $response,
                    "src" => $answeredcallRecordsArray[$x]['src'],
                    "dst" => $answeredcallRecordsArray[$x]['dst'],                    
                    "start" => $answeredcallRecordsArray[$x]['start'],
                    "end" => $answeredcallRecordsArray[$x]['end'],
                    "duration" => $answeredcallRecordsArray[$x]['duration'],
                    "calleenum" => $answeredcallRecordsArray[$x]['calleenum'],
                    "trunk" => $trunk,                                    
                ];
                // $answeredcallRecordsArray[$x]["response"] = $response;
                array_push($answeredButNoAudioFilesArray, $newArray);
            }


        }

        return $answeredButNoAudioFilesArray;
    }

    /**
     * <b>Calls play audio controller function with text parameter</b>
     * <p>This function calls the play audio file controller function in FTP controller class through
     * a CURL request by passing the parameters as GET variables. If received reponse is not empty, not false 
     * and not equalt to the wording "Found", returns the response String, otherwise False</p>
     * 
     * @since 2018-08-22
     * @author Sandun
     * 
     * @param String $dateTime Date and time of cdr record
     * @param String $callType Incoming or Outgoing indication
     * @param String $extensionNumber Call answered or intiated extension
     * @param String $customerNumber Outside phone number which made or received the call
     * 
     * @return String/Boolean If true, received response String will be returned, otherwise False
     */
    private function callPlayAudio($dateTime, $callType, $extensionNumber, $customerNumber){
        // = $_GET['datetime'];//Yii::$app->request->get("datetime");
        $dateTime = str_replace(" ", "+", $dateTime);
        // = $_GET['callType'];//Yii::$app->request->get("callType");
        // = $_GET['ext'];//Yii::$app->request->get("ext");
        // = $_GET['phoneNumber'];

        $url = Url::base(true)."/index.php?r=ftp/playaudiofile&datetime=$dateTime&callType=$callType&ext=$extensionNumber&phoneNumber=$customerNumber&returnType=Text";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        curl_close($ch);

        if($response != false && $response != "" && strchr($response, "Available")){
            //have the audio file in NAS or PBX
            return false;
        }else{
            //doesnt have the audio file in NAS or PBX            
            return $response;
        }
    }
    
}
