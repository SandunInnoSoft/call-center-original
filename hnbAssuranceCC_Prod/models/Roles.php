<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
/**
 * Description of Roles
 *
 * @author Sandun
 * @since 2017-12-13
 * 
 */
class Roles extends ActiveRecord{
    
    /**
     * <b>Returns the one to many relation with the call_center_user model class to be used with joins</b>
     * @return Active Query object
     * 
     * @author Sandun
     * @since 2017-12-13
     */
    public function getCall_center_users() {
        return $this->hasMany(call_center_user::className(), ['role_id' => 'id']);
    }
}
