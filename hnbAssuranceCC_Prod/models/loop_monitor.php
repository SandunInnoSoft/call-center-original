<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is used to interract with loop_monitor table in DB3
 *
 * @author Sandun
 * @since 2017-09-27
 */
class loop_monitor extends ActiveRecord {

    /**
     * <b>Overrides and returns the Active record connection for DB3</b>
     * @author Sandun
     * @since 2017-09-27
     * @return DB3 Active record connection object
     */
    public static function getDb() {
        return Yii::$app->db3;
    }

    /**
     * <b>Sets current unix timestamp to timestamp field of the loop_monitor table</b>
     * 
     * @author Sandun
     * @since 2017-09-27
     * @return boolean
     */
    public static function setTimestamp() {
        $loop_monitor = loop_monitor::find()
                ->one();

        $loop_monitor->timestamp = time();
        return $loop_monitor->update();
    }

    /**
     * <b>Returns the timestamp from the loop_monitor table</b>
     * 
     * @author Sandun
     * @since 2017-09-27
     * @return string
     */
    public static function getLastTimestamp() {
        $timestamp = loop_monitor::find()
                        ->one();
        return $timestamp['timestamp'];
    }
    
    public static function setAnswerLoopTimestamp(){
        $loop_monitor = loop_monitor::find()
                ->one();

        $loop_monitor->answercallLoop = time();
        return $loop_monitor->update();        
    }
    
    public static function getLastAnswerLoopTimestamp(){
        $timestamp = loop_monitor::find()
                        ->one();
        return $timestamp['answercallLoop'];        
    }
    
    public static function setHungupLoopTimestamp(){
        $loop_monitor = loop_monitor::find()
                ->one();

        $loop_monitor->hungupcallloop = time();
        return $loop_monitor->update();        
    }
    
    public static function getLastHungupLoopTimestamp(){
        $timestamp = loop_monitor::find()
                        ->one();
        return $timestamp['hungupcallloop'];        
    }    

}
