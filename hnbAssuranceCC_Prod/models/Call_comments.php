<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is to interact with the call_comments database table
 *
 * @author Sandun
 * @since 2017-1-27
 */
class Call_comments extends ActiveRecord{
    
    /**
     * <b>Inserts a new comment</b>
     * 
     * @param type $uniqueid
     * @param type $comment
     * @param type $callerNumber
     * @param type $answeredExtension
     * @return int 
     * 
     * @author Sandun
     * @since 2017-11-27
     */
    public static function saveComment($uniqueid, $comment, $callerNumber, $answeredExtension){
        $newComment = new Call_comments();
        $newComment->cdr_unique_id = $uniqueid;
        $newComment->comment = $comment;
        $newComment->caller_number = $callerNumber;
        $newComment->answered_extension = $answeredExtension;
        $newComment->created_datetime = date("Y-m-d H:i:s");
        return $newComment->insert();
    }
    
    /**
     * <b>Returns the comment data of the call</b>
     * <p>This function returns the saved comment data of the answered call record matches with the unique id passed as the parameter</p>
     * @param string $uniqueId
     * @return array
     * 
     * @since 2017-11-27
     * @author Sandun
     */
    public static function getCommentDataFromUniqueId($uniqueId){
        return Call_comments::find()
                ->where("cdr_unique_id = '$uniqueId'")
                ->one();
    }
    
}
