<?php
/**
 * <b>This index php was created to redirect to the web/index.php file</b>
 * @author Sandun
 * @since 2017-10-02
 */

/**
 * <b>Returns the URL of this file location</b>
 * @return String
 * @author Sandun
 * @since 2017-10-02
 */
function url(){
  return sprintf(
    "%s://%s%s",
    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
    $_SERVER['SERVER_NAME'].":90",
    $_SERVER['REQUEST_URI']
  );
}

$systemUrl = url()."web/index.php";
header("Location: ".$systemUrl);
die();