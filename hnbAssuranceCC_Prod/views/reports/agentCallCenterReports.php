<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<style>
    thead{
        font-weight: bold;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;"  id="agentCallCenterReportsDIV">

    <style>
        .missed-calls-table th{
            width: 150px;
        }
    </style>
    <div class="row">
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Missed Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($missed_calls) ?></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($missed_calls) { ?>        
                    <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Caller Number
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php for($i = 0; $i < count($missed_calls); $i++){?>
                                <tr>
                                    <td><?=$missed_calls[$i]['date']?></td>
                                    <td><?=$missed_calls[$i]['time']?></td>
                                    <td><?=$missed_calls[$i]['caller_num']?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">IVR Received Calls</a>
                    <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($ivrCallsData) ?></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($ivrCallsData) { ?>        
                    <table class="table table-fixed table-striped missed-calls-table" style="border-radius: 5px; height: 300px; overflow-y: scroll; display: block">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date Time
                                </th>
                                <th>
                                    IVR
                                </th>
                                <th>
                                    Caller Number
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php for($i = 0; $i < count($ivrCallsData); $i++){?>
                                <tr>
                                    <td><?=$ivrCallsData[$i]['start']?></td>
                                    <td><?=$ivrCallsData[$i]['dst']?></td>
                                    <td><?=$ivrCallsData[$i]['src']?></td>
                                </tr>
                            <?php }?>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
    <br>
    <style type="text/css">
        .tableElementVMGapExtender{
            width: 50%;
        }
    </style>
        <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Received Voice Mail Calls</a>
                <a class="btn btn-md btn-info" href="#" style="margin-top: 3%"><?= count($vmData) ?></a>
            </div>
        </div>
        <div class="row navbar-default" style="border: 1px solid #cdcdcd">
            <?php if ($vmData) { ?>        
                <table id="vmCallsTable" class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                    <thead class="call-history-thead">
                        <tr>
                            <th class="tableElementVMGapExtender">
                                Caller Number
                            </th> 
                            <th class="tableElementVMGapExtender">
                                Voice Mail Number
                            </th>                      
                            <th class="tableElementVMGapExtender">
                                Answered Time
                            </th>
                            <th class="tableElementVMGapExtender">
                                End Time
                            </th>
                            <th class="tableElementVMGapExtender">
                                Duration
                            </th>
                        </tr>
                    </thead>
                    <tbody class="call-history-tbody">
                        <?php
                        for ($index = 0; $index < count($vmData); $index++) {

                           $seconds = $vmData[$index]['duration'];
    
                           $hours = floor($seconds / 3600);
                           $mins = floor($seconds / 60 % 60);
                           $secs = floor($seconds % 60);
    
                           $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                            ?>
                            <tr>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['src'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['dst'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['answer'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $vmData[$index]['end'] ?></td>
                                <td class="tableElementVMGapExtender"><?= $timeFormat ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
</div>

