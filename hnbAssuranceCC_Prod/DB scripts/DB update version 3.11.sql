# HNB Assurance Call Center DB update version 3.11 
# @author Sandun
# @since 2018-1-19

-- creates a new table to maintain the IVR numbers associated with an extension queue
CREATE TABLE `queue_ivr_number` (
`id`  int NOT NULL AUTO_INCREMENT,
`ivr_number`  varchar(20) NULL ,
`ext_queue_id`  int NULL ,
`description`  varchar(255) NULL ,
PRIMARY KEY (`id`)
);

