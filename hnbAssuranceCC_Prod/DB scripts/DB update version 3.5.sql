# HNB Assurance DB update version 3.5
# @author Sandun
# @since 2017-11-29'
# @target DB3 callevents

-- -------------- This script creates a new table in the callevents DB to record call answer time
CREATE TABLE `call_answer_times` (
`id`  int NOT NULL AUTO_INCREMENT ,
`caller_number`  int(11) NULL ,
`answered_extension`  int(6) NULL ,
`answered_time`  varchar(20) NULL ,
PRIMARY KEY (`id`)
);

-- ---------- This creates a unique composite key combining answered time and answered extension to not to include duplicates
ALTER TABLE call_answer_times
ADD CONSTRAINT answerTimesUniqueness UNIQUE (answered_time,answered_extension);
-- ---------------- END SCRIPT -----------------------------------------------------------------
