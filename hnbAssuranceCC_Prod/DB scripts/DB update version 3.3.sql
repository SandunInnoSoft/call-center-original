# HNB Assurance Call center DB update 3.3
# @author Sandun
# @since 2017-11-27

-- This script will create a new table in the main DB to maintain call comments

CREATE TABLE `call_comments` (
`id`  int NULL ,
`cdr_unique_id`  varchar(100) NULL ,
`comment`  varchar(500) NULL ,
`created_datetime`  varchar(30) NULL ,
`caller_number`  varchar(20) NULL ,
`answered_extension`  varchar(20) NULL 
)
;


-- -------------- added missing primary key auto increment field for comments -----

ALTER TABLE `call_comments`
MODIFY COLUMN `id`  int(11) NOT NULL AUTO_INCREMENT FIRST ,
ADD PRIMARY KEY (`id`);


