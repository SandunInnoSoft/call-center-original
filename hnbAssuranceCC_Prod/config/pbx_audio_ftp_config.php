<?php

return [
    'serverIpAddress' => '192.168.192.61', // Ip address of audio file ftp server
    'serverUrl'=>'https://192.168.192.61:9999/audioRecordingDirectoryScanner.php?ext=', // Path to get audio files 
    'ftp_path' => '/var/spool/asterisk/monitor/recording', // Path to audio file directory
    'username' => 'root', // FTP login user name
    'password' => 'Password@123', // FTP login password
    'fileExtension' => 'wav', // audio file extension
    'audioFilePath' =>'https://192.168.192.61:9999/', // PBX audio file path url
    'NAS_audio_file_Location' =>'E:/Zycoo/<date>/monitor/recording/<extension>/', // Audio file save location in the NAS
    'NAS_audio_file_Location_part_2' =>'/monitor/recording/', // Audio file save location in the NAS after the date directory
    'JRE_HOME' =>'C:\\Java\\bin\\java.exe', // JRE exe file path
    'onDemandDownloaderJar' =>'C:\\xampp\\htdocs\\hnbAssuranceCC\\web\\onDemandFetch\\SampleTestJarApp.jar', // On demand audio file downloader jar file path
];
