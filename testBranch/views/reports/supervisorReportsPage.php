<?php

use yii\helpers\Url;
use yii\helpers\Html;

$data_breaksFullData = $breakFullData;
$data_breaksFullData = json_encode($data_breaksFullData);

$data_breaksDailyData = $breakDailyData;
$data_answeredCallsData = [];
//print_r($breakDailyData);
//echo 'Right here';
$data_breaksDailyData = json_encode($data_breaksDailyData);

$others_answered_count = $all_answered_calls_count - $agent_answered_calls_count;
$thisMonthOthersAnsweredPercentage = 0;
if ($allCallsInTheMonth > 0) {
    $thisMonthOthersAnsweredPercentage = (($allCallsInTheMonth - $allCallsForAgentThisMonth) / $allCallsInTheMonth) * 100;
}
$thisMonthAgentAnsweredPercentage = 0;
if ($allCallsForAgentThisMonth > 0) {
    $thisMonthAgentAnsweredPercentage = ($allCallsForAgentThisMonth / $allCallsInTheMonth) * 100;
}
//$monthlyCallsData = array('0' => $allCallsInTheMonth, '1' => $allCallsForAgentThisMonth); //[$allCallsInTheMonth,$allCallsForAgentThisMonth];


$agent_answered_percnt = 0;
if ($agent_answered_calls_count != 0 && $all_answered_calls_count != 0) {
    $agent_answered_percnt = ($agent_answered_calls_count / $all_answered_calls_count) * 100;
}
$others_answered_percnt = 0;
if ($others_answered_count != 0 && $all_answered_calls_count != 0) {
    $others_answered_percnt = ($others_answered_count / $all_answered_calls_count) * 100;
}

$agent_answered_percnt = bcadd($agent_answered_percnt, '0', 2);
$others_answered_percnt = bcadd($others_answered_percnt, '0', 2);

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
    .panel-heading{
        font-size: 90%;
    }
    thead{
        font-weight: bold;
    }
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }

    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;"  id="agentReportsDIV">
    <div class="row" style="padding-top: 0%">
        <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2"><?= $agent_name ?> : <?= $agent_extension ?></div>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div><br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Break Reports</a> 
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Daily break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php if ($breakDailyData[0] != null || $breakDailyData[1] != null) { ?>
                                                    <td><?php
                                                        $work = ($breakDailyData[1] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
                                                        echo round($work, 2);
                                                        ?>%</td>
                                                    <td><?php
                                                        $break = ($breakDailyData[0] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
                                                        echo round($break, 2);
                                                        ?>%</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo convertToHoursMins($breakDailyData[1], '%02d hours %02d minutes'); ?></td>
                                                    <td><?php echo convertToHoursMins($breakDailyData[0], '%02d hours %02d minutes'); ?></td>
                                                </tr>
                                            <?php } else { ?>
                                            <td>0%</td>
                                            <td>0%</td>

                                            </tr>
                                            <tr>
                                                <td>00 hours 00 minutes</td>
                                                <td>00 hours 00 minutes</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_breaksDailyData ?>,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(200, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total break times this month comparison 
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php if ($breakFullData[0] != null || $breakFullData[1] != null) { ?>
                                                    <td><?php
                                                        $workM = ($breakFullData[1] / ($breakFullData[0] + $breakFullData[1])) * 100;
                                                        echo round($workM, 2);
                                                        ?>%</td>
                                                    <td><?php
                                                        $breakM = ($breakFullData[0] / ($breakFullData[0] + $breakFullData[1])) * 100;
                                                        echo round($breakM, 2);
                                                        ?>%</td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo convertToHoursMins($breakFullData[1], '%02d hours %02d minutes'); ?></td>
                                                    <td><?php echo convertToHoursMins($breakFullData[0], '%02d hours %02d minutes'); ?></td>
                                                </tr>
                                            <?php } else { ?>
                                            <td>0%</td>
                                            <td>0%</td>

                                            </tr>
                                            <tr>
                                                <td>00 hours 00 minutes</td>
                                                <td>00 hours 00 minutes</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: <?= $data_breaksFullData ?>,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(200, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>                
                </div>            
            </div>
        </div>
    </div><br>

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Service Level Figures</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Daily Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= $others_answered_percnt . '%' ?></td>
                                                <td><?= $agent_answered_percnt . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $others_answered_count . ' Calls' ?></td>
                                                <td><?= $agent_answered_calls_count . ' Calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Agent Answered calls %", "Others Answered calls %"],
                                datasets: [{
                                        //                            label: '# of Votes',
//                                        data: <? = $data_breaksFullData ?>,
                                        data: [<?= ($agent_answered_calls_count) ?>,<?= $others_answered_count ?>],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(54, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <!--<td><? = $others_answered_percnt . '%' ?></td>-->
                                                <td><?= round($thisMonthOthersAnsweredPercentage, 2) . '%' ?></td>
                                                <!--<td><? = $agent_answered_percnt . '%' ?></td>-->
                                                <td><?= round($thisMonthAgentAnsweredPercentage, 2) . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= ($allCallsInTheMonth - $allCallsForAgentThisMonth ) . ' Calls' ?></td>
                                                <td><?= $allCallsForAgentThisMonth . ' Calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                                        
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Agent Answered calls %", "Others Answered calls %"],
                                datasets: [{
//                                        label: '# of Votes',
//                                        data: <? = $data_breaksDailyData ?>,
//                                        data: <? = $monthlyCallsData ?>,
                                        data: [<?= $allCallsForAgentThisMonth ?>,<?= ($allCallsInTheMonth - $allCallsForAgentThisMonth ) ?>],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(54, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>            
            </div>
        </div>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Agent Answered Calls <span class="label label-primary" id="printAgentAnsweredCount">0</span></a>
        </div>
    </div>
    <style>
        .tableElementGapExtender{
            width: 25% !important;
        }

    </style>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($answered_calls) { ?>        
            <table class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th class="tableElementGapExtender">
                            Caller Number 
                        </th>
    <!--                        <th>
                            Start Time
                        </th>-->
                        <th class="tableElementGapExtender">
                            Answered Time
                        </th>
                        <th class="tableElementGapExtender">
                            End Time
                        </th>
                        <th class="tableElementGapExtender">
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $AgentAnsweredCallsCountCount = '0';
                    for ($index = 0; $index < count($answered_calls); $index++) {

//                        $seconds = $answered_calls[$index]['duration'];
//                        $seconds = 5;
//
//                        $hours = floor($seconds / 3600);
//                        $mins = floor($seconds / 60 % 60);
//                        $secs = floor($seconds % 60);
//
//                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        $timeDifference = 0;
                        $timeFirst = strtotime($answered_calls[$index]['answer']);
                        $timeSecond = strtotime($answered_calls[$index]['end']);
                        if ($timeFirst != '0000-00-00 00:00:00' && $timeSecond != '0000-00-00 00:00:00') {
                            $timeDifference = $timeSecond - $timeFirst;
                            if ($timeDifference < 0) {
                                $timeDifference = 0;
                            }
                        }
                        ?>
                        <tr>
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['src'] ?></td>
                            <!--<td><? = $answered_calls[$index]['start'] ?></td>-->
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['answer'] ?></td>
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['end'] ?></td>
                            <!--<td><? = $timeFormat ?></td>-->
                            <td><?= $timeDifference . ' seconds' ?></td>
                        </tr>
                        <?php $AgentAnsweredCallsCountCount ++;
                    } ?>
                </tbody>
                <script>
                    $(document).ready(function () {
                        var str_Count = "<?= $AgentAnsweredCallsCountCount ?>";
                        $('#printAgentAnsweredCount').html(str_Count);
                    });
                </script>
            </table>
<?php } ?>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Agent Outgoing Calls <span class="label label-primary" id="printAgentOutgoingCount">0</span></a>
        </div>                    
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd; ">
<?php if ($outgoing_calls) { ?>
            <table class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th>
                            Outgoing Number
                        </th>
                        <th>
                            Start Time
                        </th>
                        <th>
                            Answered Time
                        </th>
                        <th>
                            End Time
                        </th>
                        <th>
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $AgentOutgoingCount = '0';
                    for ($index = 0; $index < count($outgoing_calls); $index++) {

                        $seconds = $outgoing_calls[$index]['duration'];

                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);

                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        ?>
                        <tr>
                            <td><?= $outgoing_calls[$index]['dst'] ?></td>
                            <td><?= $outgoing_calls[$index]['start'] ?></td>
                            <td><?= $outgoing_calls[$index]['answer'] ?></td>
                            <td><?= $outgoing_calls[$index]['end'] ?></td>
                            <td><?= $timeFormat ?></td>
                        </tr>
                        <?php $AgentOutgoingCount ++;
                    }
                    ?>
                </tbody>
                <script>
                    $(document).ready(function () {
                        var str_Count = "<?= $AgentOutgoingCount ?>";
                        $('#printAgentOutgoingCount').html(str_Count);
                    });
                </script>
            </table>
<?php } ?>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-6" style="padding-right: 2%">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Abandoned Calls <span class="label label-primary" id="printAbandonedCallsCount">0</span></a>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php //if ($agent_abandoned_calls) {      ?> 
<?php if ($agent_abandoned_calls) { ?>
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Caller Number
                                </th>
                                <th>
                                    Abandoned Date
                                </th>
                                <th>
                                    Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            $AbandonedCallsCount = '0';
                            for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

                                $timestamp = $agent_abandoned_calls[$index]['timestamp'];
                                $date = date('Y-m-d', $timestamp);
                                $time = date('H:i:s', $timestamp);
                                ?>
                                <tr>
                                    <td><?= $agent_abandoned_calls[$index]['caller_number'] ?></td>
                                    <td><?= $date ?></td>
                                    <td><?= $time ?></td>
                                </tr>
                                <?php
                                $AbandonedCallsCount ++;
                            }
                            ?>
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $AbandonedCallsCount ?>";
                                $('#printAbandonedCallsCount').html(str_Count);
                            });
                        </script>
                    </table>
<?php } ?>

            </div>

        </div>

        <!--<div class="col-xs-1"></div>-->
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold; ">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Login Details <span class="label label-primary" id="printAgentLoginCount">0</span></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd;">
<?php if ($agent_abandoned_calls) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Logged Date
                                </th>
                                <th>
                                    Logged Time
                                </th>
                                <th>
                                    Logout Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            $AgentLoginCount = '0';
                            for ($index = 0; $index < count($login_records); $index++) {

                                $login_time_sig = $login_records[$index]['logged_in_time'];
                                $login_date = date('Y-m-d', $login_time_sig);
                                $login_time = date('H:i:s', $login_time_sig);

                                $logged_time_sig = $login_records[$index]['time_signature'];
                                $logged_date = date('Y-m-d', $logged_time_sig);
                                $logged_time = date('H:i:s', $logged_time_sig);
                                ?>
                                <tr>
                                    <td><?= $login_date ?></td>
                                    <td><?= $login_time ?></td>
                                    <td><?= $logged_time ?></td>
                                </tr>
                                <?php
                                $AgentLoginCount ++;
                            }
                            ?>
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $AgentLoginCount ?>";
                                $('#printAgentLoginCount').html(str_Count);
                            });
                        </script>
                    </table>
<?php } ?>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent DND Records <span class="label label-primary" id="printCount">0</span> </a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php if ($agent_dnd_reocrds) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Mode
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            /*
                             * @author : Shanila
                             * Since : 2017/09/26
                             * @Added this funcntion to take data from dnd_records model as a array and display
                              those data in supervisor reports page
                             */
                            $count = '0';
                            for ($index = 0; $index < count($agent_dnd_reocrds); $index++) {

                                $date_time = $agent_dnd_reocrds[$index]['dnd_on_timestamp'];
                                $explod_ontime = explode(" ", $date_time);
                                $off_date_time = $agent_dnd_reocrds[$index]['dnd_off_timestamp'];
                                $explod_offtime = explode(" ", $off_date_time);


                                if ($explod_offtime !== null) {

                                    if (in_array(null, $explod_offtime) != true) {
                                        $dnd_off_mode = "Off";
                                        ?>   
                                        <tr>
                                            <td><?= $explod_offtime[0]; ?></td>
                                            <td><?= $explod_offtime[1]; ?></td>
                                            <td><?= $dnd_off_mode; ?></td>
                                        </tr>
                                        <?php
                                        $count ++;
                                    }
                                }

                                if ($explod_ontime !== null) {
                                    $dnd_on_mode = "On";
                                    ?>      
                                    <tr>
                                        <td><?= $explod_ontime[0]; ?></td>
                                        <td><?= $explod_ontime[1]; ?></td>
                                        <td><?= $dnd_on_mode; ?></td>
                                    </tr>

                                    <?php
                                    $count ++;
                                }
                                ?>

    <?php }
    ?>  
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $count ?>";
                                $('#printCount').html(str_Count);
                            });
                        </script>
                    </table>
<?php } ?>
            </div>
        </div>
    </div>
</div>

