<?php

use yii\helpers\Url;
use yii\helpers\Html;

$data_breaksFullData = $breakFullData;
$data_breaksFullData = json_encode($data_breaksFullData);

$data_breaksDailyData = $breakDailyData;
$data_breaksDailyData = json_encode($data_breaksDailyData);

$dailyBreakPercentage = 0;
$dailyWorkPercentage = 0;

// Break and work times of the given period
$timePeriodBreakPercentage = 0;
$timePeriodWorkPercentage = 0;
$dailyBreakTime = '00 hours 00 minutes';
$dailyWorkTime = '00 hours 00 minutes';

$timePeriodBreakValue = '00 hours 00 minutes';
$timePeriodWorkValue = '00 hours 00 minutes';

if ($breakDailyData[0] != null && $breakDailyData[0] != 0) {
    $dailyBreakPercentage = ($breakDailyData[0] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
    $dailyBreakTime = convertToHoursMins($breakDailyData[0], '%02d hours %02d minutes');
}
if ($breakDailyData[1] != null && $breakDailyData[1] != 0) {
    $dailyWorkPercentage = ($breakDailyData[1] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
    $dailyWorkTime = convertToHoursMins($breakDailyData[1], '%02d hours %02d minutes');
}

if ($breakFullData[0] != null) {
    $timePeriodBreakPercentage = ($breakFullData[0] / ($breakFullData[0] + $breakFullData[1])) * 100;
    $timePeriodBreakValue = convertToHoursMins($breakFullData[0], '%02d hours %02d minutes');
}
if ($breakFullData[1] != null) {
    $timePeriodWorkPercentage = ($breakFullData[1] / ($breakFullData[0] + $breakFullData[1])) * 100;
    $timePeriodWorkValue = convertToHoursMins($breakFullData[1], '%02d hours %02d minutes');
}
$agentDailyAnsweredCalls = 0;
$agent_answered_percnt = 0;
$others_answered_percnt = 0;
$others_answered_count = $all_answered_calls_count - $agent_answered_calls_count;
if ($agent_answered_calls_count != 0) {
    $agent_answered_percnt = ($agent_answered_calls_count / $all_answered_calls_count) * 100;
}
if ($all_answered_calls_count != 0) {
    $others_answered_percnt = ($others_answered_count / $all_answered_calls_count) * 100;
}

// ======== Monthly calls data =============
$agentAnsweredCallsThisMonth = 0;
$agentAnsweredCallsPercentageThisMonth = 0;
$othersAnsweredCallsThisMonth = 0;
$otherAnsweredCallsPercentageThisMonth = 0;
if ($monthlyCallsData[0] != null && $monthlyCallsData[0] != 0) {
    $agentAnsweredCallsThisMonth = $monthlyCallsData[0];
    $agentAnsweredCallsPercentageThisMonth = (($monthlyCallsData[0] / $monthlyCallsData[1]) * 100);
}
if ($monthlyCallsData[1] != null && $monthlyCallsData[1] != 0) {
    $othersAnsweredCallsThisMonth = $monthlyCallsData[1] - $monthlyCallsData[0];
    $otherAnsweredCallsPercentageThisMonth = (($othersAnsweredCallsThisMonth / $monthlyCallsData[1]) * 100);
}
// ==========================================

$agent_answered_percnt = bcadd($agent_answered_percnt, '0', 2);
$others_answered_percnt = bcadd($others_answered_percnt, '0', 2);

function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
    .panel-heading{
        font-size: 90%;
    }
    thead{
        font-weight: bold;
    }
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }

    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;"  id="agentReportsDIV">
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Break Reports</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Daily break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($dailyWorkPercentage, 2) ?>%</td>
                                                <td><?= round($dailyBreakPercentage, 2) ?>%</td>
                                            </tr>
                                            <tr>
                                                <td><?= $dailyWorkTime ?></td>
                                                <td><?= $dailyBreakTime ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_breaksDailyData ?>,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(200, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($timePeriodWorkPercentage, 2) . ' %' ?></td>
                                                <td><?= round($timePeriodBreakPercentage, 2) . ' %' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $timePeriodWorkValue ?></td>
                                                <td><?= $timePeriodBreakValue ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: <?= $data_breaksFullData ?>,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(200, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>                
                </div>            
            </div>
        </div>
    </div><br>

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Service Level Figures</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Daily Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= $others_answered_percnt . '%' ?></td>
                                                <td><?= $agent_answered_percnt . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $others_answered_count . ' calls' ?></td>
                                                <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Agent Answered calls %", "Others Answered calls %"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: [<?= $agent_answered_calls_count ?>,<?= $others_answered_count ?>],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(54, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            This Month Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($otherAnsweredCallsPercentageThisMonth, 2) . '%' ?></td>
                                                <td><?= round($agentAnsweredCallsPercentageThisMonth, 2) . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $othersAnsweredCallsThisMonth . ' calls' ?></td>
                                                <td><?= $agentAnsweredCallsThisMonth . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                                        
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Agent Answered calls ", "Others Answered calls "],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: [<?= $agentAnsweredCallsThisMonth ?>,<?= $othersAnsweredCallsThisMonth ?>],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(54, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>            
            </div>
        </div>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Agent Answered Calls <span class="label label-primary" id="printAgentAnsweredCount">0</span></a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($answered_calls) { ?>        
            <table class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th>
                            Caller Number
                        </th>
                        <th>
                            Start Time
                        </th>
                        <th>
                            Answered Time
                        </th>
                        <th>
                            End Time
                        </th>
                        <th>
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $AgentAnsweredCallsCount = '0';
                    for ($index = 0; $index < count($answered_calls); $index++) {

//                        $seconds = $answered_calls[$index]['duration'];
//
//                        $hours = floor($seconds / 3600);
//                        $mins = floor($seconds / 60 % 60);
//                        $secs = floor($seconds % 60);
//
//                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        $timeDifference = 0;
                        $timeFirst = strtotime($answered_calls[$index]['answer']);
                        $timeSecond = strtotime($answered_calls[$index]['end']);
                        if ($timeFirst != '0000-00-00 00:00:00' && $timeSecond != '0000-00-00 00:00:00') {
                            $timeDifference = $timeSecond - $timeFirst;
                            if ($timeDifference < 0) {
                                $timeDifference = 0;
                            }
                        }
                        ?>
                        <tr>
                            <td><?= $answered_calls[$index]['src'] ?></td>
                            <!--<td><? = $answered_calls[$index]['start'] ?></td>-->
                            <td> - </td>
                            <td><?= $answered_calls[$index]['answer'] ?></td>
                            <td><?= $answered_calls[$index]['end'] ?></td>
                            <td><?= $timeDifference ?> secounds</td>
                        </tr>
                        <?php
                        $AgentAnsweredCallsCount++;
                    }
                    ?>
                </tbody>
                <script>
                    $(document).ready(function () {

                        var str_Count = "<?= $AgentAnsweredCallsCount ?>";
                        $('#printAgentAnsweredCount').html(str_Count);
                    });
                </script>
            </table>
        <?php } ?>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Agent Outgoing Calls  <span class="label label-primary" id="printOutgoingCount">0</span></a>
        </div>                    
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($outgoing_calls) { ?>
            <table class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th>
                            Outgoing Number
                        </th>
                        <th>
                            Start Time
                        </th>
                        <th>
                            Answered Time
                        </th>
                        <th>
                            End Time
                        </th>
                        <th>
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $agentOutgoingCount = '0';
                    for ($index = 0; $index < count($outgoing_calls); $index++) {

                        $seconds = $outgoing_calls[$index]['duration'];

                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);

                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        ?>
                        <tr>
                            <td><?= $outgoing_calls[$index]['dst'] ?></td>
                            <td><?= $outgoing_calls[$index]['start'] ?></td>
                            <td><?= $outgoing_calls[$index]['answer'] ?></td>
                            <td><?= $outgoing_calls[$index]['end'] ?></td>
                            <td><?= $timeFormat ?></td>
                        </tr>
                        <?php
                        $agentOutgoingCount++;
                    }
                    ?>
                </tbody>
                <script>
                    $(document).ready(function () {
                        var str_Count = "<?= $agentOutgoingCount ?>";
                        $('#printOutgoingCount').html(str_Count);

                    });
                </script>
            </table>
        <?php } ?>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-5" style="">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Abandoned Calls <span class="label label-primary" id="printAbandonedCallsCount">0</span></a>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($agent_abandoned_calls) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Caller Number
                                </th>
                                <th>
                                    Abandoned Date
                                </th>
                                <th>
                                    Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            $abandonedCallsCount = '0';
                            for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

                                $timestamp = $agent_abandoned_calls[$index]['timestamp'];
                                $date = date('Y-m-d', $timestamp);
                                $time = date('H:i:s', $timestamp);
                                ?>
                                <tr>
                                    <td><?= $agent_abandoned_calls[$index]['caller_number'] ?></td>
                                    <td><?= $date ?></td>
                                    <td><?= $time ?></td>
                                </tr>
                                <?php
                                $abandonedCallsCount++;
                            }
                            ?>
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $abandonedCallsCount ?>";
                                $('#printAbandonedCallsCount').html(str_Count);

                            });

                        </script>
                    </table>
                <?php } ?>

            </div>

        </div>

        <div class="col-xs-1"></div>
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Login Details <span class="label label-primary" id="printLoginCount">0</span></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <?php if ($login_records) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Logged Date
                                </th>
                                <th>
                                    Logged Time
                                </th>
                                <th>
                                    Logout Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            $loginCount = '0';
                            for ($index = 0; $index < count($login_records); $index++) {

                                $login_time_sig = $login_records[$index]['logged_in_time'];
                                $login_date = date('Y-m-d', $login_time_sig);
                                $login_time = date('H:i:s', $login_time_sig);

                                $logged_time_sig = $login_records[$index]['time_signature'];
                                $logged_date = date('Y-m-d', $logged_time_sig);
                                $logged_time = date('H:i:s', $logged_time_sig);
                                ?>
                                <tr>
                                    <td><?= $login_date ?></td>
                                    <td><?= $login_time ?></td>
                                    <td><?= $logged_time ?></td>
                                </tr>
                                <?php $loginCount++;
                            }
                            ?>
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $loginCount ?>";
                                $('#printLoginCount').html(str_Count);
                            });

                        </script>
                    </table>
<?php } ?>
            </div>
        </div>
    </div>
</div>

