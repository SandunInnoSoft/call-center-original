0<?php

use yii\helpers\Url;
use yii\helpers\Html;

$data_breaksFullData = $breakFullData;
$data_breaksFullData = json_encode($data_breaksFullData);

$data_breaksDailyData = $breakDailyData;
$data_breaksDailyData = json_encode($data_breaksDailyData);

// Calls Answered in given time Data
$callsAnsweredGivenPeriodAgent = 0;
$callsAnsweredGivenPeriodAgentPercentage = 0;
$callsAnsweredGivenPeriodOthers = 0;
$callsAnsweredGivenPeriodOthersPercentage = 0;
if ($callAnswerCounts[0] != null && $callAnswerCounts[1]) {
    $callsAnsweredGivenPeriodAgent = $callAnswerCounts[0];
    $callsAnsweredGivenPeriodOthers = $callAnswerCounts[1] - $callAnswerCounts[0];
    $callsAnsweredGivenPeriodAgentPercentage = ($callAnswerCounts[0] / $callAnswerCounts[1]) * 100;
    $callsAnsweredGivenPeriodOthersPercentage = (($callAnswerCounts[1] - $callAnswerCounts[0]) / $callAnswerCounts[1]) * 100;

//$totalCallsFigure
}

// =================================

$agent_answered_percnt = 0;
$others_answered_percnt = 0;
$others_answered_count = 0;
if ($all_answered_calls_count != 0) {
    $agent_answered_percnt = ($agent_answered_calls_count / $all_answered_calls_count) * 100;
    $others_answered_percnt = ($others_answered_count / $all_answered_calls_count) * 100;
    $others_answered_count = $all_answered_calls_count - $agent_answered_calls_count;
}

$agent_answered_percnt = bcadd($agent_answered_percnt, '0', 2);
$others_answered_percnt = bcadd($others_answered_percnt, '0', 2);
// ====================================================================
$dailyBreakPercentage = 0;
$dailyWorkPercentage = 0;
$dailyBreakTime = '00 hours 00 minutes';
$dailyWorkTime = '00 hours 00 minutes';
if ($breakDailyData[0] != null) {
    $dailyBreakPercentage = ($breakDailyData[0] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
}
if ($breakDailyData[1] != null) {
    $dailyWorkPercentage = ($breakDailyData[1] / ($breakDailyData[0] + $breakDailyData[1])) * 100;
}
if ($breakDailyData[2] != null) {
    $dailyBreakTime = $breakDailyData[2];
}
if ($breakDailyData[3] != null) {
    $dailyWorkTime = $breakDailyData[3];
}

// Break and work times of the given period
$timePeriodBreakPercentage = 0;
$timePeriodWorkPercentage = 0;
$timePeriodBreakValue = '00 hours 00 minutes';
$timePeriodWorkValue = '00 hours 00 minutes';
if ($breakFullData[0] != null) {
    $timePeriodBreakPercentage = ($breakFullData[0] / ($breakFullData[0] + $breakFullData[1])) * 100;
}
if ($breakFullData[1] != null) {
    $timePeriodWorkPercentage = ($breakFullData[1] / ($breakFullData[0] + $breakFullData[1])) * 100;
}
if ($breakFullData[2] != null) {
    $timePeriodBreakValue = $breakFullData[2];
}
if ($breakFullData[3] != null) {
    $timePeriodWorkValue = $breakFullData[3];
}
?>

<script type="text/javascript" src="js/chartjs.js"></script>
<style>
    .panel-heading{
        font-size: 90%;
    }
    thead{
        font-weight: bold;
    }
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }

    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<div class="container-fluid"  style="font-family: 'Lato' , sans-serif;border: 1px solid #10297d;"  id="agentReportsDIV">
    <div class="row" style="padding-top: 0%">
        <div style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%" class="col-xs-2"><?= $agent_name ?> : <?= $agent_extension ?></div>
        <div class="col-xs-8"></div>
        <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
    </div><br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Break Reports</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px;">
                        <div class="panel-heading" style="height: 60px">
                            Daily break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($dailyWorkPercentage, 2) ?>%</td>
                                                <td><?= round($dailyBreakPercentage, 2) ?>%</td>
                                            </tr>
                                            <tr>
                                                <td><?= $dailyWorkTime ?></td>
                                                <td><?= $dailyBreakTime ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
                                        //                            label: '# of Votes',
                                        data: <?= $data_breaksDailyData ?>,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(200, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total break times comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Worked</td>
                                                <td>Breaks</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= round($timePeriodWorkPercentage, 2) . ' %' ?></td>
                                                <td><?= round($timePeriodBreakPercentage, 2) . ' %' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $timePeriodWorkValue ?></td>
                                                <td><?= $timePeriodBreakValue ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="breakTimeFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("breakTimeFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Breaks", "Worked"],
                                datasets: [{
//                                        label: '# of Votes',
                                        data: <?= $data_breaksFullData ?>,
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(200, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>                
                </div>            
            </div>
        </div>
    </div><br>

    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold;">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Service Level Figures</a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Daily Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?= $others_answered_percnt . '%' ?></td>
                                                <td><?= $agent_answered_percnt . '%' ?></td>
                                            </tr>
                                            <tr>
                                                <td><?= $others_answered_count . ' calls' ?></td>
                                                <td><?= $agent_answered_calls_count . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelDailyChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelDailyChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Agent Answered calls %", "Others Answered calls %"],
                                datasets: [{
                                        //                            label: '# of Votes',
//                                        data: <? = $data_breaksFullData ?>,
                                        data: [<?= $agent_answered_calls_count ?>,<?= ($all_answered_calls_count - $agent_answered_calls_count) ?>],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(54, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-5" style="padding-top: 15px">
                    <div class="panel" style="border: 1px solid #10297d;border-radius: 5px;margin-bottom: 5px">
                        <div class="panel-heading" style="height: 60px">
                            Total Answered Calls comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <td>Others</td>
                                                <td>Agent</td>
                                            </tr>                                            
                                        </thead>
                                        <tbody>
                                            <tr>
<!--                                                <td><? = $others_answered_percnt . '%' ?></td>
                                                <td><? = $agent_answered_percnt . '%' ?></td>-->
                                                <td><?= round($callsAnsweredGivenPeriodOthersPercentage, 2) . '%' ?></td>
                                                <td><?= round($callsAnsweredGivenPeriodAgentPercentage, 2) . '%' ?></td>
                                            </tr>
                                            <tr>
<!--                                                <td><? = $others_answered_count ?></td>
                                                <td><? = $agent_answered_calls_count ?></td>-->
                                                <td><?= $callsAnsweredGivenPeriodOthers . ' calls' ?></td>
                                                <td><?= $callsAnsweredGivenPeriodAgent . ' calls' ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                                        
                </div>                            
                <div class="col-xs-7" style="padding-bottom: 5px">
                    <canvas id="serviceLevelFullChart" height="250px"></canvas>
                    <script>
                        var ctx = document.getElementById("serviceLevelFullChart").getContext('2d');
                        var myPieChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: {
                                labels: ["Agent Answered calls %", "Others Answered calls %"],
                                datasets: [{
//                                        label: '# of Votes',
//                                        data: <? = $data_breaksDailyData ?>,
                                        data: [<?= $callsAnsweredGivenPeriodAgent ?>,<?= $callsAnsweredGivenPeriodOthers ?>],
                                        backgroundColor: [
                                            'rgba(255, 99, 132, 0.5)',
                                            'rgba(54, 162, 235, 0.5)'
                                        ]
                                    }]
                            },
                            options: {
                            }
                        });
                    </script>
                </div>            
            </div>
        </div>
    </div>
    <br>
    <style>
        .tableElementGapExtender{
            width: 25% !important;
        }
    </style>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Agent Answered Calls <span class="label label-primary" id="printAgentAnsweredCount">0</span></a>
        </div>
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
        <?php if ($answered_calls) { ?>        
            <table class="table table-fixed table-striped answered-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th class="tableElementGapExtender">
                            Caller Number
                        </th>
    <!--                        <th>
                            Start Time
                        </th>-->
                        <th class="tableElementGapExtender">
                            Answered Time
                        </th>
                        <th class="tableElementGapExtender">
                            End Time
                        </th>
                        <th class="tableElementGapExtender">
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $AgentAnsweredCallsCount = '0';
                    for ($index = 0; $index < count($answered_calls); $index++) {

//                        $seconds = $answered_calls[$index]['duration'];
//
//                        $hours = floor($seconds / 3600);
//                        $mins = floor($seconds / 60 % 60);
//                        $secs = floor($seconds % 60);
//
//                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        $timeFormat = 0;
                        $timeFirst = strtotime($answered_calls[$index]['answer']);
                        $timeSecond = strtotime($answered_calls[$index]['end']);
                        if ($timeFirst != '0000-00-00 00:00:00' && $timeSecond != '0000-00-00 00:00:00') {
                            $timeFormat = $timeSecond - $timeFirst;
                            if ($timeFormat < 0) {
                                $timeFormat = 0;
                            }
                        }
                        ?>
                        <tr>
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['src'] ?></td>
                            <!--<td><? = $answered_calls[$index]['start'] ?></td>-->
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['answer'] ?></td>
                            <td class="tableElementGapExtender"><?= $answered_calls[$index]['end'] ?></td>
                            <td class="tableElementGapExtender"><?= $timeFormat ?> seconds</td>
                        </tr>
                        <?php $AgentAnsweredCallsCount ++;
                    }
                    ?>
                </tbody>
                <script>
                    $(document).ready(function () {

                        var str_Count = "<?= $AgentAnsweredCallsCount ?>";
                        $('#printAgentAnsweredCount').html(str_Count);
                    });
                </script>
            </table>
<?php } ?>
    </div>
    <br>
    <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Agent Outgoing Calls <span class="label label-primary" id="printOutgoingCount">0</span></a>
        </div>                    
    </div>
    <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php if ($outgoing_calls) { ?>
            <table class="table table-fixed table-striped outgoing-calls-history-table" style="border-radius: 5px">
                <thead class="call-history-thead">
                    <tr>
                        <th>
                            Outgoing Number
                        </th>
                        <th>
                            Start Time
                        </th>
                        <th>
                            Answered Time
                        </th>
                        <th>
                            End Time
                        </th>
                        <th>
                            Duration
                        </th>
                    </tr>
                </thead>
                <tbody class="call-history-tbody">
                    <?php
                    $agentOutgoingCount = '0';
                    for ($index = 0; $index < count($outgoing_calls); $index++) {

                        $seconds = $outgoing_calls[$index]['duration'];

                        $hours = floor($seconds / 3600);
                        $mins = floor($seconds / 60 % 60);
                        $secs = floor($seconds % 60);

                        $timeFormat = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                        ?>
                        <tr>
                            <td><?= $outgoing_calls[$index]['dst'] ?></td>
                            <td><?= $outgoing_calls[$index]['start'] ?></td>
                            <td><?= $outgoing_calls[$index]['answer'] ?></td>
                            <td><?= $outgoing_calls[$index]['end'] ?></td>
                            <td><?= $timeFormat ?></td>
                        </tr>
                        <?php
                        $agentOutgoingCount ++;
                    }
                    ?>
                </tbody>
                <script>
                    $(document).ready(function () {
                        var str_Count = "<?= $agentOutgoingCount ?>";
                        $('#printOutgoingCount').html(str_Count);

                    });

                </script>
            </table>
<?php } ?>
    </div>
    <br>
    <div class="row">
        <div class="col-xs-6" style="padding-right: 2%">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Abandoned Calls <span class="label label-primary" id="printAbandonedCallsCount">0</span></a>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php if ($agent_abandoned_calls) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Caller Number
                                </th>
                                <th>
                                    Abandoned Date
                                </th>
                                <th>
                                    Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            $abandonedCallsCount = '0';
                            for ($index = 0; $index < count($agent_abandoned_calls); $index++) {

                                $timestamp = $agent_abandoned_calls[$index]['timestamp'];
                                $date = date('Y-m-d', $timestamp);
                                $time = date('H:i:s', $timestamp);
                                ?>
                                <tr>
                                    <td><?= $agent_abandoned_calls[$index]['caller_number'] ?></td>
                                    <td><?= $date ?></td>
                                    <td><?= $time ?></td>
                                </tr>
                                <?php $abandonedCallsCount ++;
                            }
                            ?>
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $abandonedCallsCount ?>";
                                $('#printAbandonedCallsCount').html(str_Count);

                            });

                        </script>
                    </table>
<?php } ?>

            </div>

        </div>

        <!--<div class="col-xs-1"></div>-->
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent Login Details  <span class="label label-primary" id="printLoginCount">0</span></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php //if ($agent_abandoned_calls) {      ?>        
<?php if ($login_records) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Logged Date
                                </th>
                                <th>
                                    Logged Time
                                </th>
                                <th>
                                    Logout Time
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            $loginCount = '0';
                            for ($index = 0; $index < count($login_records); $index++) {

                                $login_time_sig = $login_records[$index]['logged_in_time'];
                                $login_date = date('Y-m-d', $login_time_sig);
                                $login_time = date('H:i:s', $login_time_sig);

                                $logged_time_sig = $login_records[$index]['time_signature'];
                                $logged_date = date('Y-m-d', $logged_time_sig);
                                $logged_time = date('H:i:s', $logged_time_sig);
                                ?>
                                <tr>
                                    <td><?= $login_date ?></td>
                                    <td><?= $login_time ?></td>
                                    <td><?= $logged_time ?></td>
                                </tr>
                                <?php
                                $loginCount ++;
                            }
                            ?>
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $loginCount ?>";
                                $('#printLoginCount').html(str_Count);
                            });

                        </script>
                    </table>
<?php } ?>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-xs-6">
            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Agent DND Records <span class="label label-primary" id="printDndCount">0</span></a>
                </div>    
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
<?php if ($agent_dnd_reocrds) { ?>        
                    <table class="table table-fixed table-striped abandoned-calls-history-table" style="border-radius: 5px">
                        <thead class="abandoned-call-history-thead">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Mode
                                </th>                        
                            </tr>
                        </thead>
                        <tbody class="abandoned-call-history-tbody">
                            <?php
                            /*
                             * @author : Shanila
                             * Since : 2017/09/26
                             * @Added this funcntion to take data from dnd_records model as a array and display
                              those data in supervisor reports by date page according to fromdate and todate given by the user
                             */
                            $dndCount = '0';
                            for ($index = 0; $index < count($agent_dnd_reocrds); $index++) {
                                $date_time = $agent_dnd_reocrds[$index]['dnd_on_timestamp'];
                                $explod_ontime = explode(" ", $date_time);
                                $off_date_time = $agent_dnd_reocrds[$index]['dnd_off_timestamp'];
                                $explod_offtime = explode(" ", $off_date_time);



                                if (in_array(null, $explod_offtime) != true) {
                                    $dnd_off_mode = "Off";
                                    ?>   
                                    <tr>
                                        <td><?= $explod_offtime[0]; ?></td>
                                        <td><?= $explod_offtime[1]; ?></td>
                                        <td><?= $dnd_off_mode; ?></td>
                                    </tr>
                                    <?php
                                    $dndCount ++;
                                }


                                if ($explod_ontime !== null) {
                                    $dnd_on_mode = "On";
                                    ?>      
                                    <tr>
                                        <td><?= $explod_ontime[0]; ?></td>
                                        <td><?= $explod_ontime[1]; ?></td>
                                        <td><?= $dnd_on_mode; ?></td>
                                    </tr>

                                    <?php
                                    $dndCount ++;
                                }
                                ?>

    <?php } ?> 
                        </tbody>
                        <script>
                            $(document).ready(function () {
                                var str_Count = "<?= $dndCount ?>";
                                $('#printDndCount').html(str_Count);
                            });

                        </script>
                    </table>
<?php } ?>
            </div>
        </div>
    </div>
</div>

