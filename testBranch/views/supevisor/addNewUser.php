<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add New User </title>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>--> 
    </head>
    <body>
        <script>
//            var arrayState = 0;
//            arrayState = "<? = count($user_data) ?>";
//            if (arrayState != 0) {
//
//                alert();
//                $('#txtName').val("<? = $user_data['name'] ?>");
//
//                4
// A $( document ).ready() block.
            $(document).ready(function () {
                $('#txtName').val("<?= $user_data['fullname'] ?>");
                $('#selectUserRole').val("<?= $user_data['role_id'] ?>");
                $('#selectVoipExtension').val("<?= $user_data['extension'] ?>");
                $('#txtUserName').val("<?= $user_data['name'] ?>");
                $('#editUserId').val("<?= $user_data['id'] ?>");
                $('#txtUserName').val("<?= $user_data['name'] ?>");
                $('#profilePicPreview').attr('src', "<?= $user_data['user_profile_pic'] ?>").height(100);
            });
//            }
        </script>

        <div class = "container-fluid">
            <div class = "row">
                <a href = "<?= Url::to(['supevisor/manage']) ?>" class = "btn" type = "button">< Back</a>
                <div class = "col-md-12">
                    <h3 class = "text-primary text-center">
                        <b> User Data </b>
                    </h3>
                    <form role = "form" id = "idNewUser" onreset = "clearImagePreview();">
                        <div class = "form-group">

                            <label for = "exampleInputEmail1">
                                Name
                            </label>
                            <input type = "text" class = "form-control" id = "txtName" name = "txtName" />
                            <input type = "hidden" class = "form-control" id = "editUserId" name = "editUserId" value = ""/>
                        </div>
                        <div class = "form-group">

                            <label for = "exampleInputEmail1">
                                User Role
                            </label>
                            <select class = "form-control" id = "selectUserRole" name = "selectUserRole">
                                <option selected = "" value = "0">Select</option>
                                <!--this field will only add to admin-->
                                <option value = "3">Supervisor</option>
                                <!---------->
                                <option value = "4">Senior Agent</option>
                                <option value = "2">Agent</option>
                            </select>
                        </div>
                        <div class = "form-group">

                            <label for = "exampleInputEmail1">
                                User Name
                            </label>
                            <input type = "text" class = "form-control" id = "txtUserName" name = "txtUserName" />

                        </div>
                        <div class = "form-group">

                            <label for = "exampleInputPassword1">
                                Password
                            </label>
                            <input type = "password" class = "form-control" id = "txtPassword" name = "txtPassword"/>
                        </div>
                        <div class = "form-group">

                            <label for = "exampleInputPassword1">
                                Confirm Password
                            </label>
                            <input type = "password" class = "form-control" id = "txtConfirmPassword"/>
                        </div>
                        <div class = "form-group">

                            <label for = "exampleInputVOIPExtension">
                                VOIP Extension
                            </label>
                            <!--<input type = "password" class = "form-control" id = "txtConfirmPassword"/>-->

                            <?php
                            if (count($availableExtensions) > 0) {
                                // have available extensions
                                ?>
                                <select id="selectVoipExtension" name="selectVoipExtension" class="form-control">
                                    <?php
                                    for ($x = 0; $x < count($availableExtensions); $x++) {
                                        echo "<option value='" . $availableExtensions[$x] . "'>" . $availableExtensions[$x] . "</option>";
                                    }
                                    ?>
                                </select>
                                <?php
                            } else {
                                ?>
                            <select id="selectVoipExtension" name="selectVoipExtension" class="form-control" disabled="">
                                    <?php
                                    echo "<option selected value=''> NO AVAILABLE VOIP EXTENTIONS </option>";
                                    ?>
                                </select>
                                <?php
                            }
                            ?>

                        </div>
                        <div class = "form-group">
                            <label for = "exampleInputFile" id = "">
                                File input
                            </label>
                            <div class = "row" style = "height: 100px;">
                                <div class = "col-xs-4">
                                    <input type = "file" id = "imgUserProfile" name = "imgUserProfile" onchange = "readURL(this);"/>
                                    <input type="hidden" id="picChanged" name="picChanged" value="0">
                                </div>
                                <div class = "col-xs-4">
                                    <img id = "profilePicPreview" >
                                </div>
                            </div>
                        </div>

                        <button type = "button" id = "btnSaveUser" class = "btn btn-success">
                            Save
                        </button>
                        <button type = "reset" class = "btn btn-info">
                            Cancel
                        </button>
                        <button type = "button" class = "btn btn-warning">
                            Close
                        </button>

                    </form>
                </div>
            </div>
        </div>
        <script>
            function validateForm() {
                var returnState = true;
                if ($('#txtName').val() === '') {
                    returnState = false;
                }
                var selected = $("#selectUserRole option:selected").val();
                if (selected === '0') {
                    returnState = false;
                }
                if ($('#txtPassword').val() === '' && $('#editUserId').val() === '') {
                    returnState = false;
                }
                if ($('#txtPassword').val() !== $('#txtConfirmPassword').val()) {
                    returnState = false;
                }

                if ($("#selectVoipExtension option:selected").val() == '') {
                    returnState = false;
                }

                return returnState;
            }
            function clearImagePreview() {
                $('#profilePicPreview')
                        .attr('src', '')
                        .height(100);
            }

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#profilePicPreview')
                                .attr('src', e.target.result)
                                .height(100);
                        $('#picChanged').val('1');
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(function () {
//                $('#imgUploadBtn').click(function (e) {
//                    e.preventDefault();
//                    $('#profilePicUpload').click();
//                }
//                );
                $('#btnSaveUser').click(function (e) {
                    if (validateForm()) {
                        $.ajax({
                            type: "POST",
                            url: "<?= Url::to(['supevisor/adduser']) ?>",
                            contentType: false,
                            processData: false,
                            data: new FormData($('#idNewUser')[0]),
                            error: function (str) {
//                                notifyFail();
                                alert(str.responseText);
                            },
                            success: function (str) {
                                if (str == 0) {
//                                    notifyFail();

                                } else if (str == 1) {

                                    swal({
                                        title: 'Record Saved!!!',
                                        type: "success",
                                        timer: 2000
                                    }).then(
                                            function () {
                                                document.getElementById("idNewUser").reset();
                                                $('#uploadimg')
                                                        .attr('src', '')
                                                        .height(125);
                                                window.location.replace("<?= Url::to(['supevisor/insert']) ?>");
                                            },
                                            // handling the promise rejection
                                                    function (dismiss) {
                                                        document.getElementById("idNewUser").reset();
                                                        $('#uploadimg')
                                                                .attr('src', '')
                                                                .height(125);
                                                        window.location.replace("<?= Url::to(['supevisor/insert']) ?>");
                                                    }
                                            );

//                                            setTimeout(function () {
//
//                                            }, 2000);



//                                            swal({
//                                                title: 'Record Saved!!!',
//                                                type: "success"
//                                            });
//                                    notifySuccess();

                                        }
                            }
                        });
                    } else {
                        alert('Form validation issue. Please check the content and re submit ');
                    }
                }
                );


            });
        </script>
    </body>
</html>
