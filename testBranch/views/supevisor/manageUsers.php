<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .btn-default{
                background-color: #10297d !important;
                color: #faa61a !important;
            }
            .text-success{
                color: #10297d !important;
            }

        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="margin-top: 1%;">
                <div class="col-md-12">
                    <h3 class="text-left text-success"><strong>
                            Manage Users
                        </strong>
                    </h3>
                    <br>
                    <a name="btnAddNewUser" id="btnAddNewUser" class="btn btn-default btn-lg" href="<?= Url::to(['supevisor/insert']) ?>">Add New User</a>
                </div>

                <div class="col-md-10">
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-5" style="">


                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-4" style="padding-left: 0px;">  
                        </div>

                        <div class="col-md-7" style="  margin-top: 1%; padding-left: 0%;">


                            <input id="txtSearchName" name="txtSearchName" type="text" class="form-control" placeholder="Search by name.."/>
                        </div>
                        <div class="col-md-1" style="margin-top: 1%; padding: 0%">
                            <button type="button" id="btnFind" class="btn btn-default btn-md" style="color: #ffffff; margin-bottom: 0%;">
                                Find
                            </button> 

                        </div>
                    </div>
                </div>

            </div>
            <style type="text/css">
                /*
                <p> added a search loding image to supervisor mange user table search function </p>
                 
                author: shanila
                since: 2017/09/22
                 
                */
                .searchLoader{
                    position:absolute;
                    top:0;
                    left:0;
                    width:100%;
                    height:100%;
                    display:none;
                    background:url('images/searchLoader.gif') no-repeat 50%
                } 
            </style>
            <script>
                /**
                 * <p> this function is created to handle click event or keypress event </p>
                 * @author Shanila
                 * @since 2017/09/21
                 *
                 *modified: added a jquery function to display search loader image.
                 * 
                 */
                $(document).ready(function () {
                    $('.searchLoader').css('display', 'block');
                    $('#userTableFrame').load("<?= Url::to(['supevisor/managetable']) ?>");
                    $("#btnFind").click(function () {

                        searchAgents();

                    });

                    $("#txtSearchName").keyup(function (event) {
                        if (event.keyCode == 13) {

                            searchAgents();

                        }
                    });
                });
                /**
                 * <p> this function takes user input and search for relevant users </p>
                 * 
                 * @author Shanila  
                 * @since 2017/09/21
                 * 
                 * modified: added a jquery function to fadeout the search loader image.
                 */
                function searchAgents() {
                    $('.searchLoader').css('display', 'block');
                    var txtSearchNamekeyword = $("#txtSearchName").val();

                    if (txtSearchNamekeyword == "") {

                        $('#userTableFrame').load("<?= Url::to(['supevisor/managetable']) ?>");
                        $('.searchLoader').fadeOut("slow");


                    } else {

                        $('#userTableFrame').load("<?= Url::to(['supevisor/managetable']) ?>" + "&txtSearchNamekeyword=" + txtSearchNamekeyword);
                        $('.searchLoader').fadeOut("slow");
                    }
                }


            </script>


            <div class="row" style="margin-top: 3%;" id="userTableFrame">
                <div class="searchLoader"></div> 

            </div>

    </body>
</html>
