<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\Call_records;
use app\models\agent_notifications;

/**
 * This controller is to get caller information from hnb
 *
 * @author Vikumn
 * @since 2017-06-27
 */
class DataController extends Controller {

    public function actionSearch_customer_data() {
        $val = $_GET['q'];
//        $result = '-1';
        if (strpos($val, '-') !== false) {
            $result = $this->searchCustomerData('vehicle_number', $val);
        } else if (preg_match("/[a-z]/i", $val)) {
            $result = $this->searchCustomerData('vehicle_number', $val);
//        } else if (preg_match('/^[0-9]+$/', $val)) {
        } else if (preg_match('/^\d{8,10}$/', $val)) {
            $result = $this->searchCustomerData('contact_number', $val);
        } else if (preg_match('/^\+\d/', $val)) {
            $result = $this->searchCustomerData('contact_number', $val);
        }
        $data = json_decode($result, TRUE);
        if (count($data['content']) > 0) {
            $result_array = array(
//                'record_id' => $insert,
                'customer_insuered_name' => $data['content'][0]['insuredName'],
                'customer_gender' => $data['content'][0]['insurerGender'],
                'customer_address' => $data['content'][0]['insuredAddress'],
                'customer_contact' => $data['content'][0]['contactNo'],
                'customer_email' => $data['content'][0]['insuredEmail'],
                'customer_dob' => $data['content'][0]['insuredDob']
            );
        } else {
            $result_array = array(
                'record_id' => $insert,
                'customer_insuered_name' => '',
                'customer_gender' => '',
                'customer_address' => '',
                'customer_contact' => '',
                'customer_email' => '',
                'customer_dob' => ''
            );
        }
        return json_encode($result_array);
    }

    private function searchCustomerData($type, $value) {
        $logs = require(__DIR__ . '/../config/api_config.php');

        $url = $logs['url'];
        $userName = $logs['user'];
        $password = $logs['passsword'];

        if ($type == 'contact_number') {
            $url = $url . "contactNo=$value";
        } else if ($type == 'vehicle_number') {
            $url = $url . "vehicleNo=$value";
        }
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "Authorization: Basic " . base64_encode("$userName:$password")
            )
        );
        $context = stream_context_create($opts);
        $file = file_get_contents($url, false, $context);
        return $file;
    }

}
