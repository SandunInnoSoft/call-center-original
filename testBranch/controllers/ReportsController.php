<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\abandon_calls;
use app\models\call_center_user;
use app\models\Dnd_records;
use app\models\Call_records;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportsController
 *
 * @since 28/07/2017
 * @author Prabath
 */
class ReportsController extends Controller {

    /**
     * <b>Render the Reports</b>
     * 
     * @return render the performance overview
     * 
     * @author Vikum
     * @since 2017-07-07
     * 
     */
    public function actionPerformanceoverview() {
        $user_id = Yii::$app->session->get('user_id');
        $agent_ex = Yii::$app->session->get('voip');

        $dailyBreakTime = Agent_requests::getDailyBreakTime($user_id);
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($user_id);

        $monthlyBreakTime = Agent_requests::getThisMonthBreakTime($user_id); // Total breaks taken by the agent this month
        $monthlyWorkTime = Logged_in_users::getThisMonthWorkedTime($user_id); // Total working hours by an agent of the current month

        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime];
//        $breaksFullData = [0.5, 2.5];
        $breaksFullData = [$monthlyBreakTime, $monthlyWorkTime]; // Report data for the current month word graph
        /**
         * @author supun
         * @since 2017/08/16
         */
//        $answered_calls = cdr::getAnsweredCallsListByUser($agent_ex);
        $answered_calls_data = Call_records::getAllAgentCallRecords($user_id);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        foreach ($answered_calls_data as $key) {
//            $date1 = date_create($key['call_date']);
//            $date2 = date_create($key['call_end_time']);
//            $dteStart = new DateTime($key['call_date']);
//            $dteEnd = new DateTime($key['call_end_time']);
//            $dteDiff  = $dteStart->diff($dteEnd); 

            $temp = array(
                'start' => 0,
                'src' => $key['caller_number'],
                'answer' => $key['call_date'],
                'duration' => 0,
//            date_diff($date1, $date2),
                'end' => $key['call_end_time']
            );
            array_push($answered_calls, $temp);
        }

        $agent_answered_calls_count = Call_records::getDailyAnsweredCallsByAgent($user_id); // All Calls Answered by the agent in current day
        $all_answered_calls_count = Call_records::getDailyAnsweredCalls(); // All Calls answered in the current day

        $all_agent_answered_calls_count_thismonth = Call_records::getAgentAnsweredCallsThisMonth($user_id);
        $all_answered_calls_count_thisMonth = Call_records::getAllAnsweredCallsThisMonth(); // All the calls answered by the call center current month
        $monthlyCallsData = [$all_agent_answered_calls_count_thismonth, $all_answered_calls_count_thisMonth];

        $outgoing_calls = cdr::getOutgoingCallsListByUser($agent_ex);

        $agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgentByUser($agent_ex);

//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByUser($user_id);

        return $this->render('performanceOverview', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'monthlyCallsData' => $monthlyCallsData]);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render a specific agent performance overview page
     * 
     * @author Prabath
     * @since 03-08-2017
     * 
     */
    public function actionPerformanceoverviewbyagent() {
        $agent_id = $_POST['agent_id'];
        $agent_ex = $_POST['agent_ex'];
        $agent_name = $_POST['agent_name'];


        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id); // Sum of break times taken by the agent of the current day
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id); // Sum of working hours of the agent on Current Day

        $monthlyBreakTime = Agent_requests::getThisMonthBreakTime($agent_id); // Total breaks taken by the agent this month
        $monthlyWorkTime = Logged_in_users::getThisMonthWorkedTime($agent_id); // Total working hours by an agent of the current month

        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime]; // Report data for the current work time graph
        $breaksFullData = [$monthlyBreakTime, $monthlyWorkTime]; // Report data for the current month word graph
//========================================================================================================================================
//        $answered_calls = cdr::getAnsweredCallsListByUser($agent_ex); // This need to be changed to get data from CallRecords table *** NOT MINE
        $answered_calls_data = Call_records::getAllAgentCallRecords($agent_id);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        foreach ($answered_calls_data as $key) {
//            $date1 = date_create($key['call_date']);
//            $date2 = date_create($key['call_end_time']);
//            $dteStart = new DateTime($key['call_date']);
//            $dteEnd = new DateTime($key['call_end_time']);
//            $dteDiff  = $dteStart->diff($dteEnd); 
            $temp = array(
                'id' => $key['id'],
                'start' => 0,
                'src' => $key['caller_number'],
                'answer' => $key['call_date'],
                'duration' => 0,
//            date_diff($date1, $date2),
                'end' => $key['call_end_time']
            );
            array_push($answered_calls, $temp);
        }

        $agent_answered_calls_count = Call_records::getDailyAnsweredCallsByAgent($agent_id); // All Calls Answered by the agent in current day
        $all_answered_calls_count = Call_records::getDailyAnsweredCalls(); // All Calls answered in the current day

        $all_agent_answered_calls_count_thismonth = Call_records::getAgentAnsweredCallsThisMonth($agent_id);
        $all_answered_calls_count_thisMonth = Call_records::getAllAnsweredCallsThisMonth(); // All the calls answered by the call center current month
        $monthlyCallsData = [$all_agent_answered_calls_count_thismonth, $all_answered_calls_count_thisMonth];
        //
//========================================================================================================================================        

        $outgoing_calls = cdr::getOutgoingCallsListByUser($agent_ex); // This takes all outgoing calls taken by the agent



        $agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgentByUser($agent_ex);
        //$agent_abandoned_calls = array();
//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount(); // This need to be changed to get all answered calls from CallRecords

        $logged_in_records = Logged_in_users::getLoginRecordsByUser($agent_id);

        $agent_dnd_records = Dnd_records::getDndRecordsByUser($agent_id);

        return $this->render('supervisorReportsPage', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_answered_calls_count' => $agent_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'agent_extension' => $agent_ex, 'agent_name' => $agent_name, 'agent_dnd_reocrds' => $agent_dnd_records, 'allCallsInTheMonth' => $all_answered_calls_count_thisMonth, 'allCallsForAgentThisMonth' => $all_agent_answered_calls_count_thismonth, 'monthlyCallsData' => $monthlyCallsData]);
    }

    public function actionPerformanceoverviewbydate() {


        $agent_id = $_POST['agent_id'];
        $agent_ex = $_POST['agentex'];
        $agent_name = $_POST['agentname'];
        $searchContact = $_POST['contactNum'];
//        $searchContact = '';
        $fromDate = $_POST['fromdate'];
        $toDate = $_POST['todate'];
        $dateRange = array($fromDate, $toDate);

//        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id);
//        $dailyBreakTime = Agent_requests::getBreakTimeByDate($agent_id,$fromDate);
//        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id,$toDate);
        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id); // Sum of break times taken by the agent of the current day
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id); // Sum of working hours of the agent on Current Day
//        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime]; // Report data for the current work time graph


        $dailyBreakTimeReal = $this->convertToHoursMins($dailyBreakTime, '%02d hours %02d minutes');
        $dailyWorkTimeReal = $this->convertToHoursMins($dailyWorkTime, '%02d hours %02d minutes');
        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime, "$dailyBreakTimeReal", "$dailyWorkTimeReal"];


        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByDate($agent_id, $fromDate, $toDate);
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getDailyWorkedTimeInGivenRange($agent_id, $fromDate, $toDate);
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $breaksFullData = [$totalBreakTimeOfTheTimePeriod, $totalWorkTimeOfTheTimePeriod, $totalBreakTimeOfTheTimePeriodRealTime, $totalWorkTimeOfTheTimePeriodRealTime]; // This is where full break set will come
//        $answered_calls = cdr::getAnsweredCallsListByDate($agent_ex, $fromDate, $toDate);
//        $answered_calls_data = Call_records::getAllAgentCallRecords($agent_id);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        $answered_calls_set = Call_records::getAllCallRecordsByDate($fromDate, $toDate, $searchContact, $agent_id); // This search necessary data from Call_records table
        foreach ($answered_calls_set as $key) {
            $temp = array(
                'id' => $key['id'],
                'start' => 0,
                'src' => $key['caller_number'],
                'answer' => $key['call_date'],
                'duration' => 0,
//            date_diff($date1, $date2),
                'end' => $key['call_end_time']
            );
            array_push($answered_calls, $temp);
        }
        $agent_answered_calls_count = Call_records::getDailyAnsweredCallsByAgent($agent_id); // All Calls Answered by the agent in current day

        $all_answered_calls_count = Call_records::getDailyAnsweredCalls(); // All Calls answered in the current day

        $allCallsAnsweredByAgentCountInGivenPeriod = Call_records::allCallsAnsweredInGivenPeriod($agent_id, $fromDate, $toDate);
        $allCallsAnsweredByCountInGivenPeriod = Call_records::allCallsAnsweredInGivenPeriod('0', $fromDate, $toDate);
        $totalCallsAnsweredSet = array($allCallsAnsweredByAgentCountInGivenPeriod, $allCallsAnsweredByCountInGivenPeriod); // [agent, all]


        $outgoing_calls = cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate);

        //$agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgent($agent_ex);
        $agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgentByDate($agent_ex, $agent_id, $fromDate, $toDate);

//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByDate($agent_id, $fromDate, $toDate);

        /*
         * Modified
         * Shanila
         * 2017/10/02
         * @updated the function in order to change supervisor user manage view page result according to todate.
         */


        $agent_dnd_records = Dnd_records::getDndRecordsByDate($agent_id, $fromDate, $toDate);
        if (isset($agent_dnd_records[0])) {

            $dbDateObject = $agent_dnd_records[0]['dnd_off_timestamp'];
            $dnd_on_DateObject = new \DateTime($dbDateObject);
            $inputDateObject = new \DateTime($toDate);
            $dnd_on_timeResult = date_diff($inputDateObject, $dnd_on_DateObject);
            $daysCount = $dnd_on_timeResult->format('%d');

            if ($daysCount > '0') {
                unset($agent_dnd_records[0]['dnd_off_timestamp']);
            }
        }
        return $this->render('supervisorReportsByDate', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_answered_calls_count' => $agent_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'agent_extension' => $agent_ex, 'agent_name' => $agent_name, 'agent_dnd_reocrds' => $agent_dnd_records, 'dateRange' => $dateRange, 'callAnswerCounts' => $totalCallsAnsweredSet]);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render a supervisor reports page
     * 
     * @author Prabath
     * @since 03-08-2017
     * 
     */
    public function actionSupervisorreports() {
        $agentsList = call_center_user::getAllAgents();
        return $this->render('supervisorReports', ['agents' => $agentsList]);
    }

    private function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * @author Shanila
     * @date 2017/09/28 
     * <p> This function is created to test the dnd_records model</p>
     */
//    public function actionTesting(){
//        $agent_id = $_GET['agent_id'];
//        $fromDate = $_GET['fromdate'];
//        $toDate = $_GET['todate'];
//        
//        $agent_dnd_records = Dnd_records::test($agent_id, $fromDate, $toDate);
//        
//        
//        print_r($agent_dnd_records);
//        
//    }
}
