<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Contact_list;
use app\models\Agent_requests;
use app\models\Customer;

/**
 * This SuperuserController class is to perform functions of a super user
 *
 * @author Sandun
 * @since 2017-09-6
 */
class SuperuserController extends Controller {

    public function actionDashboard() {
        $session = Yii::$app->session;
        if (!$session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        } else {
            $user_role = $session->get('user_role');

            if ($user_role == 1 || $user_role == 3 || $user_role == 4) {
                // user is an admin or a supervisor or a senior agent (superuser). should show super agent dashboard
                $webphoneParams = require(__DIR__ . '/../config/webphone.php');
                $webphoneParams['username'] = Yii::$app->session->get('voip');
                $contacts = $this->allcontacts();
                $agentId = Yii::$app->session->get("user_id");
                $consumedAgentRequest = Agent_requests::getConsumedAgentRequests($agentId);
                $requestData = array();
                $i = 0;
                foreach ($consumedAgentRequest as $key) {
//                $type = $key['request_type'];
                    $requestData[$i]['type'] = $key['request_type'];
                    $requestData[$i]['id'] = $key['id'];
                    $requestData[$i]['status'] = $key['request_status'];
                    $requestData[$i]['time'] = $key['approved_time_period'];
                    $i++;
                }

                if (isset($_GET['customer'])) {
                    $customerId = $_GET['customer'];
                    $customerInformation = Customer::getCustomerInformation($customerId);

                    return $this->render('superuserView', ['customerInformation' => $customerInformation, 'webphoneParams' => $webphoneParams, 'contacts' => $contacts, 'agent_request_onload' => $requestData]);
                } else {
                    $customerInformation = Customer::getCustomerInformation(1);
                    return $this->render('superuserView', ['customerInformation' => $customerInformation, 'webphoneParams' => $webphoneParams, 'contacts' => $contacts, 'agent_request_onload' => $requestData]);
                }
            } else {
                // user is not an admin or a supervisor or a super user, should show the agent dashboard
                $this->redirect('index.php?r=agent/callerinformation');
            }
        }
    }

    private function allcontacts() {

        $contactRecord = Contact_list::loadAllContact();
        $returnData = array();
        if ($contactRecord) {
            foreach ($contactRecord as $key) {
                $val = array(
                    'number' => $key['contact_number'],
                    'name' => $key['contact_name']
                );
                array_push($returnData, $val);
            }

            return json_encode($returnData);
        } else {
            return 0;
        }
    }

}
