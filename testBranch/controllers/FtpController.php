<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;

/**
 * This controller is used to perform FTP operations with another remote server
 * 
 * @author Sandun
 * @since 2017-08-31
 */
class FtpController extends Controller {

    public function actionGetaudiofile() {
        $ftpParams = include __DIR__ . '/../config/pbxAudioFTP_config.php';
//        $agentExtension = "2020";
        $agentExtension = "800";
//        $fileName = "20170727-192929-2010-2020-1501163969.11-4";
//        $fileName = "20170816-155341-771980774-800-1502879013.210";
        $fileName = "Linkin_Park_-_Battle_Symphony_lyrics";
        header('Content-Type: application/octet-stream');
        header('Content-Type: audio/wav');
//
//
        header('Cache-Control: no-cache');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
//        header('Content-Type: audio/x-gsm');
//        header("Content-disposition: attachment; filename=file.gsm");

//        ini_set("zlib.output_compression", "Off");
        $fileData = $this->getAudioFile($agentExtension, $fileName);
    }

    private function getAudioFile($agentExtension, $fileName) {
//        $ipAddress = "10.100.21.238";
        $ipAddress = "192.168.1.254";
        $user = "root";
        $pass = "Password123";
//        $extension = "gsm";
        $extension = "wav";
        $filePath = "/var/spool/asterisk/monitor/recording/$agentExtension/$fileName.$extension";

        $c = curl_init("sftp://$user:$pass@$ipAddress:22/$filePath");
        curl_setopt($c, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }

    public function actionAudio() {
        return $this->render("testAudio");
    }

}
