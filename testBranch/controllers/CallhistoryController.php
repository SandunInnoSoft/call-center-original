<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CallhistoryController
 * 
 * @author Prabath
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\cdr;

class CallhistoryController extends Controller {

    
    public function actionCallhistory() {
        $callHistory = cdr::getCallHistory();
        $json_callHistory = json_encode($callHistory);
        return $this->render('callhistoryPage', ['callHistory' => $json_callHistory]);
    }

}
