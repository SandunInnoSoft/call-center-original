<?php

/*
 * This controller handle all supevisor wise controls
 * @author: Vikum
 * @date: 27/06/2017
 * 
 */

namespace app\controllers;

//defined('BASEPATH') OR exit('No direct script access allowed');


use Yii;
use yii\db\ActiveRecord;
use yii\web\Controller;
use app\models\Customer;
use app\models\call_center_user;
use app\models\Agent_requests;
use app\models\agent_notifications;
use app\models\web_presence;
use app\models\Miss_calls_email_log;
use app\models\cdr;
use app\models\Call_records;
use app\models\Contact_list;

class SupevisorController extends Controller {

//    public function __construct() {
//
//        parent::__construct();
//    }

    public function actionOverview() {
        $session = Yii::$app->session;
        if (!$session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        } else {
            $user_role = $session->get('user_role');
            if ($user_role == '1' || $user_role == '3') {
                $query_customers = Customer::find();
                $liveCalls = $query_customers->all();
                $webphoneParams = require(__DIR__ . '/../config/webphone.php');
                $webphoneParams['username'] = Yii::$app->session->get('voip');
                $contacts = $this->allcontacts();
                return $this->render('coordinatorOverview', ['liveCalls' => $liveCalls, 'webphoneParams' => $webphoneParams, 'contacts' => $contacts]);
            } else {
                $this->redirect('index.php?r=user/login_view');
            }
        }
    }

    public function actionNotifyagent() {
        $notify_type = $_POST['dial_type'];
        $agent_id = $_POST['agent_id'];

        $session = Yii::$app->session;
        $supervisor_id = $session->get('user_id');

        $time = date("Y-m-d H:i:s");
        $notif_status = 'active';

        $agent_notif = new agent_notifications();
        $agent_notif->agent_id = $agent_id;
        $agent_notif->supervisor_id = $supervisor_id;
        $agent_notif->notif_type = $notify_type;
        $agent_notif->notif_status = $notif_status;
        $agent_notif->notif_time = $time;
        $insert = $agent_notif->insert();

        echo $insert;
    }

    public function actionGetuser() {
        $customerId = $_POST['q'];
        $customerInfo = Customer::getCustomerInformation($customerId);
        $fullDate = $customerInfo[0]['dob'];
        $pieces = explode(" ", $fullDate);
        // piece1
        $customerData = array(
            'customer_name' => $customerInfo[0]['customer_name'],
            'email' => $customerInfo[0]['email'],
            'contact' => $customerInfo[0]['contact_number'],
            'address' => $customerInfo[0]['address'],
            'dob' => $pieces[0],
            'gender' => $customerInfo[0]['gender'],
        );


        echo json_encode($customerData);
    }

    // This function loads Agent data according to the supervisor rank
    public function actionManage() {
        return $this->render('manageUsers');
    }
        /**
         * 
         * @author
         * @since 
         * @updated Shanila 2017/09/21
         * @description <p>updated this function to add a search function for supervisor</p>
         * 
         */
    public function actionManagetable() {
        $session = Yii::$app->session;
        $user_role = $session->get('user_role');
        if (!$session->has('user_id') || $user_role == '2' || $user_role == '4') {
            echo $user_role . ' ++++';
//            echo $user_role.' ';            
            $this->redirect('index.php?r=user/login_view');
        } else {

            if (isset($_GET['txtSearchNamekeyword'])) {
                if ($_GET['txtSearchNamekeyword'] != "") {

                    $userNameKeyword = $_GET['txtSearchNamekeyword'];
                    $userInfo = call_center_user::searchUserData($userNameKeyword);
                } else {
                    $userInfo = call_center_user::getUserData($user_role);
//            print_r($userInfo);
                }
            } else {
                $userInfo = call_center_user::getUserData($user_role);
            }
            $result = array();
            for ($i = 0; $i < count($userInfo); $i++) {
//                echo 'Hello '.$userInfo[$i]['name'];
                $result[$i]['id'] = $userInfo[$i]['id'];
                $result[$i]['name'] = $userInfo[$i]['name'];
                $result[$i]['fullname'] = $userInfo[$i]['fullname'];
                $result[$i]['role_id'] = $userInfo[$i]['role_id'];
                $result[$i]['status'] = $userInfo[$i]['status'];
            }

            return $this->render('manageUsersTable', ['user_data' => $result]);
        }
//        return $this->render('manageUsersTable');
    }

    public function actionInsert() {
        $this->checksession();
        $user = 'null';
        $userInfo = array();
        $availableExtensions = $this->getAvailableVOIPExtensions();
        if (isset($_GET['user'])) {
            $id = $_GET['user'];
            $userInfo = call_center_user::getUser($id);
            $result = array(
                'id' => $userInfo[0]['id'],
                'name' => $userInfo[0]['name'],
                'fullname' => $userInfo[0]['fullname'],
                'role_id' => $userInfo[0]['role_id'],
                'extension' => $userInfo[0]['voip_extension'],
                'user_profile_pic' => $userInfo[0]['user_profile_pic']
            );

//            if (count($availableExtensions) > 0) {
            array_push($availableExtensions, $userInfo[0]['voip_extension']);
            sort($availableExtensions);
//            }else{
//                $availableExtensions = array();
//            }


            return $this->render('addNewUser', ['user_data' => $result, 'availableExtensions' => $availableExtensions]);
        } else {
            return $this->render('addNewUser', ['user_data' => array('id' => '', 'name' => '', 'fullname' => '', 'role_id' => '', 'extension' => '', 'user_profile_pic' => ''), 'availableExtensions' => $availableExtensions]);
        }





//        return $this->render('addNewUser');
    }

    /**
     * <b>Get all available extension numbers</b>
     * <p>This function returns all the available extension numbers sorted by assending order.
     * If no available extensions, will return 0 </p>
     * 
     * @return array available VOIP Extension numbers / 0
     * @since 2017-08-04
     * @author Sandun
     */
    private function getAvailableVOIPExtensions() {
//    public function actionGetavailableextensions() {
        $allExtensions = web_presence::getAllVoipExtensions();
        $takenExtensions = call_center_user::getAllAssignedVOIPExtensions();

        $availableExtentions = array_diff($allExtensions, $takenExtensions);
        if (count($availableExtentions) > 0) {
            sort($availableExtentions);
            return $availableExtentions;
        } else {
            return array();
        }
    }

    private function checksession() {
        $session = Yii::$app->session;
        if (!$session->has('user_id')) {
            $this->redirect('index.php?r=user/login_view');
        } else {
            return true;
        }
    }

    public function actionAdduser() {
        $userId = $_POST['editUserId'];
        $recordType = 'save';
        if ($userId != '') {
            $recordType = 'update';
        }

        $data = array(
            'recordState' => 0, // 1 for password changed, 2 for profile pic changed, 3 for both changed, 0 both not changed
            'name' => $_POST['txtUserName'],
            'userId' => $userId,
            'role_id' => $_POST['selectUserRole'],
            'fullname' => $_POST['txtName'],
            'extension' => $_POST['selectVoipExtension'],
            'status' => 'active',
            'created_date' => date('Y-m-d H:i:s')
        );
        $password = $_POST['txtPassword'];
        if ($password != '') {
            $hashedPassword = md5($password);
            $data['hashedPassword'] = $hashedPassword;
            $data['recordState'] = 1;
        }
        $picChanged = $_POST['picChanged'];
        if ($picChanged == '1') {
            $data['user_profile_pic'] = $this->do_upload();
            if ($data['recordState'] == 1) {
                $data['recordState'] = 3;
            } else if ($data['recordState'] == 0) {
                $data['recordState'] = 2;
            }
        }
        if (call_center_user::saveCallCenterUser($data, $recordType)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    private function do_upload() {
        $type = explode('.', $_FILES["imgUserProfile"]["name"]);
        $type = strtolower($type[count($type) - 1]);
        $url = "hnb_images/user_profile_pics/" . uniqid(rand()) . '.' . $type;
        $thumbUrl = "hnb_images/user_profile_pics/thumbs/" . uniqid(rand()) . '.' . $type;
        if (in_array($type, array("jpg", "jpeg", "gif", "png")))
            if (is_uploaded_file($_FILES["imgUserProfile"]["tmp_name"])) {
                if (move_uploaded_file($_FILES["imgUserProfile"]["tmp_name"], $url)) {
                    $this->generate_image_thumbnail($url, $thumbUrl);
                    return $url;
                }
            }
        return "";
    }

    /*
     * PHP function to resize an image maintaining aspect ratio
     *
     * Creates a resized (e.g. thumbnail, small, medium, large)
     * version of an image file and saves it as another file
     */

    public function generate_image_thumbnail($source_image_path, $thumbnail_image_path) {

        $thumb_max_height = 250;
        $thumb_max_width = 250;

        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
        switch ($source_image_type) {
            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($source_image_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gd_image = imagecreatefromjpeg($source_image_path);
                break;
            case IMAGETYPE_PNG:
                $source_gd_image = imagecreatefrompng($source_image_path);
                break;
        }
        if ($source_gd_image === false) {
            return false;
        }
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = $thumb_max_width / $thumb_max_height;
        if ($source_image_width <= $thumb_max_width && $source_image_height <= $thumb_max_height) {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
            $thumbnail_image_width = (int) ($thumb_max_height * $source_aspect_ratio);
            $thumbnail_image_height = $thumb_max_height;
        } else {
            $thumbnail_image_width = $thumb_max_width;
            $thumbnail_image_height = (int) ($thumb_max_width / $source_aspect_ratio);
        }
        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);
        return true;
    }

    /*
     * This function will response agent requests
     * @author: Vikum
     * @since: 19/07/2017
     *     
     */

    public function actionAgentresponse() {
        $requestId = $_POST['q'];
        $response = $_POST['res'];
        $approveTime = $_POST['time'];
        $supervisorId = Yii::$app->session->get("user_id");
        $requestType = Agent_requests::getAgentRequest($requestId);
        if ($requestType == 'lunch') {
            $approveTime = '45';
        } else if ($requestType == 'short') {
            $approveTime = '5';
        } else if ($requestType == 'other') {
            $approveTime = '15';
        }
        $data = array(
            'responded_time' => date("Y-m-d H:i:s"),
            'approved_time_period' => $approveTime,
            'request_status' => $response,
            'supervisor_id' => $supervisorId,
            'consumed' => 0
        );
        $requestResponce = Agent_requests::setAgentRequestResponse($data, $requestId);
        if ($requestResponce) {
            return 1;
        } else {
            return 0;
        }
    }

    public function actionSend_misscall_email() {
        $lastRecord = Miss_calls_email_log::getLastEmailTimestamp();
        $lastTimestamp = $lastRecord->timestamp;
        $cdrCallRecords = cdr::getMissedCalls($lastTimestamp);
        $cdrCalls = array();
        foreach ($cdrCallRecords as $record) {
            $val = $record['uniqueid'];
            array_push($cdrCalls, "$val");
        }
        $callRecordsCalls = Call_records::selectLatestCallNumbers($lastTimestamp);
        $callRecordMissCalls = array();
        foreach ($callRecordsCalls as $callRecordsCall) {
            $val = $callRecordsCall['cdr_unique_id'];
            array_push($callRecordMissCalls, "$val");
        }
        $result = array_diff($cdrCalls, $callRecordMissCalls);
        $missedRecords = cdr::getCallsFromSelectedList($result);
        // Start email section

        if (count($missedRecords) > 0) {
            $adminEmail = call_center_user::getSupervisorEmail();
            $lastTime = gmdate("Y-m-d H:i:s", $lastTimestamp); // Last email log time
            $currentTime = date("Y-m-d H:i:s"); // Current email log time
            $body = "Hi <br> "
                    . "This email contains missed calls reported from $lastTime to $currentTime <br>";
            for ($i = 0; $i < count($missedRecords); $i++) {
                $body = $body . $missedRecords[$i]['src'] . "<br>";
            }
            $timezone = "Asia/Colombo";
            date_default_timezone_set($timezone);
            $body = $body . " Regards,<br>";
            $dataSet = array(
                'sentTo' => $adminEmail,
                'start_time' => $lastTime,
                'end_time' => $currentTime,
                'body' => $body
            );
            $this->sendMisscallEmail($dataSet);
//            print_r($dataSet);
        }
        // End email section
//        print_r($missedRecords);
    }

    private function sendMisscallEmail($data) {
        $timeRangeStart = $data['start_time'];
        $timeRangeEnd = $data['end_time'];
        Yii::$app->mailer->compose("@app/mail/layouts/html", ['contactForm' => $data['body']])
                ->setFrom('HNB General Insuerence Call Center')
                ->setTo($data['sentTo'])
                ->setTextBody($data['body'])
                ->setSubject("Call Center Application Miss Calls List within $timeRangeStart to $timeRangeEnd")
                ->send();
        return true;
    }

    public function actionDashboard() {
        return $this->render('dashboradView');
    }

    public function actionSavecontact() {
        $phone_number = $_POST['contact_number'];
        $contact_name = $_POST['contact_name'];
        $data = array(
            'number' => $phone_number,
            'name' => $contact_name,
            'created_date_time' => date("Y-m-d H:i:s")
        );
        $contactRecord = Contact_list::saveContact($data);
        if ($contactRecord) {
            echo 1;
        } else {
            echo 0;
        }
    }

    private function allcontacts() {

        $contactRecord = Contact_list::loadAllContact();
        $returnData = array();
        if ($contactRecord) {
            foreach ($contactRecord as $key) {
                $val = array(
                    'id' => $key['id'],
                    'number' => $key['contact_number'],
                    'name' => $key['contact_name']
                );
                array_push($returnData, $val);
            }

            return json_encode($returnData);
        } else {
            return 0;
        }
    }

    public function actionRefreshcontacts() {
        echo $this->allcontacts();
    }

    public function actionSearchcontact() {
        $search = $_POST['q'];
        if ($search != '') {
            $contactRecord = Contact_list::searchContact($search);
        } else {
            $contactRecord = Contact_list::loadAllContact();
        }
        if ($contactRecord) {
            $returnData = array();
            foreach ($contactRecord as $key) {
                $val = array(
                    'id' => $key['id'],
                    'number' => $key['contact_number'],
                    'name' => $key['contact_name']
                );
                array_push($returnData, $val);
            }
            echo json_encode($returnData);
        } else {
            echo 0;
        }
    }

    public function actionDelete_contact() {
        $id = $_POST['id'];
        $deleted = Contact_list::deleteContact($id);
        if ($deleted) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function actionPromoteagent() {
        $userId = $_GET['userId'];
        return call_center_user::setUserRole($userId, 4);
    }

    public function actionDemotesuperagent() {
        $userId = $_GET['userId'];
        return call_center_user::setUserRole($userId, 2);
    }

}
