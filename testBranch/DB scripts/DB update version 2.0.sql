
# HNB call center DB version 2.0
# @author Sandun
# @since 2017-08-13

# Database dump until this point of updates
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+05:30' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Table structure for table `agent_notifications`
--

DROP TABLE IF EXISTS `agent_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `notif_type` int(11) DEFAULT NULL,
  `notif_status` int(11) DEFAULT NULL,
  `notif_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `agent_requests`
--

DROP TABLE IF EXISTS `agent_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_type` enum('lunch','washroom','sick','other') DEFAULT NULL,
  `requested_time` datetime DEFAULT NULL,
  `responded_time` datetime DEFAULT NULL,
  `approved_time_period` float DEFAULT NULL COMMENT 'Only for a sick break and other break. Value will be on minutes',
  `request_status` enum('approved','pending','denied','closed','taken') DEFAULT NULL COMMENT '''Approved after supervisor approve the request, pending after agent requested the request, closed after closing the request''. Denied after supervisor denied the request, taken once agent take the request',
  `taken_time` datetime DEFAULT NULL,
  `closed_time` datetime DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `consumed` int(11) DEFAULT NULL COMMENT 'This column has a flag value to make sure record fetch by server events',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `call_center_user`
--

DROP TABLE IF EXISTS `call_center_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_center_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_profile_pic` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  `created_date` datetime DEFAULT NULL,
  `contact_number` int(10) DEFAULT NULL,
  `voip_extension` varchar(45) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL COMMENT 'This will be mandatory for supervisors',
  PRIMARY KEY (`id`),
  KEY `user_role_key_idx` (`role_id`),
  CONSTRAINT `user_role_key` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='This table is temporary, will be replaced with a oracle db table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `call_center_user`
--

LOCK TABLES `call_center_user` WRITE;
/*!40000 ALTER TABLE `call_center_user` DISABLE KEYS */;
INSERT INTO `call_center_user` VALUES (1,'admin','21232f297a57a5a743894a0e4a801fc3','HNB Administrator',1,NULL,'active',NULL,NULL,'807',NULL);
/*!40000 ALTER TABLE `call_center_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `call_records`
--

DROP TABLE IF EXISTS `call_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `call_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cdr_unique_id` varchar(50) DEFAULT NULL,
  `call_date` datetime DEFAULT NULL,
  `call_time` datetime DEFAULT NULL,
  `caller_number` varchar(20) DEFAULT NULL,
  `cli_number` int(11) DEFAULT NULL COMMENT 'This keeps the phone number of the call made from',
  `policy_number` int(11) DEFAULT NULL COMMENT 'This keeps the phone number that is actually associated with the customer data in the customer database',
  `comment` varchar(500) DEFAULT NULL COMMENT 'This keeps the typed comment of the call record',
  `notes` varchar(1000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `latest` int(11) DEFAULT NULL,
  `consumed` int(11) DEFAULT NULL,
  `timestamp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `callrecord_user_key_idx` (`user_id`),
  CONSTRAINT `callrecord_user_key` FOREIGN KEY (`user_id`) REFERENCES `call_center_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `dob` datetime DEFAULT NULL COMMENT 'This table is temporary, this table is to represent the oracle db',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `help_requests`
--

DROP TABLE IF EXISTS `help_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `consumed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 COMMENT='This table records the help requests made by agents';
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `logged_in_users`
--

DROP TABLE IF EXISTS `logged_in_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logged_in_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_logged_ip_address` varchar(60) DEFAULT NULL,
  `logged_in_time` varchar(50) DEFAULT NULL,
  `time_signature` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `miss_calls_email_log`
--

DROP TABLE IF EXISTS `miss_calls_email_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `miss_calls_email_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numbers` int(11) DEFAULT NULL COMMENT 'Count number of missed calls recorded in the given time.',
  `timestamp` varchar(50) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin'),(2,'Agent'),(3,'Supervisor'),(4,'Senior-Agent');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- Dump completed on 2017-08-13 15:53:37
