# This query will add a new column in Call_records table to keep call end time
ALTER TABLE `call_records` ADD `call_end_time` DATETIME NOT NULL AFTER `call_waiting_duration`;