#Changed 'mode' column to dnd_on_timestamp and 'timestamp' column to dnd_off_timestamp in dnd_records table. 
ALTER TABLE `hnbproject`.`dnd_records` 
CHANGE COLUMN `timestamp` `dnd_on_timestamp` DATETIME NULL DEFAULT NULL ,
CHANGE COLUMN `mode` `dnd_off_timestamp` DATETIME NULL DEFAULT NULL ;
