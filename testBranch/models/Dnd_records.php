<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is used to transfer data with the databse table dnd_records 
 *
 * @author Sandun
 * @since 2017-08-13
 * 
 * @updated Shanila
 * @since 2017/09/22
 *
 * 
 */
class Dnd_records extends ActiveRecord {

    /**
     * @author 
     * @since
     * 
     * @modified
     * @shanila
     * 2017/09/22
     * <p> modified the DND records table in order to remove dnd_mode and added a function to add 
      on, off timestamps separately to the dnd_records table </p>
     */
    public static function insertNewDNDRecord($state) {
        $Dnd_records = new Dnd_records();

        if ($state == "On") {
            $Dnd_records->user_id = Yii::$app->session->get('user_id');
            $Dnd_records->dnd_on_timestamp = date("Y-m-d H:i:s");

            return $Dnd_records->insert();
        } else if ($state == "Off") {
            $userId = Yii::$app->session->get('user_id');
            $Dnd_records = Dnd_records::find()
                    ->where("user_id = $userId")
                    ->andWhere("dnd_off_timestamp IS NULL")
                    ->one();
            $Dnd_records->dnd_off_timestamp = date("Y-m-d H:i:s");
            return $Dnd_records->update();
        }
    }

    /**
     * 
     * @param type $userId
     * @return type
     * 
     * @Modified
     * Shanila
     * 2017/09/28
     * <p> Modified the function to get the latest dnd state of user</p>
     * 
     */
    public static function getLatestDNDStateOfUser($userId) {
        $lastDndState = Dnd_records::find()
                ->select('dnd_off_timestamp')
                ->where("user_id = $userId")
                ->orderBy(["dnd_on_timestamp" => SORT_DESC])
                ->one();
        return $lastDndState['dnd_off_timestamp'];
    }

    /**
     * 
     * @param type $user_id
     * @author supun
     * @since 2017/08/14
     * @return DND records
     * 
     * @modified
     * Shanila
     * 2017/09/26
     * <p>Modified this function in order to select all dnd records by user</P>
     */
    public static function getDndRecordsByUser($user_id) {
        $obj = new Dnd_records();

        return $obj->find()
                        ->select('*')
                        ->where("user_id=$user_id")
                        ->orderBy(["dnd_on_timestamp" => SORT_DESC])
                        ->all();
    }

    /**
     * 
     * @param type $agent_id
     * @param type $fromDate
     * @param type $todate
     * @return DND record filter by date
     * @author supun
     * @since 2017/08/16
     * 
     * @Modified
     * Shanila
     * 2017/09/28
     * <p>Modified the function in order to search dnd_records by fromDate, toDate according to the changes made to dnd_records table in DB</p>
     */
    public static function getDndRecordsByDate($agent_id, $fromDate, $todate) {
        $obj = new dnd_records();
        //$filter = $todate;
        if ($agent_id != null && $fromDate != null && $todate != null) {
            return $obj->find()
                            ->select('*')
                            ->where(['between', 'dnd_on_timestamp', $fromDate, $todate])
                            ->orWhere(['between', 'dnd_off_timestamp', $fromDate, $todate])
                            ->andWhere("user_id = $agent_id")
                            ->orderBy(["dnd_on_timestamp" => SORT_DESC])
                            ->all();
        } else if ($agent_id != null && $fromDate == null && $todate != null) {
            return $obj->find()
                            ->select('*')
                            ->where("user_id=$agent_id")
                            ->andWhere(['between', 'dnd_off_timestamp', $todate . " 00:00:00", $todate . " 23:59:59"])
                            ->orderBy(["dnd_off_timestamp" => SORT_DESC])
                            ->all();
        } else if ($agent_id != null && $fromDate != null && $todate == null) {
            return $obj->find()
                            ->select('*')
                            ->where("user_id=$agent_id")
                            ->andWhere(['between', 'dnd_on_timestamp', $fromDate, date('Y-m-d h:i:s')])
                            ->orderBy(["dnd_on_timestamp" => SORT_DESC])
                            ->all();
        } else {
            return $obj->find()
                            ->select('*')
                            ->where("user_id=$agent_id")
                            ->orderBy(["dnd_off_timestamp" => SORT_DESC])
                            ->all();
        }
    }

}
