<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * abandon_calls model refers abandon_calls table in db3 'callevents'.
 *
 * @since 02/08/2017
 * @author Prabath
 */
class abandon_calls {

    public static function getDb() {
        return Yii::$app->db3;
    }

    /*
     * This function will get a list of abandoned calls by a specific agent by date. 
     * @author: Supun
     * @since: 16/08/2017
     *     
     */    
    public static function getAbandonedCallsByAgentByDate($agent_ex,$agent_id,$fromDate,$toDate) {
        $connection = Yii::$app->db3;
        if($agent_ex != null && $fromDate != null && $toDate != null )
        {
            $unixFromdate = strtotime($fromDate);
            $unixTodate = strtotime($toDate);
            $command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex' and timestamp > '$unixFromdate' and timestamp < '$unixTodate' order by timestamp desc");
            
        }
        else if($agent_ex != null && $fromDate == null && $toDate != null )
        {
            $unixFromdate = strtotime($toDate." 00:00:00");
            $unixTodate = strtotime($toDate." 23:59:59");
            $command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex' and timestamp > '$unixFromdate' and timestamp < '$unixTodate' order by timestamp desc");
            
        }
        else if($agent_ex != null && $fromDate != null && $toDate == null )
        {
            $unixFromdate = strtotime($fromDate);
            $unixTodate = strtotime(date('Y-m-d h:i:s'));
            $command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex' and timestamp > '$unixFromdate' and timestamp < '$unixTodate' order by timestamp desc");
            
        }
        else
        {
            $command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex' order by timestamp desc");
        }
        //$command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex'");
        
        $abandoned_history = $command->queryAll();
        //print_r($abandoned_history);
        //print_r($command);
        return $abandoned_history;
    }
    /*
     * This function will get a list of abandoned calls by a specific agent. 
     * @author: Supun
     * @since: 15/08/2017
     *     
     */    
    public static function getAbandonedCallsByAgentByUser($agent_ex) {
        $connection = Yii::$app->db3;
        
        $command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex' order by timestamp desc");

        
        //$command = $connection->createCommand("SELECT caller_number,timestamp FROM abandon_calls where agent_id = '$agent_ex'");
        
        $abandoned_history = $command->queryAll();
        //print_r($command);
        return $abandoned_history;
    }

}
