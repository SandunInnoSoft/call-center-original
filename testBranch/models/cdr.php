<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is to exchange data with cdr table in the PBX db
 *
 * @author Sandun
 * @since 2017-06-30
 */
class cdr extends ActiveRecord {

    public static function getDb() {
        return Yii::$app->db2;
    }

    /*
     * This function will get list of answered call by a specific agent. 
     * @author: Prabath
     * @since: 31/07/2017
     *     
     */

    public static function getAnsweredCallsListByUser($agent_ex) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming'  group by uniqueid order by start desc;");

        $answered_history = $command->queryAll();
        return $answered_history;
    }

    /*
     * return Answerd call details filter by date
     * @author: Supun
     * @since: 16/08/2017
     *     
     */

    public static function getAnsweredCallsListByDate($agent_ex, $fromDate, $toDate) {
        $connection = Yii::$app->db2;

        if ($agent_ex != null && $fromDate != null && $toDate != null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$fromDate' and '$toDate 23:59:59' order by start desc;");
        } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$toDate 00:00:00' and '$toDate 23:59:59' order by start desc;");
        } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' order by start desc;");
        } else {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' order by start desc;");
        }

        //$command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where dst = $agent_ex  and calltype = 'incoming' and start between '$fromDate' and '$toDate 23:59:59' order by start desc;");

        $answered_history = $command->queryAll();
        return $answered_history;
    }

    /*
     * return Outgoing call details filter by user
     * @author: Supun
     * @since: 16/08/2017
     *     
     */

    public static function getOutgoingCallsListByUser($agent_ex) {
        $connection = Yii::$app->db2;
//        $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");
        $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = '$agent_ex'  and calltype = 'outgoing' and DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d') = CURRENT_DATE()  group by uniqueid;");
//SELECT src,dst,start,answer,end,duration FROM cdr where src = '802'  and calltype = 'outgoing' and DATE_FORMAT(STR_TO_DATE(START, '%Y-%c-%e %H:%i:%s'), '%Y-%m-%d') = CURRENT_DATE()  group by uniqueid;
        $outgoing_history = $command->queryAll();
        return $outgoing_history;
    }

    /*
     * return Outgoing call details filter by date
     * @author: Supun
     * @since: 16/08/2017
     *     
     */

    public static function getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate) {
        $connection = Yii::$app->db2;

        if ($agent_ex != null && $fromDate != null && $toDate != null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing' and start between '$fromDate' and '$toDate 23:59:59' order by start desc;");
        } else if ($agent_ex != null && $fromDate == null && $toDate != null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing' and start between '$toDate 00:00:00' and '$toDate 23:59:59' order by start desc;");
        } else if ($agent_ex != null && $fromDate != null && $toDate == null) {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing' and start between '$fromDate' and '" . date('Y-m-d h:i:s') . "' order by start desc;");
        } else {
            $command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");
        }
        //$command = $connection->createCommand("SELECT src,dst,start,answer,end,duration FROM cdr where src = $agent_ex  and calltype = 'outgoing';");

        $outgoing_history = $command->queryAll();
        return $outgoing_history;
    }

    /*
     * This function will get list of answered call by a specific agent. 
     * @author: Prabath
     * @since: 31/07/2017
     *     
     */

    public static function getAnsweredCallsCount() {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT AcctId FROM cdr where calltype = 'incoming' group by uniqueid;");

        $outgoing_history = $command->queryAll();
        return count($outgoing_history);
    }

    public static function getHighestCdrID() {
//        $cdr = new cdr();
//        return $cdr->find()
//                        ->max('AcctId')
//                        ->all();
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT max(AcctId) FROM cdr;");

        $call_records = $command->queryAll();
        return $call_records[0]["max(AcctId)"];
    }

    public static function getNewCDRInfo($cdrId) {
//        $cdr = new cdr();
//        return $cdr->find()
//                        ->where("AcctId = $cdrId")
//                        ->one()
//                        ->all();
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT * FROM cdr where AcctId = $cdrId;");

        $call_records = $command->queryAll();
        return $call_records;
    }

    public static function getCallHistory() {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT src as caller,dst as reciever,start,end,duration FROM cdr order by start desc;");

        return $callHistory = $command->queryAll();
    }

    /**
     * <b>Get the longest call made by the given extension</b>
     * <p>This function returns the data associated with the longes call taken by the VOIp extension number passed as the paramater</p>
     * 
     * @param int $extensionNumber
     * @return array
     * @since 2017-07-25
     * @author Sandun
     */
    public static function getLongestCall($extensionNumber) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT * "
                . "FROM cdr  "
                . "where dst = '$extensionNumber' "
                . "order by duration desc "
                . "limit 1;");

        return $command->queryAll();
    }

    /**
     * <b>Get latest unique id for the number passed in parameters</b>
     * <p>This function the most recent unique id for the number passed in parameter</p>
     * 
     * @param int $callerNumber
     * @return array
     * @since 2017-08-02
     * @author Vikum
     */
    public static function getMostRecentUniqueId($callerNumber) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT uniqueid "
                . "FROM cdr  "
                . "where (unix_timestamp() - 20 < unix_timestamp(end)) AND"
                . "`src` = '$callerNumber'"
                . "group by uniqueid");

        return $command->queryAll();
    }

    public static function getMissedCalls($timestamp) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT uniqueid "
                . "FROM cdr  "
                . "where uniqueid > $timestamp "
                . "group by uniqueid");
        return $command->queryAll();
    }

    public static function getCallsFromSelectedList($data) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT src "
                . "FROM cdr  "
                . "where  uniqueid in (" . implode(',', $data) . ")"
                . "AND LENGTH(src) > 6 "
                . "group by src");
        return $command->queryAll();
    }

    /**
     * <b>Get cdr records of user</b>
     * <p>This function returns cdr records for a user number</p>
     * 
     * @author Vikum
     * @since 2017-07-21
     * 
     * @param number
     * @return array
     */
    public static function getRecordsByNumber($number) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("SELECT *, dst as comment, lastapp as status " // temporary fix to form the array for comments and status
                . "FROM cdr  "
                . "where  src = '$number' "
                . "group by uniqueid "
                . "order by AcctId desc");
        return $command->queryAll();
    }

    /**
     * <b>Get calls came today</b>
     * <p>This function all customer data of the current day</p>
     * 
     * @author Vikum
     * @since 2017-08-17
     * 
     * @param number
     * @return array
     */
    public static function getTodaysCalls() {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("select COUNT(DISTINCT uniqueid) as num "
                . "FROM cdr  "
                . "where STR_TO_DATE(start,'%Y-%m-%d') = CURDATE()");
        return $command->queryAll();
    }

    /**
     * <b>Get Call duration sum of the records by unique id</b>
     * <p>This function return all cdr records of the given unique id</p>
     * 
     * @author Vikum
     * @since 2017-08-21
     * 
     * @param unique_id
     * @return array
     */
    public static function getRecordsDurationByUniqueId($uniqueId) {
        $connection = Yii::$app->db2;
        $command = $connection->createCommand("select SUM(duration) as num "
                . "FROM cdr  "
                . "where uniqueid = $uniqueId");
        return $command->queryAll();
    }

    /**
     * <b>Returns cdr data matches with unique id and extension</b>
     * <p>This function returns a dataset array with all the call record information of the cdr record matches to the
     * unique id and the extension number passed as the paramater. If found returns the array else returns null</p>
     * 
     * @param string $uniqueId
     * @param int $extension
     * @return array / NULL
     * @since 2017-09-11
     * @author Sandun
     */
    public function getIncommingCdrDataByUniqueIdAndExtension($uniqueId, $extension) {
        $cdr = cdr::find()
                ->where("uniqueid = '$uniqueId'")
                ->andWhere("dst = '$extension'")
                ->one();
        
        if (count($cdr) > 0) {
            return $cdr;
        } else {
            return NULL;
        }
    }

}
