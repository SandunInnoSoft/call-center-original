<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is used to interract with web_presence table in DB3
 *
 * @author Sandun
 * @since 2017-08-03
 */
class web_presence extends ActiveRecord {

    /**
     * <b>Get all ongoing calls</b>
     * <p>This function returns all ongoing calls data in a array</p>
     * 
     * @since 2017-08-03
     * @author Sandun
     * @return Array Ongoing calls
     */
    public static function getOngoingCalls() {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("SELECT *, unix_timestamp() - callstart AS duration " // 19800 means seconds in 5.30 hours to match with sri lanka timezone
                . "FROM web_presence "
                . "WHERE state = 'INUSE' "
                . "AND cidnum IS NOT NULL AND CHAR_LENGTH(cidnum) > 6;");
        return $command->queryAll();
    }

    /**
     * <b>Get all the VOIP numbers</b>
     * <p>This function returns an array of all the VOIP numbers in the table</p>
     *
     * @return array Voip extension numbers
     * @since 2017-08-03
     * @author Sandun
     */
    public static function getAllVoipExtensions() {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("SELECT ext FROM web_presence");
        $allExtensions = $command->queryAll();

        $allExtensionsRemade = array();

        for ($x = 0; $x < count($allExtensions); $x++) {
            $allExtensionsRemade[$x] = $allExtensions[$x]['ext'];
        }

        return $allExtensionsRemade;
    }

    /**
     * <b>Get Current Summary</b>
     * <p>This function returns an array of all the VOIP numbers in the table</p>
     *
     * @return array all Summary
     * @since 2017-08-17
     * @author Vikum
     */
    public static function getAllSummary() {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("select state,count(ext) as num from web_presence GROUP BY state");
//        $command = $connection->createCommand(" SELECT DISTINCT(state),count(ext)  as num"
//                . " FROM web_presence"
//                . " GROUP BY 'state'");
//        select state,count(ext) from web_presence GROUP BY state;
        return $command->queryAll();
    }

    /**
     * <b>Returns current state of the extention</b>
     * <p>This function returns the current state of the extension passed as the parameter</p>
     * 
     * @param int $extension
     * @return String state
     * 
     * @author Sandun
     * @since 2017-08-24
     */
    public static function getExtensionState($extension) {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("SELECT state FROM web_presence WHERE ext = '$extension'");
        $extensionState = $command->queryAll();
        
        return $extensionState[0]['state'];
        
    }

}
