<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is to exchange data with call events table in the callevents db
 *
 * @author Vikum
 * @since 2017-08-02
 */
class callevents extends ActiveRecord {

    /**
     * <b>Get all queued calls</b>
     * <p>This function get all queued calls in the callevents table</p>
     * 
     * @author Sandun
     * @since 2017-08-02
     * @return Array Queued calls
     */
    public static function getAllQueuedCalls() {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("SELECT * FROM call_queue order by id asc;");

        return $command->queryAll();
    }

}
