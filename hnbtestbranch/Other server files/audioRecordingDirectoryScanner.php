/*
* This file should be uploaded to the "www" directory in PBX root, in order to audio playback functionality to work
* This scans the directory identified by the "ext" GET parameter passed by the URL and
* returns the file names of the audio files in the directory.
* 
* @since 2017-09-26
*/
<?php 
$extension = $_GET['ext'];
echo json_encode(scandir("/var/spool/asterisk/monitor/recording/$extension"))

?>