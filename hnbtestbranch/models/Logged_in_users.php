<?php

/**
 * This model clas is to interract with the logged_in_users table in the DB
 *
 * @author Sandun
 */

namespace app\models;

use yii\db\ActiveRecord;

class Logged_in_users extends ActiveRecord {

    public static function getLoggedAgents() {

        $agents = new Logged_in_users();
        return $agents->find()
                        ->select('*')
                        ->where("unix_timestamp() - 10 < time_signature")
                        ->andWhere("active = 1")
                        ->all();
    }

    public static function getDailyWorkedTime($id) {
        $loggedInUsers = new Logged_in_users();
        if ($id != 0) { // user id is given
            $data = $loggedInUsers->find()
//                ->select('')
                    ->where(['not', ['time_signature' => null]])
                    ->andWhere(['not', ['logged_in_time' => null]])
                    ->andWhere("DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%e') = CURDATE()")
                    ->andWhere("user_id = $id")
//                ->orWhere('request_status = closed')
//                ->andWhere('DATE(call_date) = CURDATE()')
                    ->sum('(time_signature - logged_in_time )/60');
        } else {
            $data = $loggedInUsers->find()
//                ->select('')
                    ->where(['not', ['time_signature' => null]])
                    ->andWhere(['not', ['logged_in_time' => null]])
                    ->andWhere("DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%e') = CURDATE()")
//                ->andWhere("user_id = $id")
//                ->orWhere('request_status = closed')
//                ->andWhere('DATE(call_date) = CURDATE()')
                    ->sum('(time_signature - logged_in_time )/60');
        }
        return $data;
//        return 7.5;
    }

    public static function getDailyWorkedTimeInGivenRange($id, $dateFrom, $dateTo) {
        if ($dateFrom != '' && $dateTo != '') {
            $loggedInUsers = new Logged_in_users();
            if ($id != 0) { // Id not for all users
                $data = $loggedInUsers->find()
//                ->select('')
                        ->where(['not', ['time_signature' => null]])
                        ->andWhere(['not', ['logged_in_time' => null]])
                        ->andWhere("DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%d') > STR_TO_DATE('$dateFrom', '%Y-%m-%d')")
                        ->andWhere("DATE_FORMAT(FROM_UNIXTIME(`time_signature`), '%Y-%m-%d') < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
                        ->andWhere("user_id = $id")
//                ->orWhere('request_status = closed')
//                ->andWhere('DATE(call_date) = CURDATE()')
                        ->sum('(time_signature - logged_in_time )/60');
            } else {
                $data = $loggedInUsers->find()
//                ->select('')
                        ->where(['not', ['time_signature' => null]])
                        ->andWhere(['not', ['logged_in_time' => null]])
                        ->andWhere("DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%d') > STR_TO_DATE('$dateFrom', '%Y-%m-%d')")
                        ->andWhere("DATE_FORMAT(FROM_UNIXTIME(`time_signature`), '%Y-%m-%d') < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
//                    ->andWhere("user_id = $id")
//                ->orWhere('request_status = closed')
//                ->andWhere('DATE(call_date) = CURDATE()')
                        ->sum('(time_signature - logged_in_time )/60');
            }
            return $data;
        } else {
            return 0;
        }

//        return 7.5;
    }

    public static function getThisMonthWorkedTime($id) {
        $loggedInUsers = new Logged_in_users();
        if ($id != 0) { // Id not for all users
            $data = $loggedInUsers->find()
//                ->select('')
                    ->where(['not', ['time_signature' => null]])
                    ->andWhere(['not', ['logged_in_time' => null]])
                    ->andWhere("MONTH(DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%e')) = MONTH(CURDATE())")
                    ->andWhere("user_id = $id")
//                ->orWhere('request_status = closed')
//                ->andWhere('DATE(call_date) = CURDATE()')
                    ->sum('(time_signature - logged_in_time )/60');
        } else { // For all users
            $data = $loggedInUsers->find()
//                ->select('')
                    ->where(['not', ['time_signature' => null]])
                    ->andWhere(['not', ['logged_in_time' => null]])
                    ->andWhere("MONTH(DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%e')) = MONTH(CURDATE())")
//                    ->andWhere("user_id = $id")
//                ->orWhere('request_status = closed')
//                ->andWhere('DATE(call_date) = CURDATE()')
                    ->sum('(time_signature - logged_in_time )/60');
        }
        return $data;
//        return 7.5;
    }

    public static function getLoginRecordsByUser($agent_id) {
        $login_records = new Logged_in_users();
        if ($agent_id != 0) { // IF agent id not for all users
            return $login_records->find()
                            ->select('logged_in_time,time_signature')
                            ->where("user_id = $agent_id")
                            ->andWhere("MONTH(DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%e')) = MONTH(CURDATE())")
                            ->orderBy("logged_in_time desc")
                            ->all();
        } else {
            return $login_records->find()
                            ->select('logged_in_time,time_signature')
                            ->where("MONTH(DATE_FORMAT(FROM_UNIXTIME(`logged_in_time`), '%Y-%m-%e')) = MONTH(CURDATE())")
                            ->orderBy("logged_in_time desc")
                            ->all();
        }
    }

    /**
     * 
     * @param type $agent_id
     * @param type $fromDate
     * @param type $toDate
     * @return Login Records filter by date
     * @author supun
     * @since 2017/08/16
     */
    public static function getLoginRecordsByDate($agent_id, $fromDate, $toDate) {
        $login_records = new Logged_in_users();
        if ($agent_id != 0) { // Agent id not for all agents
            if ($agent_id != null && $fromDate != null && $toDate != null) {
                $unixFromdate = strtotime($fromDate);
                $unixTodate = strtotime($toDate);
                $unixTodate = $unixTodate + 86400; // 86400 means the seconds of 24 hour period
                //$connection = Yii::$app->db;
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
                                ->where("user_id = $agent_id")
                                ->andWhere("logged_in_time > '$unixFromdate'")
                                ->andWhere("logged_in_time < '$unixTodate'")
                                ->orderBy("logged_in_time desc")
                                ->all();
            } else if ($agent_id != null && $fromDate == null && $toDate != null) {
                $unixFromdate = strtotime($toDate . " 00:00:00");
                $unixTodate = strtotime($toDate . " 23:59:59");
                $unixTodate = $unixTodate + 86400; // 86400 means the seconds of 24 hour period
                //$connection = Yii::$app->db;
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
                                ->where("user_id = $agent_id")
                                ->andWhere("logged_in_time > '$unixFromdate'")
                                ->andWhere("logged_in_time < '$unixTodate'")
                                ->orderBy("logged_in_time desc")
                                ->all();
            } else if ($agent_id != null && $fromDate != null && $toDate == null) {
                $unixFromdate = strtotime($fromDate);
                $unixTodate = strtotime(date('Y-m-d h:i:s'));
                $unixTodate = $unixTodate + 86400; // 86400 means the seconds of 24 hour period
                //$connection = Yii::$app->db;
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
                                ->where("user_id = $agent_id")
                                ->andWhere("logged_in_time > '$unixFromdate'")
                                ->andWhere("logged_in_time < '$unixTodate'")
                                ->orderBy("logged_in_time desc")
                                ->all();
            } else {
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
                                ->where("user_id = $agent_id")
                                ->orderBy("logged_in_time desc")
                                ->all();
            }
        } else {
            if ($agent_id != null && $fromDate != null && $toDate != null) {
                $unixFromdate = strtotime($fromDate);
                $unixTodate = strtotime($toDate);
                $unixTodate = $unixTodate + 86400; // 86400 means the seconds of 24 hour period
                //$connection = Yii::$app->db;
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
//                                ->where("user_id = $agent_id")
                                ->where("logged_in_time > '$unixFromdate'")
//                                ->andWhere("logged_in_time > '$unixFromdate'")
                                ->andWhere("logged_in_time < '$unixTodate'")
                                ->orderBy("logged_in_time desc")
                                ->all();
            } else if ($agent_id != null && $fromDate == null && $toDate != null) {
                $unixFromdate = strtotime($toDate . " 00:00:00");
                $unixTodate = strtotime($toDate . " 23:59:59");
                $unixTodate = $unixTodate + 86400; // 86400 means the seconds of 24 hour period
                //$connection = Yii::$app->db;
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
//                                ->where("user_id = $agent_id")
                                ->where("logged_in_time > '$unixFromdate'")
//                                ->andWhere("logged_in_time > '$unixFromdate'")
                                ->andWhere("logged_in_time < '$unixTodate'")
                                ->orderBy("logged_in_time desc")
                                ->all();
            } else if ($agent_id != null && $fromDate != null && $toDate == null) {
                $unixFromdate = strtotime($fromDate);
                $unixTodate = strtotime(date('Y-m-d h:i:s'));
                $unixTodate = $unixTodate + 86400; // 86400 means the seconds of 24 hour period
                //$connection = Yii::$app->db;
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
//                                ->where("user_id = $agent_id")
                                ->where("logged_in_time > '$unixFromdate'")
//                                ->andWhere("logged_in_time > '$unixFromdate'")
                                ->andWhere("logged_in_time < '$unixTodate'")
                                ->orderBy("logged_in_time desc")
                                ->all();
            } else {
                return $login_records->find()
                                ->select('logged_in_time,time_signature')
//                                ->where("user_id = $agent_id")
                                ->orderBy("logged_in_time desc")
                                ->all();
            }
        }



//        if($agent_id != null && $fromDate != null && $toDate != null )
//        {
//            $command = $connection->createCommand("SELECT logged_in_time,time_signature FROM logged_in_users where user_id = '$agent_id' and timestamp > '$unixFromdate' and timestamp < '$unixTodate'");
//            
//        }
//        else
//        {
//            
//        }

        $login_records = $command->queryAll();
        return $abandoned_history;
    }

    public static function getLoggedInTime($agentId) {
        $agent = new Logged_in_users();
        return $agent->find()
                        ->select("logged_in_time")
                        ->where("user_id = $agentId")
                        ->one();
    }

    /**
     * <b>Inserts a new record for the logged in user</b>
     * <p>this function inserts a new row to the logged in users table with data of just logged in agent</p>
     * 
     * @param int $userId
     * @param string $ipAddress
     * @return boolean
     * @since 2017-07-31
     * @author Sandun
     */
    public static function insertNewRecordForLogin($userId, $ipAddress) {

        $newRecord = new Logged_in_users();
        $newRecord->user_id = $userId;
        $newRecord->user_logged_ip_address = $ipAddress;
        $newRecord->logged_in_time = time();
        $newRecord->time_signature = time();
        $newRecord->active = 1;
        return $newRecord->insert();
    }

    /**
     * <b>sets old active records of the user to inactive</b>
     * <p>This function sets the old active record of the user to inactive</p>
     * 
     * @param int $userId
     * @return boolean
     * @since 2017-07-31
     * @author Sandun
     */
    public static function setOldRecordInactive($userId) {
        $user = Logged_in_users::findOne(['user_id' => $userId, 'active' => 1]);
        if ($user) {
            // old active record exists
            $user->active = 0;
            $user->update();
            return TRUE;
        } else {
            // no old active records
            return FALSE;
        }
    }

}
