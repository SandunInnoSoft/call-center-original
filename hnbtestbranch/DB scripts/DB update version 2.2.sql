# HNB Call center DB update version 2.2
# @author Sandun
# @since 2017-08-14

-- This script changes the data type of the `notif_status` from int to varchar(45)
ALTER TABLE `agent_notifications` CHANGE `notif_status` `notif_status` VARCHAR(45) NULL DEFAULT NULL;