<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .btn-default{
                background-color: #10297d !important;
                color: #faa61a !important;
            }
            .text-success{
                color: #10297d !important;
            }

        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="margin-top: 1%;">
                <div class="col-md-12">
                    <h3 class="text-left text-success"><strong>
                            Manage Users
                        </strong>
                    </h3>
                    <br>
                    <a name="btnAddNewUser" id="btnAddNewUser" class="btn btn-default btn-lg" href="<?= Url::to(['supevisor/insert']) ?>">Add New User</a>
                </div>

                <div class="col-md-10">
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-md-5" style="">


                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-4" style=" padding-left: 0px;  ">                                    
<!--                            <select id="id_Select_Action" class="form-control" style="  height: 30px;">                 
                                <option selected="selected"  value="sAction">Select Action</option>   
                                <option value="A">Active</option>
                                <option value="DA">De-active</option>                                                                                
                                <option value="D">Delete</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-default btn-block btn-sm" id="btnPlayerAction">
                                Apply
                            </button>
                        </div>                               
                        <div class="col-md-3" style=" margin-top: 1% ; padding-left: 0%; ">

                            <input id="txtSearchName" name="txtSearchName" type="text" class="form-control" placeholder="Search by name.."style="height: 22px;  " />
                        </div>
                        <div class="col-md-3" style="width: 15%; margin-top: 1% ;">
                            <button type="button" id="btnFind" class="btn btn-default btn-xs" style="color: #ffffff; margin-bottom: 0%;">
                                Find
                            </button>  -->                             
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row" style="margin-top: 3%;" id="userTableFrame">


        </div>
        <script>
            $(document).ready(function () {
                $('#userTableFrame').load("<?php echo Url::to(['supevisor/managetable']) ?>");
            });
        </script>
    </body>
</html>
