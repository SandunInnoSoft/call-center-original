<?php

return [
    'serverIpAddress' => '192.168.1.254', // Ip address of audio file ftp server
	'serverUrl'=>'https://192.168.1.254:9999/audioRecordingDirectoryScanner.php?ext=', // Path to get audio files 
    'ftp_path' => '/var/spool/asterisk/monitor/recording', // Path to audio file directory
    'username' => 'root', // FTP login user name
    'password' => 'Password123', // FTP login password
    'fileExtension' => 'wav', // audio file extension
    'audioFilePath' =>'https://192.168.1.254:9999/', // PBX audio file path url
];
