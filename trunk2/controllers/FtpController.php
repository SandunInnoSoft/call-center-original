<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Call_records;
use app\models\cdr;

/**
 * This controller is used to perform FTP operations with another remote server
 * 
 * @author Sandun
 * @since 2017-08-31
 */
class FtpController extends Controller {

    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        }
    }

    public function actionGetaudiofile() {
        $recordId = $_GET['id'];
        $direction = $_GET['direction'];
        $agentExtension = $_GET['extension'];
        $cdrUniqueId = Call_records::getCallRecordUniqueIdbyCallId($recordId);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';


//        $agentExtension = "2020";
//        $agentExtension = "800";

        if ($direction == "incoming") {
            $direction = 1;
        } else if ($direction == "outgoing") {
            $direction = 2;
        } else {
            $direction = 0;
        }

//        $fileName = $this->audioFileNameOfTheCall($cdrUniqueId, $agentExtension, $direction);
        $fileName = $this->getAudioFileName($cdrUniqueId, $agentExtension);
//        $fileName = "20170727-192929-2010-2020-1501163969.11-4";
//        $fileName = "20170919-174325-117221980-800-1505823203.75";
//        $fileName = "Linkin_Park_-_Battle_Symphony_lyrics";
        header('Content-Type: application/octet-stream');
        if ($ftpParams['fileExtension'] == "wav") {
            header('Content-Type: audio/wav');
        }if ($ftpParams['fileExtension'] == "gsm") {
            header('Content-Type: audio/x-gsm');
        }

//
//
        header('Cache-Control: no-cache');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
//        header('Content-Type: audio/x-gsm');
//        header("Content-disposition: attachment; filename=file.gsm");
//        ini_set("zlib.output_compression", "Off");
        $fileData = $this->getAudioFile($agentExtension, $fileName);
    }

    /**
     * <b>Returns the audio file from the remote server directory through SFTP</b>
     * <p></p>
     * 
     * @param type $agentExtension
     * @param type $fileName
     * @return type
     * @author Sandun
     * @since 2017-09-20
     */
    private function getAudioFile($agentExtension, $fileName) {
        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

//        $ipAddress = "10.100.21.238";
        $ipAddress = $ftpParams['serverIpAddress']; //"192.168.1.254";
        $ftpDirectory = $ftpParams['ftp_path'];
        $user = $ftpParams['username']; //"root";
        $pass = $ftpParams['password']; //"Password123";
//        $extension = "gsm";
        $extension = $ftpParams['fileExtension']; //"wav";
//        $filePath = "$ftpDirectory/$agentExtension/$fileName.$extension";
        $filePath = "$ftpDirectory/$agentExtension/$fileName";

        $c = curl_init("sftp://$user:$pass@$ipAddress:22/$filePath");
        curl_setopt($c, CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
        $data = curl_exec($c);
        curl_close($c);
        return $data;
    }

    public function actionAudio() {
        return $this->render("testAudio");
    }

    private function audioFileNameOfTheCall($cdrUniqueId, $agentExtension, $direction) {
        if ($direction == 1) {
// This means incomming call
            $cdrData = cdr::getIncommingCdrDataByUniqueIdAndExtension($cdrUniqueId, $agentExtension);
        } else if ($direction == 2) {
// This means outgoing call
            $cdrData = cdr::getOutgoingCdrDataByUniqueIdAndExtension($cdrUniqueId, $agentExtension);
        }

        if ($cdrData != Null) {
// has the data array
            $fileName = "";
            $dateAndTime = $cdrData['start'];
            $dateAndTime = str_replace('-', "", $dateAndTime);
            $dateAndTime = str_replace(':', "", $dateAndTime);
            $dateAndTime = str_replace(' ', "-", $dateAndTime);
            $fileName = $dateAndTime; // Date and time

            $fileName .= "-" . $cdrData['src']; // source number of the call

            $fileName .= "-" . $cdrData['dst']; // destination number of the call

            $fileName .= "-" . $cdrData['uniqueid']; // unique ID

            $fileName .= "-" . $cdrData['duration']; // call duration



            return $fileName;
        } else {
// doesnt have the data array
            return false;
        }
    }

    public function actionFilename() {
        $uniqueId = '1505297916.49';
// 1505972540.88 , 805 = Test data 1 
// 1505825433.90 , 805 = Test data 2
// 1504240332.10, 807 = Test data 3
        $extension = '807';
        $caller = '117221980';
        echo 'File name will be: ' . $this->getAudioFileName($uniqueId, $extension);
    }

    /**
     * <b>Get Call audio file name</b>
     * 
     * @param type $uniqueId
     * @param type $exension     
     * 
     * @author Vikum
     * @since 2017-09-25
     */
    private function getAudioFileName($uniqueId, $exension) {
        if ($uniqueId != null && $exension != null) {
            $fileNames = $this->getFileNames($exension);
            $cdrRecord = cdr::getIncommingCdrDataByUniqueIdAndExtension($uniqueId, $exension);
            $cdrStartDate = $cdrRecord['start'];

            $dateStart = strtotime($cdrStartDate);
            $cdrStartDate = date('Y-m-d H:i:s', $dateStart);

            $cdrEndDate = $cdrRecord['end'];
            $cdrEndDate = strtotime($cdrEndDate);
            $cdrEndDate = date('Y-m-d H:i:s', $cdrEndDate);

            $fileName = '';
//            $cdrEndDate = $cdrRecord['end'];
            if (count($fileNames) > 0) {
                foreach ($fileNames as $key) {

                    $splitName = explode("-", $key);
                    if (count($splitName) > 2) {
                        $date = substr($splitName[0], 0, -4) . '-' . substr($splitName[0], 4, -2) . '-' . substr($splitName[0], 6);
                        $time = substr($splitName[1], 0, -4) . ':' . substr($splitName[1], 2, -2) . ':' . substr($splitName[1], 4);

                        $createdDate = $date . ' ' . $time; // Find Date from file name
                        $createdDateTime = strtotime($createdDate);
                        $createdDate = date('Y-m-d H:i:s', $createdDateTime);

                        if ($createdDate > $cdrStartDate && $createdDate < $cdrEndDate) {
                            $fileName = $key;
                            break;
                        }
                    } else {
                        continue;
                    }
                }
            }

            return $fileName;
        }
    }

    private function getFileNames($extension) { // This function suppose to return all file names from remote directory which named extension passed through parameters
//    public function actionGetaud() { // This function suppose to return all file names from remote directory which named extension passed through parameters
//        return array('20170921-103738-117221980-805-1505970454.68.wav', '20170921-105704-803-805-1505971624.79-4.wav', '20170921-111227-117221980-805-1505972541.89.wav', '20170921-111833-117221980-805-1505972905.117.wav');
//        $extension = '801';
        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

//        $ipAddress = "10.100.21.238";
        $url = $ftpParams['serverUrl']; //"192.168.1.254";
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $dataset = file_get_contents($url . $extension, false, stream_context_create($arrContextOptions));
//        print_r(json_decode($dataset, true));
        return json_decode($dataset, true);
    }

}
