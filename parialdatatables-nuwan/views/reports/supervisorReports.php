<?php

use yii\helpers\Url;
use yii\helpers\Html;

$ftpParams = include __DIR__ . '/../../config/pbx_audio_ftp_config.php';
?>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>-->
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="js/chartjs.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<style>
    thead{
        font-weight: bold;
    }

    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }
    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<style>
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }

    .channelLabel{
        margin-right: 10px;
    }
</style>



<div  style="font-family: 'Lato' , sans-serif;">

    <div class="col-xs-2" >        
        <?php
        if (Yii::$app->session->get("user_role") != "2") {
            // user is a Agent
            if (Yii::$app->session->get("user_role") != "1") {
                // user is a supervisor
                ?>
                <div class="row" style="">
                    <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                        <div class="panel-heading"  style="height: 40px">
                            <h4 class="panel-title">
                                <a>Agents</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="" style="height: 650px;overflow-y: scroll">
                            <ul id="agentListGroup" class="list-group" style="">
                                <a id="0" style="cursor: pointer" onclick="loadQueueDataForSupervisor(this)" class="list-group-item">All records</a>
                                <a id="<?= Yii::$app->session->get('voip') ?>" style="cursor: pointer" onclick="callAgentReports(<?= Yii::$app->session->get('user_id') ?>,<?= Yii::$app->session->get('voip') ?>, '<?= Yii::$app->session->get('full_name') ?>', this.id)" class="list-group-item"><?= Yii::$app->session->get('full_name') . " : " . Yii::$app->session->get('voip') ?></a>
                                <?php for ($index = 0; $index < count($agents); $index++) { ?>
                                    <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['fullname'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['fullname'] . " : " . $agents[$index]['voip_extension'] ?></a>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                // user is an admin
                ?>

                <div class="row" style="">
                    <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                        <div class="panel-heading"  style="height: 40px">
                            <h4 class="panel-title">
                                <a>Agents</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="" style="height: 325px;overflow-y: scroll">
                            <ul id="agentListGroup" class="list-group" style="">
                                <a id="0" style="cursor: pointer" onclick="callAgentReports(0, 0, 0, 0)" class="list-group-item">All Agents</a>
                                <?php for ($index = 0; $index < count($agents); $index++) { ?>
                                    <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['fullname'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['fullname'] . " : " . $agents[$index]['voip_extension'] ?></a>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row" style="">
                    <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                        <div class="panel-heading"  style="height: 40px">
                            <h4 class="panel-title">
                                <a>Extension Queues</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="" style="height: 325px;overflow-y: scroll">
                            <ul id="queueListGroup" class="list-group" style="">
                                <?php for ($index = 0; $index < count($queues); $index++) { ?>
                                    <a id="<?= $queues[$index]['id'] ?>" style="cursor: pointer" onclick="callQueueReports(this.innerHTML, this.id)" class="list-group-item"><?= $queues[$index]['name'] ?></a>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <?php
            }
        } else {
            ?>
            <style>
                .agent-row{

                    margin-left: 60px;
                    margin-right: -48px;

                }               
            </style>
            <?php
        }
        ?>
    </div>
    <div class="agent-row">
        <div class="col-xs-2"></div>
        <div class="col-xs-10" style="padding: 0%">
            <div class="row">
                <div class="col-xs-3">
                    <label for="fromdate">Date from*</label>
                    <!--<input class="form-control activateFields" type="date" id="fromdate">-->       
                    <div class="input-group date" id="fromdate">
                        <input type="text" class="form-control" id="dateFromValue"/>	<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                    </div>
                    <label for="fromtime">Time from*</label>
                    <div class="form-group" id="fromtime">
                        <select id="timeFromSelect" class="form-control">
                            <?php
                            for ($i = 0; $i < 24; $i++) {
                                if ($i < 10) {
                                    ?>
                                    <option value="<?= '0' . $i . ':00:00' ?>"><?= '0' . $i . ':00' ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?= $i . ':00:00' ?>"><?= $i . ':00' ?></option>
                                    <?php
                                }
                            }
                            ?>
                            <option value="23:59:59">End of the day</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <label for="todate">Date to*</label>                
                    <!--<input class="form-control activateFields" type="date" id="todate">-->
                    <div class="input-group date" id="todate">
                        <input type="text" class="form-control" id="dateToValue"/>	<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                    </div>
                    <label for="fromtime">Time to*</label>
                    <div class="form-group" id="totime">
                        <select id="timeToSelect" class="form-control">
                            <?php
                            for ($i = 0; $i < 24; $i++) {
                                if ($i < 10) {
                                    ?>
                                    <option value="<?= '0' . $i . ':00:00' ?>"><?= '0' . $i . ':00' ?></option>
                                    <?php
                                } else {
                                    ?>
                                    <option value="<?= $i . ':00:00' ?>"><?= $i . ':00' ?></option>
                                    <?php
                                }
                            }
                            ?>
                            <option value="23:59:59" selected>End of the day</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <label for="contactNum">Contact</label>
                    <input class="form-control activateFields" type="tel" id="contactNum">
                </div>
                <div class="col-xs-3" style="padding-top: 23px">
                    <div class="row" style="padding-bottom: 1%">
                        <div class="col-xs-12">
                            <?php
                            if (yii::$app->session->get("user_role") == "2") {
                                ?>
                                <a class="btn btn-block btn-primary" id="btnSummeryView" onclick="filterRecordsSummery()">View Summery Report</a>
                                <?php
                            } else {
                                ?>
                                <a class="disabled btn btn-block btn-primary" id="btnSummeryView" onclick="filterRecordsSummery()">View Summery Report</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 1%">
                        <div class="col-xs-12">
                            <?php
                            if (yii::$app->session->get("user_role") == "2") {
                                ?>
                                <a class="btn btn-block btn-primary" id="btnView" onclick="showAnsweredCalls()">View Full Report</a> 
                                <?php
                            } else {
                                ?>
                                <a class="disabled btn btn-block btn-primary" id="btnView" onclick="showAgentAnsweredCallsForSupervisor()">View Full Report</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>                   
                </div>
                <input type="hidden" id="hiddenAgentId" />
                <input type="hidden" id="hiddenAgentExt" />
                <input type="hidden" id="hiddenAgentName" />
                <input type="hidden" id="hiddenQueueId" />
            </div>       
            <span id="searchTimeSpan"></span>
            <br>
            <img class="img-responsive" src="images/loading.gif" id="loadingGifImg" style="display: none">
            <div class="row" id="agentReportsDiv" style="padding-left: 1%"></div>
        </div>

    </div>
</div>

<!--View all the answered calls to the supervisor(triggered from the supervisor statistics)-->
<div id="supervisor" style="display:none">
    <div class="topic-label">
        <label>
            Incoming Answered Calls
        </label>       
    </div>
    <br/>
    <table id="table_supervisorAgent" class="display">
        <thead>
            <tr>
                <th class="incomingReceiver">Agent Name</th>
                <th class="incomingReceiver">Caller Number</th>
                <th class="incomingAnsweredTime">Call Answered Time</th>
                <th class="incomingEndTime">Call End Time</th>
                <th class="incomingDuration">Duration</th>
                <th class="incomingCallerPlyback">Incoming Call Playback</th>
            </tr>
        </thead>
        <tbody id="tbody-incoming-supervisor">

        </tbody>        
    </table>
</div>
<br/>




<!--show agent answered calls(triggered from the agent personal performance)
author: Nuwan 
-->
<div id="incoming" style="display:none">
    <div class="topic-label">
        <label>
            Incoming Answered Calls
        </label>       
    </div>
    <br/>
    <table id="table_id" class="display">
        <thead>
            <tr>
                <th class="incomingReceiver">Caller Number</th>
                <th class="incomingAnsweredTime">Call Answered Time</th>
                <th class="incomingEndTime">Call End Time</th>
                <th class="incomingDuration">Duration</th>
                <th class="incomingCallerPlyback">Incoming Call Playback</th>
            </tr>
        </thead>
        <tbody id="tbody-incoming">

        </tbody>        
    </table>
</div>
<br/>

<!--//View all the outgoing calls of the agent(Triggered from the agent personal performance)-->
<div id="outgoing" style="display:none">
    <div class="topic-label">
        <label>
            Outgoing Calls
        </label>       
    </div>
    <br/>    
    <table id="table_new" class="display">
        <thead>
            <tr>
                <th class="outgoingReceivedTime">Outgoing Receiver</th>
                <th class="outgoingAnsweredTime">Outgoing Answered Time</th>
                <th class="outgoingEndTime">Outgoing End Time</th>
                <th class="outgoingDuration">Outgoing Duration</th>
                <th class="outgoingCallerPlayback">Outgoing Call Playback</th>              
            </tr>
        </thead>
        <tbody id="tbody-outgoing">

        </tbody>        
    </table>
</div>

<!--View agent abondoned calls to the agent(Triggeres from the agent personal performance)-->
<br/>
<div id="abondond" style="display:none">
    <div class="topic-label">
        <label>
            Agent Abondand Calls
        </label>       
    </div>
    <br/>
    <table id="table_new2" class="display">
        <thead>
            <tr>
                <th class="abondondCallSource">Caller Source</th>
                <th class="abondondCallEnd">Call End Time</th>            

            </tr>
        </thead>
        <tbody id="tbody-agent-abondond">

        </tbody>        
    </table>
</div>
<br/>

<!--View agent DND records to the agent(Triggered from the agent personal perfomance)-->
<br/>
<div id="agentDnd" style="display:none">
    <div class="topic-label">
        <label>
            Agent DND Records
        </label>       
    </div>
    <br/>
    <table id="table_new3" class="display">
        <thead>
            <tr>              
                <th class="abondonDndMode">DND Mode</th>            
                <th class="abondondDndTimestamp">Date And Time</th>            

            </tr>
        </thead>
        <tbody id="tbody-agent-dnd">

        </tbody>        
    </table>
</div>
<br/>

<!--View all the logged in records of the agent(Triggered from the agent personal performance)-->
<div id="logging-agent" style="display:none">
    <div class="topic-label">
        <label>
            Agent Logged In Records
        </label>       
    </div>
    <br/>
    <table id="table_agent-logged" class="display">
        <thead>
            <tr>
                <th class="incomingReceiver">Agent Logged Ip</th>
                <th class="incomingAnsweredTime">Agent Logged Time</th>
                <th class="incomingEndTime">Agent Signature</th>            
            </tr>
        </thead>
        <tbody id="tbody-agent-logged">

        </tbody>        
    </table>
</div>
<br/>

<!-- audio playback modal -->
<div id="audioPlaybackModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="text-align:center">Call Audio Player</h4>
            </div>
            <div class="modal-body">
              <!-- <p>Some text in the modal.</p> -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="audioPlayerDiv">
                                <audio id="audioPlayer" controls class="audioPlayerClass">
                                    <source id="audioPlayerSource" src="" type="audio/wav">
                                </audio>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-md btn-info btn-block" onclick="downloadCurrentlyPlayingAudioFile()">Download</a>
                        </div>
                    </div>
                    <!-- Transfers section -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <h4 class="modal-title" style="text-align:center" id = "transfersH4Id">Call Transfers</h4> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <table id="transfersTable" class="table table-fixed table-striped" style="display: none">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Caller Number
                                        </th>
                                        <th>
                                            Answered Time
                                        </th>
                                        <th>
                                            Agent Name
                                        </th>
                                        <th>
                                            End Time
                                        </th>
                                        <th>
                                            Duration
                                        </th>
                                        <th>
                                            Call Playback
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End of Transfers section -->
                    <!-- Loading screen section -->
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <img class="img-responsive" src="images/loading.gif" id="transferDataLoadingGifImg" style="display: none">
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row" id="noTransfersAvailableMsgDiv" style="display: none">
                        <div class="col-md-12">
                            <div class="alert alert-warning" style="text-align: center">
                                <strong>No transfers available</strong>
                            </div>
                        </div>
                    </div>
                    <!-- End of Loading screen section -->                    
                </div>
            </div>
        </div>

    </div>
</div>


<script>
//Show all the answered calls to the supervisor(Trigger from the supervisor statics)    
    function showAgentAnsweredCallsForSupervisor() {

        $('#supervisor').css('display', 'block');

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var queueId = $('#hiddenQueueId').val();
        contactNumber = contactNumber.trim();


        if (contactNumber != "") {
            var pattern = /[^0-9]/;
            var result = contactNumber.match(pattern);
            if (result) {
                swal(
                        'Oops!!!',
                        'Contact number is incorrect. Please remove non numeric characters',
                        'error'
                        );
                return false;
            }
        }



        if (fromDate != "" && toDate != "") {

            if (queueId == "") {
                var url = "<?= Url::to(['reports/testing']) ?>";
            } else {
                var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
            }

            var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
            var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

            var fromDateConverted = new Date(fromDate + " " + fromTime);
            var toDateConverted = new Date(toDate + " " + toTime);

            if (fromDateConverted < toDateConverted) {

                alert(agentEx);

                var selectedChannels = getSelectedChannelIds();

                $('#table_supervisorAgent').DataTable({
                    "bFilter": false,

                    serverSide: true,

                    ajax: {
                        url: url,
                        type: 'GET',
                        data: {
                            fromDate: fromDate,
                            toDate: toDate,
                            agent_id: agentId,
                            agentex: agentEx,
                            agentname: agentName,
                            contactNum: contactNumber,
                            queue_id: queueId,
                            fromTime: fromTime,
                            toTime: toTime
                        }
                    },
                    columns: [
                        {data: 'name'},
                        {data: 'src'},
                        {data: 'start'},
                        {data: 'answer'},
                        {data: 'duration'},
                        {data: 'voip'}
                    ],
                    "columnDefs": [
                        {
                            "targets": 4,
                            "render": function (data, type, row) {
                                return `<a data-toggle="modal" id="audioplay" data-target="#audioPlaybackModal" data-start='${row.start}' data-src=${row.src} data-voip=${row.voip} data-type='I'  class="btn btn-md btn-success btn-block">Play</a>`;
                            },
                            "className": 'text-center'
                        }
                    ],
                    "responsive": true
                });
            } else {
                alert("Select a valid date range");
            }
        } else {
            alert("Date range cannot be empty");
        }
        //showOutgoingCallsTable();
    }





//Get all the logged in records of the agent (Triggered from the agent personal performance)
    function showAgentLoggedInRecords() {

        $('#logging-agent').css('display', 'block');
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var queueId = $('#hiddenQueueId').val();
        var queueId = $('#hiddenQueueId').val();
        contactNumber = contactNumber.trim();
        if (contactNumber != "") {
            var pattern = /[^0-9]/;
            var result = contactNumber.match(pattern);
            if (result) {
                swal(
                        'Oops!!!',
                        'Contact number is incorrect. Please remove non numeric characters',
                        'error'
                        );
                return false;
            }
        }

        var url = "<?= Url::to(['reports/agentloggedinrecords']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);

        if (fromDate != "" && fromTime != "") {

            if (fromDateConverted < toDateConverted) {

                var selectedChannels = getSelectedChannelIds();
                $('#table_agent-logged').DataTable({
                    "bFilter": false,
                    serverSide: true,
                    ajax: {
                        url: url,
                        type: 'GET',
                        data: {
                            fromDate: fromDate,
                            toDate: toDate,
                            agent_id: agentId,
                            agentex: agentEx,
                            agentname: agentName,
                            contactNum: contactNumber,
                            queue_id: queueId,
                            fromTime: fromTime,
                            toTime: toTime
                        }
                    },
                    columns: [
                        {data: 'ip'},
                        {data: 'time'},
                        {data: 'signature'}
                    ]
                });
            } else {
                alert("Select a valid date range");
            }
        }
    }


//download audio record function
    function downloadCurrentlyPlayingAudioFile() {
        var url = $("#audioPlayerSource").attr("src");
        if (url != "") {
            console.log("downloading audio file from " + url);
            $("#downloaderIframe").attr("src", url);
        } else {
            console.log("No audio url to download");
        }
    }

    //Agent dnd records view to the data tables 
    function agentDndRecords() {

        $('#agentDnd').css('display', 'block');
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var url = "<?= Url::to(['reports/agentdndrecordsfun']) ?>";
        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);
        if (fromDateConverted < toDateConverted) {
            var selectedChannels = getSelectedChannelIds();
            $('#table_new3').DataTable({
                "bFilter": false,
                serverSide: true,
                ajax: {
                    url: url,
                    type: 'GET',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        agent_id: agentId,
                        fromTime: fromTime,
                        toTime: toTime
                    }
                },
                columns: [
                    {data: 'dndMode'},
                    {data: 'time'}
                ]
            });
        } else {
            alert("Select a valid date range");
        }
        showAgentLoggedInRecords();
    }

    //Agent Abondond calls function 
    function agentAbondondCalls() {

        $('#abondond').css('display', 'block');
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var queueId = $('#hiddenQueueId').val();
        contactNumber = contactNumber.trim();
        if (contactNumber != "") {
            var pattern = /[^0-9]/;
            var result = contactNumber.match(pattern);
            if (result) {
                swal(
                        'Oops!!!',
                        'Contact number is incorrect. Please remove non numeric characters',
                        'error'
                        );
                return false;
            }
        }


        var url = "<?= Url::to(['reports/agentabondondcalls']) ?>";
        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);
        if (fromDateConverted < toDateConverted) {
            var selectedChannels = getSelectedChannelIds();
            $('#table_new2').DataTable({
                "bFilter": false,
                serverSide: true,
                ajax: {
                    url: url,
                    type: 'GET',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id: queueId,
                        fromTime: fromTime,
                        toTime: toTime
                    }
                },
                columns: [
                    {data: 'src'},
                    {data: 'end'}
                ]
            });
        } else {
            alert("Select a valid date range");
        }
        agentDndRecords();
    }

//incoming answered call records for the agent for personal performance  
    function showAnsweredCalls() {

        var incomingNew = document.getElementById('incoming');
        if ($(incomingNew).css("display") === "none") {
            $(incomingNew).css("display", "block");
        }

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var queueId = $('#hiddenQueueId').val();
        contactNumber = contactNumber.trim();
        if (contactNumber != "") {
            var pattern = /[^0-9]/;
            var result = contactNumber.match(pattern);
            if (result) {
                swal(
                        'Oops!!!',
                        'Contact number is incorrect. Please remove non numeric characters',
                        'error'
                        );
                return false;
            }
        }

        var queueId = $('#hiddenQueueId').val();
        if (queueId == "") {
            var url = "<?= Url::to(['reports/testing']) ?>";
        } else {
            var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);
        if (fromDateConverted < toDateConverted) {

            var selectedChannels = getSelectedChannelIds();
            $('#table_id').DataTable({
                "bFilter": false,
                serverSide: true,
                ajax: {
                    url: url,
                    type: 'GET',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id: queueId,
                        fromTime: fromTime,
                        toTime: toTime
                    }
                },
                columns: [
                    {data: 'src'},
                    {data: 'start'},
                    {data: 'answer'},
                    {data: 'duration'},
                    {data: 'voip'}
                ],
                "columnDefs": [
                    {
                        "targets": 4,
                        "render": function (data, type, row) {
                            return `<a data-toggle="modal" id="audioplay" data-target="#audioPlaybackModal" data-start='${row.start}' data-src=${row.src} data-voip=${row.voip} data-type='I'  class="btn btn-md btn-success btn-block">Play</a>`;
                        },
                        "className": 'text-center'
                    }
                ],
                "responsive": true
            });
        } else {
            alert("Select a valid date range");
        }

        showOutgoingCallsTable();
    }
//pause audio file
    function pauseAudio() {
        //                    alert($('#audioPlayer')[0].paused);
        if ($('.audioPlayerClass')[0].paused == false) {
            $('.audioPlayerClass')[0].pause();
        }
    }

    //For display the model

    $('#audioPlaybackModal').on('hidden.bs.modal', function () {
        $("#transfersTable").css("display", "none");
        pauseAudio();
    });
    $('html').on('click', '#audioplay', function (e) {
        e.preventDefault();
        var start = e.target.getAttribute('data-start');
        var src = e.target.getAttribute('data-src');
        var ext = e.target.getAttribute('data-voip');
        var type = e.target.getAttribute('data-type');
        playCallRecord(type, ext, src, start);
    });
//Play incoming call records and outgoing records play function     

    function playCallRecord(direction, extension, customerNumber, datetime) {

        var audioFilePath = "<?= Url::to(['ftp/playaudiofile']) ?>" + "&datetime=" + datetime.replace(" ", "+") + "&callType=" + direction + "&ext=" + extension + "&phoneNumber=" + customerNumber;
        $(".audioPlayerClass").remove();
        var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
        $("#audioPlayerDiv").append(newAudioElement);
        $(newAudioElement)[0].play();
    }

//outgoing calls records. trigger from the agent individual performance
    function showOutgoingCallsTable() {

        $('#outgoing').css("display", "block");
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var queueId = $('#hiddenQueueId').val();
        contactNumber = contactNumber.trim();
        if (contactNumber != "") {
            var pattern = /[^0-9]/;
            var result = contactNumber.match(pattern);
            if (result) {
                swal(
                        'Oops!!!',
                        'Contact number is incorrect. Please remove non numeric characters',
                        'error'
                        );
                return false;
            }
        }

//        var queueId = $('#hiddenQueueId').val();
//        if (queueId == "") {
        var url = "<?= Url::to(['reports/testoutgoingcalls']) ?>";
//        } else {
//            var url = "";
//        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);
        if (fromDateConverted < toDateConverted) {

            var selectedChannels = getSelectedChannelIds();
            $('#table_new').DataTable({
                "bFilter": false,
                serverSide: true,
                ajax: {
                    url: url,
                    type: 'GET',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        agent_id: agentId,
                        agentex: agentEx,
                        agentname: agentName,
                        contactNum: contactNumber,
                        queue_id: queueId,
                        fromTime: fromTime,
                        toTime: toTime
                    }
                },
                columns: [
                    {data: 'voip'},
                    {data: 'answer'},
                    {data: 'end'},
                    {data: 'duration'},
                    {data: 'src'}
                ],
                "columnDefs": [
                    {
                        "targets": 4,
                        "render": function (data, type, row) {
                            return `<a data-toggle="modal" id="audioplay" data-target="#audioPlaybackModal" data-start=${row.start} data-src=${row.src} data-voip=${row.voip} data-type='O'  class="btn btn-md btn-success btn-block">Play</a>`;
                        },
                        "className": 'text-center'
                    }
                ],
                "responsive": true
            });
        } else {
            alert("Select a valid date range");
        }

        agentAbondondCalls();
    }

//    function playCallRecord(playAudioAnchor, direction, extension, customerNumber, datetime) {
//        var row = playAudioAnchor.parentNode.parentNode;
//        var id = row.id;
//        var audioFilePath = "<? = Url::to(['ftp/playaudiofile']) ?>" + "&datetime=" + datetime + "&callType=" + direction + "&ext=" + extension + "&phoneNumber=" + customerNumber;
//        console.log("Playing audio file path = " + audioFilePath);
//        $(".audioPlayerClass").remove();
//        var newAudioElement = $("<audio id='audioPlayer' class='audioPlayerClass' controls></audio>").append("<source id='audioPlayerSource' src='" + audioFilePath + "' type='audio/wav'> ");
//        $("#audioPlayerDiv").append(newAudioElement);
//        $(newAudioElement)[0].play();
//        //createShowTransfersButton(direction, extension, customerNumber, datetime, id);
//    }


//
//    function testFuc() {
//
//        var searchStartDate = new Date();
//
//        var fromDate = $('#dateFromValue').val();
//        var toDate = $('#dateToValue').val();
//        var agentId = $('#hiddenAgentId').val();
//        var agentEx = $('#hiddenAgentExt').val();
//        var agentName = $('#hiddenAgentName').val();
//        var contactNumber = $('#contactNum').val();
//        contactNumber = contactNumber.trim();
//
//        if (contactNumber != "") {
//            var pattern = /[^0-9]/;
//            var result = contactNumber.match(pattern);
//            if (result) {
//                swal(
//                        'Oops!!!',
//                        'Contact number is incorrect. Please remove non numeric characters',
//                        'error'
//                        );
//                return false;
//            }
//        }
//
//
//        var queueId = $('#hiddenQueueId').val();
//        if (queueId == "") {
//            var url = "< ?= Url::to(['reports/performanceoverviewbydate']) ?>";
//        } else {
//            var url = "< ?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
//        }
//
//        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
//        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
//
//        var fromDateConverted = new Date(fromDate + " " + fromTime);
//        var toDateConverted = new Date(toDate + " " + toTime);
//
//        var selectedChannels = getSelectedChannelIds();
//
//        if (fromDateConverted < toDateConverted) {
//            showLoadingScreen();
//            $.ajax({
//                type: "POST",
//                url: url,
//                data: {
//                    fromdate: fromDate,
//                    todate: toDate,
//                    agent_id: agentId,
//                    agentex: agentEx,
//                    agentname: agentName,
//                    contactNum: contactNumber,
//                    queue_id: queueId,
//                    fromTime: fromTime,
//                    toTime: toTime,
//                    channels: selectedChannels
//                },
//                success: function (data)
//                {
//                    hideLoadingScreen();
//                    console.log(data);
//                    //  var result = $(data).find('#agentReportsDIV');
//                    //$("#agentReportsDiv").html(result);
//                    //showSearchTimeDuration(searchStartDate, new Date());
//                },
//                error: function (xhr, ajaxOptions, thrownError) {
//                    hideLoadingScreen();
//                    showErrorMessage();
//                    console.log('filter records : ' + xhr.responseText + ', \n\
//                        status: ' + xhr.status + ', \n\
//                        thrownError: ' + thrownError);
//                    //                    alert(xhr.responseText);
//                    //                    alert(xhr.status);
//                    //                    alert(thrownError);
//                    //                    alert(xhr.responseText);
//
//                }
//            });
//        } else {
//            swal(
//                    'Oops!!!',
//                    'Invalid dates, please select a valid date range.',
//                    'error'
//                    );
//        }
//
//    }

    $('#fromdate').datetimepicker({
        //        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#todate').datetimepicker({
        //        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    function callAgentReports(agent_id, agent_ex, agent_name, id) {
        //active view button
<?php
if (Yii::$app->session->get("user_role") == "1") {
// user is admin
    ?>
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label>No channels available</label>");
    <?php
}
?>
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(agent_ex);
        $('#hiddenAgentName').val(agent_name);
        $('#hiddenAgentId').val(agent_id);
        $('#hiddenQueueId').val("");
        var url = "<?= Url::to(['reports/performanceoverviewbyagent']) ?>";
        //Start : set agents panel css
        {
            var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");
        }
    }

    function filterRecords() {
        var searchStartDate = new Date();
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();
        if (contactNumber != "") {
            var pattern = /[^0-9]/;
            var result = contactNumber.match(pattern);
            if (result) {
                swal(
                        'Oops!!!',
                        'Contact number is incorrect. Please remove non numeric characters',
                        'error'
                        );
                return false;
            }
        }


        var queueId = $('#hiddenQueueId').val();
        if (queueId == "") {
            var url = "<?= Url::to(['reports/performanceoverviewbydate']) ?>";
        } else {
            var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);
        var selectedChannels = getSelectedChannelIds();
        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    fromdate: fromDate,
                    todate: toDate,
                    agent_id: agentId,
                    agentex: agentEx,
                    agentname: agentName,
                    contactNum: contactNumber,
                    queue_id: queueId,
                    fromTime: fromTime,
                    toTime: toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
                    hideLoadingScreen();
                    console.log(data);
                    //  var result = $(data).find('#agentReportsDIV');
                    //$("#agentReportsDiv").html(result);
                    //showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
                    console.log('filter records : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
                    //                    alert(xhr.responseText);
                    //                    alert(xhr.status);
                    //                    alert(thrownError);
                    //                    alert(xhr.responseText);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }


    }

    /**
     * 
     * @returns {undefined}
     * 
     * @since 2017-10-30
     * @author Sandun
     * 
     */
    function filterRecordsSummery() {

        var searchStartDate = new Date();
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var queueId = $('#hiddenQueueId').val();
        if (queueId == "") {
            var url = "<?= Url::to(['reports/summery']) ?>";
        } else {
            var url = "<?= Url::to(['reports/summerybyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        //        alert(fromDate + " +++ " + toDate);
        var fromDateConverted = new Date(fromDate + " " + fromTime);
        var toDateConverted = new Date(toDate + " " + toTime);
        var selectedChannels = getSelectedChannelIds();
        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    fromdate: fromDate,
                    todate: toDate,
                    agent_id: agentId,
                    agentex: agentEx,
                    agentname: agentName,
                    contactNum: contactNumber,
                    queue_id: queueId,
                    fromTime: fromTime,
                    toTime: toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
                    //                    alert(data);
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
                    alert(xhr.responseText);
                    console.log('filter records summery : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }

    }

    function showLoadingScreen() {
        $("#agentReportsDiv").empty();
        $("#loadingGifImg").css("display", "inline");
        $("#searchTimeSpan").html(""); // clears the search time diplay span   
        return true;
    }

    function hideLoadingScreen() {
        $("#loadingGifImg").css("display", "none");
        return true;
    }

    function showErrorMessage() {
        $("#agentReportsDiv").empty();
        $("#agentReportsDiv").append("ERROR Occured. Please try again!");
    }


    function callQueueReports(queue_name, id) {
        //active view button
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val(queue_name);
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val(id);
        //Start : set agents panel css
        {
            var items = document.getElementById('queueListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
            $("#agentListGroup > a").css("backgroundColor", "white");
            $("#agentListGroup > a").css("color", "black");
        }

        loadChannelsInfoOfTheQueue(id);
    }



    function loadChannelsInfoOfTheQueue(extQueueId) {
        if (extQueueId) {
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label>Loading channels...</label>");
            $.ajax({
                type: "GET",
                url: "<?= Url::to(['reports/findchannelsforqueue']) ?>",
                data: {
                    queueId: extQueueId
                },
                success: function (data) {
                    $("#channelsListDiv").empty();
                    if (data != "0") {
                        var queueChannelsObj = JSON.parse(data);
                        for (var x = 0; x < queueChannelsObj.length; x++) {
                            var channelTickBox = $("<input>");
                            $(channelTickBox).attr("type", "checkbox");
                            $(channelTickBox).val(queueChannelsObj[x]['id']);
                            $(channelTickBox).addClass("channelCheckBoxesClass");
                            $(channelTickBox).attr("id", "channel" + queueChannelsObj[x]['channel_number']);
                            var channelName = " " + queueChannelsObj[x]['channel_name'] + " ";
                            var channelNameLabel = $("<label></label>");
                            $(channelNameLabel).attr("data-toggle", "tooltip");
                            $(channelNameLabel).attr("data-placement", "left");
                            $(channelNameLabel).addClass("channelLabel");
                            $(channelNameLabel).attr("title", queueChannelsObj[x]['channel_description']);
                            $(channelNameLabel).append(channelTickBox);
                            $(channelNameLabel).append(channelName);
                            $("#channelsListDiv").append(channelNameLabel);
                        }
                    } else {
                        $("#channelsListDiv").append("<label>No channels available</label>");
                    }
                },
                error: function (error) {
                    console.log("load channels error = " + error.responseText);
                    $("#channelsListDiv").empty();
                    $("#channelsListDiv").append("<label style='color:red'>Error loading channels</label>");
                }
            });
        } else {
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label style='color:red'>Queue ID is incorrect. Please select the queue again</label>");
        }
    }

    function loadQueueDataForSupervisor(element) {
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val("");
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val("");
        var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        var items = document.getElementById('agentListGroup').getElementsByTagName('a');
        for (var i = 0; i < items.length; i++) {
            items[i].style.backgroundColor = "white";
            items[i].style.color = "black";
        }
        element.style.backgroundColor = "grey";
        element.style.color = "white";
        $("#queueListGroup > a").css("backgroundColor", "white");
        $("#queueListGroup > a").css("color", "black");
    }

    function getSelectedChannelIds() {
        if ($(".channelCheckBoxesClass:checked").length > 0) {
            // channels selected
            var selectedChannelsCsvString = "";
            $(".channelCheckBoxesClass:checked").each(function () {
                var channelNumber = $(this).attr('id');
                channelNumber = channelNumber.replace("channel", "");
                selectedChannelsCsvString = selectedChannelsCsvString + channelNumber + ",";
            });
            return selectedChannelsCsvString;
        } else {
            // no channels selected
            return "0";
        }
    }

    function showSearchTimeDuration(start, end) {
        var diff = end - start;
        $("#searchTimeSpan").html("Search time = " + (diff / 1000) + " seconds");
        return true;
    }

</script>