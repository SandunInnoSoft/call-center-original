# HNB Assurance DB update version 3.7
# @author Sandun
# @since 2017-12-11
# @target DB1 hnbassuranceccdb

-- --- This script creates 2 tables to record extension queues information and queue_extension intermediate table

CREATE TABLE `extension_queue` (
`id`  int NOT NULL AUTO_INCREMENT ,
`name`  varchar(255) NULL ,
PRIMARY KEY (`id`)
);

CREATE TABLE `ext_queue_intermediate` (
`id`  int NOT NULL AUTO_INCREMENT ,
`queueid`  int(11) NULL ,
`voip_extension`  int(11) NULL ,
PRIMARY KEY (`id`)
);

-- --- Adds a new column to the extension queue table to record the supervisor id in charge for the queue
ALTER TABLE `extension_queue`
ADD COLUMN `supervisor_id`  int(11) NULL AFTER `name`;





