# HNB DB update version 1.25
# @author Sandun
# @since 2017-08-08

-- This script sets the DB timezone to Sri lanka timezone
SET GLOBAL time_zone = '+5:30';
SET @@global.time_zone = '+05:30';