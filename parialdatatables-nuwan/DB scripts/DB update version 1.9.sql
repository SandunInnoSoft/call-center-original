# HNB Call center DB update version 1.9
# @author Sandun
# @since 2017-07-13


-- This inserts the a new column to the help_requests table to mark the records as consumed
ALTER TABLE `help_requests` 
ADD COLUMN `consumed` TINYINT(1) NULL AFTER `datetime`;
