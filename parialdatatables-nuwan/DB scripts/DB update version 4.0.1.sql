/*
HNB Assurance DB update version 4.0.1
@author Sandun
@since 2018-1-30
@target DB3 callevents


Navicat MariaDB Data Transfer

Source Server         : HNB Assurance CC DB prod
Source Server Version : 100128
Source Host           : 192.168.192.62:3306
Source Database       : callevents

Target Server Type    : MariaDB
Target Server Version : 100128
File Encoding         : 65001

Date: 2018-01-30 11:09:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for abandon_calls
-- ----------------------------
DROP TABLE IF EXISTS `abandon_calls`;
CREATE TABLE `abandon_calls` (
  `agent_id` varchar(45) DEFAULT NULL,
  `caller_number` varchar(45) DEFAULT NULL,
  `timestamp` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table ';

-- ----------------------------
-- Table structure for call_answer_times
-- ----------------------------
DROP TABLE IF EXISTS `call_answer_times`;
CREATE TABLE `call_answer_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caller_number` int(11) DEFAULT NULL,
  `answered_extension` int(6) DEFAULT NULL,
  `answered_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `answerTimesUniqueness` (`answered_time`,`answered_extension`)
) ENGINE=InnoDB AUTO_INCREMENT=2925 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for call_forwards
-- ----------------------------
DROP TABLE IF EXISTS `call_forwards`;
CREATE TABLE `call_forwards` (
  `caller_number` int(11) NOT NULL COMMENT 'caller cli number',
  `agent_extension` int(11) DEFAULT NULL COMMENT 'agent extension number',
  `time_stamp` varchar(100) NOT NULL DEFAULT '0' COMMENT 'Unix timestamp ',
  `created_date` datetime DEFAULT NULL COMMENT 'Current date time',
  `state` int(11) DEFAULT '1',
  PRIMARY KEY (`caller_number`,`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for call_queue
-- ----------------------------
DROP TABLE IF EXISTS `call_queue`;
CREATE TABLE `call_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`number`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for loop_monitor
-- ----------------------------
DROP TABLE IF EXISTS `loop_monitor`;
CREATE TABLE `loop_monitor` (
  `timestamp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of loop_monitor
-- ----------------------------
INSERT INTO `loop_monitor` VALUES ('1517290822');

-- ----------------------------
-- Table structure for web_presence
-- ----------------------------
DROP TABLE IF EXISTS `web_presence`;
CREATE TABLE `web_presence` (
  `ext` varchar(32) NOT NULL,
  `state` varchar(16) NOT NULL,
  `cidnum` varchar(64) DEFAULT NULL,
  `cidname` varchar(64) DEFAULT NULL,
  `inorout` varchar(1) DEFAULT NULL,
  `callstart` int(11) DEFAULT NULL,
  PRIMARY KEY (`ext`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_presence
-- ----------------------------
INSERT INTO `web_presence` VALUES ('4000', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4010', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4020', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4030', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4040', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4050', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4060', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4070', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4080', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4090', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4100', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4110', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4120', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4130', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4140', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4150', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4160', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4170', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4180', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4190', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4200', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4210', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4220', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4230', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4240', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4250', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4260', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4270', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4280', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4290', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4300', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4310', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4320', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4330', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4340', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4350', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4360', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4370', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4380', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4390', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4400', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4410', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4420', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4430', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4440', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4450', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4460', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4470', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4480', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4490', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4500', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4510', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4520', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4530', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4540', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5000', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5010', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5020', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5030', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5040', '0', null, null, null, null);
