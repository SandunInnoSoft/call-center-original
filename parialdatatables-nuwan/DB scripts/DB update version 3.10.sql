# HNB Assurance call center database update version 3.10
# @since 2018-01-10
# @author Sandun

-- ----------- creates a new table to maintain the voicemail extension numbers of the extension queues
CREATE TABLE `voicemail_extension` (
`id`  int NOT NULL AUTO_INCREMENT,
`voicemail_extension`  varchar(50) NULL ,
`ext_queue_id`  int NULL ,
`description`  varchar(255) NULL ,
PRIMARY KEY (`id`)
);

