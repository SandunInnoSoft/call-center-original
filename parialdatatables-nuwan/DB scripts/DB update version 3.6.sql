# HNB Assurance DB update version 3.6
# @author Sandun
# @since 2017-12-8
# @target DB1 hnbassurancedb

-- This script adds a new column to the call_center_user table to record agent webphone state
ALTER TABLE `call_center_user`
ADD COLUMN `webphone`  enum('Enabled','Disabled') NULL DEFAULT 'Enabled' COMMENT 'This fiels is to maintain the state of the agent webphone state' AFTER `user_email`;

