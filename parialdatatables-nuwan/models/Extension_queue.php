<?php


namespace app\models;
use yii;
use yii\db\ActiveRecord;

/**
 * This model class interacts with extension_queue table in DB1
 *
 * @author Sandun
 * @since 2017-12-12
 */
class Extension_queue extends ActiveRecord{
    
    /**
     * <b>Returns all assigned supervisor ids</b>
     * @return array
     * 
     * @since 2017-12-12
     * @author Sandun
     */
    public static function getAllAssignedSupervisorIds(){
        return Extension_queue::find()
                ->select("supervisor_id")
                ->all();
        
    }
    
    public static function insertNewExtensionQueue($queueName, $supervisorId){
        $newExtensionQueue = new Extension_queue();
        $newExtensionQueue->name = $queueName;
        $newExtensionQueue->supervisor_id = $supervisorId;
        $newExtensionQueue->insert();
        return $newExtensionQueue->getPrimaryKey();
    }
    
    public static function getExtensionQueueIdOfTheSupervisor($supervisorId){
        $extensionQueueData = Extension_queue::find()
                ->select("id")
                ->where("supervisor_id = $supervisorId")
                ->one();
        
        if($extensionQueueData['id']){
            return $extensionQueueData['id'];
        }else{
            return NULL;
        }
    }
    
    /**
     * <b>Checks if the supervisor has an extension queue assigned</b>
     * <p></p>
     * 
     * @author Sandun
     * @since 2017-12-14
     * 
     * @return queue id if assigned / FALSE if not assigned
     */
    public static function isSupervisorAssignedToAnExtensionQueue($supervisorId){
        $supervisorQueue = Extension_queue::find()
                ->where("supervisor_id = $supervisorId")
                ->one();
        
        if($supervisorQueue){
            return $supervisorQueue['id'];
        }else{
            return false;
        }
    }
    
    /**
     * <b>Get all available queues data</b>
     * <p>This function returns data of all available queues</p>
     * 
     * @return array all available queues
     * 
     * @since 2017-12-15
     * @author Sandun
     */
    public static function getAllAvailableQueuesData(){
        return Extension_queue::find()
                ->all();
    }
    
    /**
     * 
     * @param type $queueId
     * @return type
     * 
     * @since 2018-1-2
     * @author Sandun
     */
    public function getExtensionQueueNameByQueueId($queueId){
        $queueName = Extension_queue::find()
                ->select("name")
                ->where("id = $queueId")
                ->one();
        return $queueName["name"];
    }
    
}
