<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of help_requests
 *
 * @author Sandun
 * @since 2017-07-12
 */
class Queue_ivr_number extends ActiveRecord {
    
    /**
     * 
     * @param int $queueId
     * @return boolean|array Returns false if no IVR number was found for the queue else 1D array of IVR numbers for the queue
     * 
     * @since 2018-1-22
     * @author Sandun
     */
    public static function getIvrNumbersOfQueue($queueId){
            $ivrNumbersQuery = Queue_ivr_number::find();
                                $ivrNumbersQuery->select("ivr_number");
                                if($queueId != NULL){
                                    $ivrNumbersQuery->where("ext_queue_id = $queueId");
                                }
            $ivrNumbers = $ivrNumbersQuery->all();

        $ivrNumbersRemade = array();
        if(count($ivrNumbers) > 0){
            for($x = 0; $x < count($ivrNumbers); $x++){
                array_push($ivrNumbersRemade, $ivrNumbers[$x]['ivr_number']);
            }
            return $ivrNumbersRemade;
        }else{
            return FALSE;
        }
    }

}
