<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is to exchange data with the call_records database table
 *
 * @author Sandun
 * @since 2017-06-30
 */
class Call_records extends ActiveRecord {

    public static function getHighestCallRecordID() {
//        $call_record = new Call_records();
//        return $call_record->find()
//                ->max('cdr_id')
//                ->all();
        $connection = Yii::$app->db;
        $command = $connection->createCommand("SELECT max(cdr_id) FROM call_records;");

        $call_records = $command->queryAll();
        return $call_records[0]["max(cdr_id)"];
    }

    public static function insertNewCallRecord($arrayOfNewCallRecordData) {
        $call_record = new Call_records();
        $call_record->call_date = $arrayOfNewCallRecordData['call_date'];
        $call_record->call_time = $arrayOfNewCallRecordData['call_time'];
        $call_record->caller_number = $arrayOfNewCallRecordData['caller_number'];
        $call_record->cli_number = $arrayOfNewCallRecordData['cli_number'];
        $call_record->user_id = $arrayOfNewCallRecordData['user_id'];
        $call_record->active = $arrayOfNewCallRecordData['active'];
        $call_record->created_date_time = $arrayOfNewCallRecordData['created_date_time'];
        $call_record->timestamp = $arrayOfNewCallRecordData['timestamp'];
        $call_record->insert();
        return $call_record->getPrimaryKey();
    }

    public static function getCallRecordFormId($id) {
        return Call_records::findOne($id);
    }

    public static function updateCallRecord($data, $id) {
        $callRecord = new Call_records();
        return $callRecord->updateAll($data, "id = $id");
    }

    public static function selectLatestCallNumbers($timestamp) {
        $callRecord = new Call_records();
        return $callRecord->find()
                        ->select('cdr_unique_id')
                        ->where("timestamp > $timestamp")
                        ->groupBy('cdr_unique_id')
                        ->all();
    }

    public static function getAllCallsToday() {
        $callRecord = new Call_records();
        $data = $callRecord->find()
                ->select('cdr_unique_id')
                ->where('DATE(call_date) = CURDATE()')
                ->distinct()
                ->count();

        return $data;
    }

    /**
     * <b>Get call record information by cdr unique Id</b>
     * <p>This function gets the call record information associated with the unique id passed as the parameter</p>
     * 
     * @param string $uniqueId
     * @return array call record data
     */
    public function getCallRecordInfoWithUniqueId($uniqueId) {
        $callRecords = new Call_records();
        return $callRecords->find()
                        ->where("cdr_unique_id = $uniqueId")
                        ->one();
    }

    public static function getAverageWaitingTime() {
        $callRecord = new Call_records();
        $data = $callRecord->find()
                ->select('call_waiting_duration')
                ->where('DATE(call_date) = CURDATE()')
                ->average('call_waiting_duration');
        return $data;
    }

    public static function getMaxWaitingTime() {
        $callRecord = new Call_records();
        $data = $callRecord->find()
                ->select('call_waiting_duration')
                ->where('DATE(call_date) = CURDATE()')
                ->max('call_waiting_duration');
        return $data;
    }

    public static function getServiceLeveledCalls() {
        $callRecord = new Call_records();
        $data = $callRecord->find()
                ->select('call_waiting_duration')
                ->where('call_waiting_duration > 5')
                ->andWhere('call_waiting_duration < 3600')
                ->andWhere('DATE(call_date) = CURDATE()')
                ->count();
        return $data;
    }

    public static function getDailyAnsweredCallsByAgent($agentId) {
        $callRecord = new Call_records();
        if ($agentId != 0) { // If aget id not for all users
            $data = $callRecord->find()
//                ->select('call_waiting_duration')
                    ->where("user_id = $agentId")
                    ->andWhere('DATE(call_date) = CURDATE()')
//                ->groupBy($columns)
                    ->count();
        } else { // If id for all users
            $data = $callRecord->find()
//                ->select('call_waiting_duration')
                    ->where('DATE(call_date) = CURDATE()')
//                    ->where("user_id = $agentId")
//                    ->andWhere('DATE(call_date) = CURDATE()')
//                ->groupBy($columns)
                    ->count();
        }
        return $data;
    }

    public static function getDailyAnsweredCalls() {
        $callRecord = new Call_records();
        $data = $callRecord->find()
//                ->select('call_waiting_duration')
//                ->where("user_id = $agentId")               
                ->where('DATE(call_date) = CURDATE()')
//                ->andWhere('DATE(call_date) = CURDATE()')
//                ->groupBy($columns)
                ->count();
        return $data;
    }

    public static function getAllAnsweredCallsThisMonth() {
        $callRecord = new Call_records();
        $data = $callRecord->find()
//                ->select('call_waiting_duration')
//                ->where("user_id = $agentId")               
//                ->where('DATE(call_date) = CURDATE()')
                ->where('(MONTH(DATE(call_time)) = MONTH(CURDATE()))')
//                ->andWhere('DATE(call_date) = CURDATE()')
//                ->groupBy($columns)
                ->count();
        return $data;
    }

    public static function getAgentAnsweredCallsThisMonth($agentId) {
        $callRecord = new Call_records();
        if ($agentId != 0) { // Id not for all users
            $data = $callRecord->find()
//                ->select('call_waiting_duration')
                    ->where("user_id = $agentId")
//                ->where('DATE(call_date) = CURDATE()')
                    ->andWhere('(MONTH(DATE(call_time)) = MONTH(CURDATE()))')
//                ->andWhere('DATE(call_date) = CURDATE()')
//                ->groupBy($columns)
                    ->count();
        } else { // Id for all users
            $data = $callRecord->find()
//                ->select('call_waiting_duration')
//                    ->where("user_id = $agentId")
                    ->where('(MONTH(DATE(call_time)) = MONTH(CURDATE()))')
//                ->where('DATE(call_date) = CURDATE()')
//                    ->andWhere('(MONTH(DATE(call_time)) = MONTH(CURDATE()))')
//                ->andWhere('DATE(call_date) = CURDATE()')
//                ->groupBy($columns)
                    ->count();
        }
        return $data;
    }

    public static function getAllAgentCallRecords($userId) {
        $callRecord = new Call_records();
        if ($userId != 0) { // If user id not for all users
//            $result = $callRecord->find()
            $users = call_center_user::findOne($userId);
            $result = $users->getCall_records()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
//                    ->select('`call_records`.`id`, `call_records`.`caller_number`, `call_records`.`call_date` , `call_records`.`call_end_time`, `call_center_user`.`name` ')
                    ->innerJoin('call_center_user', 'call_center_user.id = call_records.user_id')

//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                    ->where('(MONTH(DATE(call_time)) = MONTH(CURDATE()))')
                    ->andWhere("user_id = $userId")
                    ->orderBy("call_time desc")
                    ->with('call_center_user')
                    ->all();
            $data = $result;
        } else {

//            $users = call_center_user::findOne($userId);
//            $result = $users->getCall_records()
            $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
//                    ->select('`id`, `caller_number`, `call_date` , `call_end_time`, `call_end_time` ')
//                    ->select('`call_records`.`id`, `call_records`.`caller_number`, `call_records`.`call_date` , `call_records`.`call_end_time`, `call_records`.`call_end_time`, `call_center_user`.`name` ')
                    ->innerJoin('call_center_user', 'call_center_user.id = call_records.user_id')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                    ->where('(MONTH(DATE(call_time)) = MONTH(CURDATE()))')
                    ->orderBy("call_time desc")
                    ->with('call_center_user')
//                ->andWhere("user_id = $userId")
                    ->all();
        }
        return $data;
    }

    public static function getAllCallRecordsByDate($dateFrom, $dateTo, $contact, $agent_id) {
        $callRecord = new Call_records();
        if ($contact == '' || $contact == NULL) {
            if ($agent_id != 0) { // Id not for all users
                $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
//                        ->select('`id`, `caller_number`, `call_date` , `call_end_time` ')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                        ->innerJoin('call_center_user', 'call_center_user.id = call_records.user_id')
                        ->where("call_date > '$dateFrom'")
                        ->andWhere("call_date < '$dateTo'")
                        ->andWhere("user_id = '$agent_id'")
                        ->with('call_center_user')
                        ->all();
            } else {
                $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
//                        ->select('`id`, `caller_number`, `call_date` , `call_end_time` ')
                        ->innerJoin('call_center_user', 'call_center_user.id = call_records.user_id')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                        ->where("call_date > '$dateFrom'")
                        ->andWhere("call_date < '$dateTo'")
                        ->with('call_center_user')
//                        ->andWhere("user_id = '$agent_id'")
                        ->all();
            }
            return $data;
        } else {
            if ($agent_id != 0) { // Id not for all users
                $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
                        ->select('`id`, `caller_number`, `call_date` , `call_end_time` ')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                        ->where("call_date > '$dateFrom'")
                        ->andWhere("call_date < '$dateTo'")
                        ->andWhere(['like', 'caller_number', $contact])
                        ->andWhere("user_id = '$agent_id'")
                        ->all();
            } else {
                $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
                        ->select('`id`, `caller_number`, `call_date` , `call_end_time` ')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                        ->where("call_date > '$dateFrom'")
                        ->andWhere("call_date < '$dateTo'")
                        ->andWhere(['like', 'caller_number', $contact])
//                    ->andWhere("user_id = '$agent_id'")
                        ->all();
            }
            return $data;
        }
    }

    public static function allCallsAnsweredInGivenPeriod($agentId, $dateFrom, $dateTo) {// This function returns all the calls answered in the given period
        $callRecord = new Call_records();
        if ($agentId == '0') {
            $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
//                    ->select('`caller_number`, `call_date` , `call_end_time` ')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                    ->where("DATE(call_date) > STR_TO_DATE('$dateFrom', '%Y-%m-%d')")
                    ->andWhere("DATE(call_date) < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
//                    ->andWhere("user_id = '$agent_id'")
                    ->count();
            return $data;
        } else {
            $data = $callRecord->find()
//                ->select(['caller_number' => 'src', 'call_date' => 'answer',  'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)' => 'duration','call_end_time' => 'end'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'duration' => 'TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`)','end' => 'call_end_time'])
//                ->select(['src' => 'caller_number', 'answer' => 'call_date',  'end' => 'call_end_time'])
//                    ->select('`caller_number`, `call_date` , `call_end_time` ')
//                ->select(`caller_number`, `call_date` , [TIMESTAMPDIFF(SECOND, `call_date`, `call_end_time`) => `duration`], `call_end_time` )
                    ->where("DATE(call_date) > STR_TO_DATE('$dateFrom', '%Y-%m-%d')")
                    ->andWhere("DATE(call_date) < STR_TO_DATE('$dateTo', '%Y-%m-%d')")
                    ->andWhere("user_id = '$agentId'")
                    ->count();
            return $data;
        }
    }
    
    public static function answeredCallsCountBetweenDates($agentId, $from, $to){
        if($agentId == '0'){
            // all agents
            $callRecords = Call_records::find()
                    ->select("cdr_unique_id")
                    ->where("STR_TO_DATE(call_date,'%Y-%m-%d') >= STR_TO_DATE('$from','%Y-%m-%d')")
                    ->andWhere("STR_TO_DATE(call_date,'%Y-%m-%d') <= STR_TO_DATE('$to','%Y-%m-%d')")
//                    ->andWhere("cdr_unique_id IS NOT NULL")
                    ->distinct()
                    ->count();
            return $callRecords;
        }else{
            // single agent
            $callRecords = Call_records::find()
                    ->select("cdr_unique_id")
                    ->where("STR_TO_DATE(call_date,'%Y-%m-%d') >= STR_TO_DATE('$from','%Y-%m-%d')")
                    ->andWhere("STR_TO_DATE(call_date,'%Y-%m-%d') <= STR_TO_DATE('$to','%Y-%m-%d')")
                    ->andWhere("user_id = $agentId")
//                    ->andWhere("cdr_unique_id IS NOT NULL")
                    ->distinct()
                    ->count();
            return $callRecords;
        }
    }

    public static function getCallRecordUniqueIdbyCallId($id) {
        $uniqueId = Call_records::find()
                ->select("cdr_unique_id")
                ->where("id = $id")
                ->one();
        return $uniqueId['cdr_unique_id'];
    }

    /**
     * <b>Saves comment to the latest call record of the agent</b>
     * <p>This function fetches the record for the latest call record which matches with the user id in the session and the caller number passed as a parameter
     * when found, sets the comment value and updates the record</p>
     * 
     * @param type $callerNumber
     * @param type $comment
     * @return type
     * @author Sandun
     * @since 2017-09-27
     */
    public static function saveCommentOfLatestCallToAgent($callerNumber, $comment) {
        $callRecord = Call_records::find()
                ->select("id")
                ->where("caller_number = $callerNumber")
                ->andWhere("user_id = " . Yii::$app->session->get("user_id"))
                ->orderBy("call_time desc")
                ->one();

        $callRecord->comment = $comment;
        return $callRecord->update();
    }

    public function getCall_center_user() {
//        return $this->hasMany(call_center_user::className(), ['user_id' => 'id']);
        return $this->hasOne(call_center_user::className(), ['id' => 'user_id']);
    }
    
    public function getTotalCallRecordsCount(){
        return Call_records::find()
                ->count();
    }
    
    public function getTotalCallRecordsCountBetweenDates($from, $to){
        return Call_records::find()
                ->select("cdr_unique_id")
                ->where("DATE(call_date) >= '$from'")
                ->andWhere("DATE(call_date) <= '$to'")
                ->distinct()
                ->count();
    }
    
    public static function getAllUniqueIdsBetweenDates($from, $to){
        $uniqueIds = Call_records::find()
                ->select("cdr_unique_id")
                ->where("DATE(call_date) >= DATE('$from')")
                ->andWhere("DATE(call_date) <= DATE('$to')")
                ->distinct()
                ->all();
        $uniqueIdsArray = array();
        for($i = 0; $i < count($uniqueIds); $i++){
            if($uniqueIds[$i]['cdr_unique_id'] != "" && $uniqueIds[$i]['cdr_unique_id'] != NULL){
                array_push($uniqueIdsArray, $uniqueIds[$i]['cdr_unique_id']);   
            }
        }
        
        return $uniqueIdsArray;
    }

}
