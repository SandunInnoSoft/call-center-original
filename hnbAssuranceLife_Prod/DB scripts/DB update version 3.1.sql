# HNB Iceland update version 3.1 
# @author Sandun
# @since 2017-10-19

# Adds 10 new extension numbers to the webpresence table
INSERT INTO web_presence (ext) VALUES (4400);
INSERT INTO web_presence (ext) VALUES (4410);
INSERT INTO web_presence (ext) VALUES (4420);
INSERT INTO web_presence (ext) VALUES (4430);
INSERT INTO web_presence (ext) VALUES (4440);
INSERT INTO web_presence (ext) VALUES (4450);
INSERT INTO web_presence (ext) VALUES (4460);
INSERT INTO web_presence (ext) VALUES (4470);
INSERT INTO web_presence (ext) VALUES (4480);
INSERT INTO web_presence (ext) VALUES (4490);