<?php

use yii\helpers\Url;
use yii\helpers\Html;
$ftpParams = include __DIR__ . '/../../config/pbx_audio_ftp_config.php';
?>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>-->
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="js/chartjs.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<style>
    thead{
        font-weight: bold;
    }

    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }
    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<style>
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
<script>
//    $(function () {
//
//    });

//    $(document).ready(function () {
//        console.log('On top');
//        $("#datepicker").datepicker();
//        console.log("ready! bang");
//    });
</script>



<div  style="font-family: 'Lato' , sans-serif;">

    <div class="col-xs-2" >
        <div class="row" style="margin-bottom: 2%">
            <a  onclick="window.open('<?=$ftpParams['audioFilePath']?>', '_blank');" class="btn btn-block btn-success" style="width: 90%"><b>Call Audio Playback</b></a>
        </div>
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Agents</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 650px;overflow-y: scroll">
                    <ul id="agentListGroup" class="list-group" style="">
                        <a id="0" style="cursor: pointer" onclick="callAgentReports(0, 0, 0, 0)" class="list-group-item">All records</a>
                        <?php for ($index = 0; $index < count($agents); $index++) { ?>
                            <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['name'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['name'] ?></a>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="col-xs-10" style="padding: 0%">
        <div class="row">
            <div class="col-xs-3">
                <label for="fromdate">Date from*</label>
                <!--<input class="form-control activateFields" type="date" id="fromdate">-->       
                <div class="input-group date" id="fromdate">
                    <input type="text" class="form-control" id="dateFromValue"/>	<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="todate">Date to*</label>                
                <!--<input class="form-control activateFields" type="date" id="todate">-->
                <div class="input-group date" id="todate">
                    <input type="text" class="form-control" id="dateToValue"/>	<span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="contactNum">Contact</label>
                <input class="form-control activateFields" type="number" id="contactNum">
            </div>
            <div class="col-xs-3" style="padding-top: 23px">
                <div class="row" style="padding-bottom: 1%">
                    <div class="col-xs-12">
                        <a class="disabled btn btn-block btn-primary" id="btnSummeryView" onclick="filterRecordsSummery()">View Summery Report</a>
                    </div>
                </div>
                <div class="row" style="padding-top: 1%">
                    <div class="col-xs-12">
                       <a class="disabled btn btn-block btn-primary" id="btnView" onclick="filterRecords()">View Full Report</a> 
                    </div>
                </div>
            </div>
            <input type="hidden" id="hiddenAgentId" />
            <input type="hidden" id="hiddenAgentExt" />
            <input type="hidden" id="hiddenAgentName" />
        </div><br>
        <div class="row" id="agentReportsDiv" style="padding-left: 1%"></div>
    </div>
</div>

<script>
    $('#fromdate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#todate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    function callAgentReports(agent_id, agent_ex, agent_name, id) {
//        alert(agent_id+" "+agent_ex+" "+agent_name+" "+ id);
        //active view button
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(agent_ex);
        $('#hiddenAgentName').val(agent_name);
        $('#hiddenAgentId').val(agent_id);
        var url = "<?= Url::to(['reports/performanceoverviewbyagent']) ?>";

        //Start : set agents panel css
        {
            var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
        }
        //End : set agents panel css
        //alert(agent_ex);
//alert();
        showLoadingScreen();
        $.ajax({
            type: "POST",
            url: url,
            data: {agent_id: agent_id, agent_ex: agent_ex, agent_name: agent_name},
            success: function (data)
            {
//                alert(data);
                var result = $(data).find('#agentReportsDIV');
                $("#agentReportsDiv").html(result);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                showErrorMessage();
                alert(xhr.responseText);
//                alert(xhr.status);
//                alert(thrownError);
//                alert(xhr.responseText);
            }
        });
    }
    function filterRecords() {


        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var url = "<?= Url::to(['reports/performanceoverviewbydate']) ?>";

//        alert(fromDate + " +++ " + toDate);
        var fromDateConverted = new Date(fromDate);
        var toDateConverted = new Date(toDate);
//        if (fromDateConverted < toDateConverted) {
//            alert('Yes here');
//        } else if (fromDateConverted > toDateConverted) {
//            alert('Greater than');
//        } else {
//            alert('No here');
//        }
//        if (fromDate != '' && toDate != '') {
        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "POST",
                url: url,
                data: {fromdate: fromDate, todate: toDate, agent_id: agentId, agentex: agentEx, agentname: agentName, contactNum: contactNumber},
                success: function (data)
                {

                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showErrorMessage();
                    console.log('%%% Error from reports filter ajax: responseText: ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
//                    alert(xhr.responseText);
//                    alert(xhr.status);
//                    alert(thrownError);
//                    alert(xhr.responseText);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }


    }
    
    /**
     * 
     * @returns {undefined}
     * 
     * @since 2017-10-30
     * @author Sandun
     * 
     */
    function filterRecordsSummery(){
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        var url = "<?= Url::to(['reports/summery']) ?>";

//        alert(fromDate + " +++ " + toDate);
        var fromDateConverted = new Date(fromDate);
        var toDateConverted = new Date(toDate);
        
        if (fromDateConverted <= toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "GET",
                url: url,
                data: {fromdate: fromDate, todate: toDate, agent_id: agentId, agentex: agentEx, agentname: agentName, contactNum: contactNumber},
                success: function (data)
                {
//                    alert(data);
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showErrorMessage();
//                    alert(xhr.responseText);
                    console.log('%%% Error from reports filter ajax: responseText: ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
//                    alert(xhr.responseText);
//                    alert(xhr.status);
//                    alert(thrownError);
//                    alert(xhr.responseText);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }
        
    }
    
    function showLoadingScreen(){
//        <!--<i id="loadingCallDataIndicator" style="display: none;" class="fa fa-refresh fa-spin fa-3x fa-fw"></i>-->
        $("#agentReportsDiv").empty();
//        var loadingIcon = document.createElement("i");
//        $(loadingIcon).addClass("fa fa-refresh fa-spin fa-5x fa-fw");
        $("#agentReportsDiv").append("Loading");
        return true;
    }
    
    function showErrorMessage(){
        $("#agentReportsDiv").empty();
        $("#agentReportsDiv").append("ERROR Occured. Please try again!");
    }
    
</script>