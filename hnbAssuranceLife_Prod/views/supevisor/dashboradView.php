<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .tileHeader{
                height: 60px;
                border: 2px solid black;   

            }
            .tileBody{
                height: 128px;
                border: 2px solid black;
                margin-bottom: 3px;
                text-align: center;
                padding: 2%;
            }
            .navbar-inverse{
                display: none !important;
            }
            .container{
                width: 1355px;
                margin-top: -34px;
                margin-bottom: 0px;
                margin-left: -4px;
            }
            .wrap{
                height: 100% !important;
                border:1px !important;
                overflow: hidden;
            }
            .bodyElement{
                font-size: 700% !important;
                color: #000;                
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-4" style="">
                    <!--Total calls-->
                    <div class="row tileHeader" style="border-radius: 10px;  background-color: #0000ff;  text-align: center; ">
                        <h1 style="color: white; margin: 3%">Total Calls</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #7f7fff; ">
                        <span class="bodyElement" id="idTotalCalls"></span>
                    </div>
                </div>
                <div class="col-xs-4" style=" ">
                    <!--Answered Calls-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #00aa00;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Calls Answered</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #00ff00; ">
                        <span class="bodyElement" id="idAnsweredCalls"></span>
                    </div>
                </div>
                <div class="col-xs-4" style=" ">
                    <!--UnAnswered Calls-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #ff0000;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Calls Unanswered</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #ff7f7f; ">
                        <span class="bodyElement" id="idUnAnsweredCalls" ></span>
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-xs-4" style="">
                    <!--Calls Ringing-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #55aaff; text-align: center;">
                        <h1 style="color: white; margin: 3%">Calls Waiting</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #d5eaff; ">
                        <span class="bodyElement" id="idCallsWaiting"></span>
                    </div>
                </div>
                <div class="col-xs-4" style="">
                    <!--Agents Idle-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #55aaff; text-align: center;">
                        <h1 style="color: white; margin: 3%">Agents Idle</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #d5eaff; ">
                        <span class="bodyElement" id="idAgentsIdle"></span>
                    </div>
                </div>
                <div class="col-xs-4" style="">
                    <!--Agents Busy-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #ff007f;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Agents Busy</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #ff7fbf; ">
                        <span class="bodyElement" id="idAgentsBusy"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4" style=" ">
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #55aaff;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Agents Paused</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #d5eaff; ">
                        <span class="bodyElement" >0</span>
                    </div>
                </div>
                <div class="col-xs-4" style=" ">
                    <!--Average Waiting time-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #fa0;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Average Waiting Time</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #ffd57f; ">
                        <span class="bodyElement" id="idAverageWaitingTime"></span>
                    </div>
                </div>
                <div class="col-xs-4" style=" ">
                    <!--Maximum Waiting Time-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #ff0000;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Max Wait Time</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #ff7f7f; ">
                        <span class="bodyElement" id="idMaxWaitingTime"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4" style=" ">
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #0000ff;   text-align: center;">
                        <h1 style="color: white; margin: 3%">Calls Answered (%)</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #7f7fff; ">
                        <span class="bodyElement" id="idAnsweredPercentage"></span>
                    </div>
                </div>
                <div class="col-xs-4" style=" ">
                    <!--Average Waiting time-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #00aa00;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Calls Unanswered (%)</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #00ff00; ">
                        <span class="bodyElement" id="idUnansweredPercentage"></span>
                    </div>
                </div>
                <div class="col-xs-4" style=" ">
                    <!--Maximum Waiting Time-->
                    <div class="row tileHeader" style="border-radius: 10px; background-color: #fa0;  text-align: center;">
                        <h1 style="color: white; margin: 3%">Service Level (%)</h1>
                    </div>                    
                    <div class="row tileBody" style="border-radius: 10px; background-color: #ffd57f; ">
                        <span class="bodyElement" id="idServiceLevelPercentage"></span>
                    </div>
                </div>
            </div>
        </div> 
    </body>
    <script type="text/javascript" >
        $(document).ready(function () {
//        var getOnlineAgentsInterval = setInterval(getOnlineUsersAJAX(), 3000);
            setInterval(function () {
                getDashboardRecords();

            }, 2000);
        });

        function getDashboardRecords() {
            $.ajax({
                url: "<?= Url::to(['dashboard/dashboard_records']) ?>",
                type: 'POST',
                success: function (data, textStatus, jqXHR) { 

                    var data = JSON.parse(data);
                    $('#idTotalCalls').text(data['allCalls']);
                    $('#idAnsweredCalls').text(data['answeredCalls']);
                    var unanswered = data['allCalls'] - data['answeredCalls'];
                    $('#idUnAnsweredCalls').text(unanswered);

                    $('#idAgentsBusy').text(data['agentSummary']['INUSE']);
                    var idle = +data['agentSummary']['RINGING'] + +data['agentSummary']['NOT_INUSE'];
                    $('#idAgentsIdle').text(idle);
                    var totalCalls=data['allCalls'];
                    var answeredCalls= data['answeredCalls'];
                    var answeredPercentage=0;
                    if(answeredCalls != 0)
                        answeredPercentage = (answeredCalls/totalCalls)*100;
                    var UnAnsweredPercentage = 0;
                    if(unanswered != 0)
                        UnAnsweredPercentage = (unanswered/totalCalls)*100;
                    var serviceLeveledCalls = data['serviceLevelCalls'];
                    var serviceLevelPercentage=0;
                    if(serviceLeveledCalls != 0)
                        serviceLevelPercentage = (serviceLeveledCalls / answeredCalls)*100;
                    $('#idCallsWaiting').text(data['agentSummary']['RINGING']);
                    $('#idAverageWaitingTime').text(data['waiting']);
                    $('#idMaxWaitingTime').text(data['maxWaiting']);
                    $('#idAnsweredPercentage').text(answeredPercentage.toFixed(2)+" %");
                    $('#idUnansweredPercentage').text(UnAnsweredPercentage.toFixed(2)+" %");
                    $('#idServiceLevelPercentage').text(serviceLevelPercentage.toFixed(2)+" %");

                },
                error: function (jqXHR, textStatus, errorThrown) {
//                console.log("online agents fail timestamp " + Date.now() + " : " + number);
//                number++;
                }
            });
        }
    </script>
</html>
