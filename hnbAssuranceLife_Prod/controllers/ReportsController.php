<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\Customer;
use app\models\Help_requests;
use app\models\Agent_requests;
use app\models\cdr;
use app\models\Logged_in_users;
use app\models\abandon_calls;
use app\models\call_center_user;
use app\models\Dnd_records;
use app\models\Call_records;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportsController
 *
 * @since 28/07/2017
 * @author Prabath
 */
class ReportsController extends Controller {

    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        }        
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render the performance overview
     * 
     * @author Vikum
     * @since 2017-07-07
     * 
     */
    public function actionPerformanceoverview() {
        $user_id = Yii::$app->session->get('user_id');
        $agent_ex = Yii::$app->session->get('voip');

        $dailyBreakTime = Agent_requests::getDailyBreakTime($user_id);
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($user_id);

        $monthlyBreakTime = Agent_requests::getThisMonthBreakTime($user_id); // Total breaks taken by the agent this month
        $monthlyWorkTime = Logged_in_users::getThisMonthWorkedTime($user_id); // Total working hours by an agent of the current month

        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime];
//        $breaksFullData = [0.5, 2.5];
        $breaksFullData = [$monthlyBreakTime, $monthlyWorkTime]; // Report data for the current month word graph
        /**
         * @author supun
         * @since 2017/08/16
         */
//        $answered_calls = cdr::getAnsweredCallsListByUser($agent_ex);
        $answered_calls_data = Call_records::getAllAgentCallRecords($user_id);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        foreach ($answered_calls_data as $key) {
//            $date1 = date_create($key['call_date']);
//            $date2 = date_create($key['call_end_time']);
//            $dteStart = new DateTime($key['call_date']);
//            $dteEnd = new DateTime($key['call_end_time']);
//            $dteDiff  = $dteStart->diff($dteEnd); 

            $temp = array(
                'start' => 0,
                'src' => $key['caller_number'],
                'answer' => $key['call_date'],
                'duration' => 0,
//            date_diff($date1, $date2),
                'end' => $key['call_end_time']
            );
            array_push($answered_calls, $temp);
        }

        $agent_answered_calls_count = Call_records::getDailyAnsweredCallsByAgent($user_id); // All Calls Answered by the agent in current day
        $all_answered_calls_count = Call_records::getDailyAnsweredCalls(); // All Calls answered in the current day

        $all_agent_answered_calls_count_thismonth = Call_records::getAgentAnsweredCallsThisMonth($user_id);
        $all_answered_calls_count_thisMonth = Call_records::getAllAnsweredCallsThisMonth(); // All the calls answered by the call center current month
        $monthlyCallsData = [$all_agent_answered_calls_count_thismonth, $all_answered_calls_count_thisMonth];

        $outgoing_calls = cdr::getOutgoingCallsListByUser($agent_ex);

        $agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgentByUser($agent_ex);

//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByUser($user_id);

        return $this->render('performanceOverview', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'monthlyCallsData' => $monthlyCallsData]);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render a specific agent performance overview page
     * 
     * @author Prabath
     * @since 03-08-2017
     * 
     */
    public function actionPerformanceoverviewbyagent() {
        $agent_id = $_POST['agent_id'];
        $agent_ex = $_POST['agent_ex'];
        $agent_name = $_POST['agent_name'];

        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id); // Sum of break times taken by the agent of the current day, Also done with all user
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id); // Sum of working hours of the agent on Current Day, Done for All users

        $monthlyBreakTime = Agent_requests::getThisMonthBreakTime($agent_id); // Total breaks taken by the agent this month, Done for all users
        $monthlyWorkTime = Logged_in_users::getThisMonthWorkedTime($agent_id); // Total working hours by an agent of the current month, Done for all users

        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime]; // Report data for the current work time graph
        $breaksFullData = [$monthlyBreakTime, $monthlyWorkTime]; // Report data for the current month word graph
//========================================================================================================================================
//        $answered_calls = cdr::getAnsweredCallsListByUser($agent_ex); // This need to be changed to get data from CallRecords table *** NOT MINE
        $answered_calls_data = Call_records::getAllAgentCallRecords($agent_id);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
//        foreach ($answered_calls_data as $key) {
        for ($i = 0; $i < count($answered_calls_data); $i++) {
//            $date1 = date_create($key['call_date']);
//            $date2 = date_create($key['call_end_time']);
//            $dteStart = new DateTime($key['call_date']);
//            $dteEnd = new DateTime($key['call_end_time']);
//            $dteDiff  = $dteStart->diff($dteEnd); 
//            $temp = array(
//                'id' => $key['id'],
//                'start' => 0,
//                'src' => $key['caller_number'],
//                'answer' => $key['call_date'],
//                'duration' => 0,
//                'name' => $key['name'],
////            date_diff($date1, $date2),
//                'end' => $key['call_end_time']
//            );
            $temp = array(
                'id' => $answered_calls_data[$i]['id'],
                'start' => 0,
                'src' => $answered_calls_data[$i]['caller_number'],
                'answer' => $answered_calls_data[$i]['call_date'],
                'duration' => 0,
                'name' => $answered_calls_data[$i]['call_center_user']['name'],
//                'name' => $key['name'],
//            date_diff($date1, $date2),
                'end' => $answered_calls_data[$i]['call_end_time']
            );
            array_push($answered_calls, $temp);
        }

        $agent_answered_calls_count = Call_records::getDailyAnsweredCallsByAgent($agent_id); // All Calls Answered by the agent in current day
        $all_answered_calls_count = Call_records::getDailyAnsweredCalls(); // All Calls answered in the current day

        $all_agent_answered_calls_count_thismonth = Call_records::getAgentAnsweredCallsThisMonth($agent_id);
        $all_answered_calls_count_thisMonth = Call_records::getAllAnsweredCallsThisMonth(); // All the calls answered by the call center current month
        $monthlyCallsData = [$all_agent_answered_calls_count_thismonth, $all_answered_calls_count_thisMonth];
        //
//========================================================================================================================================        

        $outgoing_calls = cdr::getOutgoingCallsListByUser($agent_ex); // This takes all outgoing calls taken by the agent
        if ($agent_ex == 0) {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                if ($name) {
                    $outgoing_calls[$i]['name'] = $name;
                } else {
                    $outgoing_calls[$i]['name'] = '';
                }
            }
        }


        $agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgentByUser($agent_ex);
        if ($agent_ex == 0) {
            $missedCalls = $this->getAllMissedCallsBetweenDates(NULL, NULL);
        }else{
            $missedCalls = null;
        }
        //$agent_abandoned_calls = array();
//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount(); // This need to be changed to get all answered calls from CallRecords

        $logged_in_records = Logged_in_users::getLoginRecordsByUser($agent_id);

        $agent_dnd_records = Dnd_records::getDndRecordsByUser($agent_id);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';

        return $this->render('supervisorReportsPage', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_answered_calls_count' => $agent_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'agent_extension' => $agent_ex, 'agent_name' => $agent_name, 'agent_dnd_reocrds' => $agent_dnd_records, 'allCallsInTheMonth' => $all_answered_calls_count_thisMonth, 'allCallsForAgentThisMonth' => $all_agent_answered_calls_count_thismonth, 'monthlyCallsData' => $monthlyCallsData, 'ftpParams' => $ftpParams, 'agent_id' => $agent_id, 'missed_calls' => $missedCalls]);
    }

    public function actionPerformanceoverviewbydate() {


        $agent_id = $_POST['agent_id'];
        $agent_ex = $_POST['agentex'];
        $agent_name = $_POST['agentname'];
        $searchContact = $_POST['contactNum'];
//        $searchContact = '';
        $fromDate = $_POST['fromdate'];
        $toDate = $_POST['todate'];
        $dateRange = array($fromDate, $toDate);

//        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id);
//        $dailyBreakTime = Agent_requests::getBreakTimeByDate($agent_id,$fromDate);
//        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id,$toDate);
        $dailyBreakTime = Agent_requests::getDailyBreakTime($agent_id); // Sum of break times taken by the agent of the current day, Done for all users
        $dailyWorkTime = Logged_in_users::getDailyWorkedTime($agent_id); // Sum of working hours of the agent on Current Day, Done for all users
//        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime]; // Report data for the current work time graph


        $dailyBreakTimeReal = $this->convertToHoursMins($dailyBreakTime, '%02d hours %02d minutes');
        $dailyWorkTimeReal = $this->convertToHoursMins($dailyWorkTime, '%02d hours %02d minutes');
        $breaksDailyData = [$dailyBreakTime, $dailyWorkTime, "$dailyBreakTimeReal", "$dailyWorkTimeReal"];


        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByDate($agent_id, $fromDate, $toDate); // Total break requests of the given time, Done for all users
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getDailyWorkedTimeInGivenRange($agent_id, $fromDate, $toDate); // Total work time of the given time period, Done for all users
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $breaksFullData = [$totalBreakTimeOfTheTimePeriod, $totalWorkTimeOfTheTimePeriod, $totalBreakTimeOfTheTimePeriodRealTime, $totalWorkTimeOfTheTimePeriodRealTime]; // This is where full break set will come
//        $answered_calls = cdr::getAnsweredCallsListByDate($agent_ex, $fromDate, $toDate);
//        $answered_calls_data = Call_records::getAllAgentCallRecords($agent_id);
//        $answered_calls  = Call_records::getAllAgentCallRecords($agent_id);
        $answered_calls = array();
        $answered_calls_set = Call_records::getAllCallRecordsByDate($fromDate, $toDate, $searchContact, $agent_id); // This search necessary data from Call_records table
//        foreach ($answered_calls_set as $key) {
//            $temp = array(
//                'id' => $key['id'],
//                'start' => 0,
//                'src' => $key['caller_number'],
//                'answer' => $key['call_date'],
//                'duration' => 0,
////            date_diff($date1, $date2),
//                'end' => $key['call_end_time']
//            );
//            array_push($answered_calls, $temp);
//        }
        for ($i = 0; $i < count($answered_calls_set); $i++) {
            $temp = array(
                'id' => $answered_calls_set[$i]['id'],
                'start' => 0,
                'src' => $answered_calls_set[$i]['caller_number'],
                'answer' => $answered_calls_set[$i]['call_date'],
                'duration' => 0,
                'name' => $answered_calls_set[$i]['call_center_user']['name'],
//                'name' => $key['name'],
//            date_diff($date1, $date2),
                'end' => $answered_calls_set[$i]['call_end_time']
            );
            array_push($answered_calls, $temp);
        }
        $agent_answered_calls_count = Call_records::getDailyAnsweredCallsByAgent($agent_id); // All Calls Answered by the agent in current day

        $all_answered_calls_count = Call_records::getDailyAnsweredCalls(); // All Calls answered in the current day

        $allCallsAnsweredByAgentCountInGivenPeriod = Call_records::allCallsAnsweredInGivenPeriod($agent_id, $fromDate, $toDate);
        $allCallsAnsweredByCountInGivenPeriod = Call_records::allCallsAnsweredInGivenPeriod('0', $fromDate, $toDate);
        $totalCallsAnsweredSet = array($allCallsAnsweredByAgentCountInGivenPeriod, $allCallsAnsweredByCountInGivenPeriod); // [agent, all]


        $outgoing_calls = cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate);
        if ($agent_ex == 0) {
            for ($i = 0; $i < count($outgoing_calls); $i++) {
                $name = $this->getExtensionFromCdrChannel($outgoing_calls[$i]['channel']);
                if ($name) {
                    $outgoing_calls[$i]['name'] = $name;
                } else {
                    $outgoing_calls[$i]['name'] = '';
                }
            }
        }

        //$agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgent($agent_ex);
        $agent_abandoned_calls = abandon_calls::getAbandonedCallsByAgentByDate($agent_ex, $agent_id, $fromDate, $toDate);
        $missedCalls = $this->getAllMissedCallsBetweenDates($fromDate, $toDate);
//        $agent_answered_calls_count = count($answered_calls);
//        $all_answered_calls_count = cdr::getAnsweredCallsCount();
        $logged_in_records = Logged_in_users::getLoginRecordsByDate($agent_id, $fromDate, $toDate);

        $agent_dnd_records = Dnd_records::getDndRecordsByDate($agent_id, $fromDate, $toDate);

        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';


        return $this->render('supervisorReportsByDate', ['breakFullData' => $breaksFullData, 'breakDailyData' => $breaksDailyData, 'answered_calls' => $answered_calls, 'outgoing_calls' => $outgoing_calls, 'agent_answered_calls_count' => $agent_answered_calls_count, 'all_answered_calls_count' => $all_answered_calls_count, 'agent_answered_calls_count' => $agent_answered_calls_count, 'agent_abandoned_calls' => $agent_abandoned_calls, 'login_records' => $logged_in_records, 'agent_extension' => $agent_ex, 'agent_name' => $agent_name, 'agent_dnd_reocrds' => $agent_dnd_records, 'dateRange' => $dateRange, 'callAnswerCounts' => $totalCallsAnsweredSet, 'ftpParams' => $ftpParams, 'agent_id' => $agent_id, 'missed_calls' => $missedCalls]);
    }

    /**
     * <b>Render the Reports</b>
     * 
     * @return render a supervisor reports page
     * 
     * @author Prabath
     * @since 03-08-2017
     * 
     */
    public function actionSupervisorreports() {
//        if (Yii::$app->session->get('user_role') != '1' || Yii::$app->session->get('user_role') != '3') {
//            //logged in user is not an agent or a supervisor
//            $this->redirect('index.php?r=user/login_view');
//        }          
        $agentsList = call_center_user::getAllAgents();
        return $this->render('supervisorReports', ['agents' => $agentsList]);
    }

    private function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * 
     * @return type
     * @since 2017-10-30
     * @author Sandun
     */
    public function actionSummery() {

        $agent_id = $_GET['agent_id'];
        $agent_ex = $_GET['agentex'];
        $agent_name = $_GET['agentname'];
        $searchContact = $_GET['contactNum'];
//        $searchContact = '';
        $fromDate = $_GET['fromdate'];
        $toDate = $_GET['todate'];


        $totalBreakTimeOfTheTimePeriod = Agent_requests::getBreakTimeByDate($agent_id, $fromDate, $toDate); // Total break requests of the given time, Done for all users
        $totalWorkTimeOfTheTimePeriod = Logged_in_users::getDailyWorkedTimeInGivenRange($agent_id, $fromDate, $toDate); // Total work time of the given time period, Done for all users
        $totalBreakTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalBreakTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $totalWorkTimeOfTheTimePeriodRealTime = $this->convertToHoursMins($totalWorkTimeOfTheTimePeriod, '%02d hours %02d minutes');
        $dateRange = array($fromDate, $toDate);

        $agentName = "";
        $totalCallsMissed = 0;
        $totalCallsAbandoned = 0;
        $totalCallsReceived = cdr::getTotalCallsReceivedCountByDate($fromDate, $toDate);
        $totalCallsAnswered = 0;
        $totalOutgoingCalls = 0;
        $noOfAgents = call_center_user::getRegisteredAgentsCountBetweenDates($fromDate, $toDate);
        $agentDndOnTimes = 0;
        if ($agent_id == 0) {
            // all agents
            $agentName = "All agents";
            $totalCallsAnswered = Call_records::answeredCallsCountBetweenDates('0', $fromDate, $toDate);
            $totalCallsMissed = $totalCallsReceived - $totalCallsAnswered;
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate('0', $fromDate, $toDate)); //cdr::getOutgoingCallsCountByUser(0);
        } else {
            // single agent
            $agentName = $agent_name . "'s";
            $totalCallsAbandoned = count(abandon_calls::getAbandonedCallsByAgentByDate($agent_ex, $agent_id, $fromDate, $toDate));
            $totalOutgoingCalls = count(cdr::getOutgoingCallsListByDate($agent_ex, $fromDate, $toDate));
            $totalCallsAnswered = Call_records::answeredCallsCountBetweenDates($agent_id, $fromDate, $toDate);
            $agentDndOnTimes = Dnd_records::noOfTimesPutDndOnBetweenDates($agent_id, $fromDate, $toDate);
        }



        $agentInformation = array(
            "agentName" => $agentName,
            "agentExt" => $agent_ex,
            "noOfAgents" => $noOfAgents,
        );
        $summeryFieldsCalls = array(
            'totalCallsReceived' => $totalCallsReceived,
            'totalCallsAnswered' => $totalCallsAnswered,
            'totalCallsUnanswered' => 0,
            'totalCallsAbandoned' => $totalCallsAbandoned,
            'totalCallsMissed' => $totalCallsMissed,
            'totalCallsOutgoing' => $totalOutgoingCalls
        );
        $summeryFieldsWorkTime = array(
            'totalTimeWorked' => $totalWorkTimeOfTheTimePeriodRealTime,
            'totalBreakTime' => $totalBreakTimeOfTheTimePeriodRealTime,
            'dndOnCount' => $agentDndOnTimes
        );
        return $this->render("supervisorSummeryReport", ["calls" => $summeryFieldsCalls, "work" => $summeryFieldsWorkTime, "agentInfo" => $agentInformation]);
    }

    /**
     * <b>Extract caller extension from channel</b>
     * <p>This function will extract agent extension from passed channel data and return user name of the agent</p>
     * 
     * @author Vikum
     * @since 27-10-2017
     * 
     */
    private function getExtensionFromCdrChannel($channel) {
        preg_match("'SIP/(.*?)-'si", $channel, $match);
        if ($match) {
            if ($match[1] != '' && strlen($match[1]) < 5) {
                $userName = call_center_user::getUserInfoFromVOIPExtension($match[1]);
                return $userName['name'];
//                return implode(" ",$userName);
//                return $match[1];
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    private function getAllMissedCallsBetweenDates($from, $to) {
        $result = array();
        $missedCalls = array();
        if ($from && $to) {
            // return data for the period
            $missedCalls = cdr::getMissedCallsBetweenDates($from, $to);
        } else {
            // return data for the month
            $missedCalls = cdr::getMissedCallsBetweenDates(date("Y-m-01"), date("Y-m-d"));
        }

        if ($missedCalls != NULL && count($missedCalls) > 0) {
            // have missed calls
            for ($i = 0; $i < count($missedCalls); $i++) {
                $callDateExplode = explode(" ", $missedCalls[$i]['start']);
                $missedCallInfo = array(
                    "date" => $callDateExplode[0],
                    "time" => $callDateExplode[1],
                    "caller_num" => $missedCalls[$i]['src']
                );

                array_push($result, $missedCallInfo);
                $missedCallInfo = array();
            }

            return $result;
        } else {
            // no missed calls
            return null;
        }
    }

    public function actionTestmissedcalls() {
        $from = date("Y-m-01");
        $to = date("Y-m-d");
//        echo "missed calls <br>";
//        print_r($this->getAllMissedCallsBetweenDates($from, $to));
        echo "missed calls from $from to $to <br>";
        echo count($this->getAllMissedCallsBetweenDates($from, $to));
    }

}
