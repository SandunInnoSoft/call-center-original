<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\Logged_in_users;
use app\models\Call_records;
use app\models\cdr;
use app\models\Customer;
use app\models\Help_requests;
use app\models\call_center_user;
use app\models\Agent_requests;
use app\models\agent_notifications;
use app\models\callevents;
use app\models\web_presence;

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

/**
 * This controller is to handle the reciving calls
 *
 * @author Sandun
 * @since 2017-06-27
 */
class EventsController extends Controller {

    /**
     * <b>Overrides parent class controller constructor</b>
     * 
     * @param type $id
     * @param type $module
     * @param type $config
     * 
     * @author Sandun
     * @since 2017-09-25
     */
    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
//        if (!Yii::$app->session->has('user_id')) {
//            $this->redirect('index.php?r=user/login_view');
//        }
    }

        /**
        *<b>Server sent event call to update online user timestamp</b>
        *
        * @modified Sandun 2017-10-10
        * @description This update adds a checking to echo data:-1, if user_id session value is not found
        **/
        public function actionEvent() {
        // $user_id = Yii::$app->session->get('user_id');
        if(Yii::$app->session->has('user_id')){
            $isUser = $this->updateLiveUser();
                if ($isUser) {
                    echo "data:-1 \n\n";
                } else {
                    echo "data:1 \n\n";
                }
            } else {
                echo "data:-1 \n\n";
            }
            flush();
        }
    /**
     * <b>Get all the online agents and offline agents</b>
     * <p>This function prints a json encoded data set of all the online agents at the invoked moment of time and the rest of teh agents at the database as not online.
     * Online agents lists at the top of the array</p>
     * 
     * @update Sandun 2017-08-25
     * <p>Added the isAgentBusy() private function call to check if the currently online user is busy or not, the state of the agent is passed to the Online Agents AgentData array</p>
     */
    public function actionLiveagents() {
        $session = Yii::$app->session;
        $user_extension = $session->get('voip');
        $user_id = $session->get('user_id');

        $agentsList = call_center_user::getAllAgentsExceptMe();
        $liveAgents = Logged_in_users::getLoggedAgents();
        $lateBreakArrivals = Agent_requests::getLateBreakArrivals();
        $agentNotifications = agent_notifications::getAgentNotifications($user_id);

        $liveAgentsIds = [];
        $liveAgentsLoggedTimes = [];
        $agents = [];
        $lateArrivedAgents = [];

        $eventDataArr = [];
        $agentName = NULL;

        if (count($agentNotifications) > 0) {
            $userData = call_center_user::getUserInfoFromVOIPExtension($agentNotifications[0]['supervisor_id']);
            $agentName = $userData['name'];
        }

        $agentNotifs = [
            $agentNotifications[0]['id'], $agentNotifications[0]['notif_type'], $agentNotifications[0]['agent_id'],
            $agentNotifications[0]['supervisor_id'], $agentNotifications[0]['notif_status'], $agentNotifications[0]['notif_time'], $agentName
        ];

        for ($a = 0; $a < count($lateBreakArrivals); $a++) {
            array_push($lateArrivedAgents, $lateBreakArrivals[$a]['agent_id']);
        }

        for ($a = 0; $a < count($liveAgents); $a++) {
            array_push($liveAgentsIds, $liveAgents[$a]['user_id']);
            array_push($liveAgentsLoggedTimes, $liveAgents[$a]['logged_in_time']);
        }

        $onlineAgents = array();
        $offlineAgents = array();

        for ($a = 0; $a < count($agentsList); $a++) {
            if (in_array($agentsList[$a]['id'], $liveAgentsIds)) {
                $index = array_search($agentsList[$a]['id'], $liveAgentsIds);
                $loggedInTime = $liveAgentsLoggedTimes[$index];
//                $unixDateTimeOfLoggedIntime = gmdate("Y-m-d\TH:i:s\Z", $loggedInTime);
                $agentsList[$a]['status'] = '1'; //Online
//                $loggedInTime = Logged_in_users::getLoggedInTime($agentsList[$a]['id']);
                $agentBusyState = $this->isAgentBusy($agentsList[$a]['id']);
                $agent_data = [$agentsList[$a]['id'],
                    $agentsList[$a]['name'],
                    $agentsList[$a]['fullname'],
                    $agentsList[$a]['status'],
                    $agentsList[$a]['voip_extension'],
                    date("h:i:A", $loggedInTime), //date_format(date_create($unixDateTimeOfLoggedIntime), "h:i:A")//date_format(date_create($loggedInTime), "h:i:A")
                    $agentBusyState
                ];
                array_push($onlineAgents, $agent_data);
            } else {
                $agentsList[$a]['status'] = '0'; //Offline
                $agent_data = [$agentsList[$a]['id'], $agentsList[$a]['name'], $agentsList[$a]['fullname'],
                    $agentsList[$a]['status']
                ];
                array_push($offlineAgents, $agent_data);
            }
        }

        if (count($onlineAgents) > 0) {
            // have online agents
            for ($x = 0; $x < count($onlineAgents); $x++) {

                array_push($agents, $onlineAgents[$x]);
            }
        }


        if (count($offlineAgents) > 0) {
            // have offline agents
            for ($y = 0; $y < count($offlineAgents); $y++) {
                array_push($agents, $offlineAgents[$y]);
            }
        }



        array_push($eventDataArr, $agents);
        array_push($eventDataArr, $lateArrivedAgents);
        array_push($eventDataArr, $agentNotifs);
        $json_eventsDataArray = json_encode($eventDataArr);

        echo $json_eventsDataArray;
//        flush();
    }

//    public function actionIsagentbusy(){

    /**
     * <b>Checks whether the agent is busy or not</b>
     * <p>This function takes the agent ID as the parameter and get its VOIP extension number through a call_center_user model function
     * Then it passes to a web_presence model function to get its current state
     * If the current state is "INUSE" which means the agent is busy with a call, returns 1.
     * Else returns 0</p>
     * 
     * @param int $agentId
     * @return int 1 or 0
     * 
     * @author Sandun
     * @since 2017-08-24
     */
    private function isAgentBusy($agentId) {
//        $agentId = $_GET['id'];
        $extension = call_center_user::getUserVoipExtension($agentId);
        $state = web_presence::getExtensionState($extension);
        if ($state == "INUSE") {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * <b>Update Live user</b>
     * <p> This method Update live user record time signature. Once the api grab live users, it can return users who's last time signature has not expired</p>
     *      
     * @since 2017/06/30
     * @author Vikum
     * @updated Sandun 2017-06-30
     * 
     * @return Bool TRUE
     */
    private function updateLiveUser() {
        // update time signature in logged in user table
        $t = time();
        $session = Yii::$app->session;
        $userId = $session->get('user_id');
        $session->set('access_time', $t);
        $query_loggedinuser = Logged_in_users::findOne(['user_id' => $userId, 'active' => 1]);
        if ($query_loggedinuser) {
            $query_loggedinuser->time_signature = $t;
            $query_loggedinuser->update();
            return TRUE;
        } else {
//            $loggedInUser = new Logged_in_users();
//            $loggedInUser->user_id = $userId;
//            $loggedInUser->user_logged_ip_address = $_SERVER['REMOTE_ADDR'];
//            $loggedInUser->logged_in_time = $t;
//            $loggedInUser->time_signature = $t;
//            $loggedInUser->active = 1;
//            $loggedInUser->insert();
            return FALSE;
        }
    }

    /**
     * <b> Removed logged in user</b>
     * <p> The user record will be removed from logged in user table</p>
     * 
     * @param type $userId
     * @return type
     * @since 2017/06/30
     * @author Sandun 
     */
    private function removeLoggedInUser($userId) {
        return Logged_in_users::deleteAll("user_id = $userId");
    }

    private function copyNewCallRecordFromCDR($cdrId) {
        $newCdr = cdr::getNewCDRInfo($cdrId);
        $customerId = $this->getCustomerInformationByPhoneNumber($newCdr[0]['src']);
        $array = array(
            "cdr_id" => $newCdr[0]['AcctId'],
            "call_date" => date("Y-m-D"),
            "caller_number" => $newCdr[0]['src'],
            "customer_id" => $customerId,
            "user_id" => $newCdr[0]['clid'],
            "created_date_time" => date("Y-m-D")
        );

        Call_records::insertNewCallRecord($array);
        return $array;
    }

    private function getCustomerInformationByPhoneNumber($customerPhoneNumber) {
        $customerId = Customer::getCustomerIdByPhoneNumber($customerPhoneNumber);
        return $customerId;
    }

    public function actionRequests() {
        $unconsumedHelpRequests = Help_requests::getUnconsumedHelpRequests();
        $unconsumedAgentRequests = Agent_requests::getUnconsumedAgentRequest();
        $csvString = "";
        if (count($unconsumedHelpRequests) > 0 && count($unconsumedAgentRequests) > 0) {
            // Have unconsumed help requests
            $agentInfo = call_center_user::getUser($unconsumedHelpRequests[0]['agentId']);
            $agentInfoRequest = call_center_user::getUser($unconsumedAgentRequests[0]['agent_id']);
            $csvString = $unconsumedHelpRequests[0]['id'] . "," . $agentInfo[0]['name'] . "," . $unconsumedHelpRequests[0]['datetime'] . "," . $unconsumedAgentRequests[0]['id'] . "," . $agentInfoRequest[0]['name'] . "," . $unconsumedAgentRequests[0]['request_type'];
            Help_requests::markConsumed($unconsumedHelpRequests[0]['id']);
            echo $csvString;
//            flush();
        } else if (count($unconsumedHelpRequests) > 0) {
            $agentInfo = call_center_user::getUser($unconsumedHelpRequests[0]['agentId']);
            $csvString = $unconsumedHelpRequests[0]['id'] . "," . $agentInfo[0]['name'] . "," . $unconsumedHelpRequests[0]['datetime'] . ",0,0,0";
            Help_requests::markConsumed($unconsumedHelpRequests[0]['id']);
            echo $csvString;
//            flush();
        } else if (count($unconsumedAgentRequests) > 0) {
            $agentInfo = call_center_user::getUser($unconsumedAgentRequests[0]['agent_id']);
            $csvString = "0,0,0," . $unconsumedAgentRequests[0]['id'] . "," . $agentInfo[0]['name'] . "," . $unconsumedAgentRequests[0]['request_type'];
            echo $csvString;
//            flush();
        } else {
            echo "data:-1";
//            flush();
        }
    }

//    public function actionHelprequests() {
//        $unconsumedHelpRequests = Help_requests::getUnconsumedHelpRequests();
//        if (count($unconsumedHelpRequests) > 0) {
//            // Have unconsumed help requests
//            $agentInfo = call_center_user::getUser($unconsumedHelpRequests[0]['agentId']);
//            $csvString = $unconsumedHelpRequests[0]['id'] . "," . $agentInfo[0]['name'] . "," . $unconsumedHelpRequests[0]['datetime'];
//            Help_requests::markConsumed($unconsumedHelpRequests[0]['id']);
//            echo "data:$csvString \n\n";
//            flush();
//        } else {
//            echo "data:-1 \n\n";
//            flush();
//        }
//    }

    public function actionAgentrequests() {
        $unconsumedAgentRequests = Agent_requests::getUnconsumedAgentRequest();
        if (count($unconsumedAgentRequests) > 0) {
            // Have unconsumed help requests
            $agentInfo = call_center_user::getUser($unconsumedAgentRequests[0]['agent_id']);
            $csvString = $unconsumedAgentRequests[0]['id'] . "," . $agentInfo[0]['name'] . "," . $unconsumedAgentRequests[0]['requested_time'];
//            Help_requests::markConsumed($unconsumedHelpRequests[0]['id']);
            echo "data:$csvString \n\n";
            flush();
        } else {
            echo "data:-1 \n\n";
            flush();
        }
    }

    /**
     * <b> Get break request responses</b>
     * <p> This function will look for approved or denied requests</p>
     * 
     * 
     * @return type
     * @since 2017/07/20
     * @author Vikum 
     */
    public function actionAgentresponses() {
//        $unconsumedAgentRequests = Agent_requests::getUnconsumedAgentRequest();
        $user_id = Yii::$app->session->get('user_id');
        $unconsumedSupervisorRequests = Agent_requests::getSupervisorResponse($user_id);
        if (count($unconsumedSupervisorRequests) > 0) {
            // Have unconsumed agent requests
//            $agentInfo = call_center_user::getUser($unconsumedSupervisorRequests[0]['agent_id']);
            $csvString = $unconsumedSupervisorRequests[0]['id'] . "," . $unconsumedSupervisorRequests[0]['request_type'] . "," . $unconsumedSupervisorRequests[0]['request_status'] . "," . $unconsumedSupervisorRequests[0]['approved_time_period'];

            echo $csvString;
//            flush();
        } else {
            echo "data:-1";
//            flush();
        }
    }

    /**
     * <b>Gets queued calls information</b>
     * <p>This function gets the queued calls information from the </p>
     * 
     * @author Sandun
     * @since 2017-08-02
     */
    public function actionGetcalls() {
        $queuedCalls = callevents::getAllQueuedCalls();
        $ongoingCalls = web_presence::getOngoingCalls();
        $callInfo = array();
        $callInfo['queuedCalls'] = $queuedCalls;
        $callInfo['ongoingCalls'] = $ongoingCalls;
        if (count($queuedCalls) > 0 || count($ongoingCalls) > 0) {
            echo json_encode($callInfo);
//            flush();
        } else {
            echo "data:-1";
//            flush();
        }
    }

}
