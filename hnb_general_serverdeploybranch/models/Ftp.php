<?php


namespace app\models;
use yii;
use yii\db\ActiveRecord;

/**
 * This model class interacts with extension_queue table in DB1
 *
 * @author Sandun
 * @since 2019-01-16
 */
class Ftp extends ActiveRecord{
    
    public static function checkIfAudioFileExists($dateTime, $callType, $extensionNumber, $customerNumber, $callEndedTime){

        $audioFilePathArray = Ftp::getAudioFilePathFromNAS($dateTime, $callType, $extensionNumber, $customerNumber);
        if($audioFilePathArray != false){
            // audio file exists
            $selectedAudioFilePath = Ftp::locateMatchingFilePath($audioFilePathArray, $dateTime, $callEndedTime);
            if($selectedAudioFilePath != false){
                return "Found";
            }else{
                return "Not found";
            }
            // }
        }else{
            //audio file doesnt exist in NAS
            return "Not in NAS";
        }
    }

    private function getAudioFilePathFromNAS($callStartDatetime, $callType, $extensionNumber, $customerNumber){
        $dateTimeExplodedArray = explode(" ", $callStartDatetime);
        $ftpParams = include __DIR__ . '/../config/pbx_audio_ftp_config.php';
        $audioFileDirectoryfile = $ftpParams['NAS_audio_file_Location'];
        $date = $dateTimeExplodedArray[0];
        $date = str_replace("-", "_", $date);
        $audioFileDirectoryfile = str_replace("<date>", $date, $audioFileDirectoryfile);
        $audioFileDirectoryfile = str_replace("<extension>", $extensionNumber, $audioFileDirectoryfile);
        // echo "Audio directory file = $audioFileDirectoryfile";
        if(file_exists($audioFileDirectoryfile) == true){
             // directory exitsts
            $audioFileNamesArray = scandir($audioFileDirectoryfile);
 
             $callStartDatetime = str_replace("-", "", $callStartDatetime);
             $callStartDatetime = str_replace(":", "", $callStartDatetime);        
             $callStartDatetime = str_replace(" ", "-", $callStartDatetime);
             if($callType == "I"){
                 // call type is incoming
                 $audioFileNamePattern = "-".$customerNumber."-".$extensionNumber."-";
             }else{
                 // call type is outgoing
                 $audioFileNamePattern = "-".$extensionNumber."-".$customerNumber."-";
             }
             
             $matchingFileNamesArray = array();
             // echo "Pattern to search = $audioFileNamePattern <br>";
             foreach ($audioFileNamesArray as $key) {
                 if(preg_match("/($audioFileNamePattern)/", $key) == 1){
                     // matching file name exists
                     array_push($matchingFileNamesArray, $audioFileDirectoryfile.$key);
                 }
             }
 
             if(count($matchingFileNamesArray) == 0){
                 // echo "No match found<br>";
                 return false;
             }else{
                 return $matchingFileNamesArray;
             }
 
        }else{
             // echo "directory does not exist<br>";
             // echo "<br>";
             // echo $audioFileDirectoryfile;
             // echo "<br>";
              return false;
        }
         
     }

     private function locateMatchingFilePath($audioFilePathArray, $datetimeToMatch, $callEndedTime){
        $explodedDateTime = explode(" ", $datetimeToMatch);
        $dateToMatch = $explodedDateTime[0];
        $timeToMatch = $explodedDateTime[1];
        $unixTimestampToMatch = strtotime($datetimeToMatch);
        $unixTimestampCompareFrom = $unixTimestampToMatch - 5;
        $unixTimestampCompareTo = $unixTimestampToMatch + 60;
        $foundFiles = array();
        for($x = 0; $x < count($audioFilePathArray); $x++){
            $explodedAudioFileFullPath = explode("/", $audioFilePathArray[$x]);
            $audioFileName = end($explodedAudioFileFullPath);
            $fileDateTime = Ftp::extractDatetimeFromAudioFileName($audioFileName);
            $unixTimestampOfFileDateTime = strtotime($fileDateTime);
            if($unixTimestampOfFileDateTime >= $unixTimestampCompareFrom && $unixTimestampOfFileDateTime <= $unixTimestampCompareTo){
                // match found
                array_push($foundFiles, $audioFilePathArray[$x]);
//                return $audioFilePathArray[$x];
            }else{
                // no match found
                $unixTimeOfEnddedTime = strtotime($callEndedTime);
                $unixStartedTime = strtotime($datetimeToMatch);
                if($unixTimestampOfFileDateTime >= $unixStartedTime && $unixTimestampOfFileDateTime <= $unixTimeOfEnddedTime){
                    // has a match
                    array_push($foundFiles, $audioFilePathArray[$x]);
                }
            }
        }
        
        if(count($foundFiles) == 1){
            // one match found
            return $foundFiles[0];
        }else if(count($foundFiles) > 1){
            // has more than 1 match
            $largestFile = $foundFiles[0];
            for($x = 1; $x < count($foundFiles) - 1; $x++){
                if(filesize($foundFiles[$x]) > filesize($largestFile)){
                    $largestFile = $foundFiles[$x];
                }
            }
            
            return $largestFile;
        }else{
            return false;   
        }
    }
    

    private function extractDatetimeFromAudioFileName($audioFileName){
        $audioFileNameExploded = explode("-", $audioFileName);
        $dateSection = $audioFileNameExploded[0];
        $timeSection = $audioFileNameExploded[1];

        $dateSplittedString = str_split($dateSection, 2);
        $dateTime = $dateSplittedString[0].$dateSplittedString[1]."-".$dateSplittedString[2]."-".$dateSplittedString[3]." ";

        $timeSplittedString = str_split($timeSection, 2);
        $dateTime = $dateTime.$timeSplittedString[0].":".$timeSplittedString[1].":".$timeSplittedString[2];
        return $dateTime;
    }

}
