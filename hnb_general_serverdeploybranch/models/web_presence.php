<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is used to interract with web_presence table in DB3
 *
 * @author Sandun
 * @since 2017-08-03
 */
class web_presence extends ActiveRecord {
    
    
    /**
     * <b>Overrides the get db function of the parent class to use the DB3 connection</b>
     * @return type
     */
    public static function getDb() {
        return Yii::$app->db3;
    }

    /**
     * <b>Get all ongoing calls</b>
     * <p>This function returns all ongoing calls data in a array</p>
     * 
     * @since 2017-08-03
     * @author Sandun
     * @return Array Ongoing calls
     */
    public static function getOngoingCalls() {
        $connection = Yii::$app->db3;
        if(Yii::$app->session->get("user_role") != "1"){
            // user is not admin
            $queueExtensionsCsvString = web_presence::getExtensionsOfTheUserQueueInCSV();
        
            $command = $connection->createCommand("SELECT *, unix_timestamp() - callstart AS duration " 
                . "FROM web_presence "
                . "WHERE state = 'INUSE' "
                . "AND cidnum IS NOT NULL AND CHAR_LENGTH(cidnum) > 6 "
                . "AND ext IN ($queueExtensionsCsvString);");
        }else{
            // user is admin
            $command = $connection->createCommand("SELECT *, unix_timestamp() - callstart AS duration " 
                . "FROM web_presence "
                . "WHERE state = 'INUSE' "
                . "AND cidnum IS NOT NULL AND CHAR_LENGTH(cidnum) > 6;");
        }
        return $command->queryAll();
    }
    
    private static function getExtensionsOfTheUserQueueInCSV(){
        $extensionsOfTheQueue = Ext_queue_intermediate::getAllExtensionsOfTheQueue(Yii::$app->session->get("extQueueId"));
        return implode(",", $extensionsOfTheQueue);
    }

    /**
     * <b>Get all the VOIP numbers</b>
     * <p>This function returns an array of all the VOIP numbers in the table</p>
     *
     * @return array Voip extension numbers
     * @since 2017-08-03
     * @author Sandun
     */
    public static function getAllVoipExtensions() {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("SELECT ext FROM web_presence");
        $allExtensions = $command->queryAll();

        $allExtensionsRemade = array();

        for ($x = 0; $x < count($allExtensions); $x++) {
            $allExtensionsRemade[$x] = $allExtensions[$x]['ext'];
        }

        return $allExtensionsRemade;
    }

    /**
     * <b>Get Current Summary</b>
     * <p>This function returns an array of all the VOIP numbers in the table</p>
     *
     * @return array all Summary
     * @since 2017-08-17
     * @author Vikum
     */
    public static function getAllSummary($extensionsInQueue) {
        $connection = Yii::$app->db3;
        if($extensionsInQueue == NULL){
            $command = $connection->createCommand("select state,count(ext) as num from web_presence GROUP BY state");            
        }else{
            $extensionsInQueueCsvString = implode(",", $extensionsInQueue);
            $command = $connection->createCommand("select state,count(ext) as num from web_presence WHERE ext IN ($extensionsInQueueCsvString) GROUP BY state");            
        }
        return $command->queryAll();
    }

    /**
     * <b>Returns current state of the extention</b>
     * <p>This function returns the current state of the extension passed as the parameter</p>
     * 
     * @param int $extension
     * @return String state
     * 
     * @author Sandun
     * @since 2017-08-24
     */
    public static function getExtensionState($extension) {
        $connection = Yii::$app->db3;
        $command = $connection->createCommand("SELECT state FROM web_presence WHERE ext = '$extension'");
        $extensionState = $command->queryAll();

        return $extensionState[0]['state'];
    }

    public static function updatePresenceStateForNotInUse($state, $device) {
//        $sql = "update callevents.web_presence set state='$state',cidnum = NULL, cidname = NULL, inorout = NULL, callstart = NULL "
//                . "where ext='$device'";

        $presence = web_presence::find()
                ->where("ext='$device'")
                ->one();

        $presence->state = $state;
        $presence->cidnum = NULL;
        $presence->cidname = NULL;
        $presence->inorout = NULL;
        $presence->callstart = NULL;
        return $presence->update();
    }

    public static function updatePresenceStateForOtherStates($state, $device) {
//        $sql = "update callevents.web_presence set state='$state',cidnum = NULL, cidname = NULL, inorout = NULL, callstart = NULL "
//                . "where ext='$device'";

        $presence = web_presence::find()
                ->where("ext='$device'")
                ->one();

        $presence->state = $state;
        return $presence->update();
    }
    
    public static function updateInboundPresenceCall($cidnum, $cidname, $ts, $exten, $channel){
        $presence = web_presence::find()
                ->where("ext = '$exten'")
//                ->andWhere("cidnum is NULL")
                ->one();
        $presence->cidnum = $cidnum;
        $presence->cidname = $cidname;
        $presence->inorout = 'I';
        $presence->callstart = $ts;
        $presence->update();
        
        $otherPrsence = web_presence::find()
                ->where("ext = '$channel'")
//                ->andWhere("cidnum is null")
                ->one();
        $otherPrsence->cidnum = $exten;
        $otherPrsence->inorout = "O";
        $otherPrsence->callstart = $ts;
        return TRUE;
    }
    
    public static function updateOutboundPresenceCall($dialed, $ts, $channel){
        $presence = web_presence::find()
                ->where("ext = '$channel'")
                ->one();
        $presence->cidnum = $dialed;
        $presence->inorout = "O";
        $presence->callstart = $ts;
        return $presence->update();
        
    }
    
    public static function updateOutboundPresenceCallToInuse($pickup, $cidname, $ts, $channel){
        $presence = web_presence::find()
                ->where("ext = $channel")
                ->one();
        $presence->cidnum = $pickup;
        $presence->cidname = $cidname;
        $presence->inorout = "O";
        $presence->state = "INUSE";
        $presence->callstart = $ts;
        return $presence->update();
    }
    
    /**
     * <b>Returns web presence infromation when extension is ringing</b>
     * <p>This function returns the data recorded in the web presence table when the extension is ringing</p>
     * 
     * @param int $extension
     * @return array
     * 
     * @since 2017-12-06
     * @author Sandun
     */
    public static function ringingExtensionInformation($extension){
        return web_presence::find()
                ->where("state = 'RINGING'")
                ->andWhere("ext = '$extension'")
                ->one();
    }
    
    /**
     * <b>Inserts a new extension to the web presence table</b>
     * @param type $newExtension
     * @return type
     * 
     * @since 2017-12-21
     * @author Sandun
     */
    public static function insertNewExtensionNumber($newExtension){
        $webPresence = new web_presence();
        $webPresence->ext = $newExtension;
        $webPresence->state = "UNAVAILABLE";
        return $webPresence->insert();
    }
    
    /**
     * <b>Check if the extension is already registered or not</b>
     * <p>This function checks the extension number passed into function is already exists in the we_presence table.
     * If exists, returns false, if not exists returns true</p>'
     * 
     * @param int $extensionNumber
     * @return boolean
     * 
     * @since 2018-1-3
     * @author Sandun
     */
    public static function isExtensionAvailable($extensionNumber){
        $extension = web_presence::find()
                ->where("ext = '$extensionNumber'")
                ->one();
        if(!$extension){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
