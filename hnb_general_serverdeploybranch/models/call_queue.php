<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Description of call_queue
 *
 * @author Sandun
 * @since 2017 - 09 - 13
 */
class call_queue extends ActiveRecord{
    
    
    public static function addToCallQueue($cidNumber){
        $call = new call_queue();
        $call->number = $cidNumber;
        return $call->insert();
    }
    
    public static function deleteCallRecordFromQueue($cidnum){
        $call = call_queue::find()
                ->where("number = $cidnum")
                ->one();
        return $call->delete();
    }
    
    
}
