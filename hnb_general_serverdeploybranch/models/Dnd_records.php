<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model class is used to transfer data with the databse table dnd_records 
 *
 * @author Sandun
 * @since 2017-08-13
 */
class Dnd_records extends ActiveRecord {

    /**
     * 
     * @param type $state
     * @return boolean
     * @modified 2017-12-04 Sandun
     * @description Added a checking to perform when inserting a new DND record to the db, checks if the latest DND record of the user 
     * is NOT same as the new state, then only the new one gets inserted to the DB
     * 
     */
    public static function insertNewDNDRecord($state) {
        $latestDndState = Dnd_records::getLatestDNDStateOfUser(Yii::$app->session->get('user_id'));
        if ($latestDndState['dnd_mode'] != $state) {
            $dnd_record = new Dnd_records();
            $dnd_record->user_id = Yii::$app->session->get('user_id');
            $dnd_record->dnd_mode = $state;
            $dnd_record->timestamp = date("Y-m-d H:i:s");
            return $dnd_record->insert();
        }else{
            return FALSE;
        }
    }

    public static function getLatestDNDStateOfUser($userId) {
        return Dnd_records::find()
                        ->select('dnd_mode')
                        ->where("user_id = $userId")
                        ->orderBy(["timestamp" => SORT_DESC])
                        ->one();
    }

    /**
     * 
     * @param type $user_id
     * @author supun
     * @since 2017/08/14
     * @return DND records
     */
    public static function getDndRecordsByUser($user_id) {
        $obj = new dnd_records();
        if ($user_id != 0) { // If user id not for all users
            return $obj->find()
                            ->select('*')
                            ->where("user_id=$user_id")
                            ->andWhere('(MONTH(DATE(timestamp)) = MONTH(CURDATE()))')
                            ->orderBy(["timestamp" => SORT_DESC])
                            ->all();
        } else {
            return $obj->find()
                            ->select('*')
                            ->where('(MONTH(DATE(timestamp)) = MONTH(CURDATE()))')
                            ->orderBy(["timestamp" => SORT_DESC])
                            ->all();
        }
    }

    /**
     * 
     * @param type $agent_id
     * @param type $fromDate
     * @param type $todate
     * @return DND record filter by date
     * @author supun
     * @since 2017/08/16
     * 
     * @modified Sandun 2018-1-23
     * @param type $fromTime
     * @param type $toTime
     */
    public static function getDndRecordsByDate($agent_id, $fromDate, $todate, $fromTime, $toTime, $agentIdsInQueue) {
        $obj = new dnd_records();
        if ($agent_id != 0) {
            if ($agent_id != null && $fromDate != null && $todate != null) {
                return $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->andWhere(['between', 'timestamp', $fromDate." ".$fromTime, $todate." ".$toTime])
                                ->orderBy(["timestamp" => SORT_DESC])
                                ->all();
            } else if ($agent_id != null && $fromDate == null && $todate != null) {
                return $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->andWhere(['between', 'timestamp', $todate . " 00:00:00", $todate . " 23:59:59"])
                                ->orderBy(["timestamp" => SORT_DESC])
                                ->all();
            } else if ($agent_id != null && $fromDate != null && $todate == null) {
                return $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->andWhere(['between', 'timestamp', $fromDate, date('Y-m-d h:i:s')])
                                ->orderBy(["timestamp" => SORT_DESC])
                                ->all();
            } else {
                return $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->orderBy(["timestamp" => SORT_DESC])
                                ->all();
            }
        } else {
            if ($agent_id != null && $fromDate != null && $todate != null) {
                $dndRecords = $obj->find()
                                ->select('*')
//                                ->where("user_id=$agent_id")
                                ->where(['between', 'timestamp', $fromDate." ".$fromTime, $todate." ".$toTime]);
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            } else if ($agent_id != null && $fromDate == null && $todate != null) {
                $dndRecords = $obj->find()
                                ->select('*')
//                                ->where("user_id=$agent_id")
                                ->where(['between', 'timestamp', $todate . " 00:00:00", $todate . " 23:59:59"]);
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            } else if ($agent_id != null && $fromDate != null && $todate == null) {
                $dndRecords = $obj->find()
                                ->select('*')
//                                ->where("user_id=$agent_id")
                                ->where(['between', 'timestamp', $fromDate, date('Y-m-d h:i:s')]);
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            } else {
                $dndRecords = $obj->find();
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            }
        }
    }

    /**
     * <b></b>
     * <p></p>
     * @param type $agentId
     * @param type $from
     * @param type $to
     * @return int count
     * 
     * @since 2017-11-01
     * @author Sandun
     */
    public static function noOfTimesPutDndOnBetweenDates($agentId, $from, $to) {
        return Dnd_records::find()
                        ->where("user_id = $agentId")
                        ->andWhere("dnd_mode = 'On'")
                        ->andWhere(['between', 'timestamp', $from . " 00:00:00", $to . " 23:59:59"])
                        ->count();
    }




    /**
     * Added Pagination options for DND records
     * @author Vinothan
     * @since 2019/07/02
     * 
     * @param type $fromTime
     * @param type $toTime
     */
    public static function getDndRecordsByDateWithPagination($agent_id, $fromDate, $todate, $fromTime, $toTime, $agentIdsInQueue,$returnType,$limit,$offset,$orderColumn,$orderDir) {
        $obj = new dnd_records();
        if ($agent_id != 0) {
            if ($agent_id != null && $fromDate != null && $todate != null) {
                $dndRecords =  $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->andWhere(['between', 'timestamp', $fromDate." ".$fromTime, $todate." ".$toTime]);

                                if ($orderColumn !=null) {
                                    $dndRecords->orderBy("timestamp $orderDir");
                                }else{
                                    $dndRecords->orderBy("timestamp desc");
                                }
                                if ($returnType == "Count") {
                                    return $dndRecords->count();
                                }else{
                                    if ($limit != null) {
                                        $dndRecords->limit($limit)->offset($offset);
                                    }
                                    return $dndRecords->all();                                   
                                }

            } else if ($agent_id != null && $fromDate == null && $todate != null) {
                $dndRecords =  $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->andWhere(['between', 'timestamp', $todate . " 00:00:00", $todate . " 23:59:59"]);

                                if ($orderColumn !=null) {
                                    $dndRecords->orderBy("timestamp $orderDir");
                                }else{
                                    $dndRecords->orderBy("timestamp desc");
                                }
                                if ($returnType == "Count") {
                                    return $dndRecords->count();
                                }else{
                                    if ($limit != null) {
                                        $dndRecords->limit($limit)->offset($offset);
                                    }
                                    return $dndRecords->all();                                   
                                }

            } else if ($agent_id != null && $fromDate != null && $todate == null) {
                $dndRecords =  $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id")
                                ->andWhere(['between', 'timestamp', $fromDate, date('Y-m-d h:i:s')]);

                                if ($orderColumn !=null) {
                                    $dndRecords->orderBy("timestamp $orderDir");
                                }else{
                                    $dndRecords->orderBy("timestamp desc");
                                }
                                if ($returnType == "Count") {
                                    return $dndRecords->count();
                                }else{
                                    if ($limit != null) {
                                        $dndRecords->limit($limit)->offset($offset);
                                    }
                                    return $dndRecords->all();                                   
                                }

            } else {
                $dndRecords =  $obj->find()
                                ->select('*')
                                ->where("user_id=$agent_id");

                                if ($orderColumn !=null) {
                                    $dndRecords->orderBy("timestamp $orderDir");
                                }else{
                                    $dndRecords->orderBy("timestamp desc");
                                }
                                if ($returnType == "Count") {
                                    return $dndRecords->count();
                                }else{
                                    if ($limit != null) {
                                        $dndRecords->limit($limit)->offset($offset);
                                    }
                                    return $dndRecords->all();                                   
                                }

            }
        } else {
            if ($agent_id != null && $fromDate != null && $todate != null) {
                $dndRecords = $obj->find()
                                ->select('*')
//                                ->where("user_id=$agent_id")
                                ->where(['between', 'timestamp', $fromDate." ".$fromTime, $todate." ".$toTime]);
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            } else if ($agent_id != null && $fromDate == null && $todate != null) {
                $dndRecords = $obj->find()
                                ->select('*')
//                                ->where("user_id=$agent_id")
                                ->where(['between', 'timestamp', $todate . " 00:00:00", $todate . " 23:59:59"]);
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            } else if ($agent_id != null && $fromDate != null && $todate == null) {
                $dndRecords = $obj->find()
                                ->select('*')
//                                ->where("user_id=$agent_id")
                                ->where(['between', 'timestamp', $fromDate, date('Y-m-d h:i:s')]);
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            } else {
                $dndRecords = $obj->find();
                                if($agentIdsInQueue != null){
                                    $dndRecords->andWhere(['in', 'user_id', $agentIdsInQueue]);
                                }
                $dndRecords->orderBy(["timestamp" => SORT_DESC])
                                ->all();
                return $dndRecords;
            }
        }
    }    

}
