/**
* HNB call center database update version 4.8
* @author Vinothan
* @since 2019-01-31
* @target main DB
*/

-- update missing calls notification email subscribers

ALTER TABLE `missed_calls_email_subscribers` CHANGE `created_date` `created_date` DATETIME on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `missed_calls_email_subscribers` ADD `status` ENUM('active','deleted') NOT NULL DEFAULT 'active' AFTER `created_date`;