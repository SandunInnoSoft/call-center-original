#this query will add new column to record user 
ALTER TABLE `call_center_user` 
ADD COLUMN `user_email` VARCHAR(50) NULL COMMENT 'This will be mandatory for supervisors' AFTER `voip_extension`;
