import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import DAO.PbxDbConnector;
import DAO.RoutineConfig;
import DTO.DbConfig;
import DTO.LocalCdr;
import DTO.PbxCdr;

public class Program {
	public static void main(String args[]) {
		System.out.println("Hello again java");

		// pbx = new DbConfig();
		DbConfig pbx = RoutineConfig.getInstance().getPbxDbConfig();

		String tableName = pbx.getTableName();
		if (pbx.getTablePrefix().trim() != "") {
			tableName = pbx.getTableName().trim() + Calendar.getInstance().get(Calendar.YEAR) + "02";
			System.out.println("Table name is: " + tableName);
		}
		String query = "SELECT * FROM " + tableName;
		ArrayList<PbxCdr> allPbxData = new ArrayList();
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO DBUSER" + "(USER_ID, USERNAME, CREATED_BY, CREATED_DATE) VALUES"
				+ "(?,?,?,?)";
		try {
			ResultSet rs = PbxDbConnector.getDbCon().query(query);
			while (rs.next()) {
				allPbxData.add(new PbxCdr(rs.getInt("AcctId"), rs.getInt("duration"), rs.getInt("billable"),
						rs.getInt("amaflags"), rs.getDate("dateTime"), rs.getString("clid"), rs.getString("src"),
						rs.getString("dst"), rs.getString("dcontext"), rs.getString("srctrunk"),
						rs.getString("dstrunk"), rs.getString("lastapp"), rs.getString("lastdata"),
						rs.getString("disposition"), rs.getString("calltype"), rs.getString("accountcode"),
						rs.getString("uniqueid"), rs.getString("monitorpath"), rs.getString("extfld1"),
						rs.getString("extfld2"), rs.getString("extfld3")));

			}
			System.out.println("Size is = " + allPbxData.size());
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {

		}
	}

	// This method will get PBX_CDR data object and arrange them in a new array list
	// to save in localdb
	public ArrayList<LocalCdr> arrangeDataset(ArrayList<PbxCdr> pbxData) {
		ArrayList<LocalCdr> returnLocalDataSet =  new ArrayList();
		for(PbxCdr cdrIterator: pbxData) {
			String dst= cdrIterator.getDst();
			if(cdrIterator.getCalltype().equals("Outbound")) {
				String [] parts= cdrIterator.getCalltype().split("(");
				dst= parts[0]; 
			}
			returnLocalDataSet.add(new LocalCdr( duration_, billsec_, disposition_, amaflags_, recordtimelen_, ccsrc_, cdrIterator.getClid(), cdrIterator.getSrc(), dst, calleenum_, dcontext_, channel_, dstchannel_, lastapp_, lastdata_, start_, answer_, end_, calltype_, accountcode_, uniqueid_, rcfile_, recordingfilename_))
		}
		return returnLocalDataSet;
	}
}
