package DAO;

//import com.mysql.jdbc.Connection;

import DTO.DbConfig;

import java.sql.*;
import java.sql.DriverManager;

/**
 * @desc A singleton database access class for MySQL
 * @author
 */
public final class PbxDbConnector {
	public Connection conn;
	private Statement statement;
	public static PbxDbConnector db;
	public DbConfig local, pbx;

	private PbxDbConnector() {	
		System.out.println("Came at PbxDbConnector()");
		pbx = RoutineConfig.getInstance().getPbxDbConfig();
		String url = "jdbc:mysql://"+pbx.getHostName().trim()+":"+pbx.getPort().trim()+"/";
		String dbName = pbx.getDbName().trim();
		String driver = "com.mysql.jdbc.Driver";
		String userName = pbx.getUserName().trim();
		String password = pbx.getpWord().trim();
		System.out.println("Driver is: "+url+dbName);
		try {
			Class.forName(driver);
			this.conn =  DriverManager.getConnection(url + dbName, userName, password);
		} catch (Exception sqle) {
			
			sqle.printStackTrace();
		}
	}

	/**
	 *
	 * @return MysqlConnect Database connection object
	 */
	public static synchronized PbxDbConnector getDbCon() {
		if (db == null) {
			db = new PbxDbConnector();
		}
		return db;

	}

	/**
	 *
	 * @param query
	 *            String The query to be executed
	 * @return a ResultSet object containing the results or null if not available
	 * @throws SQLException
	 */
	public ResultSet query(String query) throws SQLException {
		statement = db.conn.createStatement();
		ResultSet res = statement.executeQuery(query);
		return res;
	}

	/**
	 * @desc Method to insert data to a table
	 * @param insertQuery
	 *            String The Insert query
	 * @return boolean
	 * @throws SQLException
	 */
	public int insert(String insertQuery) throws SQLException {
		statement = db.conn.createStatement();
		int result = statement.executeUpdate(insertQuery);
		return result;

	}
	

}
