package DTO;

import java.sql.Date;

public class LocalCdr {
	private int acctId_,duration_,billsec_, disposition_, amaflags_, recordtimelen_; // int fields  
	
	private String ccsrc_, clid_, src_, dst_,calleenum_, dcontext_,channel_, dstchannel_,lastapp_, lastdata_, start_, answer_, end_, 
	calltype_, accountcode_,uniqueid_,rcfile_, recordingfilename_ ; // All varchar type fields

	
	public LocalCdr( int duration_, int billsec_, int disposition_, int amaflags_, int recordtimelen_,
			String ccsrc_, String clid_, String src_, String dst_, String calleenum_, String dcontext_, String channel_,
			String dstchannel_, String lastapp_, String lastdata_, String start_, String answer_, String end_,
			String calltype_, String accountcode_, String uniqueid_, String rcfile_, String recordingfilename_) {
		super();
		
		this.duration_ = duration_;
		this.billsec_ = billsec_;
		this.disposition_ = disposition_;
		this.amaflags_ = amaflags_;
		this.recordtimelen_ = recordtimelen_;
		this.ccsrc_ = ccsrc_;
		this.clid_ = clid_;
		this.src_ = src_;
		this.dst_ = dst_;
		this.calleenum_ = calleenum_;
		this.dcontext_ = dcontext_;
		this.channel_ = channel_;
		this.dstchannel_ = dstchannel_;
		this.lastapp_ = lastapp_;
		this.lastdata_ = lastdata_;
		this.start_ = start_;
		this.answer_ = answer_;
		this.end_ = end_;
		this.calltype_ = calltype_;
		this.accountcode_ = accountcode_;
		this.uniqueid_ = uniqueid_;
		this.rcfile_ = rcfile_;
		this.recordingfilename_ = recordingfilename_;
	}

	public int getAcctId_() {
		return acctId_; 
	}

	public void setAcctId_(int acctId_) {
		this.acctId_ = acctId_;
	}

	public int getDuration_() {
		return duration_;
	}

	public void setDuration_(int duration_) {
		this.duration_ = duration_;
	}

	public int getBillsec_() {
		return billsec_;
	}

	public void setBillsec_(int billsec_) {
		this.billsec_ = billsec_;
	}

	public int getDisposition_() {
		return disposition_;
	}

	public void setDisposition_(int disposition_) {
		this.disposition_ = disposition_;
	}

	public int getAmaflags_() {
		return amaflags_;
	}

	public void setAmaflags_(int amaflags_) {
		this.amaflags_ = amaflags_;
	}

	public int getRecordtimelen_() {
		return recordtimelen_;
	}

	public void setRecordtimelen_(int recordtimelen_) {
		this.recordtimelen_ = recordtimelen_;
	}

	public String getCcsrc_() {
		return ccsrc_;
	}

	public void setCcsrc_(String ccsrc_) {
		this.ccsrc_ = ccsrc_;
	}

	public String getClid_() {
		return clid_;
	}

	public void setClid_(String clid_) {
		this.clid_ = clid_;
	}

	public String getSrc_() {
		return src_;
	}

	public void setSrc_(String src_) {
		this.src_ = src_;
	}

	public String getDst_() {
		return dst_;
	}

	public void setDst_(String dst_) {
		this.dst_ = dst_;
	}

	public String getCalleenum_() {
		return calleenum_;
	}

	public void setCalleenum_(String calleenum_) {
		this.calleenum_ = calleenum_;
	}

	public String getDcontext_() {
		return dcontext_;
	}

	public void setDcontext_(String dcontext_) {
		this.dcontext_ = dcontext_;
	}

	public String getChannel_() {
		return channel_;
	}

	public void setChannel_(String channel_) {
		this.channel_ = channel_;
	}

	public String getDstchannel_() {
		return dstchannel_;
	}

	public void setDstchannel_(String dstchannel_) {
		this.dstchannel_ = dstchannel_;
	}

	public String getLastapp_() {
		return lastapp_;
	}

	public void setLastapp_(String lastapp_) {
		this.lastapp_ = lastapp_;
	}

	public String getLastdata_() {
		return lastdata_;
	}

	public void setLastdata_(String lastdata_) {
		this.lastdata_ = lastdata_;
	}

	public String getStart_() {
		return start_;
	}

	public void setStart_(String start_) {
		this.start_ = start_;
	}

	public String getAnswer_() {
		return answer_;
	}

	public void setAnswer_(String answer_) {
		this.answer_ = answer_;
	}

	public String getEnd_() {
		return end_;
	}

	public void setEnd_(String end_) {
		this.end_ = end_;
	}

	public String getCalltype_() {
		return calltype_;
	}

	public void setCalltype_(String calltype_) {
		this.calltype_ = calltype_;
	}

	public String getAccountcode_() {
		return accountcode_;
	}

	public void setAccountcode_(String accountcode_) {
		this.accountcode_ = accountcode_;
	}

	public String getUniqueid_() {
		return uniqueid_;
	}

	public void setUniqueid_(String uniqueid_) {
		this.uniqueid_ = uniqueid_;
	}

	public String getRcfile_() {
		return rcfile_;
	}

	public void setRcfile_(String rcfile_) {
		this.rcfile_ = rcfile_;
	}

	public String getRecordingfilename_() {
		return recordingfilename_;
	}

	public void setRecordingfilename_(String recordingfilename_) {
		this.recordingfilename_ = recordingfilename_;
	}
	
}
