package home;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Vikum Samaranayake
 * @since 04/06/2018
 * 
 *
 */
public class ManageConfig {

	private ConfigData configData;


	private static String hostName, dbName, uName, pWord;

	private Document doc; 
	

	public ManageConfig() {
		try {
			File fxmlFile = new File("TransferConfig.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fxmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("PBX_FTP_Info");
			NodeList nListNas = doc.getElementsByTagName("NAS_Info");

			Node nNode = nList.item(0);
			Node nNodeNas = nListNas.item(0);

			if (nNode.getNodeType() == Node.ELEMENT_NODE && nNodeNas.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				Element eElementNas = (Element) nNodeNas;
				configData = new ConfigData();
				configData.setHostPbxIp(eElement.getElementsByTagName("HostPBX_IP").item(0).getTextContent());
				configData.setHostPbxPort(eElement.getElementsByTagName("HostPBX_Port").item(0).getTextContent());
				configData.setHostPbxUname(eElement.getElementsByTagName("UserNamePBX_SSH").item(0).getTextContent());
				configData.setHostPbxPword(eElement.getElementsByTagName("PasswordPBX_SSH").item(0).getTextContent());
				configData.setHostPbxAudioDirectory(
						eElement.getElementsByTagName("PBX_AudioFiles_Directory").item(0).getTextContent());
				configData.setNasBackupDirectory(eElementNas.getElementsByTagName("NAS_AudioFiles_Backup_Directory_Path")
						.item(0).getTextContent());
				configData.setNasAudioOriginalDirectory(eElementNas.getElementsByTagName("NAS_AudioFiles_Directory_Path")
						.item(0).getTextContent());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ConfigData getConfigData() {
		return this.configData;
	}

}
