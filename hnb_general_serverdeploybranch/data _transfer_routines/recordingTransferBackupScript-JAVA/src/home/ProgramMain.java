package home;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import missing_identifier.MissingIdentifier;

/**
 * @author Vikum Samaranayake
 * @since 04/06/2018
 *
 */
public class ProgramMain {

	/**
	 *
	 */
	public static final long DEFAULT_SLEEP_TIME = 5;

	public ProgramMain() {
		super();
	}

	/**
	 * This program moves call audio files from pbx to configured location in the
	 * network
	 */
	public static void main(String[] args) {
		 MissingIdentifier miss= new MissingIdentifier();
		 miss.test();
//		Set<String> h = new HashSet<>(Arrays.asList("f", "c", "b", "a"));
//		Set<String> i = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e", "f"));
//		i.removeAll(h);
//		Iterator iterate = i.iterator();
//		while(iterate.hasNext()) {
//			System.out.println(iterate.next());
//		}

		// ScheduledExecutorService s = Executors.newScheduledThreadPool(1);
		// s.scheduleWithFixedDelay(new Runnable() {
		// public void run() {
		// try {
		// ProgramMain.moveFiles();
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
		// }
		// }, 0, ProgramMain.DEFAULT_SLEEP_TIME, TimeUnit.MINUTES);

	}

	public static void moveFiles() {
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		ManageConfig configObj = new ManageConfig();
		ConfigData configData = configObj.getConfigData();

		if (configData != null) {
			String SFTPHOST = configData.getHostPbxIp();
			int SFTPPORT = Integer.parseInt(configData.getHostPbxPort());
			String SFTPUSER = configData.getHostPbxUname();
			String SFTPPASS = configData.getHostPbxPword();
			String SFTPWORKINGDIR = configData.getHostPbxAudioDirectory();

			try {
				URI uri = new URI(SFTPWORKINGDIR);
				JSch jsch = new JSch();
				session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
				session.setPassword(SFTPPASS);
				Properties config = new Properties();
				config.put("StrictHostKeyChecking", "no");
				session.setConfig(config);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect();
				channelSftp = (ChannelSftp) channel;
				channelSftp.cd(SFTPWORKINGDIR);
				Vector<LsEntry> folderlist = channelSftp.ls(uri.getPath());
				for (int i = 0; i < folderlist.size(); i++) {
					if (folderlist.get(i).getFilename().length() < 4) {
						continue;
					} else {
						String ext_name = folderlist.get(i).getFilename();
						System.out.println(ext_name);

						URI extensionUri = new URI(SFTPWORKINGDIR + "/" + ext_name);
						channelSftp.cd(SFTPWORKINGDIR + "/" + ext_name);
						Vector<LsEntry> filelist = channelSftp.ls(extensionUri.getPath());
						for (int x = 0; x < filelist.size(); x++) {
							if (filelist.get(x).getFilename() != null && filelist.get(x).getFilename().length() > 2) {
								String file_name = filelist.get(x).getFilename();
								System.out.println("Working on: " + file_name);

								byte[] buffer = new byte[1024];
								BufferedInputStream bis = null;
								BufferedOutputStream bos = null;
								try {
									bis = new BufferedInputStream(channelSftp.get(filelist.get(x).getFilename()));
									File newFile = new File(configData.getNasBackupDirectory() + ext_name + "/"
											+ filelist.get(x).getFilename());
									newFile.getParentFile().mkdirs();
									OutputStream os = new FileOutputStream(newFile);
									bos = new BufferedOutputStream(os);
									int readCount;
									if (newFile.exists()) {
										while ((readCount = bis.read(buffer)) > 0) {
											bos.write(buffer, 0, readCount);
										}
										System.out.println("Done file: " + filelist.get(x).getFilename());

									}

								} catch (SftpException e) {
									System.out.println("File skipped due to not found issue");
									if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
										continue;
									}

								} finally {
									if (bis != null && bos != null) {
										bis.close();
										bos.close();
									}
								}

							} else {
								continue;
							}
						}
					}

				}
				// System.out.println("Done" );
				System.out.println("Execution ended at: "
						+ new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime()));
				// System.exit(0);

			} catch (Exception ex) {
				ex.printStackTrace();

			} finally {
				channelSftp.exit();

			}
		}
	}

}