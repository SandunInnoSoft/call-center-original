/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callcentercdrflagger;

import callcenteraudiotransferroutine.TransferConfigurations;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author Sandun
 * @since 2019-01-10
 */
public class AudioFileSearcher {
    
    private static boolean isSearching = false;
    public static int count = 0;
    private static TransferConfigurations config = new TransferConfigurations();
   
    public static String searchFiles(String callStartedDate, String callStartedTime, String callType, String extensionNumber, String customerNumber, String callEndedDate, String callEndedTime, int acctId){

        System.out.println("searchFiles called");
        String nasAudioFileDirectoryPath = config.getNas_Audiofiles_directory();
        
        String dateForPath = callStartedDate.replace("-", "_");
        nasAudioFileDirectoryPath = nasAudioFileDirectoryPath.replace("DATE", dateForPath);
        nasAudioFileDirectoryPath = nasAudioFileDirectoryPath.replace("EXTENSION", extensionNumber);
        
        File nasAudioFileDirectory = new File(nasAudioFileDirectoryPath);

        if(nasAudioFileDirectory.exists() == true){
            // directory exists
            String sourceNumber;
            String destinationNumber;
            
            if(callType.matches("incoming")){
                // call type is incoming
                sourceNumber = customerNumber;
                destinationNumber = extensionNumber;
            }else{
                // call type is outgoing
                sourceNumber = extensionNumber;
                destinationNumber = customerNumber;
            }
            
            ArrayList<String> matchingFileNamesArray = new ArrayList<String>();
            File[] listOfFiles = nasAudioFileDirectory.listFiles();
            String fileName;
            
            for(int x = 0; x < listOfFiles.length ; x++){
                fileName = listOfFiles[x].getName();
                
                String[] fileNameSplitted = fileName.split("-");
                
                if(fileNameSplitted.length > 4 && fileNameSplitted[2].matches(sourceNumber) == true && fileNameSplitted[3].matches(destinationNumber) == true){
                    // filename matches to the pattern                    
                    matchingFileNamesArray.add(fileName);
                }
            }
            
            if(matchingFileNamesArray.size() > 0) {
            	// has more than 1 matches
            	
            	System.out.println("multiple matches in searchFiles - size = "+matchingFileNamesArray.size());
            	String matchingFileNameResponse = null;
            	
            	
            	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            	LocalDateTime callStartedDateTime = LocalDateTime.parse(callStartedDate+" "+callStartedTime, formatter);
            	LocalDateTime callEndedDateTime = LocalDateTime.parse(callEndedDate+" "+callEndedTime, formatter);
            	
            	callStartedDateTime.minusSeconds(5);
            	
            	for(int x = 0; x < matchingFileNamesArray.size(); x++) {
            		matchingFileNameResponse = isWithinTimePeriod(matchingFileNamesArray.get(x), callStartedDateTime, callEndedDateTime);
            		if(matchingFileNameResponse != null) {
            			// has a matching file
            			break;
            		}
            	}
            	
            	return matchingFileNameResponse;
            	
            }else {
            	// No matches
            	System.out.println("no matches in searchFiles - size 0");
            	return null;
            }
            
        }else{
            // directory doesnot exist
            System.out.println("DIRECTORY PATH DOESNT EXIST - "+nasAudioFileDirectoryPath);
            
            return null;
        }
        
    }
    
    
    /**
     * <b>Checks if the audio file name matches to the start and end times of the associated cdr record</b>
     * 
     * @param audioFileName
     * @param callStartedTime
     * @param callEndedDateTime
     * @return String matching audio file path / null
     */
    private static String isWithinTimePeriod(String audioFileName, LocalDateTime callStartedTime, LocalDateTime callEndedDateTime){
            
            String[] audioFileNameSplitted = audioFileName.split("-");
            String dateArea = audioFileNameSplitted[0];
            String timeArea = audioFileNameSplitted[1];

            int year = Integer.parseInt(dateArea.substring(0, 4));
            int month = Integer.parseInt(dateArea.substring(4, 6));
            int date = Integer.parseInt(dateArea.substring(6, 8));
            int hour = Integer.parseInt(timeArea.substring(0, 2));
            int minute = Integer.parseInt(timeArea.substring(2, 4));
            int seconds = Integer.parseInt(timeArea.substring(4, 6));
            
            try{
                LocalDateTime audioFileDateTime = LocalDateTime.of(year, month, date, hour, minute, seconds);

                if((audioFileDateTime.isAfter(callStartedTime) || audioFileDateTime.equals(callStartedTime)) && (audioFileDateTime.isBefore(callEndedDateTime) || audioFileDateTime.equals(callEndedDateTime))){
                    // is within the time period      
                	System.out.println("File found in isWithinTimePeriod - "+audioFileName);
                	return audioFileName;
                }else{
                    // not within the time period
                	System.out.println("File not found in isWithinTimePeriod - ");
                	return null;
                }


            }catch(Exception e){
                System.out.println("Exception thrown at within time period check");
                System.out.println(e.toString());
                e.printStackTrace();
                return null;
            }
    }
}
