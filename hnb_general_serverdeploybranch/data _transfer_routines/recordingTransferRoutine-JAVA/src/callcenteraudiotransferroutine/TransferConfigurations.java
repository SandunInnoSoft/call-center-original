
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * @author Sandun
 * @since 2019-01-10
 */
package callcenteraudiotransferroutine;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;

/**
 * <b>This class can be used to get transfer configuration information defined in TransferConfig.xml</b>
 * 
 * @author Sandun
 * @since 2019-01-10
 */
public class TransferConfigurations {

    private String hostPbx_FTP_IP;
    private Integer hostPbx_FTP_Port;
    private String hostPbx_FTP_Username;
    private String hostPbx_FTP_Password;
    private String hostPbx_AudioFiles_Directory;
    private String nas_Audiofiles_directory;
    private Integer routineSleepHours;
    private Integer routineSleepMinutes;
    private String replica_mysql_CDR_DB_IP;
    private Integer replica_mysql_CDR_DB_Port;
    private String replica_mysql_CDR_DB_Name;
    private String replica_mysql_CDR_DB_Username;
    private String replica_mysql_CDR_DB_Password;
    private String log_mysql_CDR_DB_IP;
    private String log_mysql_CDR_DB_Port;
    private String log_mysql_CDR_DB_Name;
    private String log_mysql_CDR_DB_Username;
    private String log_mysql_CDR_DB_Password;
    private String mainDbIpAddress;
    private Integer mainDbPort;
    private String mainDbName;
    private String mainDbUser;
    private String mainDbPassword;
    
    /**
     * <b>Constructor of TransferConfigurations</b>
     * <p>Fetches and assigns the information defined in TransferConfig.xml file as the object states</p>
     * 
     * @author Sandun
     * @since 2019-01-10
     */
    public TransferConfigurations(){
        try{
            File configFile = new File("TransferConfig.xml");
        
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(configFile);
            this.hostPbx_FTP_IP = document.getElementsByTagName("HostPBX_IP").item(0).getTextContent();
            this.hostPbx_FTP_Port = Integer.parseInt(document.getElementsByTagName("HostPBX_Port").item(0).getTextContent());
            this.hostPbx_FTP_Username = document.getElementsByTagName("UserNamePBX_SSH").item(0).getTextContent();
            this.hostPbx_FTP_Password = document.getElementsByTagName("PasswordPBX_SSH").item(0).getTextContent();
            this.hostPbx_AudioFiles_Directory = document.getElementsByTagName("PBX_AudioFiles_Directory").item(0).getTextContent();
            
            this.mainDbIpAddress = document.getElementsByTagName("Mysql_Main_IP").item(0).getTextContent();
            this.mainDbPort = Integer.parseInt(document.getElementsByTagName("Mysql_Main_Port").item(0).getTextContent());
            this.mainDbName = document.getElementsByTagName("Main_DB_Name").item(0).getTextContent();
            this.mainDbUser = document.getElementsByTagName("Main_DB_Username").item(0).getTextContent();
            this.mainDbPassword = document.getElementsByTagName("Main_DB_Password").item(0).getTextContent();
            
            this.nas_Audiofiles_directory = document.getElementsByTagName("NAS_AudioFiles_Directory_Path").item(0).getTextContent();
            this.routineSleepHours = Integer.parseInt(document.getElementsByTagName("EachHours").item(0).getTextContent());
            this.routineSleepMinutes = Integer.parseInt(document.getElementsByTagName("EachMinutes").item(0).getTextContent());
            
            this.replica_mysql_CDR_DB_IP = document.getElementsByTagName("Mysql_IP").item(0).getTextContent();
            this.replica_mysql_CDR_DB_Port = Integer.parseInt(document.getElementsByTagName("Mysql_Port").item(0).getTextContent());
            this.replica_mysql_CDR_DB_Name = document.getElementsByTagName("DB_Name").item(0).getTextContent();
            this.replica_mysql_CDR_DB_Username = document.getElementsByTagName("DB_Username").item(0).getTextContent();
            this.replica_mysql_CDR_DB_Password = document.getElementsByTagName("DB_Password").item(0).getTextContent();

            this.log_mysql_CDR_DB_IP = document.getElementsByTagName("MysqlLogDBIP").item(0).getTextContent();
            this.log_mysql_CDR_DB_Port = document.getElementsByTagName("MysqlLogDBPort").item(0).getTextContent();
            this.log_mysql_CDR_DB_Name = document.getElementsByTagName("LogDBName").item(0).getTextContent();
            this.log_mysql_CDR_DB_Username = document.getElementsByTagName("LogDBUserName").item(0).getTextContent();
            this.log_mysql_CDR_DB_Password = document.getElementsByTagName("LogDBPassword").item(0).getTextContent();
            
            configFile = null;
            documentBuilderFactory = null;
            documentBuilder = null;
            document = null;
            
        }catch(Exception e){
            System.out.println("4001 - Exception thrown at transfer config contruction");
            System.out.println(e.toString());
        }
    }
    
    /**
	 * @return the mainDbIpAddress
	 */
	public String getMainDbIpAddress() {
		return mainDbIpAddress;
	}

	/**
	 * @return the mainDbPort
	 */
	public Integer getMainDbPort() {
		return mainDbPort;
	}

	/**
	 * @return the mainDbName
	 */
	public String getMainDbName() {
		return mainDbName;
	}

	/**
	 * @return the mainDbUser
	 */
	public String getMainDbUser() {
		return mainDbUser;
	}

	/**
	 * @return the mainDbPassword
	 */
	public String getMainDbPassword() {
		return mainDbPassword;
	}

	/**
	 * @param mainDbIpAddress the mainDbIpAddress to set
	 */
	public void setMainDbIpAddress(String mainDbIpAddress) {
		this.mainDbIpAddress = mainDbIpAddress;
	}

	/**
	 * @param mainDbPort the mainDbPort to set
	 */
	public void setMainDbPort(Integer mainDbPort) {
		this.mainDbPort = mainDbPort;
	}

	/**
	 * @param mainDbName the mainDbName to set
	 */
	public void setMainDbName(String mainDbName) {
		this.mainDbName = mainDbName;
	}

	/**
	 * @param mainDbUser the mainDbUser to set
	 */
	public void setMainDbUser(String mainDbUser) {
		this.mainDbUser = mainDbUser;
	}

	/**
	 * @param mainDbPassword the mainDbPassword to set
	 */
	public void setMainDbPassword(String mainDbPassword) {
		this.mainDbPassword = mainDbPassword;
	}

	public String getHostPbx_FTP_IP() {
        return hostPbx_FTP_IP;
    }

    public Integer getHostPbx_FTP_Port() {
        return hostPbx_FTP_Port;
    }

    public String getHostPbx_FTP_Username() {
        return hostPbx_FTP_Username;
    }

    public String getHostPbx_FTP_Password() {
        return hostPbx_FTP_Password;
    }

    public String getHostPbx_AudioFiles_Directory() {
        return hostPbx_AudioFiles_Directory;
    }

    public String getNas_Audiofiles_directory() {
        return nas_Audiofiles_directory;
    }

    public Integer getRoutineSleepHours() {
        return routineSleepHours;
    }

    public Integer getRoutineSleepMinutes() {
        return routineSleepMinutes;
    }

    public String getReplica_mysql_CDR_DB_IP() {
        return replica_mysql_CDR_DB_IP;
    }

    public Integer getReplica_mysql_CDR_DB_Port() {
        return replica_mysql_CDR_DB_Port;
    }

    public String getReplica_mysql_CDR_DB_Name() {
        return replica_mysql_CDR_DB_Name;
    }

    public String getReplica_mysql_CDR_DB_Username() {
        return replica_mysql_CDR_DB_Username;
    }

    public String getReplica_mysql_CDR_DB_Password() {
        return replica_mysql_CDR_DB_Password;
    }
    
    public String getLog_mysql_CDR_DB_IP() {
        return log_mysql_CDR_DB_IP;
    }

    public String getLog_mysql_CDR_DB_Port() {
        return log_mysql_CDR_DB_Port;
    }

    public String getLog_mysql_CDR_DB_Name() {
        return log_mysql_CDR_DB_Name;
    }

    public String getLog_mysql_CDR_DB_Username() {
        return log_mysql_CDR_DB_Username;
    }

    public String getLog_mysql_CDR_DB_Password() {
        return log_mysql_CDR_DB_Password;
    }
    
    
}
