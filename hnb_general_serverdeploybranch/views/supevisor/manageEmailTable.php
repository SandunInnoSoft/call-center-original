<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
?>
<script type="text/javascript">
    function Pager(tableName, itemsPerPage) {
        this.tableName = tableName;
        this.itemsPerPage = itemsPerPage;
        this.currentPage = 1;
        this.pages = 0;
        this.inited = false;

        this.showRecords = function (from, to) {
            var rows = document.getElementById(tableName).rows;
            // i starts from 1 to skip table header row
            for (var i = 1; i < rows.length; i++) {
                if (i < from || i > to)
                    rows[i].style.display = 'none';
                else
                    rows[i].style.display = '';
            }
        }

        this.showPage = function (pageNumber) {
            if (!this.inited) {
//                alert("not inited");
                return;
            }

            var oldPageAnchor = document.getElementById('pg' + this.currentPage);
            oldPageAnchor.className = 'pg-normal';

            this.currentPage = pageNumber;
            var newPageAnchor = document.getElementById('pg' + this.currentPage);
            newPageAnchor.className = 'pg-selected';

            var from = (pageNumber - 1) * itemsPerPage + 1;
            var to = from + itemsPerPage - 1;
            this.showRecords(from, to);
        }

        this.prev = function () {
            if (this.currentPage > 1)
                this.showPage(this.currentPage - 1);
        }

        this.next = function () {
            if (this.currentPage < this.pages) {
                this.showPage(this.currentPage + 1);
            }
        }

        this.init = function () {
            var rows = document.getElementById(tableName).rows;
            var records = (rows.length - 1);
            this.pages = Math.ceil(records / itemsPerPage);
            this.inited = true;
        }

        this.showPageNav = function (pagerName, positionId) {
            if (!this.inited) {
//                alert("not inited");
                return;
            }
            var element = document.getElementById(positionId);

            var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
            for (var page = 1; page <= this.pages; page++)
                pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
            pagerHtml += '<span onclick="' + pagerName + '.next();" class="pg-normal"> Next &#187;</span>';

            element.innerHTML = pagerHtml;
        }
    }




</script>

<style>
    .pg-normal{
        cursor: pointer !important;
    }

</style>

<div class="container-fluid">
    <div class="col-md-12" id="adminUserManageTable">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered" id="userManageTable">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>

                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Last Updated
                            </th>
                            <th>
                                Status
                            </th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for ($i = 0; $i < count($email_data); $i++) {
                            $color = 'active';
                            $role = '';
                            $status = '';
                            $emailId = $email_data[$i]['id'];


                            if ($email_data[$i]['status'] == 'active') {
                                $status = 'Active';
                            } else if ($email_data[$i]['status'] == 'deleted') {
                                $status = 'Deleted';
                            }
                            ?>
                            <tr id="<?= $email_data[$i]['id'] ?>" class="<?= $color ?>">
                                <td>
                                    <?= $i + 1 ?>
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-success" onclick="deleteEmail('<?= $email_data[$i]['id'] ?>', this)">Delete</a>
                                    <!--<input type="checkbox" id="<? = $email_data[$i]['id'] ?>" value="<? = $email_data[$i]['id'] ?>">-->
                                </td>
                                <td>
                                    <?= $email_data[$i]['subsciber_name']; ?>
                                </td>
                                <td>
                                    <?= $email_data[$i]['email_address']; ?>
                                </td>
                                <td>
                                    <?= $email_data[$i]['created_date']; ?>
                                </td>
                                <td>
                                    <?= $status ?>
                                </td>
                                <td>
                                    <?php
                                    $urlSet = Url::to(['supevisor/insertemail']);
                                    $urlSet = $urlSet . '&emailId=' . $emailId;
                                    ?>
                                    <!--<a href="<?//= Url::to(['supevisor/insert&user='.$user.'']) ?>" class="btn btn-sm btn-info">Edit</a>-->
                                    <a href="<?= $urlSet ?>" class="btn btn-sm btn-info">Edit</a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?> 
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" id="pagerDiv">
            
            <script type="text/javascript">

                var pager = new Pager('userManageTable', 10);
                pager.init();
                pager.showPageNav('pager', 'pagerDiv');
                pager.showPage(1);

                function deleteEmail(eid, deleteAnchorObj) {
                    $(deleteAnchorObj).addClass("disabled");
                    $(deleteAnchorObj).html("Deleting");
                    $.ajax({
                        url: "<?= Url::to(['supevisor/deleteemail']) ?>",
                        type: 'GET',
                        data: {eid: eid},
                        success: function (data, textStatus, jqXHR) {
                            if (data == 1) {
                                $("#" + eid).remove();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                        }
                    });
                }

            </script>
        </div>
    </div>
</div>