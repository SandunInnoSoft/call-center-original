<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .btn-default{
                background-color: #10297d !important;
                color: #faa61a !important;
            }
            .text-success{
                color: #10297d !important;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" style="margin-top: 1%;">
                <div class="col-md-12">
                    <h3 class="text-left text-success"><strong>
                            Manage Emails
                        </strong>
                    </h3>
                </div>
            </div>
            <div class="row">
                <?php
                if(Yii::$app->session->get("user_role") == 1){  ?>    
                    <div class="col-md-2">
                        <a name="btnAddNewUser" id="btnAddNewUser" class="btn btn-default btn-lg" href="<?= Url::to(['supevisor/insertemail']) ?>">Add New</a>
                    </div>
                <?php } ?>
            </div>
            <br>
            <br>

        </div>
        <div class="row" style="margin-top: 3%;" id="emailTableFrame">
            Loading Email list...
        </div>

        <script>
            $(document).ready(function () {
                $('#emailTableFrame').load("<?php echo Url::to(['supevisor/manageemailtable']) ?>");
            });
        </script>
    </body>
</html>
