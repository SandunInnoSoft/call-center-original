<?php

use yii\helpers\Url;
use yii\helpers\Html;
$ftpParams = include __DIR__ . '/../../config/pbx_audio_ftp_config.php';
?>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>-->
<script src="js/jquery-3.2.1.js"></script>
<script type="text/javascript" src="js/chartjs.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<style>
    thead{
        font-weight: bold;
    }

    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }
</style>

<style>
    .answered-calls-history-table {
        width: 100%;
    }
    .outgoing-calls-history-table {
        width: 100%;
    }

    .abandoned-calls-history-table {
        width: 100%;
    }

    .call-history-thead, .call-history-tbody,.call-history-tbody tr,.call-history-tbody td,.call-history-thead th,.call-history-thead tr,.call-history-thead td {
        display: block;
    }

    .call-history-thead th {
        height: 30px;
    }

    .call-history-tbody {
        overflow-y: auto;
        height: 300px;
        font-size: 90%;
    }

    .call-history-tbody td, .call-history-thead th {
        float: left;
        width: 20%;
    }

    .abandoned-call-history-tbody td, .abandoned-call-history-thead th {
        float: left;
        width: 33.33%;
    }

    .call-history-thead tr:after, call-history-tbody tr:after {
        clear: both;
        content: ' ';
        display: block;
        visibility: hidden;
    }
</style>

<style>
    .panel-heading{
        background-color: #10297d !important;
        color: white !important;
    }

    .channelLabel{
        margin-right: 10px;
    }
</style>



<div  style="font-family: 'Lato' , sans-serif;">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="text-left text-success"><strong>
                            Search Call List of Missing Audio Files
                        </strong>
                    </h4>
                </div>
            </div><br> 
    <div class="col-xs-2" >
        <?php 
            if(Yii::$app->session->get("user_role") != "1" && Yii::$app->session->get("user_role") != "5"){
                // user is a supervisor
        ?>
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Agents</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 650px;overflow-y: scroll">
                    <ul id="agentListGroup" class="list-group" style="">
                        <a id="0" style="cursor: pointer" onclick="loadQueueDataForSupervisor(this)" class="list-group-item">All records</a>
                        <a id="<?= Yii::$app->session->get('voip') ?>" style="cursor: pointer" onclick="callAgentReports(<?= Yii::$app->session->get('user_id') ?>,<?= Yii::$app->session->get('voip') ?>, '<?= Yii::$app->session->get('full_name') ?>', this.id)" class="list-group-item"><?= Yii::$app->session->get('full_name')." : ".Yii::$app->session->get('voip') ?></a>
                        <?php for ($index = 0; $index < count($agents); $index++) { ?>
                            <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['fullname'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['fullname']." : ".$agents[$index]['voip_extension'] ?></a>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php
        }else{
            // user is an admin
        ?>
        
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Agents</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 325px;overflow-y: scroll">
                    <ul id="agentListGroup" class="list-group" style="">
                        <a id="0" style="cursor: pointer" onclick="callAgentReports(0, 0, 0, 0)" class="list-group-item">All Agents</a>
                        <?php for ($index = 0; $index < count($agents); $index++) { ?>
                            <a id="<?= $agents[$index]['voip_extension'] ?>" style="cursor: pointer" onclick="callAgentReports(<?= $agents[$index]['id'] ?>,<?= $agents[$index]['voip_extension'] ?>, '<?= $agents[$index]['fullname'] ?>', this.id)" class="list-group-item"><?= $agents[$index]['fullname']." : ".$agents[$index]['voip_extension'] ?></a>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row" style="">
            <div class="panel panel-default" style="border: 1px solid #10297d; width: 90%">
                <div class="panel-heading"  style="height: 40px">
                    <h4 class="panel-title">
                        <a>Extension Queues</a>
                    </h4>
                </div>
                <div id="collapse1" class="" style="height: 325px;overflow-y: scroll">
                    <ul id="queueListGroup" class="list-group" style="">
                        <?php for ($index = 0; $index < count($queues); $index++) { ?>
                        <a id="<?= $queues[$index]['id'] ?>" style="cursor: pointer" onclick="callQueueReports(this.innerHTML, this.id)" class="list-group-item"><?= $queues[$index]['name'] ?></a>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        
        <?php  
        }
        ?>
    </div>
    <div class="col-xs-10" style="padding: 0%">       
        <div class="row">
            <div class="col-xs-3">
                <label for="fromdate">Date from*</label>
                <!--<input class="form-control activateFields" type="date" id="fromdate">-->       
                <div class="input-group date" id="fromdate">
                    <input type="text" class="form-control" id="dateFromValue"/>    <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <label for="fromtime">Time from*</label>
                <div class="form-group" id="fromtime">
                <select id="timeFromSelect" class="form-control">
                    <?php
                    for ($i=0; $i < 24; $i++) { 
                        if($i < 10){
                            ?>
                            <option value="<?= '0'.$i.':00:00' ?>"><?= '0'.$i.':00' ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?= $i.':00:00' ?>"><?= $i.':00' ?></option>
                            <?php
                        }
                    }
                    ?>
                    <option value="23:59:59">End of the day</option>
                </select>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="todate">Date to*</label>                
                <!--<input class="form-control activateFields" type="date" id="todate">-->
                <div class="input-group date" id="todate">
                    <input type="text" class="form-control" id="dateToValue"/>  <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                </div>
                <label for="fromtime">Time to*</label>
                <div class="form-group" id="totime">
                <select id="timeToSelect" class="form-control">
                    <?php
                    for ($i=0; $i < 24; $i++) { 
                        if($i < 10){
                            ?>
                            <option value="<?= '0'.$i.':00:00' ?>"><?= '0'.$i.':00' ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?= $i.':00:00' ?>"><?= $i.':00' ?></option>
                            <?php
                        }
                    }
                    ?>
                    <option value="23:59:59" selected>End of the day</option>
                </select>
                </div>
            </div>
            <div class="col-xs-3">
                <label for="contactNum">Contact</label>
                <input class="form-control activateFields" type="tel" id="contactNum">
            </div>
            <div class="col-xs-3" style="padding-top: 23px">
                <div class="row" style="padding-top: 1%">
                    <div class="col-xs-12">
                       <a class="disabled btn btn-block btn-primary" id="btnView" onclick="filterRecordsTablle()">Search</a> 
                    </div>
                </div>
            </div>
            <input type="hidden" id="hiddenAgentId" />
            <input type="hidden" id="hiddenAgentExt" />
            <input type="hidden" id="hiddenAgentName" />
            <input type="hidden" id="hiddenQueueId" />
        </div>
        <div class="row">
            <label for="channelsListDiv" style="padding-left: 2%">Incoming Channels</label>
            <div class="col-md-12">
                <div class="well" id="channelsListDiv">
                    <?php 
                        if($channelsInfo != NULL){
                            foreach($channelsInfo as $key){
                                ?>
                                    <label data-toggle="tooltip" data-placement="left" title="<?=$key->channel_description?>"><input type="checkbox" value="<?= $key->id ?>" class="channelCheckBoxesClass" id="channel<?=$key->channel_number?>">&nbsp<?= $key->channel_name ?> &nbsp</label>
                                <?php
                            }
                        }else{
                            ?>
                            <label>No channels available</label>
                            <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <span id="searchTimeSpan"></span>
        <br>
        <img class="img-responsive" src="images/loading.gif" id="loadingGifImg" style="display: none">
        <div class="row" id="agentReportsDiv" style="padding-left: 1%"></div>

        <div style="display: none;padding-left: 2%;padding-right: 2%;border: 1px solid #cdcdcd" id="resulttable">
            <div class="row" style="padding-top: 0%">
                    <div id="agentinfo" style="background-color: #10297d;color: white;padding-top: 0%;text-align: center;font-size: 130%;" class="col-xs-2"></div>
                <div class="col-xs-8"></div>
                <div class="col-xs-2" style="text-align: right;background-color: #cdcdcd;font-size: 130%"><?= date('Y-m-d'); ?></div>
            </div>
            <br>

            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Answered Calls</a>
                </div>
            </div>
            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <br>
                <div style="margin: 10px">
                    <table id="answeredDataTable" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Caller Number</th>
                                    <th>Start Time</th>
                                    <th>Answered Time</th>
                                    <th>Agent</th>
                                    <th>End Time</th>
                                    <th>Duration</th>
                                </tr>
                            </thead>
                    </table>            
                </div>
                <br>
            </div>
            <br>

            <div class="row navbar navbar-default" style="font-family: 'Lato' , sans-serif;font-weight: bold">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Outgoing Calls </a>
                </div>                    
            </div>

            <div class="row navbar-default" style="border: 1px solid #cdcdcd">
                <br>
                <div style="margin: 10px">
                 <table id="outgoingDataTable" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Receiver</th>
                            <th>Agent</th>
                            <th>Start Time</th>
                            <th>Answered Time</th>
                            <th>End Time</th>
                            <th>Duration</th>
                        </tr>
                    </thead>
                 </table>
                </div>
                <br>
            </div>
            <br> 

        </div>


    </div>
</div>

<script>
    $('#fromdate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#todate').datetimepicker({
//        format: 'DD/MM/YYYY'
        format: 'YYYY-MM-DD'
    });
    function callAgentReports(agent_id, agent_ex, agent_name, id) {
        //active view button
        <?php 
            if(Yii::$app->session->get("user_role") == "1"){
                // user is admin
        ?>
        $("#channelsListDiv").empty();                            
        $("#channelsListDiv").append("<label>No channels available</label>");
        <?php 
            }
        ?>
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(agent_ex);
        $('#hiddenAgentName').val(agent_name);
        $('#hiddenAgentId').val(agent_id);
        $('#hiddenQueueId').val("");            
        var url = "<?= Url::to(['reports/performanceoverviewbyagent']) ?>";

        //Start : set agents panel css
        {
            var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
            
            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");
        }
    }
    function filterRecords() {
        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var url = "<?= Url::to(['reports/performanceoverviewbydate']) ?>";
        }else{
           var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    fromdate: fromDate, 
                    todate: toDate, 
                    agent_id: agentId, 
                    agentex: agentEx, 
                    agentname: agentName, 
                    contactNum: contactNumber, 
                    queue_id : queueId, 
                    fromTime : fromTime, 
                    toTime : toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
                    console.log('filter records : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
//                    alert(xhr.responseText);
//                    alert(xhr.status);
//                    alert(thrownError);
//                    alert(xhr.responseText);

                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }


    }
    
    /**
     * 
     * @returns {undefined}
     * 
     * @since 2017-10-30
     * @author Sandun
     * 
     */
    function filterRecordsSummery(){
        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        
        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
            var url = "<?= Url::to(['reports/summery']) ?>";
        }else{
            var url = "<?= Url::to(['reports/summerybyqueue']) ?>";
        }

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

//        alert(fromDate + " +++ " + toDate);
        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();
        
        if (fromDateConverted < toDateConverted) {
            showLoadingScreen();
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    fromdate: fromDate, 
                    todate: toDate, 
                    agent_id: agentId, 
                    agentex: agentEx, 
                    agentname: agentName, 
                    contactNum: contactNumber, 
                    queue_id : queueId, 
                    fromTime : fromTime, 
                    toTime : toTime,
                    channels: selectedChannels
                },
                success: function (data)
                {
//                    alert(data);
                    hideLoadingScreen();
                    var result = $(data).find('#agentReportsDIV');
                    $("#agentReportsDiv").html(result);
                    showSearchTimeDuration(searchStartDate, new Date());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    hideLoadingScreen();
                    showErrorMessage();
//                    alert(xhr.responseText);
                    console.log('filter records summery : ' + xhr.responseText + ', \n\
                        status: ' + xhr.status + ', \n\
                        thrownError: ' + thrownError);
                }
            });
        } else {
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );
        }
        
    }
    
    function showLoadingScreen(){
        $("#agentReportsDiv").empty();
        $("#loadingGifImg").css("display", "inline");
        $("#searchTimeSpan").html(""); // clears the search time diplay span   
        return true;
    }

    function hideLoadingScreen(){
        $("#loadingGifImg").css("display", "none");
        return true;
    }
    
    function showErrorMessage(){
        $("#agentReportsDiv").empty();
        $("#agentReportsDiv").append("ERROR Occured. Please try again!");
    }
    
    
    function callQueueReports(queue_name, id) {
        //active view button
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val(queue_name);
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val(id);

        //Start : set agents panel css
        {
            var items = document.getElementById('queueListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            document.getElementById(id).style.backgroundColor = "grey";
            document.getElementById(id).style.color = "white";
            
            $("#agentListGroup > a").css("backgroundColor", "white");
            $("#agentListGroup > a").css("color", "black");
        }

        loadChannelsInfoOfTheQueue(id);
     
    }



    function loadChannelsInfoOfTheQueue(extQueueId){
        if(extQueueId){
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label>Loading channels...</label>");        
            $.ajax({
                type : "GET",
                url : "<?= Url::to(['reports/findchannelsforqueue'])?>",
                data : {
                    queueId : extQueueId
                },
                success : function(data){
                    $("#channelsListDiv").empty();
                    if(data != "0"){
                        var queueChannelsObj = JSON.parse(data);
                        for(var x = 0; x < queueChannelsObj.length; x++){
                            var channelTickBox = $("<input>");
                            $(channelTickBox).attr("type", "checkbox");
                            $(channelTickBox).val(queueChannelsObj[x]['id']);
                            $(channelTickBox).addClass("channelCheckBoxesClass");
                            $(channelTickBox).attr("id", "channel"+queueChannelsObj[x]['channel_number']);
                            
                            var channelName = " "+queueChannelsObj[x]['channel_name']+" ";

                            var channelNameLabel = $("<label></label>");
                            $(channelNameLabel).attr("data-toggle", "tooltip");
                            $(channelNameLabel).attr("data-placement", "left");
                            $(channelNameLabel).addClass("channelLabel");                           
                            $(channelNameLabel).attr("title", queueChannelsObj[x]['channel_description']);
                            
                            $(channelNameLabel).append(channelTickBox);
                            $(channelNameLabel).append(channelName);
                            

                            $("#channelsListDiv").append(channelNameLabel);
                        }
                    }else{
                        $("#channelsListDiv").append("<label>No channels available</label>");                            
                    }
                },
                error : function(error){
                    console.log("load channels error = "+error.responseText);
                    $("#channelsListDiv").empty();
                    $("#channelsListDiv").append("<label style='color:red'>Error loading channels</label>");    
                }
            });
        }else{
            $("#channelsListDiv").empty();
            $("#channelsListDiv").append("<label style='color:red'>Queue ID is incorrect. Please select the queue again</label>"); 
        }
    }
    
    function loadQueueDataForSupervisor(element){
        $('#btnView').removeClass('disabled');
        $('#btnSummeryView').removeClass('disabled');
        $('#hiddenAgentExt').val(0);
        $('#hiddenAgentName').val("");
        $('#hiddenAgentId').val(0);
        $('#hiddenQueueId').val("");
        var url = "<?= Url::to(['reports/performanceoverviewbyqueue']) ?>";
        var items = document.getElementById('agentListGroup').getElementsByTagName('a');
            for (var i = 0; i < items.length; i++) {
                items[i].style.backgroundColor = "white";
                items[i].style.color = "black";
            }
            element.style.backgroundColor = "grey";
            element.style.color = "white";
            
            $("#queueListGroup > a").css("backgroundColor", "white");
            $("#queueListGroup > a").css("color", "black");
            
    }

    function getSelectedChannelIds(){
        if($(".channelCheckBoxesClass:checked").length > 0){
            // channels selected
            var selectedChannelsCsvString = "";
            $(".channelCheckBoxesClass:checked").each(function(){
                var channelNumber = $(this).attr('id');
                channelNumber = channelNumber.replace("channel", "");
                selectedChannelsCsvString = selectedChannelsCsvString+channelNumber+",";
            });
            return selectedChannelsCsvString;
        }else{
            // no channels selected
            return "0";
        }
    }

    function showSearchTimeDuration (start, end){
        var diff = end - start;
        $("#searchTimeSpan").html("Search time = "+ (diff/1000)+" seconds");
        return true;
    }


    
</script>

</div>
    <script>

        function filterRecordsTablle(){

        var searchStartDate = new Date();
        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");
        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();        
                    
                    if ($('#hiddenAgentExt').val() == 0) {
                        $("#agentinfo").html("All Agents");
                    }else{                        
                        $("#agentinfo").html($('#hiddenAgentName').val()+" "+$('#hiddenAgentExt').val());
                    }

            if (fromDateConverted < toDateConverted) {
                    $("#resulttable").css("display","block");
                        getAnsweredCallsData();
                        getOutgoingCallsData();
            }else{
            swal(
                    'Oops!!!',
                    'Invalid dates, please select a valid date range.',
                    'error'
                    );                   
            }            
        }



        function getAnsweredCallsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }

        var url = "<?= Url::to(['reports/performanceoverviewmissingaudio']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

            $('#answeredDataTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 4, 'asc' ],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate, 
                        todate: toDate, 
                        agent_id: agentId, 
                        agentex: agentEx, 
                        agentname: agentName, 
                        contactNum: contactNumber, 
                        queue_id : queueId, 
                        fromTime : fromTime, 
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Answered",
                        outputType:outputType
                    }
                }
            });        
    }



        function getOutgoingCallsData(){

        var searchStartDate = new Date();

        var fromDate = $('#dateFromValue').val();
        var toDate = $('#dateToValue').val();
        var agentId = $('#hiddenAgentId').val();
        var agentEx = $('#hiddenAgentExt').val();
        var agentName = $('#hiddenAgentName').val();
        var contactNumber = $('#contactNum').val();
        contactNumber = contactNumber.trim();

        if(contactNumber != ""){
                var pattern = /[^0-9]/;
                var result = contactNumber.match(pattern);
                if(result){
                   swal(
                    'Oops!!!',
                    'Contact number is incorrect. Please remove non numeric characters',
                    'error'
                    );
                   return false;
                }
        }


        var queueId = $('#hiddenQueueId').val();
        if(queueId == ""){
           var outputType = "byDate";
        }else{
           var outputType = "byQueue";
        }

        var url = "<?= Url::to(['reports/performanceoverviewmissingaudio']) ?>";

        var fromTime = ($("#timeFromSelect").val() != "" ? $("#timeFromSelect").val() : "00:00:00");
        var toTime = ($("#timeToSelect").val() != "" ? $("#timeToSelect").val() : "23:59:59");

        var fromDateConverted = new Date(fromDate+" "+fromTime);
        var toDateConverted = new Date(toDate+" "+toTime);

        var selectedChannels = getSelectedChannelIds();

            $('#outgoingDataTable').DataTable( {
                "processing": true,
                "serverSide": true,
                "destroy": true,
                "searching": false,
                "order": [ 4, 'asc' ],
                "ajax":{
                    type: "POST",
                    url: url,
                    data: {
                        fromdate: fromDate, 
                        todate: toDate, 
                        agent_id: agentId, 
                        agentex: agentEx, 
                        agentname: agentName, 
                        contactNum: contactNumber, 
                        queue_id : queueId, 
                        fromTime : fromTime, 
                        toTime : toTime,
                        channels: selectedChannels,
                        tableType : "Outgoing",
                        outputType:outputType
                    }
                }
            });        
    } 


    </script>
