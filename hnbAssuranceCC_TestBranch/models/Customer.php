<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This model is to interact with the customer table in the database
 *
 * @author Sandun
 * @since 2017-06-27
 */
class Customer extends ActiveRecord {

    public static function getCustomerInformation($customerId) {

        $customer = new Customer();
        return $customer->find()
                        ->where("id = $customerId")
                        ->all();
    }

    public static function getCustomerIdByPhoneNumber($phoneNumber) {
        $customer = new Customer();
        $customerId = $customer->find()
                ->select("id")
                ->where("contact_number = $phoneNumber")
                ->all();

        if ($customerId) {
            return $customerId[0]['id'];
        } else {
            return NULL;
        }
    }

    public static function getCustomerInformationByPhoneNumber($phoneNumber) {

        $customer = new Customer();
        return $customer->find()
                        ->where(['like', 'contact_number', $phoneNumber])
                        ->all();
    }

}
