<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
/**
 * <b>This model class interacts with the call_answer_times table in DB3</b>
 *
 * @author Sandun
 * @since 2017-11-30
 */
class call_answer_times extends ActiveRecord{
    
    public static function getDb() {
        return Yii::$app->db3;
    }
    
    public static function getAnswerTimeBetweenStartAndEnd($startTime, $endTime, $callerNumber, $answeredExtension){
        $answerTime = call_answer_times::find()
                ->where(['between', 'answered_time', $startTime, $endTime])
                ->andWhere("caller_number = '$callerNumber'")
                ->andWhere("answered_extension = '$answeredExtension'")
                ->one();
        
        if(isset($answerTime['answered_time'])){
            return $answerTime['answered_time'];
        }else{
            return NULL;
        }
    }
    
}
