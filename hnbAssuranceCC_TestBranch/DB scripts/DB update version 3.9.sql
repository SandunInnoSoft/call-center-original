# HNB Assurance Call center system DB update version 3.9
# since 2018-1-10
# author Sandun

-- This creates a new table to record the deleted user information for archival purpose
CREATE TABLE `deleted_call_center_user` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_profile_pic` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  `created_date` datetime DEFAULT NULL,
  `contact_number` int(10) DEFAULT NULL,
  `voip_extension` varchar(45) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL COMMENT 'This will be mandatory for supervisors',
  `webphone` enum('Enabled','Disabled') DEFAULT 'Enabled' COMMENT 'This fiels is to maintain the state of the agent webphone state',
  `created_user_id` int(11) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `deleted_user_role_key_idx` (`role_id`),
  CONSTRAINT `deleted_user_role_key` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This table is to records the deleted agent information';

-- removes callrecord_user_key foregn key contraint with call records table
ALTER TABLE `call_records` DROP FOREIGN KEY `callrecord_user_key`;