# Hnb Assurance PBX DB update version 3.4
# @author Sandun
# @since 2017-11-28

# @target PBX coovoxdb


-- -------------------- deletes the callstatus column from the cdr table
ALTER TABLE `cdr`
DROP COLUMN `callstatus`;

-- ------------ END SCRIPT ----------------------------------------------------

