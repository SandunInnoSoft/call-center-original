/*
HNB Assurance DB update version 4.0
@author Sandun
@since 2018-1-30
@target DB1


Navicat MariaDB Data Transfer

Source Server         : HNB Assurance CC DB prod
Source Server Version : 100128
Source Host           : 192.168.192.62:3306
Source Database       : hnbassuranceccdb

Target Server Type    : MariaDB
Target Server Version : 100128
File Encoding         : 65001

Date: 2018-01-30 10:47:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for abandon_calls
-- ----------------------------
DROP TABLE IF EXISTS `abandon_calls`;
CREATE TABLE `abandon_calls` (
  `agent_id` varchar(45) DEFAULT NULL,
  `caller_number` varchar(45) DEFAULT NULL,
  `timestamp` varchar(45) NOT NULL DEFAULT '0',
  PRIMARY KEY (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table ';

-- ----------------------------
-- Records of abandon_calls
-- ----------------------------

-- ----------------------------
-- Table structure for agent_notifications
-- ----------------------------
DROP TABLE IF EXISTS `agent_notifications`;
CREATE TABLE `agent_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `notif_type` int(11) DEFAULT NULL,
  `notif_status` varchar(45) DEFAULT NULL,
  `notif_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for agent_requests
-- ----------------------------
DROP TABLE IF EXISTS `agent_requests`;
CREATE TABLE `agent_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_type` enum('lunch','short','sick','other','meeting') DEFAULT NULL,
  `requested_time` datetime DEFAULT NULL,
  `responded_time` datetime DEFAULT NULL,
  `approved_time_period` float DEFAULT NULL COMMENT 'Only for a sick break and other break. Value will be on minutes',
  `request_status` enum('approved','pending','denied','closed','taken') DEFAULT NULL COMMENT '''Approved after supervisor approve the request, pending after agent requested the request, closed after closing the request''. Denied after supervisor denied the request, taken once agent take the request',
  `taken_time` datetime DEFAULT NULL,
  `closed_time` datetime DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `consumed` int(11) DEFAULT NULL COMMENT 'This column has a flag value to make sure record fetch by server events',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for call_center_user
-- ----------------------------
DROP TABLE IF EXISTS `call_center_user`;
CREATE TABLE `call_center_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_profile_pic` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  `created_date` datetime DEFAULT NULL,
  `contact_number` int(10) DEFAULT NULL,
  `voip_extension` varchar(45) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL COMMENT 'This will be mandatory for supervisors',
  `webphone` enum('Enabled','Disabled') DEFAULT 'Enabled' COMMENT 'This fiels is to maintain the state of the agent webphone state',
  `created_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_role_key_idx` (`role_id`),
  CONSTRAINT `user_role_key` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1 COMMENT='This table is temporary, will be replaced with a oracle db table';

-- ----------------------------
-- Records of call_center_user
-- ----------------------------
INSERT INTO `call_center_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'HNB Administrator', '1', null, 'active', null, null, '4090', null, 'Enabled', null);

-- ----------------------------
-- Table structure for call_comments
-- ----------------------------
DROP TABLE IF EXISTS `call_comments`;
CREATE TABLE `call_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cdr_unique_id` varchar(100) DEFAULT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `created_datetime` varchar(30) DEFAULT NULL,
  `caller_number` varchar(20) DEFAULT NULL,
  `answered_extension` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for call_forwards
-- ----------------------------
DROP TABLE IF EXISTS `call_forwards`;
CREATE TABLE `call_forwards` (
  `caller_number` int(11) NOT NULL COMMENT 'caller cli number',
  `agent_extension` int(11) DEFAULT NULL COMMENT 'agent extension number',
  `time_stamp` varchar(100) NOT NULL DEFAULT '0' COMMENT 'Unix timestamp ',
  `created_date` datetime DEFAULT NULL COMMENT 'Current date time',
  `state` int(11) DEFAULT '1',
  PRIMARY KEY (`caller_number`,`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of call_forwards
-- ----------------------------

-- ----------------------------
-- Table structure for call_queue
-- ----------------------------
DROP TABLE IF EXISTS `call_queue`;
CREATE TABLE `call_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of call_queue
-- ----------------------------

-- ----------------------------
-- Table structure for call_records
-- ----------------------------
DROP TABLE IF EXISTS `call_records`;
CREATE TABLE `call_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cdr_unique_id` varchar(50) DEFAULT NULL,
  `call_date` datetime DEFAULT NULL,
  `call_time` datetime DEFAULT NULL,
  `caller_number` varchar(20) DEFAULT NULL,
  `cli_number` int(11) DEFAULT NULL COMMENT 'This keeps the phone number of the call made from',
  `policy_number` int(11) DEFAULT NULL COMMENT 'This keeps the phone number that is actually associated with the customer data in the customer database',
  `comment` varchar(500) DEFAULT NULL COMMENT 'This keeps the typed comment of the call record',
  `notes` varchar(1000) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `latest` int(11) DEFAULT NULL,
  `consumed` int(11) DEFAULT NULL,
  `timestamp` varchar(50) DEFAULT NULL,
  `call_waiting_duration` float DEFAULT NULL,
  `call_end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `callrecord_user_key_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12634 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for contact_list
-- ----------------------------
DROP TABLE IF EXISTS `contact_list`;
CREATE TABLE `contact_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_number` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `dob` datetime DEFAULT NULL COMMENT 'This table is temporary, this table is to represent the oracle db',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customer
-- ----------------------------

-- ----------------------------
-- Table structure for deleted_call_center_user
-- ----------------------------
DROP TABLE IF EXISTS `deleted_call_center_user`;
CREATE TABLE `deleted_call_center_user` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_profile_pic` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  `created_date` datetime DEFAULT NULL,
  `contact_number` int(10) DEFAULT NULL,
  `voip_extension` varchar(45) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL COMMENT 'This will be mandatory for supervisors',
  `webphone` enum('Enabled','Disabled') DEFAULT 'Enabled' COMMENT 'This fiels is to maintain the state of the agent webphone state',
  `created_user_id` int(11) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `deleted_user_role_key_idx` (`role_id`),
  CONSTRAINT `deleted_user_role_key` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='This table is to records the deleted agent information';


-- ----------------------------
-- Table structure for dnd_records
-- ----------------------------
DROP TABLE IF EXISTS `dnd_records`;
CREATE TABLE `dnd_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `dnd_mode` enum('On','Off') DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2673 DEFAULT CHARSET=latin1 COMMENT='This table keeps all the dnd on off records of all users for reportring and monitoring purposes';


-- ----------------------------
-- Table structure for extension_queue
-- ----------------------------
DROP TABLE IF EXISTS `extension_queue`;
CREATE TABLE `extension_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for ext_queue_intermediate
-- ----------------------------
DROP TABLE IF EXISTS `ext_queue_intermediate`;
CREATE TABLE `ext_queue_intermediate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queueid` int(11) DEFAULT NULL,
  `voip_extension` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for help_requests
-- ----------------------------
DROP TABLE IF EXISTS `help_requests`;
CREATE TABLE `help_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agentId` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `consumed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='This table records the help requests made by agents';


-- ----------------------------
-- Table structure for logged_in_users
-- ----------------------------
DROP TABLE IF EXISTS `logged_in_users`;
CREATE TABLE `logged_in_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_logged_ip_address` varchar(60) DEFAULT NULL,
  `logged_in_time` varchar(50) DEFAULT NULL,
  `time_signature` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=633 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for miss_calls_email_log
-- ----------------------------
DROP TABLE IF EXISTS `miss_calls_email_log`;
CREATE TABLE `miss_calls_email_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numbers` int(11) DEFAULT NULL COMMENT 'Count number of missed calls recorded in the given time.',
  `timestamp` varchar(50) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of miss_calls_email_log
-- ----------------------------

-- ----------------------------
-- Table structure for queue_ivr_number
-- ----------------------------
DROP TABLE IF EXISTS `queue_ivr_number`;
CREATE TABLE `queue_ivr_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ivr_number` varchar(20) DEFAULT NULL,
  `ext_queue_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Admin');
INSERT INTO `roles` VALUES ('2', 'Agent');
INSERT INTO `roles` VALUES ('3', 'Supervisor');
INSERT INTO `roles` VALUES ('4', 'Senior-Agent');

-- ----------------------------
-- Table structure for voicemail_extension
-- ----------------------------
DROP TABLE IF EXISTS `voicemail_extension`;
CREATE TABLE `voicemail_extension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voicemail_extension` varchar(50) DEFAULT NULL,
  `ext_queue_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for web_presence
-- ----------------------------
DROP TABLE IF EXISTS `web_presence`;
CREATE TABLE `web_presence` (
  `ext` varchar(32) NOT NULL,
  `state` varchar(16) NOT NULL,
  `cidnum` varchar(64) DEFAULT NULL,
  `cidname` varchar(64) DEFAULT NULL,
  `inorout` varchar(1) DEFAULT NULL,
  `callstart` int(11) DEFAULT NULL,
  PRIMARY KEY (`ext`),
  KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_presence
-- ----------------------------
INSERT INTO `web_presence` VALUES ('4000', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4010', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4020', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4030', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4040', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4050', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4060', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4070', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4080', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4090', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4100', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4110', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4120', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4130', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4140', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4150', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4160', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4170', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4180', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4190', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4200', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4210', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4220', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4230', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4240', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4250', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4260', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4270', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4280', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4290', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4300', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4310', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4320', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4330', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4340', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4350', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4360', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4370', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4380', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4390', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4400', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4410', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4420', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4430', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4440', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4450', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4460', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4470', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4480', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4490', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4500', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4510', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4520', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4530', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('4540', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5000', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5010', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5020', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5030', '0', null, null, null, null);
INSERT INTO `web_presence` VALUES ('5040', '0', null, null, null, null);
