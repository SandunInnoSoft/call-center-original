# Hnb Assurance call center DB update version 3.8
# @author Sandun
# @since 2017-12-15
# @target DB1 hnbassurancecc

-- ---------- This script adds a new field to the call_center_user db table to record the id of the supervisor / admin who created the user
ALTER TABLE `call_center_user`
ADD COLUMN `created_user_id`  int NULL AFTER `webphone`;

