# HNB Assurance CC DB3 update version 3.2
# @target DB callevents
# @author Sandun
# @since 2017-11-20

-- This script creates two columns in loop_monitor table to keep the last executed timestamp of the 2 new call loops
ALTER TABLE `callevents`.`loop_monitor`
ADD COLUMN `answercallLoop`  varchar(100) NULL;

INSERT INTO `callevents`.`loop_monitor` (`answercallLoop`) VALUES ('1511171322');

ALTER TABLE `callevents`.`loop_monitor`
ADD COLUMN `hungupcallloop`  varchar(100) NULL;

INSERT INTO `callevents`.`loop_monitor` (`hungupcallloop`) VALUES ('1511171322');
