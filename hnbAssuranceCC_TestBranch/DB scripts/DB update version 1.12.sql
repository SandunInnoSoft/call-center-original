#date : 25/07/2017
#Author : Prabath
#Creates new table agent_notifications.

CREATE TABLE `agent_notifications` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `agent_id` INT NULL,
  `notif_type` INT NULL,
  `notif_status` INT NULL,
  `notif_time` DATETIME NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `agent_notifications` 
ADD COLUMN `supervisor_id` INT NULL AFTER `agent_id`;
