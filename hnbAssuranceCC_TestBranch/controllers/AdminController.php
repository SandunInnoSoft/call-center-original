<?php

namespace app\controllers;

/**
 * This controller is to perform administrator related action
 *
 * @author Sandun
 * @since 2018-1-3
 */
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use app\models\web_presence;

class AdminController extends Controller {
    
    /**
     * 
     * @since 2018-1-3
     * @author Sandun
     */
    public function actionCheckextensionavailability(){
        $typingExtension = Yii::$app->request->get("typingExtension");
        if($typingExtension != NULL && web_presence::isExtensionAvailable($typingExtension) == TRUE){
            echo 1;
        }else{
            echo 0;
        }
    }
}
